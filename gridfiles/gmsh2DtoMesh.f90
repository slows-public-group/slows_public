Program Main
  ! Transform a GMSH file into a MESH one. 
  ! Not completed now, if this program fail, please feel free to add needed section
  ! Written April 30th 2009, Adam
  IMPLICIT NONE
  CHARACTER(LEN=30)                                         :: MotCle
  CHARACTER(LEN=80)                                         :: Filename
  integer                                                   :: Npoints,Nelements,ndim
  integer                                                   :: Ntriangles,NEdges,Nquadrangles
  integer                                                   :: compteur_edges,compteur_triangles
  integer                                                   :: compteur_quadrangles
  integer                                                   :: i,stat
  integer                                                   :: nb1,nb2,nb3
  integer                                                   :: MyUnit
  real,                     dimension(:,:),allocatable      :: coor
  integer,                  dimension(:,:),allocatable      :: Triangles,Edges,Quadrangles
  integer,                  dimension(:)  ,allocatable      :: Ntags,Type,Logique_Seg,Logique_Pt
  logical                                                   :: Is_2D

  integer :: cont1, cont2, cont3

  stat =IARGC()
  IF (stat < 1) GOTO 999
  CALL GETARG(1,Filename)
  MyUnit = 1
  OPEN(unit=MyUnit, file=FileName)

  Is_2D = .true.
  Ndim  = 2

  print*,"***************LECTURE***************"
  do i=1,4
     READ(1,*) 
  end do
  READ(1,*) Npoints

  ! Allocations
  ALLOCATE(coor(3,Npoints))
  ALLOCATE(Logique_Pt(Npoints))

  ! Lecture des coordonnees
  print*, "Reading Coordinates..."
  do i=1,Npoints
     read(1,*) nb1, coor(:,i)
     if(coor(3,i) /= 0.0) then 
        Is_2D = .false.
     end if
  end do
  if(.not. Is_2D) then
     Print*, "The mesh does not seem to be two dimensional..."
     stop
  end if
  read(1,*)
  read(1,*)
  READ(1,*) Nelements

  allocate(Type(Nelements),Ntags(Nelements))
  Nedges       = 0
  Ntriangles   = 0
  Nquadrangles = 0
  ! Lecture du debut des connectivites
  print*, "Starting connectivity reading..."
  do i=1,Nelements
     read(1,*) nb1,Type(i),Ntags(i)
     select case(Type(i))
     case(15) ! Point
     case(1) ! Edge
        Nedges = Nedges + 1
     case(2) ! Triangle
        Ntriangles = Ntriangles + 1
     case(3) ! Quadrangle
        Nquadrangles = Nquadrangles + 1
     case default
        print*, "This case is not implemented at this moment"
        print*, "see the documetation of GMSH to know the element corresponding to type"
        print*, Type(i)
        stop
     end select

  end do

  close(myunit)

  OPEN(unit=MyUnit, file=FileName)
  do i=1,Npoints+8
     READ(1,*) 
  end do

  compteur_edges       = 0
  compteur_triangles   = 0
  compteur_quadrangles = 0

  allocate(Logique_Seg(Nedges),Edges(2,Nedges))
  allocate(Triangles(3,Ntriangles),Quadrangles(4,Nquadrangles))
  do i=1,Nelements
     select case(Type(i))
     case(15) ! Point
        read(1,*)
     case(1) ! Edge
        compteur_edges = compteur_edges + 1
        read(1,*) Logique_Seg(compteur_edges),nb1,nb2,nb3,nb1,Edges(:,compteur_edges)
     case(2) ! Triangle
        compteur_triangles = compteur_triangles + 1
        read(1,*) nb1,nb2,nb3,nb1,nb2,Triangles(:,compteur_triangles)
     case(3) ! Quadrangle
        compteur_quadrangles = compteur_quadrangles + 1
        read(1,*) nb1,nb2,nb3,nb1,nb2,nb3,Quadrangles(:,compteur_quadrangles)
     end select

  end do

  close(myunit)

  print*, "End Reading..."
  print*, "Found :"
  print*, "Npoints     :", Npoints
  print*, "Nelements   :", Nelements
  print*, "Nedges      :", Nedges, compteur_edges
  print*, "Ntriangles  :", Ntriangles, compteur_triangles
  print*, "NQuadrangles:", Nquadrangles,compteur_quadrangles
  print*, "Start writing in Output.mesh..."

  print*,"***************ECRITURE***************"
  myunit=2
  OPEN(unit=myunit, file="Output.mesh")
  ! Logique des points
  Logique_Pt = 0
  do i=1,Nedges
     Logique_Pt(Edges(1,i)) = max(Logique_Pt(Edges(1,i)),Logique_Seg(i))
     Logique_Pt(Edges(2,i)) = max(Logique_Pt(Edges(2,i)),Logique_Seg(i))
  end do

  write(2,*) "MeshVersionFormatted 0"
  write(2,*) "Dimension"
  write(2,*) Ndim
  write(2,*) "Vertices"
  write(2,*) Npoints
  do i=1,Npoints
     write(2,'(2ES20.8,I4)') coor(1:2,i), 0 !Logique_Pt(i)
  end do
  write(2,*) "Edges"
  write(2,*) Nedges
  do i=1,NEdges
     write(2,'(3I8)') Edges(:,i),Logique_Seg(i)
  end do
  if(Ntriangles .ne. 0) then
     write(2,*) "Triangles"
     write(2,*) Ntriangles
     do i=1,Ntriangles
        write(2,'(4I8)') Triangles(:,i),0
     end do
  end if
  if(Nquadrangles .ne. 0) then
     write(2,*) "Quadrilaterals"
     write(2,*) Nquadrangles
     do i=1,Nquadrangles
        write(2,'(5I8)') Quadrangles(:,i),0
     end do
  end if
  write(2,*) "End"

  CLOSE(myunit)
  print*,"End."
  STOP 

999 CONTINUE
  PRINT*,"ERROR : This function needs 1 argument"
  PRINT*,"It must be used the following way : "
  PRINT*,"gmsh2DtoMesh Filename"
  print*,""
  PRINT*," Filename : Name of the GMSH file to convert"
  print*
  print*,"Output file has name 'Output.mesh'"
END PROGRAM Main
