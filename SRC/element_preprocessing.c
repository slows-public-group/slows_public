/*  Copyright (C) 2021   M.Ricchiuto  <mario.ricchiuto@inria.fr>
                         M. Kazolea   <maria.kazolea@inria.fr>
                         A. Filippini <a.filippini@brgm.fr>
                         L. Arpaia    <luca.arpaia@brgm.fr>
                         N.Pattakos   <pattakosn@fastmail.com> 

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.*/

/***************************************************************************
 element_preprocessing.c
 -----------------------
 This is element_preprocessing: the geometry of each element is computed and
 processed here at midpoint configuration to have all the necessary informations 
 for the computation of the new solution
 -------------------
 ***************************************************************************/
#include <stdio.h>
#include <math.h>
#include "common.h"
#include <my_malloc.h>

extern const int vertex,size;
extern int NN, NE, NBF, NBF2, NBF3, blending, volume_q_pts,grad_reco;
extern int corner1,corner2,corner3,corner4;
extern struct element_struct *element;
extern struct node_struct *node;
extern struct boundary_nodes_struct *b_nodes;
extern struct boundary_struct *boundary;
extern double gridsize, L_ref, cut_off_U;
extern struct numerical_int_struct numerical_int;
struct boundary_nodes2_struct *b_nodes2;
struct boundary_nodes3_struct *b_nodes3;
void simple_correction(int, int, int);
void integration(int, double *, double);
double distance(double, double, double, double, double, double);
double **katz, **M2_total;

void analytic_ini(double, double, double, double*);
double analytic_sol(double, double, double);
double reconstructed_fun(double *, int , int);

/* Preprocessing for linear P1 interpolation */
void element_preprocessing(void)
{
	/* Inizialization of Volumes */
	for (int n = 0; n < NN; n++) {
		node[n].volume = 0.;	// midpoint configuration
		node[n].mod_volume = 0.;	// new configuration
		node[n].old_volume = 0.;	// old configuration
		node[n].ss_volume = 0.;	// volume for the successive correction method
		node[n].length = 0.;
	}

	/* Computation of the nodal nomals */
	//gridsize = 0. ;
	for (int e = 0; e < NE; e++) {
		element[e].length = 0.;
		int n1 = element[e].node[1];
		int n2 = element[e].node[2];
		double x1 = node[n1].coordinate[0];
		double y1 = node[n1].coordinate[1];
		double x2 = node[n2].coordinate[0];
		double y2 = node[n2].coordinate[1];
		element[e].normal[0][0] = y1 - y2;
		element[e].normal[0][1] = x2 - x1;
		if (element[e].normal[0][0] * element[e].normal[0][0] + element[e].normal[0][1] * element[e].normal[0][1] > element[e].length)
			element[e].length = element[e].normal[0][0] * element[e].normal[0][0] + element[e].normal[0][1] * element[e].normal[0][1];

		n1 = element[e].node[2];
		n2 = element[e].node[0];
		x1 = node[n1].coordinate[0];
		y1 = node[n1].coordinate[1];
		x2 = node[n2].coordinate[0];
		y2 = node[n2].coordinate[1];
		element[e].normal[1][0] = y1 - y2;
		element[e].normal[1][1] = x2 - x1;
		if (element[e].normal[1][0] * element[e].normal[1][0] + element[e].normal[1][1] * element[e].normal[1][1] > element[e].length)
			element[e].length = element[e].normal[1][0] * element[e].normal[1][0] + element[e].normal[1][1] * element[e].normal[1][1];

		n1 = element[e].node[0];
		n2 = element[e].node[1];
		x1 = node[n1].coordinate[0];
		y1 = node[n1].coordinate[1];
		x2 = node[n2].coordinate[0];
		y2 = node[n2].coordinate[1];
		element[e].normal[2][0] = y1 - y2;
		element[e].normal[2][1] = x2 - x1;
		if (element[e].normal[2][0] * element[e].normal[2][0] + element[e].normal[2][1] * element[e].normal[2][1] > element[e].length)
			element[e].length = element[e].normal[2][0] * element[e].normal[2][0] + element[e].normal[2][1] * element[e].normal[2][1];

		element[e].length = sqrt(element[e].length); // so far no square root has been taken for the norm of the normals..

		/* Computation of FV nodal normals */
		if (blending >= 2) {
			x1 = y1 = 0.0;
			for (int v = 0; v < 3; v++) {
				n1 = element[e].node[v];
				x1 += node[n1].coordinate[0] / 3.0;
				y1 += node[n1].coordinate[1] / 3.0;
			}
			element[e].cg[0] = x1;
			element[e].cg[1] = y1;

			n1 = element[e].node[0];
			n2 = element[e].node[1];
			x2 = 0.5 * (node[n1].coordinate[0] + node[n2].coordinate[0]);
			y2 = 0.5 * (node[n1].coordinate[1] + node[n2].coordinate[1]);
			element[e].FVnormal[0][0] = y1 - y2;
			element[e].FVnormal[0][1] = x2 - x1;
			element[e].mp[0][0] = x2;
			element[e].mp[0][1] = y2;

			n1 = element[e].node[0];
			n2 = element[e].node[2];
			x2 = 0.5 * (node[n1].coordinate[0] + node[n2].coordinate[0]);
			y2 = 0.5 * (node[n1].coordinate[1] + node[n2].coordinate[1]);
			element[e].FVnormal[1][0] = y2 - y1;
			element[e].FVnormal[1][1] = x1 - x2;
			element[e].mp[1][0] = x2;
			element[e].mp[1][1] = y2;

			n1 = element[e].node[1];
			n2 = element[e].node[2];
			x2 = 0.5 * (node[n1].coordinate[0] + node[n2].coordinate[0]);
			y2 = 0.5 * (node[n1].coordinate[1] + node[n2].coordinate[1]);
			element[e].FVnormal[2][0] = y1 - y2;
			element[e].FVnormal[2][1] = x2 - x1;
			element[e].mp[2][0] = x2;
			element[e].mp[2][1] = y2;
		}

		/* Computation of the volume of the element */
		element[e].volume = 0.5 * fabs(element[e].normal[2][0] * element[e].normal[1][1] - element[e].normal[1][0] * element[e].normal[2][1]);
		x1 = node[element[e].node[0]].coordinate_new[0];
		y1 = node[element[e].node[0]].coordinate_new[1];
		x2 = node[element[e].node[1]].coordinate_new[0];
		y2 = node[element[e].node[1]].coordinate_new[1];
		double x3 = node[element[e].node[2]].coordinate_new[0];
		double y3 = node[element[e].node[2]].coordinate_new[1];
		element[e].new_volume = 0.5 * fabs(x1 * (y2 - y3) + x2 * (y3 - y1) + x3 * (y1 - y2));

		//element[e].length = sqrt( 2.0*element[e].volume );
		//gridsize += element[e].length;
		x1 = node[element[e].node[0]].coordinate_old[0];
		y1 = node[element[e].node[0]].coordinate_old[1];
		x2 = node[element[e].node[1]].coordinate_old[0];
		y2 = node[element[e].node[1]].coordinate_old[1];
		x3 = node[element[e].node[2]].coordinate_old[0];
		y3 = node[element[e].node[2]].coordinate_old[1];
		double old_volume = 0.5 * fabs(x1 * (y2 - y3) + x2 * (y3 - y1) + x3 * (y1 - y2));

		/* Computation of median dual cell areas */
		for (int v = 0; v < 3; v++) {
			//if(element[e].node[v]==57 || element[e].node[v]==78) printf("%d %d %le\n",element[e].node[v],e, element[e].volume/3.);
			node[element[e].node[v]].volume += element[e].volume / 3.;
			node[element[e].node[v]].mod_volume += element[e].new_volume / 3.;
			node[element[e].node[v]].old_volume += old_volume / 3.;
			if (element[e].length > node[element[e].node[v]].length)
				node[element[e].node[v]].length = element[e].length;
		}
	}
	
	/* Volume correction for Periodic Bound. Cond. */
	for( int n = 0 ; n < NN ; n ++ ){ 
		node[n].flag = 0 ;
                node[n].ss_volume = node[n].volume;
	}

	for ( int f = 0 ; f < NBF ; f ++ ) {
		int n1 = b_nodes[f].node;
		if ( b_nodes[f].type == 10 ) {
			if ( node[n1].flag == 0 ) {
				int mate = node[n1].mate;
				node[n1].volume += node[mate].volume;
				node[mate].volume = node[n1].volume;
				node[n1].mod_volume += node[mate].mod_volume;
				node[mate].mod_volume = node[n1].mod_volume;
				node[mate].flag = 1;
				node[n1].flag = 1;
			}
		}
	}

	for (int n = 0; n < NN; n++){
		node[n].flag_boundary[0]=0; //how far is the node from the boundary
		node[n].flag_boundary[1]=0; // position in the b_nodes2
		node[n].flag_boundary[2]=0; // type of boundary
		node[n].num_neigh = 0;
		for (int j = 0; j < 15; j++)
			node[n].neigh[j] = -1; // Negative number for empty vector
	}		

	for( int n = 0 ; n < NN ; n ++ ) 
		node[n].flag = 0 ;
	
	for ( int f = 0 ; f < NBF ; f ++ ) {
		int n1 = b_nodes[f].node;
		node[n1].flag_boundary[0]=1;
		node[n1].flag_boundary[2]= b_nodes[f].type;
		if ( b_nodes[f].type == 10 ) {
			if ( node[n1].flag == 0 ) {
				int mate = node[n1].mate;
				node[n1].ss_volume += node[mate].ss_volume;
				node[mate].ss_volume = node[n1].ss_volume;
				node[mate].flag = 1;
				node[n1].flag = 1;
			}
		}
		if ( b_nodes[f].type == 20) {
				node[n1].ss_volume += node[n1].ss_volume;
		}
	}
	

	for (int e = 0; e < NE; e++){  //find the neighboring nodes of each node
		int n0 = element[e].node[0];
		int n1 = element[e].node[1];
		int n2 = element[e].node[2];
		int index0 = node[n0].num_neigh;
		int index1 = node[n1].num_neigh;
		int index2 = node[n2].num_neigh;

		int count0 = 0, count1 = 0, count2 = 0;
		int flag1 = 0, flag2 = 0;
		for (int j = 0; j<node[n0].num_neigh; j++){
			if (n1 == node[n0].neigh[j])
				flag1 += 1;
			if (n2 == node[n0].neigh[j])
				flag2 += 1;
		}
		if (flag1 == 0){
			node[n0].neigh[index0 + count0] = n1;
			count0 += 1;
		}
		if (flag2 == 0){
			node[n0].neigh[index0 + count0] = n2;
			count0 += 1;
		}

		flag1 = 0, flag2 = 0;
		for (int j = 0; j<node[n1].num_neigh; j++){
			if (n0 == node[n1].neigh[j])
				flag1 += 1;
			if (n2 == node[n1].neigh[j])
				flag2 += 1;
		}
		if (flag1 == 0){
			node[n1].neigh[index1 + count1] = n0;
			count1 += 1;
		}
		if (flag2 == 0){
			node[n1].neigh[index1 + count1] = n2;
			count1 += 1;
		}

		flag1 = 0, flag2 = 0;
		for (int j = 0; j<node[n2].num_neigh; j++){
			if (n0 == node[n2].neigh[j])
				flag1 += 1;
			if (n1 == node[n2].neigh[j])
				flag2 += 1;
		}
		if (flag1 == 0){
			node[n2].neigh[index2 + count2] = n0;
			count2 += 1;
		}
		if (flag2 == 0){
			node[n2].neigh[index2 + count2] = n1;
			count2 += 1;
		}
		
		node[n0].num_neigh += count0;
		node[n1].num_neigh += count1;
		node[n2].num_neigh += count2;
	}

	for (int f = 0; f<NBF; f++){
		int n = b_nodes[f].node;
		node[n].flag = 0;
	}
	for (int f = 0; f<NBF; f++){
		int n = b_nodes[f].node;
		//if (b_nodes[f].type==10  || b_nodes[f].type== 20){
		if (b_nodes[f].type==10 ){
			if(node[n].flag == 0) {		
				int mate = node[n].mate;
				int index = node[n].num_neigh;
				int index2 = node[mate].num_neigh;
				for (int j = 0; j<node[mate].num_neigh; j++){
					int m = node[mate].neigh[j];
					if (m == node[m].mate){
						node[n].neigh[index] = m;
						index += 1;
					}
				}
				for (int j = 0; j<node[n].num_neigh; j++){
					int m = node[n].neigh[j];
					if (m == node[m].mate){
						node[mate].neigh[index2] = m;
						index2 += 1;
					}
				}
				node[n].num_neigh = index;
				node[mate].num_neigh = index2;
				node[n].flag = 1;
				node[mate].flag = 1;
			}
		}
	}
	int index =0;
	for (int f = 0; f<NBF; f++){
		int n = b_nodes[f].node;
		for (int i=0; i<node[n].num_neigh; i++){
			int m = node[n].neigh[i];
			if(node[m].flag_boundary[0]==0) {
				node[m].flag_boundary[0]=2;
				index +=1;
			}
		}
	}
	NBF2 = index; // the number of nodes which are first neighbor to a boundary node (not included those  on the bounary)
	b_nodes2 =(struct boundary_nodes2_struct*) MA_vector( NBF2, sizeof(struct boundary_nodes2_struct) );

	int k=0;
	for(int n=0; n<NN; n++){
		if(node[n].flag_boundary[0]==2) {
			b_nodes2[k].node2 = n;
			node[n].flag_boundary[1]=k;//position in the b_nodes2 for the derivative reconstruction
			k += 1;
		}
	}

// Find the third layer which is related to the boundary nodes (neighbors of b_nodes[].node2)
	index =0;
	for (int f = 0; f<NBF2; f++){
		int n = b_nodes2[f].node2;
		for (int i=0; i<node[n].num_neigh; i++){
			int m = node[n].neigh[i];
			if(node[m].flag_boundary[0]==0) {
				node[m].flag_boundary[0]=3;
				index +=1;
			}
		}
	}

	NBF3 = index; // the number of nodes which are a second  neighbor to a boundary node 
	b_nodes3 = MA_vector( NBF3, sizeof(struct boundary_nodes3_struct) );

	k=0;
	for(int n=0; n<NN; n++){
		if(node[n].flag_boundary[0]==3) {
			b_nodes3[k].node3 = n;
			node[n].flag_boundary[1]=k;
			k += 1;
		}
	}

	if (blending>=2){ // FV computation of the Dual cell Center
		for (int n=0; n<NN; n++){
			node[n].DcC[0] = 0.;
			node[n].DcC[1] = 0.;
			node[n].ss_DcC[0] = 0.;
			node[n].ss_DcC[1] = 0.;
		}	
		for (int e = 0; e < NE; e++) {
			double xg = element[e].cg[0];
			double yg = element[e].cg[1];
			for (int i = 0; i < vertex-1; i++) {
				int ni = element[e].node[i];
				double xi = node[ni].coordinate[0];
				double yi = node[ni].coordinate[1];
				for (int j = i+1; j < vertex; j++) {
					int nj = element[e].node[j];
					double xj = node[nj].coordinate[0];
					double yj = node[nj].coordinate[1];
					double xm = element[e].mp[i+j-1][0];
					double ym = element[e].mp[i+j-1][1];
					for (int q = 0; q < volume_q_pts; q++) {
						double xq = xi*numerical_int.volume_coordinate[q][0] + xg*numerical_int.volume_coordinate[q][1] + xm*numerical_int.volume_coordinate[q][2];
						double yq = yi*numerical_int.volume_coordinate[q][0] + yg*numerical_int.volume_coordinate[q][1] + ym*numerical_int.volume_coordinate[q][2];
						node[ni].DcC[0] += element[e].volume / (6.*node[ni].volume) * numerical_int.volume_weight[q] * xq ;
						node[ni].DcC[1] += element[e].volume / (6.*node[ni].volume) * numerical_int.volume_weight[q] * yq;
						node[ni].ss_DcC[0] += element[e].volume / (6.*node[ni].ss_volume) * numerical_int.volume_weight[q] * xq ;
						node[ni].ss_DcC[1] += element[e].volume / (6.*node[ni].ss_volume) * numerical_int.volume_weight[q] * yq;

						if (ni!=node[ni].mate && node[ni].flag_boundary[2]==10){
							int mate = node[ni].mate;
							double dx = node[mate].coordinate[0] - node[ni].coordinate[0];
							double dy = node[mate].coordinate[1] - node[ni].coordinate[1];
							if (fabs(dx)<0.0000001){
								node[mate].DcC[0] += element[e].volume / (6.*node[ni].volume) * numerical_int.volume_weight[q] * xq ;
								node[mate].DcC[1] += element[e].volume / (6.*node[ni].volume) * numerical_int.volume_weight[q] * (yq + dy) ;
								node[mate].ss_DcC[0] += element[e].volume / (6.*node[ni].ss_volume) * numerical_int.volume_weight[q] * xq ;
								node[mate].ss_DcC[1] += element[e].volume / (6.*node[ni].ss_volume) * numerical_int.volume_weight[q] * (yq + dy) ;
							} else if (fabs(dy)<0.0000001){
								node[mate].DcC[0] += element[e].volume / (6.*node[ni].volume) * numerical_int.volume_weight[q] * (xq + dx) ;
								node[mate].DcC[1] += element[e].volume / (6.*node[ni].volume) * numerical_int.volume_weight[q] * yq ;
								node[mate].ss_DcC[0] += element[e].volume / (6.*node[ni].ss_volume) * numerical_int.volume_weight[q] * (xq + dx) ;
								node[mate].ss_DcC[1] += element[e].volume / (6.*node[ni].ss_volume) * numerical_int.volume_weight[q] * yq ;
							}
						}
						
						xq = xj*numerical_int.volume_coordinate[q][0] + xg*numerical_int.volume_coordinate[q][1] + xm*numerical_int.volume_coordinate[q][2];
						yq = yj*numerical_int.volume_coordinate[q][0] + yg*numerical_int.volume_coordinate[q][1] + ym*numerical_int.volume_coordinate[q][2];
						node[nj].DcC[0] += element[e].volume / (6.*node[nj].volume) * numerical_int.volume_weight[q] * xq ;
						node[nj].DcC[1] += element[e].volume / (6.*node[nj].volume) * numerical_int.volume_weight[q] * yq;
						node[nj].ss_DcC[0] += element[e].volume / (6.*node[nj].ss_volume) * numerical_int.volume_weight[q] * xq ;
						node[nj].ss_DcC[1] += element[e].volume / (6.*node[nj].ss_volume) * numerical_int.volume_weight[q] * yq;

						if (nj!=node[nj].mate && node[nj].flag_boundary[2]==10){
							int mate = node[nj].mate;
							double dx = node[mate].coordinate[0] - node[nj].coordinate[0];
							double dy = node[mate].coordinate[1] - node[nj].coordinate[1];
							if (fabs(dx)<0.0000001){
								node[mate].DcC[0] += element[e].volume / (6.*node[nj].volume) * numerical_int.volume_weight[q] * xq ;
								node[mate].DcC[1] += element[e].volume / (6.*node[nj].volume) * numerical_int.volume_weight[q] * (yq + dy) ;
								node[mate].ss_DcC[0] += element[e].volume / (6.*node[nj].ss_volume) * numerical_int.volume_weight[q] * xq ;
								node[mate].ss_DcC[1] += element[e].volume / (6.*node[nj].ss_volume) * numerical_int.volume_weight[q] * (yq + dy) ;
							} else if (fabs(dy)<0.0000001){
								node[mate].DcC[0] += element[e].volume / (6.*node[nj].volume) * numerical_int.volume_weight[q] * (xq + dx) ;
								node[mate].DcC[1] += element[e].volume / (6.*node[nj].volume) * numerical_int.volume_weight[q] * yq ;
								node[mate].ss_DcC[0] += element[e].volume / (6.*node[nj].ss_volume) * numerical_int.volume_weight[q] * (xq + dx) ;
								node[mate].ss_DcC[1] += element[e].volume / (6.*node[nj].ss_volume) * numerical_int.volume_weight[q] * yq ;
							}
						}
					}
				}
			}
		}
	}


	int c0,c2,boundary_number;
	
	/* Adaptive cut-off coefficients */
	for (int n = 0; n < NN; n++)
		node[n].cut_off_ratio = 1.0; //node[n].length/gridsize ;

	for (int e = 0; e < NE; e++)
		element[e].cut_off_ratio = 1.0;	//element[e].length/gridsize ;

        
}

/* Compute quantities for reference grid */
void ref_grid(void)
{
	/* Compute Grid Size & Dimension */
	double x_min = 0.;
	double x_max = 0.;
	double y_min = 0.;
	double y_max = 0.;
	for (int n = 0; n < NN; n++) {
		if (node[n].coordinate[0] < x_min)
			x_min = node[n].coordinate[0];
		else if (node[n].coordinate[0] > x_max)
			x_max = node[n].coordinate[0];

		if (node[n].coordinate[1] < y_min)
			y_min = node[n].coordinate[1];
		else if (node[n].coordinate[1] > y_max)
			y_max = node[n].coordinate[1];
		node[n].dtau = 0.;
	}

	L_ref = MAX(x_max - x_min, y_max - y_min);
	printf("          **       L_ref  %le       **\n", L_ref);

	/* Computing geometry for reference grid */
	gridsize = 0.0;
	for (int e = 0; e < NE; e++) {
		element[e].length = 0.;
		int n1 = element[e].node[1];
		int n2 = element[e].node[2];
		double x1 = node[n1].coordinate[0];
		double y1 = node[n1].coordinate[1];
		double x2 = node[n2].coordinate[0];
		double y2 = node[n2].coordinate[1];
		element[e].normal_0[0][0] = y1 - y2;
		element[e].normal_0[0][1] = x2 - x1;
		if (element[e].normal_0[0][0] * element[e].normal_0[0][0] + element[e].normal_0[0][1] * element[e].normal_0[0][1] > element[e].length)
			element[e].length = element[e].normal_0[0][0] * element[e].normal_0[0][0] + element[e].normal_0[0][1] * element[e].normal_0[0][1];

		n1 = element[e].node[2];
		n2 = element[e].node[0];
		x1 = node[n1].coordinate[0];
		y1 = node[n1].coordinate[1];
		x2 = node[n2].coordinate[0];
		y2 = node[n2].coordinate[1];
		element[e].normal_0[1][0] = y1 - y2;
		element[e].normal_0[1][1] = x2 - x1;
		if (element[e].normal_0[1][0] * element[e].normal_0[1][0] + element[e].normal_0[1][1] * element[e].normal_0[1][1] > element[e].length)
			element[e].length = element[e].normal_0[1][0] * element[e].normal_0[1][0] + element[e].normal_0[1][1] * element[e].normal_0[1][1];

		n1 = element[e].node[0];
		n2 = element[e].node[1];
		x1 = node[n1].coordinate[0];
		y1 = node[n1].coordinate[1];
		x2 = node[n2].coordinate[0];
		y2 = node[n2].coordinate[1];
		element[e].normal_0[2][0] = y1 - y2;
		element[e].normal_0[2][1] = x2 - x1;
		if (element[e].normal_0[2][0] * element[e].normal_0[2][0] + element[e].normal_0[2][1] * element[e].normal_0[2][1] > element[e].length)
			element[e].length = element[e].normal_0[2][0] * element[e].normal_0[2][0] + element[e].normal_0[2][1] * element[e].normal_0[2][1];

		element[e].length = sqrt(element[e].length);	// so far no square root has been taken for the norm of the normals..

		/* Computation of the volume of the element */
		element[e].volume_0 = 0.5 * fabs(element[e].normal_0[2][0] * element[e].normal_0[1][1] - element[e].normal_0[1][0] * element[e].normal_0[2][1]);

		// element[e].length = sqrt( 2.0*element[e].volume_0 ) ; 
		gridsize += element[e].length;
	}

	gridsize /= NE;

	if (cut_off_U < 0.)
		cut_off_U = (gridsize) * (gridsize) / L_ref;
	
	printf("cut_off %le grid %le L_ref %le\n",cut_off_U,gridsize,L_ref);
}

/* Compute the coefficient for quadratic least square gradient reconstruction */
// The summation on the elements counts two times the "ij" contribution if the edge connecting the two nodes is an 
// internal one, and one time if the edge is on the boundary. For this reason we do another summation on the boundary edges and
// divide everything by 2 in the end.
void coeff_qlsgr(void)
{
  	for (int n = 0 ; n < NN ; n++ )  
       		node[n].flag = 0 ;

   	for (int i = 0; i < 12; i++)
		for  (int n = 0 ; n < NN ; n ++ )
			katz[i][n] = 0.;

	for (int e = 0; e < NE ; e++) {
		for (int i = 0 ; i < vertex-1 ; i++ ) { // for on the vertices to compute the contributions 0-1 0-2 and 1-2 on the triangle
			for (int j = i+1 ; j < vertex ; j++ ) {
				int ni = element[e].node[i] ;	
				int nj = element[e].node[j] ;
				double rij_x = node[nj].coordinate[0] - node[ni].coordinate[0]; // The length of the egde ij
				double rij_y = node[nj].coordinate[1] - node[ni].coordinate[1];
				/***********************************************/
				// For the conservation of the mean:
				/***********************************************/
				rij_x = node[nj].DcC[0] - node[ni].DcC[0];
				rij_y = node[nj].DcC[1] - node[ni].DcC[1];
				/***********************************************/
				double wij = 1./sqrt(pow(rij_x,2) + pow(rij_y,2));
							
				katz[0][ni] += wij * rij_x * rij_x;
				katz[1][ni] += wij * rij_x * rij_y;
				katz[2][ni] += wij * rij_y * rij_y;
				katz[3][ni] += wij * rij_x * rij_x * rij_x;
				katz[4][ni] += wij * rij_x * rij_x * rij_y;
				katz[5][ni] += wij * rij_x * rij_y * rij_y;
				katz[6][ni] += wij * rij_y * rij_y * rij_y;
				katz[7][ni] += wij * rij_x * rij_x * rij_x * rij_x;
				katz[8][ni] += wij * rij_x * rij_x * rij_x * rij_y;
				katz[9][ni] += wij * rij_x * rij_x * rij_y * rij_y;
				katz[10][ni] += wij * rij_x * rij_y * rij_y * rij_y;
				katz[11][ni] += wij * rij_y * rij_y * rij_y * rij_y;
									
				katz[0][nj] += wij * rij_x * rij_x;
				katz[1][nj] += wij * rij_x * rij_y;
				katz[2][nj] += wij * rij_y * rij_y;
				katz[3][nj] -= wij * rij_x * rij_x * rij_x;
				katz[4][nj] -= wij * rij_x * rij_x * rij_y;
				katz[5][nj] -= wij * rij_x * rij_y * rij_y;
				katz[6][nj] -= wij * rij_y * rij_y * rij_y;
				katz[7][nj] += wij * rij_x * rij_x * rij_x * rij_x;
				katz[8][nj] += wij * rij_x * rij_x * rij_x * rij_y;
				katz[9][nj] += wij * rij_x * rij_x * rij_y * rij_y;
				katz[10][nj] += wij * rij_x * rij_y * rij_y * rij_y;
				katz[11][nj] += wij * rij_y * rij_y * rij_y * rij_y;
			}
		}
	}

	for (int f = 0; f < NBF; f++) {
		int ni = boundary[f].node[0];
		int nj = boundary[f].node[1];
		double rij_x = node[nj].coordinate[0] - node[ni].coordinate[0]; // The length of the egde ij
		double rij_y = node[nj].coordinate[1] - node[ni].coordinate[1];
		/***********************************************/
		// For the conservation of the mean:
		/***********************************************/
		rij_x = node[nj].DcC[0] - node[ni].DcC[0];
		rij_y = node[nj].DcC[1] - node[ni].DcC[1];
		/***********************************************/
		double wij = 1./sqrt(pow(rij_x,2) + pow(rij_y,2));
		
		if (b_nodes[f].type!=10){
			katz[0][ni] += wij * rij_x * rij_x;
			katz[1][ni] += wij * rij_x * rij_y;
			katz[2][ni] += wij * rij_y * rij_y;
			katz[3][ni] += wij * rij_x * rij_x * rij_x;
			katz[4][ni] += wij * rij_x * rij_x * rij_y;
			katz[5][ni] += wij * rij_x * rij_y * rij_y;
			katz[6][ni] += wij * rij_y * rij_y * rij_y;
			katz[7][ni] += wij * rij_x * rij_x * rij_x * rij_x;
			katz[8][ni] += wij * rij_x * rij_x * rij_x * rij_y;
			katz[9][ni] += wij * rij_x * rij_x * rij_y * rij_y;
			katz[10][ni] += wij * rij_x * rij_y * rij_y * rij_y;
			katz[11][ni] += wij * rij_y * rij_y * rij_y * rij_y;

			katz[0][nj] += wij * rij_x * rij_x;
			katz[1][nj] += wij * rij_x * rij_y;
			katz[2][nj] += wij * rij_y * rij_y;
			katz[3][nj] -= wij * rij_x * rij_x * rij_x;
			katz[4][nj] -= wij * rij_x * rij_x * rij_y;
			katz[5][nj] -= wij * rij_x * rij_y * rij_y;
			katz[6][nj] -= wij * rij_y * rij_y * rij_y;
			katz[7][nj] += wij * rij_x * rij_x * rij_x * rij_x;
			katz[8][nj] += wij * rij_x * rij_x * rij_x * rij_y;
			katz[9][nj] += wij * rij_x * rij_x * rij_y * rij_y;
			katz[10][nj] += wij * rij_x * rij_y * rij_y * rij_y;
			katz[11][nj] += wij * rij_y * rij_y * rij_y * rij_y;
		}
	}

	// Correction for periodic boundaries
	for ( int f = 0 ; f < NBF ; f ++ ) {
        	if(b_nodes[f].type == 10) {
	            	int n1 = b_nodes[f].node ;
			if(node[n1].flag == 0) {		
				int mate = node[n1].mate ;
				katz[0][n1] += katz[0][mate];
				katz[1][n1] += katz[1][mate];
				katz[2][n1] += katz[2][mate];
				katz[3][n1] += katz[3][mate];
				katz[4][n1] += katz[4][mate];
				katz[5][n1] += katz[5][mate];
				katz[6][n1] += katz[6][mate];
				katz[7][n1] += katz[7][mate];
				katz[8][n1] += katz[8][mate];
				katz[9][n1] += katz[9][mate];
				katz[10][n1] += katz[10][mate];
				katz[11][n1] += katz[11][mate];

				katz[0][mate] = katz[0][n1];
				katz[1][mate] = katz[1][n1];
				katz[2][mate] = katz[2][n1];
				katz[3][mate] = katz[3][n1];
				katz[4][mate] = katz[4][n1];
				katz[5][mate] = katz[5][n1];
				katz[6][mate] = katz[6][n1];
				katz[7][mate] = katz[7][n1];
				katz[8][mate] = katz[8][n1];
				katz[9][mate] = katz[9][n1];
				katz[10][mate] = katz[10][n1];
				katz[11][mate] = katz[11][n1];

				node[mate].flag = 1;
				node[n1].flag = 1;
			}
		 }
  	}

	for (int i = 0; i < 12; i++)
		for (int n = 0; n < NN; n++)
			katz[i][n] *= 0.5;
}

