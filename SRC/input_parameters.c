/*  Copyright (C) 2021   M.Ricchiuto  <mario.ricchiuto@inria.fr>
                         M. Kazolea   <maria.kazolea@inria.fr>
                         A. Filippini <a.filippini@brgm.fr>
                         L. Arpaia    <luca.arpaia@brgm.fr>
                         N.Pattakos   <pattakosn@fastmail.com> 

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.*/
	

/***************************************************************************
                              input_parameters.c
                               ----------------
This is input_parameters: it reads and saves all the necessary informations
         needed to set up and run the computation  from the inputfile
                             --------------------
 ***************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include "common.h"
int scheme, time_discretization, output_steps, write_time, exner_flux;
int numerics_friction_explicit, model_dispersion, model_breaking, model_Tmatrix, SPD_param,grad_reco;
int convergence_variable, time_step_max, stop, ite_mm;
int movie, info, bedRK2, exact_bed, save_solution, problem, Mscaling;
int steady_check, bmove;
double time_CFL, residual_treshold, t_max, gm, KK;
double manning, lim_norm, cut_off_H, cut_off_U, coarse_cut_off,output_stepsize, UUinf, VVinf, acoeff, mcoeff, pcoeff;
double brk_param[5], time_solution;
char initial_solution_file[MAX_CHAR], grid_file[MAX_CHAR], out_file[MAX_CHAR];
extern int blending,initial_state;

void input_parameters(void)
{
	int done;
	FILE *inputfile = fopen("../textinput/inputfile-ex-ex-loose.txt", "r");
	if (!inputfile) {
		printf("ERROR: inputfile-ex-ex-loose.txt not found!\n");
		exit(EXIT_FAILURE);
	}

	printf("          *************************************\n");
	printf("          **    Reading the inputfile.....   **\n");
	printf("          *************************************\n");
	printf("\n");
	fscanf(inputfile, "***************************************************\n");
	fscanf(inputfile, "***************************************************\n");
	fscanf(inputfile, "***************************************************\n");
	fscanf(inputfile, "***************************************************\n");
	fscanf(inputfile, "***************************************************\n");
	fscanf(inputfile, "**                  PHYSICS                      **\n");
	fscanf(inputfile, "***************************************************\n");
	fscanf(inputfile, "   Initial state (1-10)                      %d    \n", &initial_state);
	fscanf(inputfile, "   Initial solution file (or NULL)           %s    \n", &initial_solution_file);
	fscanf(inputfile, "   Problem                                   %d    \n", &problem);
	fscanf(inputfile, "   Gamma                                     %le    \n", &gm);
	fscanf(inputfile, "   U infinity                                %le    \n", &UUinf);
	fscanf(inputfile, "   V infinity                                %le    \n", &VVinf);
	fscanf(inputfile, "   p coefficient                             %le    \n", &pcoeff);
	fscanf(inputfile, "   A coefficient                             %le    \n", &acoeff);
	fscanf(inputfile, "   m coefficient                             %le    \n", &mcoeff);
	fscanf(inputfile, "   Friction coefficient                       %le    \n", &manning);
	fscanf(inputfile, "   Exner flux 0 (exponential), 1 (mass flux)  %d    \n", &exner_flux);
	fscanf(inputfile, "***************************************************\n");
	fscanf(inputfile, "**                  MODEL                        **\n");
	fscanf(inputfile, "***************************************************\n");
	fscanf(inputfile, "   Enable dispersion terms (Green-Naghdi)       %d \n", &model_dispersion);
	fscanf(inputfile, "   T-Matrix:   0 (full) 1 (const diagonal)         \n");
	fscanf(inputfile, "               2 (non conservative form)        %d \n", &model_Tmatrix);
	fscanf(inputfile, "   SPD Format: 0 (None) 1 (standard) 2 (SPD) %d \n", &SPD_param);
	fscanf(inputfile, "   Breaking: 0 (None) 1 (Hybrid) 2 (Turbulence) %d \n", &model_breaking);
	fscanf(inputfile, "   Hybrid Param:				      \n" );
	fscanf(inputfile, "        - gamma (surface variation crit)        %le \n", &brk_param[0]);
	fscanf(inputfile, "        - phi (slope angle crit)                %le \n", &brk_param[1]);
	fscanf(inputfile, "   Turbulence Param:				      \n" );
	fscanf(inputfile, "        - sigma (empirical const)              %le \n", &brk_param[2]);
	fscanf(inputfile, "        - cd (empirical const)                 %le \n", &brk_param[3]);
	fscanf(inputfile, "        - lt (turbulence length)               %le \n", &brk_param[4]);
	fscanf(inputfile, "***************************************************\n");
	fscanf(inputfile, "**                  GEOMETRY                     **\n");
	fscanf(inputfile, "***************************************************\n");
	fscanf(inputfile, "   External grid file                        %s    \n", grid_file);
	fscanf(inputfile, "***************************************************\n");
	fscanf(inputfile, "**                  NUMERICS                     **\n");
	fscanf(inputfile, "***************************************************\n");
	fscanf(inputfile, "   Blending: 0 (LLFs), 1 (Bc), 2 (MUSCL2), 3 (MUSCL3)     %d     \n", &blending);
	fscanf(inputfile, "   Scaling: 0 (scalar tau), 1 (matrix tau)   %d     \n", &Mscaling);
	fscanf(inputfile, "   Interpolate:  0 (std),1 (kinetic)         %d     \n", &scheme);
	fscanf(inputfile, "   Linear bathymetry (0) linear, (1) exact   %d     \n", &exact_bed);
	fscanf(inputfile, "   Time discretization: 0 (RK1), 1 (RK2), 2 (RK3)     %d     \n", &time_discretization);
	fscanf(inputfile, "   Explicit friction treatment: 0(off) 1(on) %d    \n", &numerics_friction_explicit);
	fscanf(inputfile, "   Time CFL                                  %le    \n", &time_CFL);
	fscanf(inputfile, "   Zero_H                                    %le    \n", &cut_off_H);
	fscanf(inputfile, "   Zero_U                                    %le    \n", &cut_off_U);
	fscanf(inputfile, "   Coarse cutoff                             %le    \n", &coarse_cut_off);
	fscanf(inputfile, "***************************************************\n");
	fscanf(inputfile, "**                  ALGEBRA                      **\n");
	fscanf(inputfile, "***************************************************\n");
	fscanf(inputfile, "   Convergence treshold                     %le    \n", &residual_treshold);
	fscanf(inputfile, "   Convergence limit                        %le    \n", &lim_norm);
	fscanf(inputfile, "   Check Steady State                        %d    \n", &steady_check);
	fscanf(inputfile, "   Convergence variable (0->Neq.s-1)         %d    \n", &convergence_variable);
	fscanf(inputfile, "***************************************************\n");
	fscanf(inputfile, "**                   STOP                        **\n");
	fscanf(inputfile, "***************************************************\n");
	fscanf(inputfile, "   Final time                                %le    \n", &t_max);
	fscanf(inputfile, "   Number of additional output steps	       %d    \n", &output_steps);
	fscanf(inputfile, "   Output time step size                     %le    \n", &output_stepsize);
	fscanf(inputfile, "   Maximum number of time steps              %d    \n", &time_step_max);
	fscanf(inputfile, "   Stop                                      %d    \n", &stop);
	fscanf(inputfile, "***************************************************\n");
	fscanf(inputfile, "**                  OUTPUT                       **\n");
	fscanf(inputfile, "***************************************************\n");
	fscanf(inputfile, "   Movie (0/1)                               %d    \n", &movie);
	fscanf(inputfile, "   Steps to save                             %d    \n", &save_solution);
	fscanf(inputfile, "   Time to save                              %le   \n", &time_solution);
	fscanf(inputfile, "   Information Level (0/1)                   %d    \n", &info);
	fscanf(inputfile, "   Output file                               %s    \n", out_file);
	fscanf(inputfile, "   Write solution time                       %d    \n", &write_time);
	fscanf(inputfile, "***************************************************\n");
	fscanf(inputfile, "***************************************************\n");
	fscanf(inputfile, "                      %d                           \n", &done);
	fscanf(inputfile, "***************************************************\n");
	fscanf(inputfile, "***************************************************\n");
	fscanf(inputfile, "***************************************************\n");
	
	// Fixed parameters for the Tmatrix and the SPD format
	model_Tmatrix=0;
	SPD_param=1;
	grad_reco=0;

	if (done != 8576) {
		printf("ERROR: inputfile was not read correctly!!");
		exit(EXIT_FAILURE);
	}
	fclose(inputfile);

//	if (13 == initial_state) // Vortex
//		t_max = 1./6.*1./4.;

}
