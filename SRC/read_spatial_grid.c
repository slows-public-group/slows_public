/*  Copyright (C) 2021   M.Ricchiuto  <mario.ricchiuto@inria.fr>
                         M. Kazolea   <maria.kazolea@inria.fr>
                         A. Filippini <a.filippini@brgm.fr>
                         L. Arpaia    <luca.arpaia@brgm.fr>
                         N.Pattakos   <pattakosn@fastmail.com> 

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.*/
	


/***************************************************************************
                             read_spatial_grid.c
                             -------------------
          This is read_spatial_grid: every function reading from the
                   (externally provided) grid_file is here.
MIND: The grid-file format to be used should be the .grd (modified .dpl).
      It is structured as follows:
                               - header      : dimension NE NN NF NBF
                               - blank line
                               - e:=1 -> NE  : e_node_1 ... e_node_vertex
                                               e_neigh_1 ... e_neigh_vertex
                                               e_neigh_face_1 ... e_neigh_face_vertex
                               - blank line
                               - n:=1 -> NN  : n_x_1 ... n_x_dimension n_mate
                               - blank line
                               - f:=1 -> NBF : f_n1... f_ndimension
                                               type_n1... type_ndimension
                                               f_type
                         -------------------
***************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "common.h"

extern int NE, NN, NBF, initial_state;
extern char grid_file[MAX_CHAR];
extern char grid_file_name[MAX_CHAR];
extern struct node_struct *node;
extern struct element_struct *element;
extern struct boundary_struct *boundary;

static FILE *grid = NULL;

/* Read the HEADER of the external .grd GRID_FILE */
void read_spatial_grid_header(void)
{
	if (!strcmp(grid_file, "DEFAULT")) {
		if (1 == initial_state)
			sprintf(grid_file, "wedge_sw");
		else if (2 == initial_state)
			sprintf(grid_file, "2d200");
		else if (3 == initial_state)
			sprintf(grid_file, "2d200");
		else if (4 == initial_state)
			sprintf(grid_file, "2Dbott");
		else if (5 == initial_state)
			sprintf(grid_file, "lake");
		else if (6 == initial_state)
			sprintf(grid_file, "mario2");
		else if (7 == initial_state)
			sprintf(grid_file, "dam_final");
		else if (8 == initial_state)
			sprintf(grid_file, "mario2");
		else if (9 == initial_state)
			sprintf(grid_file, "lake");
		else if (10 == initial_state)
			sprintf(grid_file, "2d200");
		else if ( (11 == initial_state) || (15 == initial_state) || (16 == initial_state) )
			sprintf(grid_file, "mario2");
		else if (13 == initial_state) // vortex test case
			sprintf(grid_file, "vort_t1-3");
		else if (20 == initial_state)
			sprintf(grid_file, "mario0");
		else if (22 == initial_state)
			sprintf(grid_file, "vort_t1-4");
		else if (24 == initial_state)
			sprintf(grid_file, "lynett_refined_mmg");
		else if (25 == initial_state)
			sprintf(grid_file, "okushiri1");
		else if (30 == initial_state)
			sprintf(grid_file, "vort_t1-3");
		else if (70 == initial_state)
			sprintf(grid_file,"solitary_unstr0P1");
		else if (71 == initial_state)
			sprintf(grid_file,"convergent_channel");
		else if (72 == initial_state)
			sprintf(grid_file,"gironde");
		else if (73 == initial_state)
			sprintf(grid_file,"garonne");
		else if (74 == initial_state)
			sprintf(grid_file,"dordogne");
		else if (75 == initial_state)
			sprintf(grid_file, "rip_current_COARSE");
		else if (76 == initial_state)
			sprintf(grid_file, "synolakis_unst_0P05");
		else if (77 == initial_state)
			sprintf(grid_file, "volker_reef_wall");
		else if (78 == initial_state)
			sprintf(grid_file, "bar_wall");
		else if (79 == initial_state)
			sprintf(grid_file, "gironde");
		else if (80 == initial_state)
			sprintf(grid_file, "shoaling_lannes");
		else if (81 == initial_state)
			sprintf(grid_file, "elliptic-shoal-adapted");
		else if (84 == initial_state)
			sprintf(grid_file, "oregon");
		else {
			printf("Default grid not specified for this case, please specify grid \n");
			exit(EXIT_FAILURE);
		}
	}

	sprintf(grid_file_name, "../input/%s.grd", grid_file);
	if (NULL == grid)
		grid = fopen(grid_file_name, "r");
	if (!grid) {
		printf("ERROR: External spatial grid file %s was not found !\n", grid_file);
		exit(EXIT_FAILURE);
	}

	int dummy;
	fscanf(grid, "%d %d %d %d \n", &dummy, &NE, &NN, &NBF);
}


/* Read the external .grd GRID_FILE */
void read_spatial_grid(void)
{
	fscanf(grid, "\n");
	for (int e = 0; e < NE; e++){
		fscanf(grid, "%d %d %d\n", &element[e].node[0], &element[e].node[1], &element[e].node[2]);
        }
	fscanf(grid, "\n");
	for (int n = 0; n < NN; n++) {
		fscanf(grid, "%le %le %d\n", &node[n].coordinate_0[0], &node[n].coordinate_0[1], &node[n].mate);
		node[n].coordinate_old[0] = node[n].coordinate_0[0];
		node[n].coordinate_old[1] = node[n].coordinate_0[1];
		node[n].coordinate_new[0] = node[n].coordinate_0[0];
		node[n].coordinate_new[1] = node[n].coordinate_0[1];
		node[n].coordinate[0] = node[n].coordinate_0[0];
		node[n].coordinate[1] = node[n].coordinate_0[1];
		node[n].vel[0] = 0.0;
		node[n].vel[1] = 0.0;
		/*node[n].coordinate[0] *= 20. ; node[n].coordinate[1] *= 20. ; */
	}
	fscanf(grid, "\n");
	for (int f = 0; f < NBF; f++) {
		fscanf(grid, "%d %d %d %d %d\n", &boundary[f].node[0], &boundary[f].node[1], &boundary[f].types[0], &boundary[f].types[1], &boundary[f].type);
	}
	fclose(grid);
	grid = NULL;

	/* Order boundary elements */
	for (int f = 0; f < NBF - 1; f++) {
		for (int k = f + 1; k < NBF; k++) {
			if (boundary[f].node[1] == boundary[k].node[0]) {
				double temp0 = boundary[k].node[0];
				double temp1 = boundary[k].node[1];
				double tempT0 = boundary[k].types[0];
				double tempT1 = boundary[k].types[1];
				double tempT = boundary[k].type;

				boundary[k].node[0]  = boundary[f + 1].node[0];
				boundary[k].node[1]  = boundary[f + 1].node[1];
				boundary[k].types[0] = boundary[f + 1].types[0];
				boundary[k].types[1] = boundary[f + 1].types[1];
				boundary[k].type     = boundary[f + 1].type;

				boundary[f+1].node[0]  = temp0;
				boundary[f+1].node[1]  = temp1;
				boundary[f+1].types[0] = tempT0;
				boundary[f+1].types[1] = tempT1;
				boundary[f+1].type     = tempT;
			}
		}
	}
}
