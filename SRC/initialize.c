/*  Copyright (C) 2021   M.Ricchiuto  <mario.ricchiuto@inria.fr>
                         M. Kazolea   <maria.kazolea@inria.fr>
                         A. Filippini <a.filippini@brgm.fr>
                         L. Arpaia    <luca.arpaia@brgm.fr>
                         N.Pattakos   <pattakosn@fastmail.com> 

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.*/


/***************************************************************************
                                 initialize.c
                                 ------------
This is initialize: it initializes some model, geometry, scheme and case
                      dependent constant and parameters
 ***************************************************************************/
#include "common.h"
const int NBN = 2;
const double c_tau = 0.5;

int iteration_max;
extern int time_step, time_discretization, time_bc_pts, initial_state, btableimax, btablejmax;
extern int NE, NN, NBF, size;
double a_RK[3][4], exnercoeff, exnerexponent;
extern double acoeff, pcoeff, mcoeff;

void read_spatial_grid_header(void);

void initialize(void)
{
	time_step = 0;

	/* Read GRID_FILE HEADER to determine the number of nodes (NN),  elements (NE), faces (NF) and boundary faces (NBF) */
	read_spatial_grid_header();

    for (int i = 0 ; i < 3 ; i++)
		for (int j = 0 ; j < 4 ; j++)
			a_RK[i][j]=0.;

	if (0 == time_discretization) {
		iteration_max = 1;
		a_RK[0][0] = 0.;
		a_RK[0][1] = 1.;
	} else if (1 == time_discretization) {
		iteration_max = 2;
		a_RK[0][0] = 0.;
		a_RK[0][1] = 1.;
		a_RK[1][0] = 0.5;
		a_RK[1][1] = 0.5;
	} else if (2 == time_discretization) {
        iteration_max = 3; 
		a_RK[0][0] = 0.;
		a_RK[0][1] = 1.;
		a_RK[1][0] = 3./4.;
		a_RK[1][1] = 1./4.;
		a_RK[2][0] = 1./3.;
		a_RK[2][1] = 2./3.;
	} else if (3 == time_discretization) {
		iteration_max = 1; //do euler for the first two steps
		a_RK[0][0] = 0.;
		a_RK[0][1] = 1.;
        //iteration_max = 3; 
	//	a_RK[0][0] = 0.;
	//	a_RK[0][1] = 1.;
	//	a_RK[1][0] = 3./4.;
	//	a_RK[1][1] = 1./4.;
	//	a_RK[2][0] = 1./3.;
	//	a_RK[2][1] = 2./3.;
	}

	exnercoeff = acoeff / (1. - pcoeff);
	exnerexponent = 0.5 * (mcoeff - 1.);

	if (initial_state == 24) {
		time_bc_pts = 1;
		btableimax = 265;
		btablejmax = 501;
	} else if (initial_state == 25) {
		time_bc_pts = 462;
		btableimax = 393;
		btablejmax = 244;
	} else if (initial_state == 84) {
		time_bc_pts = 5237; //number of lines in the files that contains the input wave series
		btableimax = 726;
		btablejmax = 2082;
	} else if (initial_state == 92) {
		time_bc_pts = 1;
		btableimax = NN;
		btablejmax = 1;
	} else {
		time_bc_pts = btableimax = btablejmax = 1;
	}
}
