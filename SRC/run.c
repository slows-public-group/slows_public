/*  Copyright (C) 2021   M.Ricchiuto  <mario.ricchiuto@inria.fr>
                         M. Kazolea   <maria.kazolea@inria.fr>
                         A. Filippini <a.filippini@brgm.fr>
                         L. Arpaia    <luca.arpaia@brgm.fr>
                         N.Pattakos   <pattakosn@fastmail.com> 

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.*/
	


/***************************************************************************
                               run.c
                                 -----
                 This is run: it drives the actual computation
                            -------------------
***************************************************************************/
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include "common.h"
#include "sparse.h"
#include "my_malloc.h"
#ifdef WITH_NHT
#include "nht.h"
#endif

int was_negative, first_cell;
extern int print_counter;
extern int time_step, time_step_max, save_solution, output_steps, model_breaking;
extern int initial_state, iteration, ee1, ee2, ee3, ee4, ee5, ee6, ee7, ee8, ee9, ee10, ee16, ee22;
extern int nn[47],grad_reco;
extern double brk_param[5],dt;
extern int iteration_max, steady_check, model_dispersion, model_Tmatrix, SPD_param;;
extern const int vertex, size;
extern double residual_treshold, residual_norm, ref_norm;
extern double gm, boundary_flux, time_solution;
double int_B_new, int_B_old, int_H_new, int_H_old;
extern double wb_mass_loss, remap_mass_loss, cut_off_H, cut_off_U;
double first_time;
double int_boundary_flux = 0.0;
extern struct node_struct *node;
extern struct element_struct *element;
double *nht_h = NULL; // used to pass h to the non hydro lib
double *nht_u = NULL; // used to pass u to the non hydro lib
double *nht_v = NULL; // used to pass v to the non hydro lib
double *nht_k = NULL;
double *nht_e = NULL;
double *nht_eta = NULL; // used to pass eta to the non hydro lib
double *nht_bed = NULL; // used to pass bed to the non hydro lib
double *nht_dxbed = NULL; // used to pass bed_x to the non hydro lib
double *nht_dybed = NULL; // used to pass bed_y to the non hydro lib
double *nht_phi = NULL; // nonHydro->output
double *nht_dxh = NULL; // used to pass dxh to the non hydro lib
double *nht_dxhu = NULL; // used to pass dxu to the non hydro lib
double *nht_dxhv = NULL; // used to pass dxv to the non hydro lib
double *nht_dxeta = NULL; // used to pass dxeta to the non hydro lib
double *nht_dyh = NULL; // used to pass dyh to the non hydro lib
double *nht_dyhu = NULL; // used to pass dyu to the non hydro lib
double *nht_dyhv = NULL; // used to pass dyv to the non hydro lib
double *nht_dyeta = NULL; // used to pass dyeta to the non hydro lib
int *nht_bb = NULL;
double *envelope = NULL;
extern int NN, NNX;
extern double Time, t_max, output_stepsize, steady_norm, lim_norm;
extern void (*write_solution) (void);
extern void (*gradient_reconstruction) (int, int, int);
extern char out_file[MAX_CHAR];
extern char scaled_mesh_name[MAX_CHAR];
char grid_file_name[MAX_CHAR];
double min_H;
extern double *max_grad;
FILE *debug_output;
FILE *timeseriesg[21];
FILE *timeseries;

void initialize_solution(void);
void compute_time_step(void);
void read_tide_amplitude(void);
void read_channel_gauge(void);
void pseudo_time_stepper(void);
//void save_the_mesh(void);
void pass_the_values( void ); //copy the values of the unknowns to nonHydroInput
void get_PHI_bar( void ); //constructs PHI_bar as to add in the equations	
void max_grad_rec() ;
void dissipation_estimate(double *);
void sample_probes(void);
double reconstructed_fun(double *, int , int );

void run(void)
{
	int counter, output_counter, time_count;
	char out_file_save[MAX_CHAR];
	char series_file_name[MAX_CHAR];


	printf("\n");
	printf("          *************************************\n");
	printf("          *************************************\n");
	printf("          **                                 **\n");
	printf("          **             End of the          **\n");
	printf("          **                                 **\n");
	printf("          **        pre-processing phase     **\n");
	printf("          **                                 **\n");
	printf("          *************************************\n");
	printf("          **                                 **\n");
	printf("          ** The real computation starts.... **\n");
	printf("          **                                 **\n");
	printf("          *************************************\n");
	printf("          *************************************\n");
	printf("\n");

	was_negative = 0;
	first_cell = -1;
	first_time = -1.;
	print_counter = 1;
	iteration = 0;
	time_step = 0;
	time_count = 0;
	counter = 0;
	output_counter = 0;
	Time = 0.0;
	min_H = 0.0;
	initialize_solution();

#ifdef WITH_NHT
	if (1 == model_dispersion) {

		nht_bed =  MA_vector( NN, sizeof(double) );
		nht_dxbed =  MA_vector( NN, sizeof(double) );
		nht_dybed =  MA_vector( NN, sizeof(double) );
		double href = 0.;
#pragma omp parallel for
	  	for ( int n = 0; n < NN; n++ ) {
			nht_bed[ n ] = node[ n ].bed[ 2 ];
			nht_dxbed[ n ] = node[ n ].dxbed[ 0 ];
			nht_dybed[ n ] = node[ n ].dybed[ 0 ];
		}
		double mu = 1.;
		if ( (71 == initial_state) || (85 == initial_state) ) // Convergent channel
			mu = 10./50000.;
		double cut_off[2] = {cut_off_H, cut_off_U};
		nht_h =  MA_vector( NN, sizeof(double) );
		nht_u = MA_vector( NN, sizeof(double) );
		nht_v = MA_vector( NN, sizeof(double) );
		nht_eta = MA_vector( NN, sizeof(double) );
		nht_dxh =  MA_vector( NN, sizeof(double) );
		nht_dxhu = MA_vector( NN, sizeof(double) );
		nht_dxhv = MA_vector( NN, sizeof(double) );
		nht_dxeta = MA_vector( NN, sizeof(double) );
		nht_dyh =  MA_vector( NN, sizeof(double) );
		nht_dyhu = MA_vector( NN, sizeof(double) );
		nht_dyhv = MA_vector( NN, sizeof(double) );
		nht_dyeta = MA_vector( NN, sizeof(double) );
		nht_phi = MA_vector( 2 * NN, sizeof(double) );
		nht_bb = MA_vector( NN, sizeof(int) );

		//sparse_initialize_fv(); //not working for periodic bc
		//FVMM_assembly();

		if ( 2 == model_breaking )
			nht_k = MA_vector( NN, sizeof(double) );
			nht_e = MA_vector( NN, sizeof(double) );
		
		if ( EXIT_SUCCESS != nht_new_from_file( scaled_mesh_name, 1.159, gm, mu, href, cut_off, model_Tmatrix, model_breaking, brk_param, SPD_param, nht_bed, nht_dxbed, nht_dybed, NN ) ) {
			printf( "error initializing non hydrodyamic terms lib. Exiting\n" );
			exit( EXIT_FAILURE );
		}
		pass_the_values(); //copy the values of the unknowns to nonHydroInput
		if ( 2 == model_breaking ) {
			/* accounts for the breaking nodes. 1 if the node is breaking 0 if it is not */
			/* energy for the GN equations */
			/* kinetic energy for the breaking model */
			if ( EXIT_SUCCESS != nht_phi_bb_k_e( nht_h, nht_u, nht_v,nht_eta, nht_dxh, nht_dxhu, nht_dxhv, nht_dxeta, nht_dyh, nht_dyhu, nht_dyhv, nht_dyeta, NN, Time, nht_phi,nht_bb, nht_e, nht_k ) ) {
				printf( "error initializing non hydrodynamic terms lib. Exiting\n" );
				exit( EXIT_FAILURE );
			}
		} else {
			/* energy for the GN equations */
			/* accounts for the breaking nodes. 1 if the node is breaking 0 if it is not */
			if ( EXIT_SUCCESS != nht_phi_bb_e( nht_h, nht_u, nht_v, nht_eta, nht_dxh, nht_dxhu, nht_dxhv, nht_dxeta, nht_dyh, nht_dyhu, nht_dyhv, nht_dyeta, NN, Time, nht_phi, nht_bb, nht_e ) ) {
				printf( "error initializing non hydrodynamic terms lib. Exiting\n" );
				exit( EXIT_FAILURE );
			}
		}

		get_PHI_bar(); //constructs PHI_bar as to add in the equations
	}
#endif
        if (85 == initial_state) //convergent-channel
                read_channel_gauge();
        if (79 == initial_state) //gironde+garonne+dordogne
                read_tide_amplitude();
	if (89 == initial_state){ // grilli shoaling of solitary wave
		envelope = MA_vector(1143, sizeof(double));
	}

	printf("output steps: %d\n", output_steps);
	printf("output step size: %le\n", output_stepsize);
	sprintf(out_file_save, "%s", out_file);
	//sprintf(debug_file, "debug_PSI.out");

	printf("Final time of the simulation =%le, Max number of time it.s =%d\n", t_max, time_step_max);


	while ((t_max > Time + small) && (time_step < time_step_max) && (steady_norm > lim_norm)) {
		while ((t_max > Time + small) && (time_step < time_step_max) && (steady_norm > lim_norm)) {

			//sprintf(debug_file, "debug_PSI%i.out", iteration);
			//printf( "while\n" ) ;
			time_step++;
			counter++;
			time_count++;
			initialize_solution();
			compute_time_step();
			pseudo_time_stepper();

			/* Print on screen */
			if (initial_state == 13) {
				double einf = 0;
				double e1 = 0;
				double e2 = 0;

				for (int n = 0; n < NN; n++) {
					double xc = node[n].coordinate_new[0] - (Time * 6.0 + 0.5);
					double yc = node[n].coordinate_new[1] - 0.5;
					double r = sqrt(xc * xc + yc * yc);
					double rho = 4. * pi * r;
					//omega = 15. * (1. + cos(rho));
					double pm = 10.;
					double dp = (225. / (gm * 16. * pi * pi)) * (2. * cos(rho) + 2. * rho * sin(rho)
									      + cos(2. * rho) / 8. + rho * sin(2. * rho) / 4. +
									      rho * rho * 12. / 16.);
					dp -= (225. / (gm * 16. * pi * pi)) * (2. * cos(pi) + 2. * pi * sin(pi)
									       + cos(2. * pi) / 8. + pi * sin(2. * pi) / 4. +
									       pi * pi * 12. / 16.);

					if (r >= 0.25) {
						//omega = 0.;
						dp = 0.;
					}

					double derr = fabs(pm + dp - node[n].P[2][0]);
					if (derr > einf)
						einf = derr;
					e1 += derr * node[n].mod_volume;
					e2 += derr * derr * node[n].mod_volume;
				}

				printf("\n");
				printf("\n");
				printf("Error w.r.t. initial solution at time %le\n", Time);
				printf("Error in Linf norm %le\n", log10(einf));
				printf("Error in L1 norm %le\n", log10(e1));
				printf("Error in L2 norm %le\n", log10(sqrt(e2)));
				printf("\n");
				printf("\n");
			}

			if (iteration == iteration_max) {
				printf("\n");
				printf("          *************************************\n");
				printf("          *************************************\n");
				printf("          **      timestep information:      **\n");
				printf("          ** MAX number of iteration reached **\n");
				printf("          *************************************\n");
				printf("               LOG10( norm1_res ) = %le           \n", residual_norm);
				printf("               Initial norm       = %le           \n", ref_norm);
				if (was_negative) {
					printf
					    ("Negative water height during computation, first time: %le, first cell: %d, minimal water height: %le\n\n",
					     first_time, first_cell, min_H);
				}
			} else {
				printf("\n");
				printf("          *************************************\n");
				printf("          *************************************\n");
				printf("          **      timestep information:      **\n");
				printf("          **  residual treshold reached in   **\n");
				printf("          *************************************\n");
				printf("                     %d iterations             \n", iteration);
				printf("               Initial norm       = %le           \n", ref_norm);
				if (was_negative) {
					printf
					    ("Negative water height during computation, first time: %le, first cell: %d, minimal water height: %le\n\n",
					     first_time, first_cell, min_H);
				}
			}

			if (steady_check) {
				printf("                     norm1_steady  = %le           \n", steady_norm);
				if (steady_norm < residual_treshold) {
					printf("\n");
					printf("          *************************************\n");
					printf("          *************************************\n");
					printf("          **      A STEADY SOLUTION has      **\n");
					printf("          **       been REACHED !!!!!!!!!    **\n");
					printf("          *************************************\n");
					printf("          *************************************\n");
					time_step = time_step_max + 1;
				}
			}

			if (was_negative) {
				printf("TIME %le\n \n", Time);
				printf("NEGATIVE DEPTH !!!\n");
				printf("EXITING ...\n");
				write_solution();
				exit(EXIT_FAILURE);
			}

			//time += dt;
			printf("                     Time Iteration = %d  (Max allowed = %d)          \n", time_step, time_step_max);
			printf("                     Time Reached = %le   (Final time = %le)         \n", Time, t_max);

			/* Writing solution */
			if (counter == save_solution) {
				counter = 0;
				write_solution();
			} else if (Time == print_counter*time_solution){
				write_solution();	
				print_counter += 1;
			}

			/* Below: sampling for Okushiri experiment */
			if (initial_state == 25) {
				double xg1 = 4.521;
				double yg1 = 1.196;
				double xg6 = 4.521;
				double yg6 = 1.696;
				double xg9 = 4.521;
				double yg9 = 2.196;
				double xg16 = 5.05;
				double yg16 = 1.900;
				double xg_vect[4] = { xg1, xg6, xg9, xg16 };
				double yg_vect[4] = { yg1, yg6, yg9, yg16 };
				int ee_vect[4] = { ee1, ee6, ee9, ee16 };
				int g_vect[4] = { 5, 7, 9, 16 };

				for (int g = 0; g < 4; g++) {
					int ee = ee_vect[g];
					double xg = xg_vect[g];
					double yg = yg_vect[g];
					double x1 = node[element[ee].node[0]].coordinate_new[0];
					double y1 = node[element[ee].node[0]].coordinate_new[1];
					double x2 = node[element[ee].node[1]].coordinate_new[0];
					double y2 = node[element[ee].node[1]].coordinate_new[1];
					double x3 = node[element[ee].node[2]].coordinate_new[0];
					double y3 = node[element[ee].node[2]].coordinate_new[1];
					double coeff[vertex][vertex];
					coeff[0][0] = x2 * y3 - x3 * y2;
					coeff[0][1] = y2 - y3;
					coeff[0][2] = x3 - x2;
					coeff[1][0] = x3 * y1 - x1 * y3;
					coeff[1][1] = y3 - y1;
					coeff[1][2] = x1 - x3;
					coeff[2][0] = x1 * y2 - x2 * y1;
					coeff[2][1] = y1 - y2;
					coeff[2][2] = x2 - x1;
					double hg = 0.;
					double bg = 0.;

					for (int v = 0; v < vertex; v++) {
						double N = coeff[v][0] + coeff[v][1] * xg + coeff[v][2] * yg;
						N /= 2.0 * element[ee].new_volume;
						hg += N * node[element[ee].node[v]].P[2][0];
						bg += N * node[element[ee].node[v]].bed[2];
					}

					if (g == 3) {
						/* if ( node[element[ee].node[0]].P[2][0] < 2.e-3 
						   && node[element[ee].node[1]].P[2][0] < 2.e-3
						   && node[element[ee].node[2]].P[2][0] < 2.e-3  ) */

						/*if ( hg < 1.0e-3  ) {
						   hg = 0.0;
						   bg = 3.817928e-02;
						} */
					}

					sprintf(series_file_name, "../output/%s_gauge%d.dat", out_file, g_vect[g]);
					timeseriesg[0] = fopen(series_file_name, "a");
					fprintf(timeseriesg[0], "%le %le %le\n", Time, hg, bg);
					fclose(timeseriesg[0]);
				}

				// Sampling every 0.1 seconds for  Okushiri
				double sample_time;
				int samples_num = 250;
				for (int n = 1; n <= samples_num; n++) {
					sample_time = n * output_stepsize;
					if (fabs(Time - sample_time) < 1.e-9)
						write_solution();
				}
			}

			/* sampling for Shelf/island experiment */
			if (initial_state == 24) {
				double xg1 = 7.5;
				double yg1 = 0.0;
				double xg2 = 13.0;
				double yg2 = 0.0;
				double xg3 = 21.0;
				double yg3 = 0.0;
				double xg4 = 7.5;
				double yg4 = 5.0;
				double xg5 = 13.0;
				double yg5 = 5.0;
				double xg6 = 21.0;
				double yg6 = 5.0;
				double xg7 = 25.0;
				double yg7 = 0.0;
				double xg8 = 25.0;
				double yg8 = 5.0;
				double xg9 = 25.0;
				double yg9 = 10.0;
				double xg10 = 21.0;
				double yg10 = -5.0;
				double xg_vect[10] = { xg1, xg2, xg3, xg4, xg5, xg6, xg7, xg8, xg9, xg10 };
				double yg_vect[10] = { yg1, yg2, yg3, yg4, yg5, yg6, yg7, yg8, yg9, yg10 };
				int ee_vect[10] = { ee1, ee2, ee3, ee4, ee5, ee6, ee7, ee8, ee9, ee10 };
				int g_vect[10] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };

				for (int g = 0; g < 10; g++) {
					int ee = ee_vect[g];
					double xg = xg_vect[g];
					double yg = yg_vect[g];
					double x1 = node[element[ee].node[0]].coordinate_new[0];
					double y1 = node[element[ee].node[0]].coordinate_new[1];
					double x2 = node[element[ee].node[1]].coordinate_new[0];
					double y2 = node[element[ee].node[1]].coordinate_new[1];
					double x3 = node[element[ee].node[2]].coordinate_new[0];
					double y3 = node[element[ee].node[2]].coordinate_new[1];
					double coeff[vertex][vertex];
					coeff[0][0] = x2 * y3 - x3 * y2;
					coeff[0][1] = y2 - y3;
					coeff[0][2] = x3 - x2;
					coeff[1][0] = x3 * y1 - x1 * y3;
					coeff[1][1] = y3 - y1;
					coeff[1][2] = x1 - x3;
					coeff[2][0] = x1 * y2 - x2 * y1;
					coeff[2][1] = y1 - y2;
					coeff[2][2] = x2 - x1;
					double hg = 0.;
					double bg = 0.;
					double hug = 0.;
					double hvg = 0.;

					for (int v = 0; v < vertex; v++) {
						double N = coeff[v][0] + coeff[v][1] * xg + coeff[v][2] * yg;
						N /= 2.0 * element[ee].new_volume;
						hg += N * node[element[ee].node[v]].P[2][0];
						bg += N * node[element[ee].node[v]].bed[2];

						if (g == 1 || g == 2 || g == 9)
							hug += N * node[element[ee].node[v]].P[2][1];

						if (g == 9)
							hvg += N * node[element[ee].node[v]].P[2][2];
					}

					sprintf(series_file_name, "../output/%s_gauge%d.dat", out_file, g_vect[g]);
					timeseriesg[0] = fopen(series_file_name, "a");
					if (g == 1 || g == 2)
						fprintf(timeseriesg[0], "%le %le %le %le\n", Time, hg, bg, hug);
					else if (g == 9)
						fprintf(timeseriesg[0], "%le %le %le %le %le\n", Time, hg, bg, hug, hvg);
					else
						fprintf(timeseriesg[0], "%le %le %le\n", Time, hg, bg);
					fclose(timeseriesg[0]);
				}

				// Sampling every 0.5 seconds for Shelf/island
				double sample_time;
				int samples_num = 2000;
				for (int n = 1; n <= samples_num; n++) {
					sample_time = n * output_stepsize;
					if (fabs(Time - sample_time) < 1.e-9)
						write_solution();
				}
			}


			/* sampling for conical island test case */
			if (initial_state == 8) {
				double xg1 = 6.82;
				double yg1 = 13.05;
				double xg6 = 12.96 - 3.6;
				double yg6 = 13.8;
				double xg9 = 12.96 - 2.6;
				double yg9 = 13.8;
				double xg16 = 12.96;
				double yg16 = 13.8 - 2.58;
				double xg22 = 12.96 + 2.6;
				double yg22 = 13.8;
				double xg_vect[5] = { xg1, xg6, xg9, xg16, xg22 };
				double yg_vect[5] = { yg1, yg6, yg9, yg16, yg22 };
				int ee_vect[5] = { ee1, ee6, ee9, ee16, ee22 };
				int g_vect[5] = { 1, 6, 9, 16, 22 };

				for (int g = 0; g < 5; g++) {
					int ee = ee_vect[g];
					double xg = xg_vect[g];
					double yg = yg_vect[g];
					double x1 = node[element[ee].node[0]].coordinate_new[0];
					double y1 = node[element[ee].node[0]].coordinate_new[1];
					double x2 = node[element[ee].node[1]].coordinate_new[0];
					double y2 = node[element[ee].node[1]].coordinate_new[1];
					double x3 = node[element[ee].node[2]].coordinate_new[0];
					double y3 = node[element[ee].node[2]].coordinate_new[1];
					double coeff[vertex][vertex];
					coeff[0][0] = x2 * y3 - x3 * y2;
					coeff[0][1] = y2 - y3;
					coeff[0][2] = x3 - x2;
					coeff[1][0] = x3 * y1 - x1 * y3;
					coeff[1][1] = y3 - y1;
					coeff[1][2] = x1 - x3;
					coeff[2][0] = x1 * y2 - x2 * y1;
					coeff[2][1] = y1 - y2;
					coeff[2][2] = x2 - x1;
					double hg = 0.;
					double bg = 0.;

					for (int v = 0; v < vertex; v++) {
						double N = coeff[v][0] + coeff[v][1] * xg + coeff[v][2] * yg;
						N /= 2.0 * element[ee].new_volume;
						hg += N * node[element[ee].node[v]].P[2][0];
						bg += N * node[element[ee].node[v]].bed[2];
					}
					sprintf(series_file_name, "../output/%s_gauge%d.dat", out_file, g_vect[g]);
					timeseriesg[0] = fopen(series_file_name, "a");
					fprintf(timeseriesg[0], "%le %le %le\n", Time, hg, bg);
					fclose(timeseriesg[0]);
				}

				// Sampling every 0.1/0.05 seconds for  Briggs
				double sample_time;
				int samples_num = 300;
				for (int n = 1; n <= samples_num; n++) {
					sample_time = n * output_stepsize;
					if (fabs(Time - sample_time) < 1.e-9)
						write_solution();
				}
			}

			/* sampling for small perturbation on lake at rest */
			if (initial_state == 4) {
				if (Time == 0.12)
					write_solution(); //1   
				if (Time == 0.24)
					write_solution(); //2
				if (Time == 0.36)
					write_solution(); //2    

			}

			/* sampling for double dam break */
			if (initial_state == 34) {
				double sample_time;
				int samples_num = 50;	//250 ;
				for (int n = 1; n <= samples_num; n++) {
					sample_time = n * output_stepsize;
					if (fabs(Time - sample_time) < 1.e-9)
						write_solution();
				}
			}


			/* Below: sampling for Run up [Carrier,2003] */
			if (initial_state == 23) {
				double dry_node_loc, shore_loc = -100000.;
				// Results specification: snap shots of water-surface and velocity profiles
				if (Time == 160.)
					write_solution();	//1   
				if (Time == 175.)
					write_solution();	//2
				if (Time == 220.)
					write_solution();	//3 

				// Computing shoreline location
				for (int n = 0; n < NN; n++) {
					if (node[n].P[2][0] < 0.1) {
						dry_node_loc = node[n].coordinate[1];
						if (dry_node_loc > shore_loc)
							shore_loc = dry_node_loc;
					}
				}

				// Results specification: temporal variations of the shoreline location 
				sprintf(series_file_name, "../output/%s_shoreline.dat", out_file);
				timeseriesg[0] = fopen(series_file_name, "a");
				fprintf(timeseriesg[0], "%le %le\n", Time, shore_loc);
				fclose(timeseriesg[0]);
			}

			if (initial_state == 15) { // Thacker
				if (fabs(Time - 2.22) < 1.e-6)
					write_solution();
				if (fabs(Time - 2.22 - 2.22 / 6.) < 1.e-6)
					write_solution();
				if (fabs(Time - 2.22 - 2.22 / 3.) < 1.e-6)
					write_solution();
				if (fabs(Time - 2.22 - 2.22 / 2.) < 1.e-6)
					write_solution();
				if (fabs(Time - 2.22 - 2.22 / 2. - 2.22 / 6.) < 1.e-6)
					write_solution();
				if (fabs(Time - 2.22 - 2.22 / 2. - 2.22 / 3.) < 1.e-6)
					write_solution();
				if (fabs(Time - 2.22 - 2.22) < 1.e-6)
					write_solution();
			}

			/* sampling for Convergent Channel test case */
			if ( (71 == initial_state) || (85 == initial_state) ) { 
				double tot_diss[4];

				// compute maximum gradient for water height along x-axis
				if ( Time > 4.0*3.14 )
					max_grad_rec() ;

				// compute dissipation estimates
				dissipation_estimate( tot_diss ) ;

				int DIM = 0;
				if (71 == initial_state)
					DIM = 11;
				else if (85 == initial_state)
					DIM = 10;

				for (int l = 0; l < DIM ; l++) {
					// Gauge[l] = loop iterator * 0.3 * Lb
					sprintf( series_file_name, "../output/%s_gauge%d.dat", out_file, l+1 ) ;
					timeseriesg[l] = fopen( series_file_name, "a" ) ;
					// Save quantities: t' h' u'				
					fprintf( timeseriesg[l], "%le %le %le\n", Time, node[nn[l]].P[2][0], node[nn[l]].Z[2][1]);
					fclose( timeseriesg[l] );
				}
			}
			if (initial_state == 72 || initial_state == 73 || initial_state == 74) {
                                double residual = 0.0;
                                for (int n = 0; n < NN; n++)
//                                      for (int i = 0; i < 3; i++)
                                                residual += fabs(node[n].Res[0]) * 1.0 / NN;
                                printf("residual %le\n",residual);
                                // Save quantities: t norm
                                sprintf( series_file_name, "../output/%s_steady_norm.dat", out_file ) ;
                                timeseriesg[0] = fopen( series_file_name, "a" ) ;
                                fprintf( timeseriesg[0], "%le %le\n", Time, residual );
                                fclose( timeseriesg[0] );
                        }
			if (initial_state == 76) { //synolakis-shoaling test set up 
				for (int l = 0; l < 10 ; l++) {
					sprintf( series_file_name, "../output/%s_gauge%d.dat", out_file, l+1 ) ;
					timeseriesg[l] = fopen( series_file_name, "a" ) ;
					// Save quantities: t  h  u				
					fprintf( timeseriesg[l], "%le %le\n", Time, node[nn[l]].P[2][0]);
					fclose( timeseriesg[l] );
				}
			}
			if (initial_state == 77) { //synolakis-shoaling test set up 
				for (int l = 0; l < 14 ; l++) {
					sprintf( series_file_name, "../output/%s_gauge%d.dat", out_file, l+1 ) ;
					timeseriesg[l] = fopen( series_file_name, "a" ) ;
					// Save quantities: t  h  u				
					fprintf( timeseriesg[l], "%le %le\n", Time, node[nn[l]].P[2][0]);
					fclose( timeseriesg[l] );
				}
			}
			if (initial_state == 78) { //wave over a bar
				for (int l = 0; l < 21 ; l++) {
					sprintf( series_file_name, "../output/%s_gauge%d.dat", out_file, l+1 ) ;
					timeseriesg[l] = fopen( series_file_name, "a" ) ;
					// Save quantities: t  h  u				
					fprintf( timeseriesg[l], "%le %le %le\n", Time, node[nn[l]].P[2][0], node[nn[l]].Z[2][1]);
					fclose( timeseriesg[l] );
				}
			}
                        if (initial_state == 79) { //gironde-garonne-dordogne
                                for (int l = 0; l < 9 ; l++) {
                                        sprintf( series_file_name, "../output/%s_gauge%d.dat", out_file, l+1 ) ;
                                        timeseriesg[l] = fopen( series_file_name, "a" ) ;
                                        // Save quantities: t h eta u v                                 
                                        fprintf( timeseriesg[l], "%le %le %le %le %le\n", Time, node[nn[l]].P[2][0], node[nn[l]].P[2][0] + node[nn[l]].bed[2], node[nn[l]].Z[2][1], node[nn[l]].Z[2][2]);
                                        fclose( timeseriesg[l] );
                                }
                        }
			if (initial_state == 80) { //synolakis-shoaling test set up 
				for (int l = 0; l < 6 ; l++) {
					sprintf( series_file_name, "../output/%s_gauge%d.dat", out_file, l+1 ) ;
					timeseriesg[l] = fopen( series_file_name, "a" ) ;
					// Save quantities: t  h  u				
					fprintf( timeseriesg[l], "%le %le\n", Time, node[nn[l]].P[2][0]);
					fclose( timeseriesg[l] );
				}
			}
			if (initial_state == 84) { //Oregon seastate
                                if( time_step % 20 ==0) {
					sprintf( series_file_name, "../output/Exp_gauges.dat") ;
					timeseries = fopen( series_file_name, "a" ) ;
                                	for(int i=0; i<4; i++) {
			        		fprintf( timeseries, "%le ", Time);
						for (int l = 0; l < 35  ; l++) {
							if (i==0)  //Save for h
								fprintf( timeseries, "%le ",  node[nn[l]].P[2][0]); 
							else if(i==1)  //Save for u
								fprintf( timeseries, "%le ",  node[nn[l]].Z[2][1]); 
							else if(i==2)  //Save for v
								fprintf( timeseries, "%le ",  node[nn[l]].Z[2][2]); 
							else if(i==3)  //Save for hU^2
								fprintf( timeseries, "%le ",  node[nn[l]].P[2][0]*sqrt(node[nn[l]].Z[2][1]*node[nn[l]].Z[2][1] + node[nn[l]].Z[2][2]*node[nn[l]].Z[2][2]));

						}
                                        	fprintf( timeseries, "\n"); 
					}
					fclose( timeseries );

					sprintf( series_file_name, "../output/Building_gauges.dat") ;
					timeseries = fopen( series_file_name, "a" ) ;
                                	for(int i=0; i<4; i++) {
			        		fprintf( timeseries, "%le ", Time); //51.425); //16.3348); //15.2645
						for (int l = 35; l < 47  ; l++) {
							if (i==0)  //Save for h
								fprintf( timeseries, "%le ",  node[nn[l]].P[2][0]); 
							else if(i==1)  //Save for u
								fprintf( timeseries, "%le ",  node[nn[l]].Z[2][1]); 
							else if(i==2)  //Save for v
								fprintf( timeseries, "%le ",  node[nn[l]].Z[2][2]); 
							else if(i==3)  //Save for hU^2
								fprintf( timeseries, "%le ",  node[nn[l]].P[2][0]*sqrt(node[nn[l]].Z[2][1]*node[nn[l]].Z[2][1] + node[nn[l]].Z[2][2]*node[nn[l]].Z[2][2]));

						}
                                        	fprintf( timeseries, "\n"); 
					}
					fclose( timeseries );
				}
			}
			if(initial_state==76){ // synolakis non-breaking case
/*                                if (Time == 20.*sqrt(1.0/gm))
                                        write_solution(); //1   
                                if (Time == 26.*sqrt(1.0/gm))
                                        write_solution(); //2
				if (Time == 32.*sqrt(1.0/gm))
                                        write_solution(); //3    
				if (Time == 38.*sqrt(1.0/gm))
                                        write_solution(); //4    
				if (Time == 44.*sqrt(1.0/gm))
                                        write_solution(); //5    
				if (Time == 50.*sqrt(1.0/gm))
                                        write_solution(); //6    
				if (Time == 56.*sqrt(1.0/gm))
                                        write_solution(); //7    
				if (Time == 62.*sqrt(1.0/gm))
                                        write_solution(); //8    
*/
				//synolakis breaking case
                                if (Time == 10.*sqrt(1.0/gm))
                                        write_solution(); //1   
                                if (Time == 15.*sqrt(1.0/gm))
                                        write_solution(); //2
				if (Time == 20.*sqrt(1.0/gm))
                                        write_solution(); //3    
				if (Time == 25.*sqrt(1.0/gm))
                                        write_solution(); //4    
				if (Time == 30.*sqrt(1.0/gm))
                                        write_solution(); //5    
				if (Time == 45.*sqrt(1.0/gm))
                                        write_solution(); //6    
				if (Time == 55.*sqrt(1.0/gm))
                                        write_solution(); //7    
				if (Time == 60.*sqrt(1.0/gm))
                                        write_solution(); //8    
				if (Time == 70.*sqrt(1.0/gm))
                                        write_solution(); //9    
				if (Time == 80.*sqrt(1.0/gm))
                                        write_solution(); //10    

                        }
                        if(initial_state==77){ // reef test case 
                                if (Time == 4.0436)
                                        write_solution(); //1   
                                if (Time == 5.053)
                                        write_solution(); //2
                                if (Time == 6.815)
                                        write_solution(); //3    
                                if (Time == 7.82)
                                        write_solution(); //4    
                                if (Time == 8.94)
                                        write_solution(); //5    
                                if (Time == 10.91)
                                        write_solution(); //6    
                                if (Time == 13.02)
                                        write_solution(); //7    
                                if (Time == 21.8082)
                                        write_solution(); //8    
                                if (Time == 28.779)
                                        write_solution(); //9    
                                if (Time == 31.6)
                                        write_solution(); //10    
                                if (Time == 32.96)
                                        write_solution(); //11    
                                if (Time == 48.96)
                                        write_solution(); //12    
                        }

			if(initial_state == 90){
				double sample_time;
				int samples_num = 50;
				for (int n = 1; n <= samples_num; n++) {
					sample_time = n * output_stepsize;
					if (fabs(Time - sample_time) < 1.e-9){
						write_solution();
						//for (int l=16820; l<=19617; l++){//for dx=0.1 // in each output_stepsize print the solution in the centerline 
						for (int l=26010; l<=33610; l++){ // for dx=0.05 //in each output_stepsize print the solution in the centerline 
						//for (int l=26010; l<=44800; l++){ // for dx=0.025 //in each output_stepsize print the solution in the centerline 
                                        		sprintf( series_file_name, "../output/%s_center%d.dat", out_file, n ) ;
                                       			timeseries = fopen( series_file_name, "a" ) ;
                                        		// Save quantities: t h eta u v                                 
                                      			fprintf( timeseries, "%le %le %le %le %le\n",  node[l].coordinate[0], node[l].P[2][0], node[l].P[2][0] + node[l].bed[2], node[l].Z[2][1], node[l].Z[2][2]);
                                      			fclose( timeseries );
						}
					}
				// Sampling every 1.0 seconds for solitary_shelf1d
				}
			}
			if(initial_state == 88){ //Hansen and Svendsen
				int k=1;
				for (int l=10815; l<=12255; l=l+4){ //dx=0.025 //in each output_stepsize print the solution in the centerline 
                                        sprintf( series_file_name, "../output/%s_center%d.dat", out_file, k ) ;
					k=k+1;
                                       	timeseries = fopen( series_file_name, "a" ) ;
                                        // Save quantities: t h eta u v                                 
                                      	fprintf( timeseries, "%le %le %le %le %le %le\n",  node[l].coordinate[0], node[l].P[2][0], node[l].P[2][0] + node[l].bed[2], node[l].Z[2][1], node[l].Z[2][2], Time);
                                      	fclose( timeseries );
				}
				
			}
			if(initial_state == 89){ //Grilli shoaling of solitary wave
				for (int l = 0; l < 10 ; l++) {
					sprintf( series_file_name, "../output/%s_gauge%d.dat", out_file, l+1 ) ;
					timeseriesg[l] = fopen( series_file_name, "a" ) ;
					// Save quantities: t  h  u				
					fprintf( timeseriesg[l], "%le %le %le %le\n", Time, node[nn[l]].P[2][0], node[nn[l]].P[2][0] + node[nn[l]].bed[2], node[nn[l]].bed[2]);
					fclose( timeseriesg[l] );
				}
				for(int n = 0; n < NN; n++)
					if ( ( n>=3441 ) && ( n<=4583) ) {
						double eta = node[n].P[2][0] + node[n].bed[2];
						if ( eta > envelope[n-3441] )
							envelope[n-3441] = eta;					
					}
				
			}

			if (75 == initial_state || 81 == initial_state || 82 == initial_state || 93 == initial_state)
				sample_probes();

			/* Below: check mass conservation */
			int_boundary_flux += boundary_flux;
			sprintf(series_file_name, "../output/%s_mass_cons.dat", out_file);
			timeseriesg[0] = fopen(series_file_name, "a");
			fprintf(timeseriesg[0], "  Time\t Mass Integral \t Bathymetry Integral \t Time integration of Boundary Flux\n");
			fprintf(timeseriesg[0], "%f  %8.8f   %8.8f   %8.8f\n", Time, int_H_new, int_B_new, int_boundary_flux);
			fclose(timeseriesg[0]);

			//if ( info ) {
			sprintf(series_file_name, "../output/%s_check_mass.dat", out_file);
			timeseriesg[0] = fopen(series_file_name, "a");
			fprintf(timeseriesg[0], "%f %8.8f %8.8f %8.8f %8.8f\n",
			                         Time, int_H_new - int_H_old - boundary_flux,
	 			                     int_B_new - int_B_old, wb_mass_loss, remap_mass_loss);
			fclose(timeseriesg[0]);
			//}

			if (output_steps > output_counter) {
				sprintf(out_file, "%s_timestep%d", out_file_save, output_counter);
				write_solution();
				output_counter++;
				t_max += output_stepsize;
			}
		}
		if (output_steps > 0) {
			sprintf(out_file, "%s_timestep%d", out_file_save, output_counter);
			write_solution();
		}
	}

	printf("Final Norm: %le\n\n", steady_norm);
	if (was_negative)
		printf("Negative water height during computation\n\n");

	if ( (71 == initial_state) || (85 == initial_state) ) {
	   sprintf( series_file_name, "../output/%s_max_grad.dat", out_file ) ;
	   timeseriesg[0] = fopen( series_file_name, "a" ) ;
	   for ( int n = 0; n < NNX; n++ )
	    	fprintf( timeseriesg[0], "%le %le\n", 
 	         	node[n+4].coordinate[0], max_grad[n]  ) ;
	   fclose( timeseriesg[0] ) ;
	}
	if ( 89 == initial_state ) {
	   sprintf( series_file_name, "../output/%s_envelope.dat", out_file ) ;
	   timeseriesg[0] = fopen( series_file_name, "a" ) ;
	   for ( int n = 0; n < NN; n++ )
		   if ( ( n>=3441 ) && ( n<=4583) )
			fprintf( timeseriesg[0], "%le %le\n", node[n].coordinate[0], envelope[n-3441] );
	   fclose( timeseriesg[0] ) ;
		
	}
}
