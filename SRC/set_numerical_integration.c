/*  Copyright (C) 2021   M.Ricchiuto  <mario.ricchiuto@inria.fr>
                         M. Kazolea   <maria.kazolea@inria.fr>
                         A. Filippini <a.filippini@brgm.fr>
                         L. Arpaia    <luca.arpaia@brgm.fr>
                         N.Pattakos   <pattakosn@fastmail.com> 

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.*/


/***************************************************************************
                         set_numerical_integration.c
                         ---------------------------
This is set_numerical_integration: the weights for the numerical evaluation
           of line, surface and volume integrals are set here
 ***************************************************************************/
#include <math.h>
#include <assert.h>
#include "common.h"

double b1[3], b2[3], b3[3];
extern double a1[3], a2[3], a3[3], ivm[3][3];
extern const int volume_q_pts;
extern int time_discretization, iteration_max;
extern struct numerical_int_struct numerical_int;


void set_RD_numerical_integration()
{
    double p0, p1, s ;

     s = 1./sqrt( 3. ) ;

     p0 = ( 1.0 - s )*0.5 ;
     p1 = ( 1.0 + s )*0.5 ;

     numerical_int.face_coordinate[0][0] = p0 ;
     numerical_int.face_coordinate[0][1] = p1 ;

     numerical_int.face_weight[0] = 0.5 ;


     s = -1./sqrt( 3 ) ;

     p0 = ( 1.0 - s )*0.5 ;
     p1 = ( 1.0 + s )*0.5 ;


     numerical_int.face_coordinate[1][0] = p0 ;
     numerical_int.face_coordinate[1][1] = p1 ;

     numerical_int.face_weight[1] = 0.5 ;


    numerical_int.volume_coordinate[0][0] = 0.816847572980459 ;
     numerical_int.volume_coordinate[0][1] = 0.091576213509771 ;
     numerical_int.volume_coordinate[0][2] = 0.091576213509771 ;

     numerical_int.volume_weight[0] = 0.109951743655322 ;

     numerical_int.volume_coordinate[1][1] = 0.816847572980459 ;
     numerical_int.volume_coordinate[1][0] = 0.091576213509771 ;
     numerical_int.volume_coordinate[1][2] = 0.091576213509771 ;

     numerical_int.volume_weight[1] = 0.109951743655322 ;

     numerical_int.volume_coordinate[2][2] = 0.816847572980459 ;
     numerical_int.volume_coordinate[2][1] = 0.091576213509771 ;
     numerical_int.volume_coordinate[2][0] = 0.091576213509771 ;

     numerical_int.volume_weight[2] = 0.109951743655322 ;


     numerical_int.volume_coordinate[3][0] = 0.108103018168070 ;
     numerical_int.volume_coordinate[3][1] = 0.445948490915965 ;
     numerical_int.volume_coordinate[3][2] = 0.445948490915965 ;

     numerical_int.volume_weight[3] = 0.223381589678011 ;

     numerical_int.volume_coordinate[4][1] = 0.108103018168070 ;
     numerical_int.volume_coordinate[4][0] = 0.445948490915965 ;
     numerical_int.volume_coordinate[4][2] = 0.445948490915965 ;

     numerical_int.volume_weight[4] = 0.223381589678011 ;

     numerical_int.volume_coordinate[5][2] = 0.108103018168070 ;
     numerical_int.volume_coordinate[5][0] = 0.445948490915965 ;
     numerical_int.volume_coordinate[5][1] = 0.445948490915965 ;

     numerical_int.volume_weight[5] = 0.223381589678011 ;

//
// Initializing the Runge-Kutta weights
//

     switch ( time_discretization )
               {
                 case 0:
                        iteration_max = 1 ;
                 
                        a1[0] = 0. ;
                        a1[1] = 0. ;
                        a1[2] = 0. ;

                        a2[0] = 0. ;
                        a2[1] = 0. ;
                        a2[2] = 0. ;

                        a3[0] = 1. ;
                        a3[1] = 0. ;
                        a3[2] = 0. ;

                        b1[0] = 0. ;
                        b1[1] = 0. ;
                        b1[2] = 0. ;

                        b2[0] = 0. ;
                        b2[1] = 0. ;
                        b2[2] = 0. ;

                        b3[0] = 0. ;
                        b3[1] = 0. ;
                        b3[2] = 0. ;
                        break ;
                  case 1:
                        iteration_max = 2 ;
                        
                        a1[0] = 0. ;
                        a1[1] = 0.5 ;
                        a1[2] = 0. ;

                        a2[0] = 0. ;
                        a2[1] = 0. ;
                        a2[2] = 0. ;

                        a3[0] = 1. ;
                        a3[1] = 0.5 ;
                        a3[2] = 0. ;

                        b1[0] = 0. ;
                        b1[1] = -1. ;
                        b1[2] = 0. ;

                        b2[0] = 0. ;
                        b2[1] = 0. ;
                        b2[2] = 0. ;

                        b3[0] = 0. ;
                        b3[1] = 1. ;
                        b3[2] = 0. ;
                        break ;
               }
    
}

void set_FV_numerical_integration()
{
    double p0, p1, s ;

     s = 1./sqrt( 3. ) ;

     p0 = ( 1.0 - s )*0.5 ;
     p1 = ( 1.0 + s )*0.5 ;

     numerical_int.face_coordinate[0][0] = p0 ;
     numerical_int.face_coordinate[0][1] = p1 ;

     numerical_int.face_weight[0] = 0.5 ;


     s = -1./sqrt( 3 ) ;

     p0 = ( 1.0 - s )*0.5 ;
     p1 = ( 1.0 + s )*0.5 ;


     numerical_int.face_coordinate[1][0] = p0 ;
     numerical_int.face_coordinate[1][1] = p1 ;

     numerical_int.face_weight[1] = 0.5 ;


    numerical_int.volume_coordinate[0][0] = 0.816847572980459 ;
     numerical_int.volume_coordinate[0][1] = 0.091576213509771 ;
     numerical_int.volume_coordinate[0][2] = 0.091576213509771 ;

     numerical_int.volume_weight[0] = 0.109951743655322 ;

     numerical_int.volume_coordinate[1][1] = 0.816847572980459 ;
     numerical_int.volume_coordinate[1][0] = 0.091576213509771 ;
     numerical_int.volume_coordinate[1][2] = 0.091576213509771 ;

     numerical_int.volume_weight[1] = 0.109951743655322 ;

     numerical_int.volume_coordinate[2][2] = 0.816847572980459 ;
     numerical_int.volume_coordinate[2][1] = 0.091576213509771 ;
     numerical_int.volume_coordinate[2][0] = 0.091576213509771 ;

     numerical_int.volume_weight[2] = 0.109951743655322 ;


     numerical_int.volume_coordinate[3][0] = 0.108103018168070 ;
     numerical_int.volume_coordinate[3][1] = 0.445948490915965 ;
     numerical_int.volume_coordinate[3][2] = 0.445948490915965 ;

     numerical_int.volume_weight[3] = 0.223381589678011 ;

     numerical_int.volume_coordinate[4][1] = 0.108103018168070 ;
     numerical_int.volume_coordinate[4][0] = 0.445948490915965 ;
     numerical_int.volume_coordinate[4][2] = 0.445948490915965 ;

     numerical_int.volume_weight[4] = 0.223381589678011 ;

     numerical_int.volume_coordinate[5][2] = 0.108103018168070 ;
     numerical_int.volume_coordinate[5][0] = 0.445948490915965 ;
     numerical_int.volume_coordinate[5][1] = 0.445948490915965 ;

     numerical_int.volume_weight[5] = 0.223381589678011 ;

//
// Initializing the Runge-Kutta weights
//

     switch ( time_discretization )
               {
                 case 0:
                        iteration_max = 1 ;
                 
                        a1[0] = 0. ;
                        a1[1] = 0. ;
                        a1[2] = 0. ;

                        a2[0] = 0. ;
                        a2[1] = 0. ;
                        a2[2] = 0. ;

                        a3[0] = 1. ;
                        a3[1] = 0. ;
                        a3[2] = 0. ;

                        b1[0] = 0. ;
                        b1[1] = 0. ;
                        b1[2] = 0. ;

                        b2[0] = 0. ;
                        b2[1] = 0. ;
                        b2[2] = 0. ;

                        b3[0] = 0. ;
                        b3[1] = 0. ;
                        b3[2] = 0. ;
                        break ;
                  case 1:
                        iteration_max = 2 ;
                        
                        a1[0] = 0. ;
                        a1[1] = 0.5 ;
                        a1[2] = 0. ;

                        a2[0] = 0. ;
                        a2[1] = 0. ;
                        a2[2] = 0. ;

                        a3[0] = 1. ;
                        a3[1] = 0.5 ;
                        a3[2] = 0. ;

                        b1[0] = 0. ;
                        b1[1] = -1. ;
                        b1[2] = 0. ;

                        b2[0] = 0. ;
                        b2[1] = 0. ;
                        b2[2] = 0. ;

                        b3[0] = 0. ;
                        b3[1] = 1. ;
                        b3[2] = 0. ;
                        break ;
               }
    
}

