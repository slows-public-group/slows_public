/*  Copyright (C) 2021   M.Ricchiuto  <mario.ricchiuto@inria.fr>
                         M. Kazolea   <maria.kazolea@inria.fr>
                         A. Filippini <a.filippini@brgm.fr>
                         L. Arpaia    <luca.arpaia@brgm.fr>
                         N.Pattakos   <pattakosn@fastmail.com> 

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.*/


/***************************************************************************
                                  driver.c
                                   -----
   This is the driver: it starts the computation and drives it to the end.
                             -------------------
 ***************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <float.h>
#include <time.h>
#include "common.h"

extern int grad_reco,blending;
extern void (*write_solution) (void);
extern const int size;

void input_parameters(void);
void run(void);
void initialize(void);
void select_function(void);
void memory_allocation(void);
void geometry(void);
void initial_conditions(void);
void install_gauge(void);

int main(void)
{
	struct timespec time_s[3], time_f[3];


	printf("\n");
	printf("          *************************************\n");
	printf("          *************************************\n");
	printf("          **                                 **\n");
	printf("          **      Pre-processing phase...    **\n");
	printf("          **                                 **\n");
	printf("          *************************************\n");
	printf("          **                                 **\n");
	printf("          **    Getting ready to compute...  **\n");
	printf("          **                                 **\n");
	printf("          *************************************\n");
	printf("          **                                 **\n");
	printf("          **            Limits:              **\n");
	printf("          **                                 **\n");
	printf("          **       EPS: %le         **\n", DBL_EPSILON);
	printf("          **       MIN_DBL: %le    **\n", DBL_MIN);
	printf("          **                                 **\n");
	printf("          *************************************\n");
	printf("\n");

	clock_gettime(CLOCK_MONOTONIC, &time_s[1]);
	input_parameters();
	initialize();
	select_function();
	memory_allocation();
	geometry();
	initial_conditions();
	clock_gettime(CLOCK_MONOTONIC, &time_f[1]);




	clock_gettime(CLOCK_MONOTONIC, &time_s[2]);
	install_gauge();
	write_solution();
	run();
	write_solution();
	printf("\n");
	printf("          *************************************\n");
	printf("          *************************************\n");
	printf("          **                                 **\n");
	printf("          **      End of the computation     **\n");
	printf("          **                                 **\n");
	printf("          **      ..cross your fingers...    **\n");
	printf("          **                                 **\n");
	printf("          *************************************\n");
	printf("          *************************************\n");
	printf("\n");
	clock_gettime(CLOCK_MONOTONIC, &time_f[2]);

	printf("\n");
	printf("          *************************************\n");
	printf("          *  CPU Time info        :    [s]     \n");
	printf("          * ===================================\n");
#define ELAPSED(start,finish) ((finish.tv_sec - start.tv_sec) + (finish.tv_nsec - start.tv_nsec) / 1000000000.0)
//	printf("          *   Init.Sparse Matrix  | %f\n", time0);
//	printf("          *   Adaptation to IC    | %f\n", time1);
//	printf("          *   Computation         | %f\n", time2);
	printf("          *   Preprocessing       | %f\n", ELAPSED(time_s[1], time_f[1]) );
	printf("          *   Computation         | %f\n", ELAPSED(time_s[2], time_f[2]) );
	printf("          *  ---------------------|------------\n");
	printf("          *   Total 	          | %f\n", ELAPSED(time_s[1], time_f[1]) + ELAPSED(time_s[2], time_f[2]) );
#undef ELAPSED
	printf("          *************************************\n");
	printf("\n");

	return EXIT_SUCCESS;
}
