/*  Copyright (C) 2021   M.Ricchiuto  <mario.ricchiuto@inria.fr>
                         M. Kazolea   <maria.kazolea@inria.fr>
                         A. Filippini <a.filippini@brgm.fr>
                         L. Arpaia    <luca.arpaia@brgm.fr>
                         N.Pattakos   <pattakosn@fastmail.com> 

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.*/

/***************************************************************************
 boundary_preprocessing.c
 -----------------------
 This is boundary_preprocessing: the geometry of the boundary is computed and
 processed here to have all the necessary informations for the computation
 -------------------
 ***************************************************************************/
#include <stdio.h>
#include <math.h>
#include <string.h>
#include "common.h"

extern char grid_file[MAX_CHAR];
extern int NBF, initial_state;
extern struct node_struct *node;
extern struct boundary_struct *boundary;
extern struct boundary_nodes_struct *b_nodes;


/* Preprocessing for linear P1 interpolation */
void boundary_preprocessing(void)
{
	for (int f = 0; f < NBF; f++)
    {
		b_nodes[f].area    = 0.;
        b_nodes[f].blength = 0.0 ;
    }

	int m = 0;
	for (int f = 0; f < NBF; f++) {
		int n1 = boundary[f].node[0];
		b_nodes[m].node = n1; 
		int n2 = boundary[f].node[1];
		double x1 = node[n1].coordinate[0];
		double y1 = node[n1].coordinate[1];
		double x2 = node[n2].coordinate[0];
		double y2 = node[n2].coordinate[1];
		double nx1 = y1 - y2;
		double ny1 = x2 - x1;
		double length = sqrt(pow(x2 - x1, 2) + pow(y2 - y1, 2));
		if (3 != boundary[f].type)
			b_nodes[m].blength += 0.5 * length;

		int flag = 0;
		for (int g = 0; g < NBF; g++) {
			if (boundary[g].node[1] == n1){
				int m1 = boundary[g].node[0];
				int m2 = boundary[g].node[1];
				x1 = node[m1].coordinate[0];
				y1 = node[m1].coordinate[1];
				x2 = node[m2].coordinate[0];
				y2 = node[m2].coordinate[1];
				double nx2 = y1 - y2;
				double ny2 = x2 - x1;
				length = sqrt(pow(x2 - x1, 2) + pow(y2 - y1, 2));
				if (3 != boundary[g].type)
					b_nodes[m].blength += 0.5 * length;

				b_nodes[m].normal[0] = 0.5 * (nx1 + nx2);
				b_nodes[m].normal[1] = 0.5 * (ny1 + ny2);

				/*******************************************
				 **** ATTENTION: Default choice for types ***
				 *******************************************/
				b_nodes[m].type = boundary[f].types[0];

				flag +=1;
			}
		}
		if (flag==0)
			printf("ERROR in boundary preprocessing, boundary not close in node %d\n",n1);

		m += 1;
	}
	if (m != NBF) 
		printf("ERROR in the Boundary preprocessing !!!!!!!!");

	/* Boundary corrections: types, normals, volume */
	m = 0;
	for (int f = 0; f < NBF; f++) {
		int n1 = b_nodes[f].node;
		if ( (2 == initial_state) || (3 == initial_state) ) {
			if (n1 == 204 || n1 == 199) {
				b_nodes[f].normal[0] = b_nodes[200].normal[0];
				b_nodes[f].normal[1] = b_nodes[200].normal[1];
			}
			if (fabs(node[n1].coordinate[0]) < 1.e-6)
				b_nodes[f].type = 2;
			if (fabs(25. - node[n1].coordinate[0]) < 1.e-6)
				b_nodes[f].type = 2;
		} else if (4 == initial_state) {
			if (n1 == 0) {
				int m1 = boundary[NBF - 1].node[0];
				int m2 = boundary[NBF - 1].node[1];
				double x1 = node[m1].coordinate[0];
				double y1 = node[m1].coordinate[1];
				double x2 = node[m2].coordinate[0];
				double y2 = node[m2].coordinate[1];
				double nx2 = y1 - y2;
				double ny2 = x2 - x1;

				b_nodes[m].normal[0] = nx2 * 0.5;
				b_nodes[m].normal[1] = ny2 * 0.5;
				b_nodes[m].type = boundary[NBF - 1].types[0];
			} else if (n1 == 1) {
				int m1 = boundary[f].node[0];
				int m2 = boundary[f].node[1];
				double x1 = node[m1].coordinate[0];
				double y1 = node[m1].coordinate[1];
				double x2 = node[m2].coordinate[0];
				double y2 = node[m2].coordinate[1];
				double nx1 = y1 - y2;
				double ny1 = x2 - x1;

				b_nodes[m].normal[0] = nx1 * 0.5;
				b_nodes[m].normal[1] = ny1 * 0.5;
				b_nodes[m].type = boundary[f].types[0];
			} else if (n1 == 2) {
				int m1 = boundary[f - 1].node[0];
				int m2 = boundary[f - 1].node[1];
				double x1 = node[m1].coordinate[0];
				double y1 = node[m1].coordinate[1];
				double x2 = node[m2].coordinate[0];
				double y2 = node[m2].coordinate[1];
				double nx2 = y1 - y2;
				double ny2 = x2 - x1;

				b_nodes[m].normal[0] = nx2 * 0.5;
				b_nodes[m].normal[1] = ny2 * 0.5;
				b_nodes[m].type = boundary[f - 1].types[0];
			} else if (n1 == 3) {
				int m1 = boundary[f].node[0];
				int m2 = boundary[f].node[1];
				double x1 = node[m1].coordinate[0];
				double y1 = node[m1].coordinate[1];
				double x2 = node[m2].coordinate[0];
				double y2 = node[m2].coordinate[1];
				double nx1 = y1 - y2;
				double ny1 = x2 - x1;

				b_nodes[m].normal[0] = nx1 * 0.5;
				b_nodes[m].normal[1] = ny1 * 0.5;
				b_nodes[m].type = boundary[f].types[0];
			}
		} else if ( (7 == initial_state) || (34 == initial_state) || (75 == initial_state) || (77 == initial_state) || (78 == initial_state) || (76 == initial_state) ) {
			// asymmetric dambreak, double dambreak, dambreak with bathimetry, rip_channel, synolakis, volker_reef, wave over a bar

			int n1 = boundary[f].node[0];
			int n2 = boundary[f].node[1];
			double x1 = node[n1].coordinate[0];
			double y1 = node[n1].coordinate[1];
			double x2 = node[n2].coordinate[0];
			double y2 = node[n2].coordinate[1];
			double nx1 = y1 - y2;
			double ny1 = x2 - x1;
			if (f == 0) {
				b_nodes[m].node = n1;
				b_nodes[m].mate = node[n1].mate;
				int m1 = boundary[NBF - 1].node[0];
				int m2 = boundary[NBF - 1].node[1];
				x1 = node[m1].coordinate[0];
				y1 = node[m1].coordinate[1];
				x2 = node[m2].coordinate[0];
				y2 = node[m2].coordinate[1];
				double nx2 = y1 - y2;
				double ny2 = x2 - x1;
				b_nodes[m].normal[0] = 0.5 * (nx1 + nx2);
				b_nodes[m].normal[1] = 0.5 * (ny1 + ny2);
			} else {
				b_nodes[m].node = n1;
				b_nodes[m].mate = node[n1].mate;
				int m1 = boundary[f - 1].node[0];
				int m2 = boundary[f - 1].node[1];
				x1 = node[m1].coordinate[0];
				y1 = node[m1].coordinate[1];
				x2 = node[m2].coordinate[0];
				y2 = node[m2].coordinate[1];
				double nx2 = y1 - y2;
				double ny2 = x2 - x1;
				b_nodes[m].normal[0] = 0.5 * (nx1 + nx2);
				b_nodes[m].normal[1] = 0.5 * (ny1 + ny2);
			}
		} else if (13 == initial_state ) {
			if (   !strcmp(grid_file, "vort-1") || !strcmp(grid_file, "vort-2")
				|| !strcmp(grid_file, "vort-3") || !strcmp(grid_file, "vort-4") ) {
				if (n1 == 0 || n1 == 1)
					b_nodes[f].type = 10;
				if (n1 == 2 || n1 == 3)
					b_nodes[f].type = 10;
			} else {
/*				if (n1 == 0)
					b_nodes[f].type = 11;
				if (n1 == 3)
					b_nodes[f].type = 11;
				if (n1 == 1) {
					b_nodes[f].normal[0] = 0.0;
					b_nodes[f].normal[1] = 1.0;
				}
				if (n1 == 2) {
					b_nodes[f].normal[0] = 0.0;
					b_nodes[f].normal[1] = -1.0;
				}
*/
				if ((node[n1].coordinate[0] == 0. && node[n1].coordinate[1] == 0.) ||
					(node[n1].coordinate[0] == 1. && node[n1].coordinate[1] == 0.)){
					b_nodes[f].type = 10;
					b_nodes[f].normal[0] = 0.0;
					b_nodes[f].normal[1] = 1.0;
					printf("%d\n",n1);
				}
				if ((node[n1].coordinate[0] == 0. && node[n1].coordinate[1] == 1.) ||
					(node[n1].coordinate[0] == 1. && node[n1].coordinate[1] == 1.)){
					b_nodes[f].type = 10;
					b_nodes[f].normal[0] = 0.0;
					b_nodes[f].normal[1] =- 1.0;
					printf("%d\n",n1);
				}
			}
		} else if ( ( 71 == initial_state ) || ( 85 == initial_state ) ) { // Convergent channel & Convergent channel zoom
			double B0 = 0.1, L_b = 1.;
			double x_start = 0.;
			double x_end = 0.;
			if ( 71 == initial_state ) // Convergent channel [0, 6]*Lb
				x_end = 6.;
			else if ( 85 == initial_state ){ // Convergent channel zoom [0.9, 1.8]*Lb
				x_start = 0.9;
				x_end = 1.8;
			}
			
			if ( ( fabs(node[n1].coordinate[0] - x_start) < 0.000001 )  && ( fabs(node[n1].coordinate[1] - 0.) < 0.000001 ) ) {
				int n1 = boundary[NBF - 1].node[0] ;
				int n2 = boundary[NBF - 1].node[1] ;
				double x1 = node[n1].coordinate[0] ;
				double y1 = node[n1].coordinate[1] ;
				double x2 = node[n2].coordinate[0] ;
				double y2 = node[n2].coordinate[1] ;
				double nx1 = y1 - y2 ;
				double ny1 = x2 - x1 ;
				//double nx1 = y2 - y1;
				//double ny1 = x1 - x2;

				b_nodes[f].normal[0] = nx1/2. ;
				b_nodes[f].normal[1] = ny1/2. ;
				b_nodes[f].type = 11;
			}

			if ( ( fabs(node[n1].coordinate[0] - x_start) < 0.000001 )  && ( fabs(node[n1].coordinate[1] - B0/L_b*exp(-x_start) ) < 0.000001 ) ) { 
				int n1 = boundary[f].node[0] ;
				int n2 = boundary[f].node[1] ;
				double x1 = node[n1].coordinate[0] ;
				double y1 = node[n1].coordinate[1] ;
				double x2 = node[n2].coordinate[0] ;
				double y2 = node[n2].coordinate[1] ;
				double nx1 = y1 - y2 ;
				double ny1 = x2 - x1 ;
				//double nx1 = y2 - y1;
				//double ny1 = x1 - x2;

				b_nodes[f].normal[0] = nx1/2. ;
				b_nodes[f].normal[1] = ny1/2. ;
				b_nodes[f].type = 11;
			}

			if ( ( fabs(node[n1].coordinate[0] - x_end) < 0.000001 )  && ( fabs(node[n1].coordinate[1] - 0.) < 0.000001 ) ) {
				int n1 = boundary[f].node[0] ;
				int n2 = boundary[f].node[1] ;
				double x1 = node[n1].coordinate[0] ;
				double y1 = node[n1].coordinate[1] ;
				double x2 = node[n2].coordinate[0] ;
				double y2 = node[n2].coordinate[1] ;
				double nx1 = y1 - y2 ;
				double ny1 = x2 - x1 ;
				//double nx1 = y2 - y1;
				//double ny1 = x1 - x2;

				b_nodes[f].normal[0] = nx1/2. ;
				b_nodes[f].normal[1] = ny1/2. ;
				b_nodes[f].type = 11;
			}   

			if ( ( fabs(node[n1].coordinate[0] - x_end) < 0.000001 ) && ( fabs(node[n1].coordinate[1] - B0/L_b*exp(-x_end)) < 0.000001 ) ) { 
				int n1 = boundary[f-1].node[0] ;
				int n2 = boundary[f-1].node[1] ;
				double x1 = node[n1].coordinate[0] ;
				double y1 = node[n1].coordinate[1] ;
				double x2 = node[n2].coordinate[0] ;
				double y2 = node[n2].coordinate[1] ;
				double nx1 = y1 - y2 ;
				double ny1 = x2 - x1 ;
				//double nx1 = y2 - y1;
				//double ny1 = x1 - x2;

				b_nodes[f].normal[0] = nx1/2. ;
				b_nodes[f].normal[1] = ny1/2. ;
				b_nodes[f].type = 11;
			}
		}
		m += 1;
	}
	
	for (int f = 0; f < NBF; f++) {
		int n1 = b_nodes[f].node;
		if (72 == initial_state) { // gironde
			if (   ((fabs( node[n1].coordinate[0] - 3.386597e+05) < 0.000001)  && (fabs(node[n1].coordinate[1] - 3.723562e+05) < 0.000001)) // node 595
				|| ((fabs( node[n1].coordinate[0] - 3.725900e+05) < 0.000001)  && (fabs(node[n1].coordinate[1] - 3.017240e+05) < 0.000001))  // node 26488
				|| ((fabs( node[n1].coordinate[0] - 3.736990e+05) < 0.000001)  && (fabs(node[n1].coordinate[1] - 3.059840e+05) < 0.000001)) ) {  // node 26754
				//printf("%d   \n",n1); 
				int n1 = boundary[f-1].node[0] ;
				int n2 = boundary[f-1].node[1] ;
				double x1 = node[n1].coordinate[0] ;
				double y1 = node[n1].coordinate[1] ;
				double x2 = node[n2].coordinate[0] ;
				double y2 = node[n2].coordinate[1] ;
				double nx1 = x1 - x2 ;
				double ny1 = y1 - y2 ;

				b_nodes[f].normal[0] = nx1/2. ;
				b_nodes[f].normal[1] = ny1/2. ;
				b_nodes[f].type = 11;
				//printf("%d %d %le %le\n",n1,n2,b_nodes[f].normal[0],b_nodes[f].normal[1]);
			}
			if ( ((fabs( node[n1].coordinate[0] - 3.347489e+05) < 0.000001)  && (fabs(node[n1].coordinate[1] - 3.689337e+05) < 0.000001)) // node 36
				|| ((fabs( node[n1].coordinate[0] - 3.734110e+05) < 0.000001)  && (fabs(node[n1].coordinate[1] - 3.017930e+05) < 0.000001))  // node 26714
				|| ((fabs( node[n1].coordinate[0] - 3.744460e+05) < 0.000001)  && (fabs(node[n1].coordinate[1] - 3.066120e+05) < 0.000001)) ) { // node 26816
				//printf("%d   \n",n1); 
				int n1 = boundary[f].node[0] ;
				int n2 = boundary[f].node[1] ;
				double x1 = node[n1].coordinate[0] ;
				double y1 = node[n1].coordinate[1] ;
				double x2 = node[n2].coordinate[0] ;
				double y2 = node[n2].coordinate[1] ;
				double nx1 = x2 - x1 ;
				double ny1 = y2 - y1 ;
				
				b_nodes[f].normal[0] = nx1/2. ;
				b_nodes[f].normal[1] = ny1/2. ;
				b_nodes[f].type = 11;
				//printf("%d %d %le %le\n",n1,n2,b_nodes[f].normal[0],b_nodes[f].normal[1]);
			}
		} else if (73 == initial_state) { // garonne
                        if (   ((fabs( node[n1].coordinate[0] - 3.734110e+05) < 0.000001)  && (fabs(node[n1].coordinate[1] - 3.017930e+05) < 0.000001)) // node 222
                                || ((fabs( node[n1].coordinate[0] - 4.116212e+05) < 0.000001)  && (fabs(node[n1].coordinate[1] - 2.558658e+05) < 0.000001)) ) {  // node 3050
                                int n1 = boundary[f].node[0] ;
                                int n2 = boundary[f].node[1] ;
                                double x1 = node[n1].coordinate[0] ;
                                double y1 = node[n1].coordinate[1] ;
                                double x2 = node[n2].coordinate[0] ;
                                double y2 = node[n2].coordinate[1] ;
                                double nx1 = y1 - y2 ;
                                double ny1 = x2 - x1 ;

                                b_nodes[f].normal[0] = nx1/2. ;
                                b_nodes[f].normal[1] = ny1/2. ;
                                b_nodes[f].type = 11;
//                              printf("%d %d %le %le %d %le %le\n",n1,n2,b_nodes[f].normal[0],b_nodes[f].normal[1],b_nodes[f+1].node,b_nodes[f+1].normal[0],b_nodes[f+1].normal[1]);
                        }

                        if ( ((fabs( node[n1].coordinate[0] - 3.725900e+05) < 0.000001)  && (fabs(node[n1].coordinate[1] - 3.017240e+05) < 0.000001)) // node 0
                                || ((fabs( node[n1].coordinate[0] - 4.116213e+05) < 0.000001)  && (fabs(node[n1].coordinate[1] - 2.559926e+05) < 0.000001)) ) { // node 9062
                                int n1 = boundary[f-1].node[0];
                                int n2 = boundary[f-1].node[1];
                                double x1 = node[n1].coordinate[0];
                                double y1 = node[n1].coordinate[1];
                                double x2 = node[n2].coordinate[0];
                                double y2 = node[n2].coordinate[1];
                                double nx1 = y1 - y2 ;
                                double ny1 = x2 - x1 ;

                                b_nodes[f].normal[0] = nx1/2.;
                                b_nodes[f].normal[1] = ny1/2.;
                                b_nodes[f].type = 11;
                                //printf("%d %le %le\n",n1,b_nodes[f].normal[0],b_nodes[f].normal[1]);
			}
 		} else if (74 == initial_state) { // dordogne
                        if (   ((fabs( node[n1].coordinate[0] - 3.744460e+05) < 0.000001)  && (fabs(node[n1].coordinate[1] - 3.066120e+05) < 0.000001)) // node 8
                                || ((fabs( node[n1].coordinate[0] - 4.213366e+05) < 0.000001)  && (fabs(node[n1].coordinate[1] - 2.824808e+05) < 0.000001)) ) {  // node 9
                                int n1 = boundary[f].node[0] ;
                                int n2 = boundary[f].node[1] ;
                                double x1 = node[n1].coordinate[0] ;
                                double y1 = node[n1].coordinate[1] ;
                                double x2 = node[n2].coordinate[0] ;
                                double y2 = node[n2].coordinate[1] ;
                                double nx1 = y1 - y2 ;
                                double ny1 = x2 - x1 ;

                                b_nodes[f].normal[0] = nx1/2. ;
                                b_nodes[f].normal[1] = ny1/2. ;
                                b_nodes[f].type = 11;
                                //printf("%d %le %le\n",n1,b_nodes[f].normal[0],b_nodes[f].normal[1]);
                        }

                        if ( ((fabs( node[n1].coordinate[0] - 3.736990e+05) < 0.000001)  && (fabs(node[n1].coordinate[1] - 3.059840e+05) < 0.000001)) // node 0
                                || ((fabs( node[n1].coordinate[0] - 4.212707e+05) < 0.000001)  && (fabs(node[n1].coordinate[1] - 2.825567e+05) < 0.000001)) ) { // node 13409
                                int n1 = boundary[f-1].node[0];
                                int n2 = boundary[f-1].node[1];
                                double x1 = node[n1].coordinate[0];
                                double y1 = node[n1].coordinate[1];
                                double x2 = node[n2].coordinate[0];
                                double y2 = node[n2].coordinate[1];
                                double nx1 = y1 - y2 ;
                                double ny1 = x2 - x1 ;

                                b_nodes[f].normal[0] = nx1/2.;
                                b_nodes[f].normal[1] = ny1/2.;
                                b_nodes[f].type = 11;
                                //printf("%d %le %le\n",n1,b_nodes[f].normal[0],b_nodes[f].normal[1]);
                        }
		} else if (79 == initial_state) { // garonne-dordogne-gironde
			if (   ((fabs( node[n1].coordinate[0] - 3.386597e+05) < 0.000001)  && (fabs(node[n1].coordinate[1] - 3.723562e+05) < 0.000001)) // node 595
					|| ((fabs( node[n1].coordinate[0] - 4.116212e+05) < 0.000001)  && (fabs(node[n1].coordinate[1] - 2.558658e+05) < 0.000001)) // node 44938
					|| ((fabs( node[n1].coordinate[0] - 4.213366e+05) < 0.000001)  && (fabs(node[n1].coordinate[1] - 2.824808e+05) < 0.000001)) ) {  // node 29858
				//printf("%d   \n",n1); 
				int n1 = boundary[f].node[0] ;
				int n2 = boundary[f].node[1] ;
				double x1 = node[n1].coordinate[0] ;
				double y1 = node[n1].coordinate[1] ;
				double x2 = node[n2].coordinate[0] ;
				double y2 = node[n2].coordinate[1] ;
				double nx1 = y1 - y2 ;
				double ny1 = x2 - x1 ;

				b_nodes[f].normal[0] = nx1/2. ;
				b_nodes[f].normal[1] = ny1/2. ;
				b_nodes[f].type = 11;
				//printf("%d %d %le %le\n",n1,n2,b_nodes[f].normal[0],b_nodes[f].normal[1]);
			}
			if ( ((fabs( node[n1].coordinate[0] - 3.347489e+05) < 0.000001)  && (fabs(node[n1].coordinate[1] - 3.689337e+05) < 0.000001)) // node 36
					|| ((fabs( node[n1].coordinate[0] - 4.116213e+05) < 0.000001)  && (fabs(node[n1].coordinate[1] - 2.559926e+05) < 0.000001)) // node 50950
					|| ((fabs( node[n1].coordinate[0] - 4.212707e+05) < 0.000001)  && (fabs(node[n1].coordinate[1] - 2.825567e+05) < 0.000001)) ) { // node 43258
				//printf("%d   \n",n1); 
				int n1 = boundary[f-1].node[0] ;
				int n2 = boundary[f-1].node[1] ;
				double x1 = node[n1].coordinate[0] ;
				double y1 = node[n1].coordinate[1] ;
				double x2 = node[n2].coordinate[0] ;
				double y2 = node[n2].coordinate[1] ;
				double nx1 = y1 - y2 ;
				double ny1 = x2 - x1 ;

				b_nodes[f].normal[0] = nx1/2. ;
				b_nodes[f].normal[1] = ny1/2. ;
				b_nodes[f].type = 11;
				//printf("%d %d %le %le\n",n1,n2,b_nodes[f].normal[0],b_nodes[f].normal[1]);
			}
		} else if (92 == initial_state) { // garonne_ex_4
			if (   ((fabs( node[n1].coordinate[0] - 4.179640e+05) < 0.000001)  && (fabs(node[n1].coordinate[1] - 6.422783e+06) < 0.000001)) // node 0
					|| ((fabs( node[n1].coordinate[0] - 4.487940e+05) < 0.000001)  && (fabs(node[n1].coordinate[1] - 6.390253e+06) < 0.000001)) ) {  // node 34266
				//printf("%d   \n",n1); 
				int n1 = boundary[f].node[0] ;
				int n2 = boundary[f].node[1] ;
				double x1 = node[n1].coordinate[0] ;
				double y1 = node[n1].coordinate[1] ;
				double x2 = node[n2].coordinate[0] ;
				double y2 = node[n2].coordinate[1] ;
				double nx1 = y1 - y2 ;
				double ny1 = x2 - x1 ;

				b_nodes[f].normal[0] = nx1/2. ;
				b_nodes[f].normal[1] = ny1/2. ;
				b_nodes[f].type = 11;
				//printf("%d %d %le %le\n",n1,n2,b_nodes[f].normal[0],b_nodes[f].normal[1]);
			}
			if ( ((fabs( node[n1].coordinate[0] - 4.183990e+05) < 0.000001)  && (fabs(node[n1].coordinate[1] - 6.422783e+06) < 0.000001)) // node 19
					|| ((fabs( node[n1].coordinate[0] - 4.487440e+05) < 0.000001)  && (fabs(node[n1].coordinate[1] - 6.390023e+06) < 0.000001)) ) { // node 34948
				//printf("%d   \n",n1); 
				int n1 = boundary[f-1].node[0] ;
				int n2 = boundary[f-1].node[1] ;
				double x1 = node[n1].coordinate[0] ;
				double y1 = node[n1].coordinate[1] ;
				double x2 = node[n2].coordinate[0] ;
				double y2 = node[n2].coordinate[1] ;
				double nx1 = y1 - y2 ;
				double ny1 = x2 - x1 ;

				b_nodes[f].normal[0] = nx1/2. ;
				b_nodes[f].normal[1] = ny1/2. ;
				b_nodes[f].type = 11;
				//printf("%d %d %le %le\n",n1,n2,b_nodes[f].normal[0],b_nodes[f].normal[1]);
			}
		}
	
	}

}
