/*  Copyright (C) 2021   M.Ricchiuto  <mario.ricchiuto@inria.fr>
                         M. Kazolea   <maria.kazolea@inria.fr>
                         A. Filippini <a.filippini@brgm.fr>
                         L. Arpaia    <luca.arpaia@brgm.fr>
                         N.Pattakos   <pattakosn@fastmail.com> 

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.*/


/*************************************************************** 
                      -------------------
 This is source.c: It contains all the source terms (friction and
 bathymetry) for  the different test cases
                    -------------------
 ***************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "common.h"

extern const int volume_q_pts, vertex;
extern int NE, NN, NBF, initial_state, numerics_friction_explicit;
extern double Time, dt, gm, VVinf, cut_off_U, manning, local_bathymetry;
extern double Dx_local_bathymetry, Dy_local_bathymetry, UUinf;
extern double (*source) (double, double);
extern struct node_struct *node;
extern struct element_struct *element;
extern struct boundary_nodes_struct *b_nodes;
extern double **btablex, **btabley, **btableb;
extern double *a_wg, *t_wg, *l_wg, *erand;
extern int freq,angle;
extern struct numerical_int_struct numerical_int;

void bed_height(double, double);
void bed_height_2d(double, double);
void bed_height_2d1(double, double);
/* node global index, local residual distributed to node, time level to update (1/2) */

/* Friction term :  f|u|u */
double interp1(double *xi, double *yi, double x, int DIM){
	double x1=0.0;
	double x2=0.0;
	double y1=0.0;
	double y2=0.0;

	for(int i=0; i<DIM-1; i++){
		if(x>=xi[i] && x<=xi[i+1]){
			x1=xi[i];
			x2=xi[i+1];
			y1=yi[i];
			y2=yi[i+1];
		}
	}
	double a = (y1-y2)/(x1-x2);
	double b = y1 - a*x1;
	double res = a*x+b;

	return res;
}
void friction(int noden, int lev)
{
	node[noden].friction[lev][0] = 0.;
	node[noden].friction[lev][1] = 0.;
	node[noden].friction[lev][2] = 0.;
	double depth = node[noden].Z[lev][0];
	if ( depth > cut_off_U * node[noden].cut_off_ratio ) {
 		double u = node[noden].Z[lev][1];
		double v = node[noden].Z[lev][2];
		double norm = u * u + v * v;
  		norm = sqrt(norm);

		double coeff = 0.;
		coeff = norm * manning * manning;
		if ( initial_state == 75 ){
			 node[noden].friction[lev][1] =  0.5 * manning * norm * depth;
			 node[noden].friction[lev][2] =  0.5 * manning * norm * depth;
		} else {
			node[noden].friction[lev][1] = gm * coeff * u / pow(depth, 1. / 3.);
			node[noden].friction[lev][2] = gm * coeff * v / pow(depth, 1. / 3.);
		}
		if (0 == numerics_friction_explicit){
			node[noden].friction[lev][1] /= depth;
			node[noden].friction[lev][2] /= depth;
		}
	}

	double loc_time;
	if (lev == 2)
		loc_time = Time;
	else
		loc_time = Time - dt;

	if (initial_state == 36) {
		double xc1 = 200.;
		double xc2 = 400.;
		double yc1 = 2.;
		double yc2 = 3.;
		double x = node[noden].coordinate[0];
		double y = node[noden].coordinate[1];

		double rr1 = (x - xc1) * (x - xc1) + (y - yc1) * (y - yc1);
		rr1 = sqrt(rr1);
		double rr2 = (x - xc2) * (x - xc2) + (y - yc2) * (y - yc2);
		rr2 = sqrt(rr2);

		if ((xc1 <= x) && (x <= xc2)) {
			if ((loc_time >= 5.) && (loc_time <= 5.2))
				node[noden].friction[2][0] = -sin(pi * (loc_time - 5) * 2.5) * sin(pi * (loc_time - 5) * 2.5) * 0.02;

			if ((loc_time > 5.2) && (loc_time <= 65.2))
				node[noden].friction[lev][0] = -0.02;

			if ((loc_time > 65.2) && (loc_time <= 65.4)) {
				rr1 = sin(pi * (loc_time - 65.2) * 2.5) * sin(pi * (loc_time - 65.4) * 2.5);
				node[noden].friction[lev][0] = -0.02 * rr1 * rr1 * rr1 * rr1;
			}
		}
	}

	if ( ( initial_state == 71 ) || (initial_state == 72) || (initial_state == 73) || (initial_state == 74) || (initial_state == 79) || (initial_state == 85) ) { // Convergent Channel || gironde || garonne || dordogne
		double u     = node[noden].Z[lev][1] ;
		double v     = node[noden].Z[lev][2] ;
		double norm  = u*u + v*v ; norm = sqrt( norm ) ;
		
		/*** Quadratic model for friction ***/
		// Typical value for Garonne estuary: coeff = 0.005; [L&S]: coeff = 0.0037;
/*		if (depth > cut_off_U * node[noden].cut_off_ratio) {
			node[noden].friction[lev][1] = manning* norm*u ;
			node[noden].friction[lev][2] = manning* norm*v ;
			if (0 == numerics_friction_explicit){
				node[noden].friction[lev][1] /= depth;
				node[noden].friction[lev][2] /= depth;
			}
		}
*/
		
		/*** Strickler's coefficient ***/
		double strickler = 50.;
		double coeff = gm*sqrt(1 + node[noden].dxbed[0]*node[noden].dxbed[0] + node[noden].dybed[0]*node[noden].dybed[0])/pow(node[noden].P[lev][0],1./3.);
		
		if (depth > cut_off_U * node[noden].cut_off_ratio) {
			node[noden].friction[lev][1] = coeff/(strickler*strickler)*norm*u ;
			node[noden].friction[lev][2] = coeff/(strickler*strickler)*norm*v ;
			if (0 == numerics_friction_explicit){
				node[noden].friction[lev][1] /= depth;
				node[noden].friction[lev][2] /= depth;
			}
		}
		/*******************************/
	}
}

/* Nodal bathymetry values */
// Bathymetry values are computed to ensure mass conservation up to the error of the volume
// quadrature formula. 
// Different volume quadrature formulas are implemented: see set_numerical_integration.c.xq(rx) ; 
// If xq(rx) => 3q(r1) nodal values are used
void RD_bathy_quadrature(void)
{
	for (int n = 0; n < NN; n++)
		node[n].bed[2] = 0.0;

	for (int e = 0; e < NE; e++) {
		int i0 = element[e].node[0];
		int i1 = element[e].node[1];
		int i2 = element[e].node[2];

		for (int i = 0; i < vertex; i++) {
			int n = element[e].node[i];
			for (int q = 0; q < volume_q_pts; q++) {
				double xq =  numerical_int.volume_coordinate[q][0] * node[i0].coordinate_new[0]
				           + numerical_int.volume_coordinate[q][1] * node[i1].coordinate_new[0]
				           + numerical_int.volume_coordinate[q][2] * node[i2].coordinate_new[0];
				double yq =  numerical_int.volume_coordinate[q][0] * node[i0].coordinate_new[1]
				           + numerical_int.volume_coordinate[q][1] * node[i1].coordinate_new[1]
				           + numerical_int.volume_coordinate[q][2] * node[i2].coordinate_new[1];
				double bq = source(xq, yq);

				node[n].bed[2] += (element[e].new_volume * numerical_int.volume_weight[q] *  bq * numerical_int.volume_coordinate[q][i])
				                 / node[n].mod_volume;
			}
		}
	}

	for ( int f = 0 ; f < NBF ; f ++ ) {
		int n1 = b_nodes[f].node;
		node[n1].flag = 0.;
	}	
	for ( int f = 0 ; f < NBF ; f ++ ) {
		int n1 = b_nodes[f].node;
		if ( b_nodes[f].type == 10 && node[n1].flag == 0.) {
			int mate = node[n1].mate;
			node[n1].bed[2] += node[mate].bed[2];
			node[mate].bed[2] = node[n1].bed[2];
			node[n1].flag = 1.;
			node[mate].flag = 1.;
		}
	}
}


void FV_bathy_quadrature(void)
{
	for (int n = 0; n < NN; n++)
		node[n].bed[2] = 0.0;
	
	if (initial_state != 24 && initial_state != 93) {
	//if (initial_state != 24) {
		for (int e = 0; e < NE; e++) {
			for (int i = 0; i < vertex; i++) {
				int i0 = element[e].node[i];
				for (int jj = 1; jj < vertex; jj++) {
					int j = (i + jj) % 3;
					int k = (i + (3 - jj)) % 3;
					int i1 = element[e].node[j];
					int i2 = element[e].node[k];
		
					for (int q = 0; q < volume_q_pts; q++) {
						double xq =  numerical_int.volume_coordinate[q][0] * node[i0].coordinate_new[0]
						           + numerical_int.volume_coordinate[q][1] * node[i1].coordinate_new[0]
						           + numerical_int.volume_coordinate[q][2] * node[i2].coordinate_new[0];
						double yq =  numerical_int.volume_coordinate[q][0] * node[i0].coordinate_new[1]
						           + numerical_int.volume_coordinate[q][1] * node[i1].coordinate_new[1]
						           + numerical_int.volume_coordinate[q][2] * node[i2].coordinate_new[1];
						double bq = source(xq, yq);
						if( initial_state == 92 ) {
							bq = numerical_int.volume_coordinate[q][0] * btableb[i0][0] 
						           + numerical_int.volume_coordinate[q][1] * btableb[i1][0]
						           + numerical_int.volume_coordinate[q][2] * btableb[i2][0];
						}
							node[i0].bed[2] += (element[e].new_volume / 6.0 * numerical_int.volume_weight[q] * bq)
						                  /node[i0].mod_volume;
							int n=i0;
					}
				}
			}
		}
	}else{
		if(initial_state ==24 || initial_state==93) {
			for (int n = 0; n < NN; n++){
				node[n].bed[2] = source(node[n].coordinate[0], node[n].coordinate[1]);
			}
		}
//		if(initial_state == 92) {
//			for (int n = 0; n < NN; n++){
//				node[n].bed[2] = btableb[n][0];
//			}
//		}
	}

	for ( int f = 0 ; f < NBF ; f ++ ) {
		int n1 = b_nodes[f].node;
		node[n1].flag = 0.;
	}	
	for ( int f = 0 ; f < NBF ; f ++ ) {
		int n1 = b_nodes[f].node;
		if ( b_nodes[f].type == 10 && node[n1].flag == 0.) {
			int mate = node[n1].mate;
			node[n1].bed[2] += node[mate].bed[2];
			node[mate].bed[2] = node[n1].bed[2];
			node[n1].flag = 1.;
			node[mate].flag = 1.;
		}
	}
}
/* Bathymetry functions */
double source_slope(double x, double y)
{
	double slope = 10 * VVinf;
	double bed = 1. - (2. * x + y) / (sqrt(5.) * slope);
	return bed;
}


extern void bed_height_2d(double x, double y)
{
	double xi0 = 1. / (10 * VVinf);
	double eta0 = UUinf;
	double bb = eta0 - 4. - xi0 * x;
	double dxbb = -xi0;
	double dybb = 0.;
	double omega, domega;
	if (y <= 1.5) {
		omega = 0.3 * (y - 1.5) * (y - 1.5) * 200. * xi0;
		domega = 0.3 * 200 * xi0 * 2. * (y - 1.5);
		bb += omega;
		dybb += domega;
	}
	if (y >= 3.5) {
		omega = 0.3 * (3.5 - y) * (3.5 - y) * (3.5 - y) * (3.5 - y) * 200. * xi0;
		domega = -0.3 * 200. * xi0 * 4. * (3.5 - y) * (3.5 - y) * (3.5 - y);
		bb += omega;
		dybb += domega;
	}
	if ((y >= 2) && (y <= 3)) {
		omega = -0.2 * sin((y - 2) * pi) * sin((y - 2) * pi) * sin((y - 2) * pi) * sin((y - 2) * pi);
		domega = -0.2 * 4. * pi * sin((y - 2) * pi) * sin((y - 2) * pi) * sin((y - 2) * pi) * cos((y - 2) * pi);
		bb += omega;
		dybb += domega;
	}

	local_bathymetry = bb;
	Dx_local_bathymetry = dxbb;
	Dy_local_bathymetry = dybb;
}

extern void bed_height_2d1(double x, double y)
{
	double xi0 = 1. / (10 * VVinf);
	double eta0 = UUinf;
	double bb = eta0 - 1. - xi0 * x;
	double dxbb = -xi0;
	double dybb = 0.;

	y = 5. * y;

	double omega, domega;
	if (y <= 1.5) {
		omega = 0.3 * (y - 1.5) * (y - 1.5) * xi0 * 50;
		domega = 0.3 * xi0 * 2. * (y - 1.5) * 50;
		bb += omega;
		dybb += domega;
	}
	if (y >= 3.5) {
		omega = 0.3 * (3.5 - y) * (3.5 - y) * (3.5 - y) * (3.5 - y) * xi0 * 30.;
		domega = -0.3 * xi0 * 4. * (3.5 - y) * (3.5 - y) * (3.5 - y) * 30;
		bb += omega;
		dybb += domega;
	}
	if ((y >= 2) && (y <= 3)) {
		omega = -0.2 * sin((y - 2) * pi) * sin((y - 2) * pi) * sin((y - 2) * pi) * sin((y - 2) * pi);
		domega = -0.2 * 4. * pi * sin((y - 2) * pi) * sin((y - 2) * pi) * sin((y - 2) * pi) * cos((y - 2) * pi);
		bb += omega;
		dybb += domega;
	}

	local_bathymetry = bb;
	Dx_local_bathymetry = dxbb;
	Dy_local_bathymetry = dybb;
}

double source_okushiri(double xp, double yp)
{
	int iM = 1;
	int jM = 1;
	double x2 = btablex[iM][jM];
	double y2 = btabley[iM][jM];
	int bdone = 0;

	double bed;
	while ((!bdone)) {
		if (x2 < xp) {
			iM++;
			x2 = btablex[iM][jM];
		} else {
			double x1 = btablex[iM - 1][jM];
			x2 = btablex[iM][jM];
			if (x1 > xp) {
				printf("Trouble: x1 > xp\n");
				exit(EXIT_FAILURE);
			}

			if (y2 < yp) {
				jM++;
				y2 = btabley[iM][jM];
			} else {
				y2 = btabley[iM][jM];
				double y1 = btabley[iM][jM - 1];
				if (y1 > yp) {
					printf("Trouble: y1 > yp\n");
					printf("jM = %d , y1 = %le, y2=%le, yp = %le\n", jM, y1, y2, yp);
					printf("iM = %d , x1 = %le, x2=%le, xp = %le\n", iM, x1, x2, xp);
					exit(EXIT_FAILURE);
				}

				double b1 = btableb[iM - 1][jM - 1];
				double b2 = btableb[iM][jM - 1];
				double b3 = btableb[iM][jM];
				double b4 = btableb[iM - 1][jM];
				bed = -b1 * ((xp - x2) / (x1 - x2)) * ((yp - y2) / (y1 - y2))
				    - b2 * ((xp - x1) / (x2 - x1)) * ((yp - y2) / (y1 - y2))
				    - b3 * ((xp - x1) / (x2 - x1)) * ((yp - y1) / (y2 - y1))
				    - b4 * ((xp - x2) / (x1 - x2)) * ((yp - y1) / (y2 - y1));	// bi-linear  interpolation

				bdone = 1;
			}
		}
	}

	if (!bdone) {
		printf("node %f %f: Bed not assigned!!\n", xp, yp);
		exit(EXIT_FAILURE);
	}

	return bed;
}

double source_oregon(double xp, double yp)
{
	int iM = 1;
	int jM = 1;
	double x2 = btablex[iM][jM];
	double y2 = btabley[iM][jM];
	int bdone = 0;

	double bed;
	while ((!bdone)) {
		if (x2 < xp) {
			jM++;
			x2 = btablex[iM][jM];
		} else {
			double x1 = btablex[iM][jM-1];
			x2 = btablex[iM][jM];
			if (x1 > xp) {
				printf("Trouble: x1 > xp\n");
				exit(EXIT_FAILURE);
			}

			if (y2 < yp) {
				iM++;
				y2 = btabley[iM][jM];
			} else {
				y2 = btabley[iM][jM];
				double y1 = btabley[iM-1][jM];
				if (y1 > yp) {
					printf("Trouble: y1 > yp\n");
					printf("jM = %d , y1 = %le, y2=%le, yp = %le\n", jM, y1, y2, yp);
					printf("iM = %d , x1 = %le, x2=%le, xp = %le\n", iM, x1, x2, xp);
					exit(EXIT_FAILURE);
				}

				double b1 = btableb[iM - 1][jM - 1];
				double b2 = btableb[iM-1][jM];
				double b3 = btableb[iM][jM];
				double b4 = btableb[iM][jM-1];
				bed = -b1 * ((xp - x2) / (x1 - x2)) * ((yp - y2) / (y1 - y2))
				    - b2 * ((xp - x1) / (x2 - x1)) * ((yp - y2) / (y1 - y2))
				    - b3 * ((xp - x1) / (x2 - x1)) * ((yp - y1) / (y2 - y1))
				    - b4 * ((xp - x2) / (x1 - x2)) * ((yp - y1) / (y2 - y1));	// bi-linear  interpolation

                                 bed=-bed;
				bdone = 1;
			}
		}
	}

	if (!bdone) {
		printf("node %f %f: Bed not assigned!!\n", xp, yp);
		exit(EXIT_FAILURE);
	}

	return bed;
}

double source_wedge(double __attribute__((unused))x, double __attribute__((unused))y)
{
	double bed = 0.;
	return bed;
}

double source_1d(double __attribute__((unused))x, double __attribute__((unused))y)
{
	double bottom;
	if (x > 8. && x < 12.)
		bottom = 0.2 - 0.05 * (x - 10.) * (x - 10.);
	else
		bottom = 0.;
	double bed = bottom;
	return bed;
}

double source_1d_hump(double __attribute__((unused))x, double __attribute__((unused))y)
{
	double bottom;
	if (x < 0.1) {
		bottom = cos(5. * M_PI * x);
		bottom *= 3. * bottom * bottom * bottom;
	} else if (x >= 1.9) {
		bottom = cos(5. * M_PI * (x - 1.));
		bottom *= 3. * bottom * bottom * bottom;
	} else if (x > 0.9 && x < 1.5) {
		double xc = (x - 1.2);
		bottom = cos(5. * M_PI * xc / 3.);
		bottom = 1.5 * bottom * bottom * bottom;
	} else
		bottom = 0.;
	double bed = bottom;
	return bed;
}

double source_2d_hump(double x, double y)
{
	double yc = (y - 1.);
	double bottom;
	if (x < 0.1) {
		bottom = cos(5. * M_PI * x);
		bottom *= 3. * bottom * bottom * bottom;
	} else if (x >= 1.9) {
		bottom = cos(5. * M_PI * (x - 1.));
		bottom *= 3. * bottom * bottom * bottom;
	} else if ((x > 0.9 && x < 1.5) && fabs(yc) <= 0.3) {
		double xc = (x - 1.2);
		double radius = sqrt(xc * xc + yc * yc);
		bottom = MAX(cos(5. * M_PI * radius / 3.), 0);
		bottom = 1.5 * bottom * bottom * bottom;
	} else
		bottom = 0.;
	double bed = bottom;
	return bed;
}

double source_lake(double x, double y)
{
	x -= 0.5;
	y -= 0.5;
	double phi = -50 * (x * x + y * y);
	double bottom = 0.8 * exp(phi);
	double bed = bottom-1.;
	return bed;
}

double source_2d_smooth(double x, double y)
{
	double pot = -5. * (x - 0.9) * (x - 0.9) - 50. * (y - 0.5) * (y - 0.5);
	//if ( ( y >= 0.3 ) && ( y <= 0.7 ) ) // Discontinuous
	//	if ( ( x >= 0.9 ) && ( x <= 1.1 ) )
	//		pot =  sqrt( (x - 0.9)*(x - 0.9) + (y - 0.5) * (y -0.5) ) ;
	double bottom = 0.8 * exp(pot);
	//bottom = 0.6 * exp(pot) ; // discontinuous
	/*if( x < 0.5)
		bottom = x + 0.0;
	else if ( x > 1.5)
		bottom = 2.0 - x;
	else
	bottom = 0.5; */
	double bed = bottom;
	return bed;
}

double source_dam(double __attribute__((unused))x, double __attribute__((unused))y)
{
	double bed = 0.;
	return bed;
}


double source_conical_island(double x, double y)
{
	x -= 12.96;
	y -= 13.80;
	double r = sqrt(x * x + y * y);
	double bed;
	if (r < 1.1)
		bed = .625;
	else if (r < 3.6)
		bed = (3.6 - r) / 4.;
	else
		bed = 0.;
	return bed;
}

double source_dry(double x, double __attribute__((unused))y)
{
	double r = fabs(x - 10.);
	//bottom = 0.2 - 10. * r;
	//bottom =sqrt( r);
	double bed;
	if (r < 1.)
		bed = 1. - r * r;	//0.2 - bottom;
	else			//*/
		bed = 0.;
	return bed;
}


double source_thacker(double x, double y)
{
	double bottom = 0.;
	double parah = 0.1;
	double para = 1.;
	double radius = (x * x + y * y) / (para * para);
	bottom = -parah * (1. - radius);
	double bed = bottom + 1.;
	return bed;
}

void bed_height(double x, double __attribute__((unused))y)
{
	//std case:
	//KR = 1 ;
	//rib_start[0] = 8. ;
	int KR = 5; // number of ribs
	double rib_start[] = {1.5, 6., 10.5, 15., 19.5};

	//  1 : parabola, 2 : sin2, 3 : sin4, 4 : sin6, 5 : sin8
	int rib_type = 2;

	double bb = 0, dxbb = 0, dybb = 0;
	for (int k = 0; k < KR; k++) {
		if ((x >= rib_start[k]) && (x <= rib_start[k] + 4.)) {
			if (rib_type == 1) {
				bb += 0.2 - 0.05 * (x - rib_start[k] - 2.) * (x - rib_start[k] - 2.);	// (x-10)^2
				dxbb += -0.1 * (x - rib_start[k] - 2.);	// (x-10)^2
			} else {
				double base = sin(0.25 * pi * (x - rib_start[k])) * sin(0.25 * pi * (x - rib_start[k]));
				if (2 == rib_type) {
					bb += 0.2 * base;	// sin^2
					dxbb += 0.1 * pi * cos(0.25 * pi * (x - rib_start[k])) * sin(0.25 * pi * (x - rib_start[k]));	// sin^2                               
				} else if (3 == rib_type) {
					bb += 0.2 * base * base;	// sin^4
					dxbb += 0.2 * 4. * base * sin(0.25 * pi * (x - rib_start[k])) * cos(0.25 * pi * (x - rib_start[k])) * 0.25 * pi;	// sin^4
				} else if (4 == rib_type) {
					bb += 0.2 * base * base * base;	// sin^6
					dxbb += 0.2 * 6. * base * base * sin(0.25 * pi * (x - rib_start[k])) * cos(0.25 * pi * (x - rib_start[k])) * 0.25 * pi;	// sin^6
				} else if (5 == rib_type) {
					bb += 0.2 * base * base * base * base;	// sin^8
					dxbb += 0.2 * 8. * base * base * base * sin(0.25 * pi * (x - rib_start[k])) * cos(0.25 * pi * (x - rib_start[k])) * 0.25 * pi;	// sin^8
				}
			}
		}
	}

	local_bathymetry = bb;
	Dx_local_bathymetry = dxbb;
	Dy_local_bathymetry = dybb;
}

double source_steady_exner(double x, double y)
{
	double xmax = 500.;
	double xmin = 300.;
	double ymax = 600.;
	double ymin = 400.;
	double argx = pi * (x - 300.) / 200;
	double argy = pi * (y - 400.) / 200;
	double bottom = 0.;
	if (x >= xmin && x <= xmax)
		if (y >= ymin && y <= ymax)
			bottom = sin(argx) * sin(argx) * sin(argy) * sin(argy);
	double bed = bottom;
	return bed;
}

double source_reef_v1(double x, double y)
{
	double rad2 = x * x + y * y;
	double rad = sqrt(rad2);
	double x_corner_inside_left = -501.0;
	double y_corner_inside_left = -sqrt(4501.0 * 4501.0 - 501.0 * 501.0);
	double x_corner_outside_left = -499.0;
	double y_corner_outside_left = -sqrt(4999.0 * 4999.0 - 501.0 * 501.0);
	double x_corner_outside_left_right = -497.5;
	double angle_inside_left = pi + atan(y_corner_inside_left / x_corner_inside_left);
	double angle_outside_left = pi + atan(y_corner_outside_left / x_corner_inside_left);
	double phi;
	if (0.0 <= x && 0.0 == y)
		phi = 0.0;
	else if (x < 0.0 && 0.0 == y)
		phi = pi;
	else if (0.0 == x && 0.0 < y)
		phi = pi / 2.0;
	else if (0.0 == x && y < 0.0)
		phi = 1.5 * pi;
	else if (0.0 < x && 0.0 < y)
		phi = atan(y / x);
	else if (x < 0.0 && 0.0 < y)
		phi = pi / 2.0 - atan(x / y);
	else if (x < 0.0 && y < 0.0)
		phi = pi + atan(y / x);
	else
		phi = 1.5 * pi - atan(x / y);

	double bed;
	if (20000.0 * 20000.0 <= rad2) // depth of the ocean
		bed = -4000.0;
	else if (4501.0 * 4501.0 <= rad2 && rad2 <= 4999.0 * 4999.0 && (0.0 <= y || x <= -501.0 || 501.0 <= x)) // highest points of the island
		bed = 10.0;
	else if (5002.5 * 5002.5 <= rad2 && rad2 <= 10000.0 * 10000.0) // linear approximation around the island
		bed = -0.4 * rad + 2000.0;
	else if (10000.0 * 10000.0 < rad2 && rad2 < 20000.0 * 20000.0) // smoothing between depth of the ocean and the linear approximation
		bed =
		    -4000.0 - (2.0 / 1000000000.0 + 0.2 / 100000000.0) * (rad - 20000.0) * (rad - 20000.0) * (rad - 20000.0) -
		    0.2 / 1000000000000.0 * (rad - 20000.0) * (rad - 20000.0) * (rad - 20000.0) * (rad - 20000.0);
	else if (4499.0 * 4499.0 < rad2 && rad2 < 4501.0 * 4501.0 && (phi <= angle_inside_left || 3.0 * pi - angle_inside_left <= phi)) // inner edge of the inner circle
		bed = 5.0 * (cos(pi * (rad - 4501.0) / 2.0) + 1.0);
	else if (4999.0 * 4999.0 < rad2 && rad2 <= 5000.0 * 5000.0 && ((phi <= angle_outside_left || 3.0 * pi - angle_outside_left <= phi))) // outer edge of the outer circle (up)
		bed = 5.0 * (cos(pi * (rad - 4999.0) / 2.0) + 1.0);
	else if (5000.0 * 5000.0 < rad2 && rad2 < 5002.5 * 5002.5 && ((phi <= angle_outside_left || 3.0 * pi - angle_outside_left <= phi))) // outer edge of the outer corner (down)
		bed =
		    -1.0 - 2.0 * (rad - 5002.5) / 5.0 + (-168.0 / 125.0 + 2.0 * pi / 5.0) * (rad - 5002.5) * (rad - 5002.5) * (rad -
															       5002.5) +
		    (-256.0 / 625.0 + 4.0 * pi / 25.0) * (rad - 5002.5) * (rad - 5002.5) * (rad - 5002.5) * (rad - 5002.5);

	else if (x_corner_inside_left < x && x < x_corner_outside_left && y_corner_outside_left <= y && y <= y_corner_inside_left) // edge of the break (left)
		bed = 5.0 * (cos(pi * (x - x_corner_inside_left) / 2.0) + 1.0);
	else if (-x_corner_outside_left < x && x < -x_corner_inside_left && y_corner_outside_left <= y && y <= y_corner_inside_left) // edge of the break (right)
		bed = 5.0 * (cos(pi * (x + x_corner_inside_left) / 2.0) + 1.0);
	else if (((x - x_corner_inside_left) * (x - x_corner_inside_left) + (y - y_corner_inside_left) * (y - y_corner_inside_left)) < 4.0
		 && y_corner_inside_left < y && angle_inside_left < phi) // corner of the inner circle (left)
		bed = 5.0 * (cos(pi * (sqrt ((x - x_corner_inside_left) * (x - x_corner_inside_left) + (y - y_corner_inside_left) * (y - y_corner_inside_left))) / 2.0) + 1.0);
	else if (((x + x_corner_inside_left) * (x + x_corner_inside_left) + (y - y_corner_inside_left) * (y - y_corner_inside_left)) < 4.0
		 && y_corner_inside_left < y && phi < 3.0 * pi - angle_inside_left) // corner of the inner circle (right)
		bed = 5.0 * (cos (pi * (sqrt ((x + x_corner_inside_left) * (x + x_corner_inside_left) + (y - y_corner_inside_left) * (y - y_corner_inside_left))) / 2.0) + 1.0);
	else if (((x - x_corner_inside_left) * (x - x_corner_inside_left) + (y - y_corner_outside_left) * (y - y_corner_outside_left)) <=
		 1.0 && y < y_corner_outside_left && angle_outside_left < phi) // corner of the outer circle (left, up)
		bed = 5.0 * (cos (pi * (sqrt ((x - x_corner_inside_left) * (x - x_corner_inside_left) +	(y - y_corner_outside_left) * (y - y_corner_outside_left))) / 2.0) + 1.0);
	else if (1.0 < ((x - x_corner_inside_left) * (x - x_corner_inside_left) + (y - y_corner_outside_left) * (y - y_corner_outside_left))
		 && ((x - x_corner_inside_left) * (x - x_corner_inside_left) + (y - y_corner_outside_left) * (y - y_corner_outside_left)) <=
		 3.5 * 3.5 && y < y_corner_outside_left && angle_outside_left < phi) { // corner of the outer circle (left, down)
		double alpha = atan((x - x_corner_inside_left) / (y_corner_outside_left - y)) * 2.0 / pi;
		double value = sqrt((x - x_corner_inside_left) * (x - x_corner_inside_left) + (y - y_corner_outside_left) * (y - y_corner_outside_left)) - 3.5;
		double function = -1.0 - 2.0 * value / 5.0 + (-168.0 / 125.0 + 2.0 * pi / 5.0) * value * value * value + (-256.0 / 625.0 + 4.0 * pi / 25.0) * value * value * value * value;
		if (alpha <= 0.0)
			bed = function;
		else if (((x - x_corner_inside_left) * (x - x_corner_inside_left) + (y - y_corner_outside_left) * (y - y_corner_outside_left)) < 4.0)
			bed = alpha * 5.0 * (cos (pi * (sqrt ((x - x_corner_inside_left) * (x - x_corner_inside_left) + (y - y_corner_outside_left) * (y - y_corner_outside_left))) / 2.0) + 1.0) + (1.0 - alpha) * function;
		else
			bed = (1.0 - alpha) * function;
	} else if (((x + x_corner_inside_left) * (x + x_corner_inside_left) + (y - y_corner_outside_left) * (y - y_corner_outside_left)) <=
		 1.0 && y < y_corner_outside_left && phi < 3.0 * pi - angle_outside_left) // corner of the outer circle (right, up)
		bed = 5.0 * (cos (pi * (sqrt ((x + x_corner_inside_left) * (x + x_corner_inside_left) + (y - y_corner_outside_left) * (y - y_corner_outside_left))) / 2.0) + 1.0);
	else if (1.0 < ((x + x_corner_inside_left) * (x + x_corner_inside_left) + (y - y_corner_outside_left) * (y - y_corner_outside_left))
		 && ((x + x_corner_inside_left) * (x + x_corner_inside_left) + (y - y_corner_outside_left) * (y - y_corner_outside_left)) <=
		 3.5 * 3.5 && y < y_corner_outside_left && angle_outside_left < phi) { // corner of the outer circle (right, down)
		double alpha = atan((-x - x_corner_inside_left) / (y_corner_outside_left - y)) * 2.0 / pi;
		double value = sqrt((x + x_corner_inside_left) * (x + x_corner_inside_left) + (y - y_corner_outside_left) * (y - y_corner_outside_left)) - 3.5;
		double function = -1.0 - 2.0 * value / 5.0 + (-168.0 / 125.0 + 2.0 * pi / 5.0) * value * value * value + (-256.0 / 625.0 + 4.0 * pi / 25.0) * value * value * value * value;
		if (alpha <= 0.0)
			bed = function;
		else if (((x + x_corner_inside_left) * (x + x_corner_inside_left) + (y - y_corner_outside_left) * (y - y_corner_outside_left)) < 4.0)
			bed = alpha * 5.0 * (cos (pi * (sqrt ((x + x_corner_inside_left) * (x + x_corner_inside_left) + (y - y_corner_outside_left) * (y - y_corner_outside_left))) / 2.0) + 1.0) + (1.0 - alpha) * function;
		else
			bed = (1.0 - alpha) * function;
	} else if ((x_corner_outside_left_right * x_corner_outside_left_right + y_corner_outside_left * y_corner_outside_left) < rad2
		 && rad2 < 5002.5 * 5002.5 && y < y_corner_outside_left) { // rest (smoothing between niveau=0 and the linear approximation around the island)
		double R2 = 3.5 * 3.5;
		double coord_dist = 4999.0;
		double y_new1 = (rad2 - R2) / (2.0 * coord_dist) + coord_dist / 2.0;
		double x_new1 = -sqrt(rad2 - y_new1 * y_new1);
		double phi_coord = angle_outside_left + atan(-x_new1 / y_new1);
		double x_new2 = rad * cos(phi_coord);
		double y_new2 = rad * sin(phi_coord);
		double alpha = atan((x_new2 - x_corner_inside_left) / (y_corner_outside_left - y_new2)) * 2.0 / pi;
		double value = sqrt((x_new2 - x_corner_inside_left) * (x_new2 - x_corner_inside_left) + (y_new2 - y_corner_outside_left) * (y_new2 - y_corner_outside_left)) - 3.5;
		double function = -1.0 - 2.0 * value / 5.0 + (-168.0 / 125.0 + 2.0 * pi / 5.0) * value * value * value + (-256.0 / 625.0 + 4.0 * pi / 25.0) * value * value * value * value;
		if (alpha <= 0.0)
			bed = function;
		else
			bed = (1.0 - alpha) * function;
	} else { // inside the inner circle
		bed = 0.0;
	}

	//set MESHSIZES
	//if ( bed < -3900 ) 
	//	bed = 1500. ;
	//else {
	//	if ( ( bed >= -3900 ) && ( bed < -1000 ) ) {
	//		bed = 750. ;
	//	} else {
	//		//if ( ( bed >= -1000 ) && ( bed < -300. ) ) bed = 500. ;
	//		//else
	//		bed = 300. ;
	//	}
	//}
	     
	//if ( rad < 5100 ) {
	//	if ( ( rad >= 4450 ) && ( rad < 5050 ) ) {
	//		if ( ( rad < 4550 ) || ( rad > 4950 ) )                 
	//			bed = 20. ;
	//		else
	//			bed = 50. ;
	//		if ( (  x > - 550  ) && ( x < 550  ) && ( y < 0. ) )
	//			if ( rad < 4550 )
	//				bed = 200. ;
	//		if ( ( fabs( x - 500 ) < 50 ) || ( fabs( x + 500 ) < 50 ) )
	//		if( y < 0. )
	//			bed = 20. ;
	//	} else {
	//		bed = 200. ;
	//	}
	//}
	return bed;
}

double source_reef_V2Jan(double x, double y)
{
	double rad2 = x * x + y * y;
	double rad = sqrt(rad2);
	double x_corner_inside_left = -512.5;
	double y_corner_inside_left = -sqrt(4512.5 * 4512.5 - 512.5 * 512.5);
	double x_corner_outside_left = -487.5;
	double y_corner_outside_left = -sqrt(4975.0 * 4975.0 - 512.5 * 512.5);
	double angle_inside_left = pi + atan(y_corner_inside_left / x_corner_inside_left);
	double angle_outside_left = pi + atan(y_corner_outside_left / x_corner_inside_left);
	double phi;
	if (0.0 <= x && 0.0 == y)
		phi = 0.0;
	else if (x < 0.0 && 0.0 == y)
		phi = pi;
	else if (0.0 == x && 0.0 < y)
		phi = pi / 2.0;
	else if (0.0 == x && y < 0.0)
		phi = 1.5 * pi;
	else if (0.0 < x && 0.0 < y)
		phi = atan(y / x);
	else if (x < 0.0 && 0.0 < y)
		phi = pi / 2.0 - atan(x / y);
	else if (x < 0.0 && y < 0.0)
		phi = pi + atan(y / x);
	else
		phi = 1.5 * pi - atan(x / y);

	double bed;
	if (20000.0 * 20000.0 <= rad2) // depth of the ocean
		bed = -4000.0;	//check
	else if (4512.5 * 4512.5 <= rad2 && rad2 <= 4975.0 * 4975.0 && (0.0 <= y || x <= -512.5 || 512.5 <= x)) // highest points of the island
		bed = 10.0;	//check
	else if (5000.0 * 5000.0 <= rad2 && rad2 <= 10000.0 * 10000.0) // linear approximation around the island
		bed = -0.4 * rad + 2000.0;	//check
	else if (4975.0 * 4975.0 <= rad2 && (phi <= angle_outside_left || 3.0 * pi - angle_outside_left <= phi) && rad2 <= 5000.0 * 5000.0)
		bed = -0.4 * rad + 2000.0;	//check (outside the pass outer)
	else if (10000.0 * 10000.0 <= rad2 && rad2 <= 20000.0 * 20000.0) // smoothing between depth of the ocean and the linear approximation
		bed = -4000.0 - (2.0 / 1000000000.0 + 0.2 / 100000000.0) * (rad - 20000.0) * (rad - 20000.0) * (rad - 20000.0) - 0.2 / 1000000000000.0 * (rad - 20000.0) * (rad - 20000.0) * (rad - 20000.0) * (rad - 20000.0);	//check
	else if (4487.5 * 4487.5 <= rad2 && rad2 <= 4512.5 * 4512.5 && (phi <= angle_inside_left || 3.0 * pi - angle_inside_left <= phi)) // inner edge of the inner circle
		bed = 0.4 * rad - 1795.0;	// check (outside the pass, inner)
	else if (x_corner_inside_left <= x && x <= x_corner_outside_left && y_corner_outside_left <= y && y <= y_corner_inside_left) // edge of the break (left)
		bed = -0.4 * x - 195.0;
	else if (-x_corner_outside_left <= x && x <= -x_corner_inside_left && y_corner_outside_left <= y && y <= y_corner_inside_left) // edge of the break (right)
		bed = 0.4 * x - 195.0;
	else if (((x - x_corner_inside_left) * (x - x_corner_inside_left) + (y - y_corner_inside_left) * (y - y_corner_inside_left)) <=
		 25.0 * 25.0 && y_corner_inside_left <= y && angle_inside_left <= phi) // corner of the inner circle (left)
		bed = -0.4 * sqrt((x - x_corner_inside_left) * (x - x_corner_inside_left) + (y - y_corner_inside_left) * (y - y_corner_inside_left)) + 10.0;
	else if (((x + x_corner_inside_left) * (x + x_corner_inside_left) + (y - y_corner_inside_left) * (y - y_corner_inside_left)) <=
		 25.0 * 25.0 && y_corner_inside_left <= y && phi <= 3.0 * pi - angle_inside_left) // corner of the inner circle (right)
		bed = -0.4 * sqrt((x + x_corner_inside_left) * (x + x_corner_inside_left) +	(y - y_corner_inside_left) * (y - y_corner_inside_left)) + 10.0;
	else if (((x - x_corner_inside_left) * (x - x_corner_inside_left) + (y - y_corner_outside_left) * (y - y_corner_outside_left)) <=
		 25.0 * 25.0 && y <= y_corner_outside_left && angle_outside_left <= phi) // corner of the outer circle (left)
		bed = -0.4 * sqrt((x - x_corner_inside_left) * (x - x_corner_inside_left) +	(y - y_corner_outside_left) * (y - y_corner_outside_left)) + 10.0;
	else if (((x + x_corner_inside_left) * (x + x_corner_inside_left) + (y - y_corner_outside_left) * (y - y_corner_outside_left)) <=
		 25.0 * 25.0 && y <= y_corner_outside_left && phi <= 3.0 * pi - angle_outside_left) // corner of the outer circle (right)
		bed = -0.4 * sqrt((x + x_corner_inside_left) * (x + x_corner_inside_left) + (y - y_corner_outside_left) * (y - y_corner_outside_left)) + 10.0;
	else // inside the inner circle
		bed = 0.;

	return bed;
}

double source_reef(double x, double y)
{
	//double hhmm = 0.625 ;
	//double rrr0 = 40.*hhmm ;
	//double ss0 = 1./( 8.*hhmm ) ;
	double rad2 = x * x + y * y;
	double rad = sqrt(rad2);
	double rrr1 = 4750.;
	double rrr2 = 5250.;
	double rrr3 = rrr2 + 5.;
	double rrr4 = rrr1 - 5.;
	double rlim = 2. * rrr2 + 200.;
	double alf = -0.05 / (rlim - 20000.);
	double bed = 10. - 0.1 * (rad - rrr2);

	if (rad > rlim)
		bed = alf * (rad - 20000.) * (rad - 20000.) - 1000.;

	if (rad > 20000.)
		bed = -1000.;

	if (rad <= rrr2)
		bed = 10.;

	//if ( rad <= rrr1  )
	//	bed = 10. - ( rrr1 - rad )/10. ;

	if (rad <= rrr1) {
		double rr = rad / (rrr1);
		double y0 = 10.;
		bed = y0 * (1. - pow(1. - rr * rr, 1. / 2.));
		//bed =  -1000 + 800*rr*rr   ;
	}

	// underwater pass: 5m  depth
	if (y < 0.)
		if ((x >= -500.) && (x <= 500))
			if ( (rad <= 5000. + 250. + 5.) && (rad >= 5000. - 250 - 5.) )
				bed = 5.;
				//bed = 50. ;

	//LEVEL SET function
	// outside reef, within pass
	if ( (rad > rrr3) && (x > -500.) && (x < 500.) && (y < 0.) ) {
		if (x >= 0.) {
			double rr = -sqrt(rrr3 * rrr3 - 550. * 500.);
			rr = (y - rr) * (y - rr) + (x - 500.) * (x - 500.);
			bed = sqrt(rr);
		} else {
			double rr = -sqrt(rrr3 * rrr3 - 550. * 500.);
			rr = (y - rr) * (y - rr) + (x + 500.) * (x + 500.);
			bed = sqrt(rr);
		}
	} else if ((rad <= rrr3) && (rad > rrr4) && (x > -500.) && (x < 500.) && (y < 0.)) { // in the pass
		if (x > 0.)
			bed = 500. - x;
		else
			bed = x + 500.;
	} else if ((rad <= rrr4) && (fabs(x) / 500. < fabs(y) / sqrt(rrr4 * rrr4 - 500. * 500.)) && (y < 0.)) {	// inside reef, in the pass cone
		double rr = -sqrt(rrr4 * rrr4 - 500. * 500.);
		if (x <= 0.) {
			rr = (y - rr) * (y - rr) + (x + 500.) * (x + 500.);
			bed = sqrt(rr);
		} else {
			rr = (y - rr) * (y - rr) + (x - 500.) * (x - 500.);
			bed = sqrt(rr);
		}
	} else {
		if (rad < rrr4)
			bed = rrr4 - rad;
		else if (rad > rrr3)
			bed = rad - rrr3;
		else {
			bed = rrr3 - rad;
			if (bed > rad - rrr4)
				bed = rad - rrr4;
		}
	}

	//bed = MAX(0., 9.5 - bed  ) ;
	
	//if ( 9.5 - bed  > 0. )
	//	bed = 1. ;       
	//else if ( 9.5 - bed  < 0. )
	//	bed = -1. ;
	//else bed = 0. ;
	
	//bed *= 1000. ;
	
	//if ( rad <= 5000. ) {
	//	bed = 10. ;
	//} else {
	//	if ( rad <= 20000.  )
	//		bed = 10 - 0.01*( rad - 5000. ) ;
	//	else
	//		bed = 10 - 0.01*( 15000. ) ;   
	//}

	//// set MESHSIZES
	//if ( bed < -1000 ) 
	//	bed = 1000. ;
	//else {
	//	if ( ( bed >= -1000 ) && ( bed < -100 ) ) {
	//		bed = 500. ;
	//	} else {
	//		if ( ( bed >= -100 ) && ( bed < -20. ) )
	//			bed = 200.;
	//		else
	//			bed = 100. ;
	//	}
	//}
	
	//if ( rad < 5100 ) {
	//	if ( ( rad >= 4450 ) && ( rad < 5050 ) ) {
	//		if ( ( rad < 4550 ) || ( rad > 4950 ) )                 
	//			bed = 5. ;
	//		else
	//			bed = 5. ;
	//		if ( (  x > - 550  ) && ( x < 550  ) && ( y < 0. ) )
	//		if ( rad < 4550 )  bed = 100. ;
	//		if ( ( fabs( x - 500 ) < 50 ) || ( fabs( x + 500 ) < 50 ) )
	//		if( y < 0. )   bed = 5. ;
	//	} else {
	//		bed = 100. ;
	//	}
	//}
	return bed;
}

double source_run_up(double __attribute__((unused))x, double y)
{
	double bottom = -y / 10.;
	double bed = bottom;
	return bed;
}

double source_shelf(double xp, double yp)
{
/*	int iM = 1;
	int jM = 1;
	double x2 = btablex[iM][jM];
	double y2 = btabley[iM][jM];
	int bdone = 0;
	double bed;
	while ((!bdone)) {
		if (x2 < xp) {
			jM++;
			x2 = btablex[iM][jM];
		} else {
			double x1 = btablex[iM][jM - 1];
			x2 = btablex[iM][jM];

			if (x1 > xp) {
				printf("Trouble: x1(= %le) > xp(= %le)\n",x1, xp);
				exit(EXIT_FAILURE);
			}

			if (y2 < yp) {
				iM++;
				y2 = btabley[iM][jM];
			} else {
				y2 = btabley[iM][jM];
				double y1 = btabley[iM - 1][jM];
				if (y1 > yp) {
					printf("Trouble: y1 > yp\n");
					printf("jM = %d , y1 = %le, y2=%le, yp = %le\n", jM, y1, y2, yp);
					printf("iM = %d , x1 = %le, x2=%le, xp = %le\n", iM, x1, x2, xp);
					exit(EXIT_FAILURE);
				}

				double b1 = btableb[iM - 1][jM - 1];
				double b2 = btableb[iM - 1][jM];
				double b3 = btableb[iM][jM];
				double b4 = btableb[iM][jM - 1];
				bed = b1 * ((xp - x2) / (x1 - x2)) * ((yp - y2) / (y1 - y2))
				            + b2 * ((xp - x1) / (x2 - x1)) * ((yp - y2) / (y1 - y2))
				            + b3 * ((xp - x1) / (x2 - x1)) * ((yp - y1) / (y2 - y1))
				            + b4 * ((xp - x2) / (x1 - x2)) * ((yp - y1) / (y2 - y1)); // bi-linear  interpolation
				bdone = 1;
			}
		}
	}

	if (!bdone) {
		printf("node %f %f : Bed not assigned!!\n", xp, yp);
		exit(EXIT_FAILURE);
	}
	return bed;
*/
// Idelized bathymetry

	double z_ex[5],x_ex[5],z_m[6],x_m[6];

	z_ex[0] = 0.0;
	z_ex[1] = 0.0;
	z_ex[2] = 0.5;
	z_ex[3] = 1.0;
	z_ex[4] = 1.0;

	x_ex[0] = -5.0;
	x_ex[1] = 10.0;
	x_ex[2] = 17.0;
	x_ex[3] = 32.5;
	x_ex[4] = 45.0;

	int DIM=5;
	double h0 = interp1(x_ex, z_ex, xp, DIM);
	double ycb = 13.0;
	double yst = yp + 13.0;
	double dist = 1.0 - MIN(1.0, fabs(yst-ycb)/ycb);
 
 	z_m[0] = 0.0;	
 	z_m[1] = 0.0;	
 	z_m[2] = 0.7+0.05*(1.0-dist);	
 	z_m[3] = 0.75;	
 	z_m[4] = 1.0;	
 	z_m[5] = 1.0;

	x_m[0] = -5.0;	
	x_m[1] = 10.0;	
	x_m[2] = 12.5+12.4999*(1.0-dist);	
	x_m[3] = 25.0;	
	x_m[4] = 32.5;	
	x_m[5] = 45.0;

	DIM=6;
	double z0 = interp1(x_m, z_m, xp, DIM);
	double x_s = 17.0;
	double r_s = 3.0;
	double h_s = 0.45;
	double h_shoal = 0.0;

	double r_loc = sqrt((xp-x_s)*(xp-x_s)+yp*yp);
	if(r_loc<r_s) 
		h_shoal=h_s*(1.0-r_loc/r_s);
	
	double bed = MAX(z0,h0)-0.78+h_shoal;

	return bed;		

}
double source_cylinder(double __attribute__((unused))x, double __attribute__((unused))y)
{
	double radious=0.16/2.0;
	double xc=8.5;
	double yc=0.275;

	double bed = -0.15;
	if(sqrt( pow(x-xc,2) +pow(y-yc,2)) <=radious) 
		//bed = -0.05;
		bed = 1.5;
//	if(x>=8.5-radious && x<=8.5+radious)
//		bed=1.5;

	return bed;
}

double source_biarritz(double __attribute__((unused))x, double __attribute__((unused))y)
{
	double bed = -50.;
	return bed;
}

double source_solitary(double __attribute__((unused))x, double __attribute__((unused))y)
{
	double bed = -1.;// -0.45; //-0.4; //-1.;
	return bed;
}

double source_convergent_channel (double __attribute__((unused))x, double __attribute__((unused))y)
{
	double bed = -1.;
	return bed;
}

double source_gironde (double __attribute__((unused))x, double __attribute__((unused))y)
{
	return 0;
}

double source_rip_channel (double x, double y)
{
	double bed;	
	if (x - 7.0 <= 0.0000001)
        	bed = - 0.5;
        else if ((x - 7.0 > 0.0000001) && (x - 25.0 < 0.0000001))
                bed = 0.1 - (25. - x)/30.*(1 + 3*exp(-(25. - x)/3.)*pow(cos((3.14*(15. - y))/30.),10));
        else
                bed = 0.1 - (25. - x)/30.;
//        	bed = - 0.5;
	return bed;
}

double source_synolakis (double x, double y)
{
	double bed;	
	//if (x <=19.85 +20)
	if (x <=19.85 )
        	bed = (-1.0/19.85)*x;// + 20./19.85;
        else
                bed = -1.0;
	
	return bed;
}

double source_grilli (double x, double y)
{
	double bed;	
	if (x >= 0.)
        	bed = 1.0/34.7 * x - 0.44;
        else
                bed = - 0.44;
	
	return bed;
}

double source_volker_reef (double x, double y)
{
	double bed;	
	if (x<=25.9 )
        	bed = -2.5;
        else if (x>25.9 && x<=56.6)
                bed = 1./12.*x - 4.658;
        else if (x>56.6 && x<=57.85)
                bed = 0.06;
        else if (x>57.85 && x<=60.95)
                //bed = -1./12.*x + 4.825 + 0.06;
		bed = -0.0667*x + 3.922;
	else
		bed = -0.145;
	
/*	if (x<=26.9+16.3)
		bed = 1./12.*tanh(x-(26.9+16.3)) - 1./12.*tanh(-1000000) - 2.5 + 30./9.*pow(10,-4);
        else if (x>26.9+16.3 && x<=56.6+16.3)
                bed = 1./12.*(x-16.3) - 4.658;
        else if (x>56.6+16.3 && x<=57.85+16.3)
                bed = 0.06;
        else if (x>57.85+16.3 && x<=60.95+16.3)
                //bed = -1./12.*x + 4.825 + 0.06;
		bed = -0.0667*(x-16.3) + 3.922;
	else
		bed = -0.145;
*/
	return bed;
}			

double source_bar (double x, double y)
{
	double bed;	
	if(x>6.0 && x<12.) 
		bed=(1.0/20.0)*x-0.3003-0.4;
	else if(x>=12. && x<=14.0) 
		bed=0.3-0.4;
	else if (x>14.0 && x<=17.0) 
		bed=-(1.0/10.0)*x+1.7-0.4;
	else
		bed = -0.4;

/*	if(y>6.0 && y<12.) 
		bed=(1.0/20.0)*y-0.3003-0.4;
	else if(y>=12. && y<=14.0) 
		bed=0.3-0.4;
	else if (y>14.0 && y<=17.0) 
		bed=-(1.0/10.0)*y+1.7-0.4;
	else
		bed = -0.4;
*/

//		bed = -0.4;
	return bed;
}			

double source_hansen (double x, double y)
{
	double bed;	
	if(x<=0.0 ) 
		bed=-0.36;
	else 
		bed=(1.0/34.26)*x -0.36;

	return bed;
}			

double source_shelf_1d (double x, double y)
{
	double bed;	
	if(x>=130.0 && x<140.0)
		bed=1.0/20.0*x -15.0/2.0;
	else if (x>=140.0)
		bed=-0.5;
	else
		bed=-1.0;

	return bed;
}			
double source_shoaling (double x, double y) //shoaling lannes
{
	double bed;	
	if(x<15.04) 
		bed=-0.218;
	else if(x>=15.04 && x<=19.4) 
		bed=(1.0/53.0*x) - 15.04/53.0 - 0.218;
	else if (x>19.4 && x<=22.33) 
		bed=(1.0/150.0)*x +(4.36/53.0 - 19.4/150.0) - 0.218;
	else
		bed = (1.0/13.0)*x+ (2.93/150.0 + 4.36/53.0 - 22.33/13.0) - 0.218;

	return bed;
}

double source_elliptic_shoal (double x, double y)
{
	double bed;	
        double GGx = x * cos(20 * pi / 180.) - y * sin(20 * pi / 180.);
        double GGy = x * sin(20 * pi / 180.) + y * cos(20 * pi / 180.);
	bed = -0.45;

        if (GGy >= -5.82) {
                if (y <= 12.)
                        bed += (5.82 + GGy) / 50.;
                else
                        bed += (5.82 + x * sin(20. * pi / 180.) + 12. * cos(20. * pi / 180.)) / 50.;
        }

        if (GGx * GGx / 16. + GGy * GGy / 9. <= 1.)
                bed += -0.3 + 0.5 * sqrt(1. - GGx * GGx / 25. - GGy * GGy / 14.0625);

/*	double bed;	
        double GGx = y * cos(20 * pi / 180.) - x * sin(20 * pi / 180.);
        double GGy = y * sin(20 * pi / 180.) + x * cos(20 * pi / 180.);
	bed = -0.45;

        if (GGy >= -5.82) {
                //if (x <= 12.)
                        bed += (5.82 + GGy) / 50.;
                //else
                //        bed += (5.82 + y * sin(20 * pi / 180.) + 12. * cos(20 * pi / 180.)) / 50.;
        }

        if (GGx * GGx / 16. + GGy * GGy / 9. <= 1.)
                bed += -0.3 + 0.5 * sqrt(1 - GGx * GGx / 25. - GGy * GGy / 14.0625);
*/
	return bed;
}

double source_circ_shoal( double x, double y)
{
	double GG = sqrt(y * (6.096 - y));
	double bed = -0.4572;

	if (x >= 10.67 +10 - GG) {
		if (x >= (18.29 + 10.) - GG)
			bed = -0.1524;
		else
			bed = -0.4572 - (10.67+10. - GG - x ) / 25.;
	}

	/* node[noden].bed =  depth0 ; */

	/* Mesh size setting */
	/*
	   if ( x < 7.67 - GG ) node[noden].bed =  0.1  ;
	   if ( x >= 17.29 - GG ) node[noden].bed =  0.05  ;
	   if ( ( x >= 7.67 - GG ) && ( x < 17.29 - GG ) ) node[noden].bed = 0.1 + ( 0.05 - 0.1 )*( x - 7.67 + GG )/( 17.29 - 7.67 ) ;
	   if ( x > 28. ) node[noden].bed =  0.05 +  0.2*( x - 28. )*( x - 28. )/38.  ; */

	/* Mesh size setting (for S. Blaise: (0.25--0.5) ) */
/*
	 if ( x < 7.67 - GG ) node[noden].bed =  0.5  ;
	 if ( x >= 17.29 - GG ) node[noden].bed =  0.25  ;
	 if ( ( x >= 7.67 - GG ) && ( x < 17.29 - GG ) ) node[noden].bed = 0.5 + ( 0.25 - 0.5 )*( x - 7.67 + GG )/( 17.29 - 7.67 ) ;
	 if ( x > 28. ) node[noden].bed =  0.25 +  0.3*( x - 28. )*( x - 28. )/38.  ;*/

	 return bed;

}

void source_wavegen( int noden )
{
	double A0, tau, omega, Xs, x;
	double li, depth0 ;

	if (81 ==initial_state){ // elliptic shoaling
		depth0 = 0.45;
		A0 = 0.0194;//0.023;//0.0232; // amplitude
		tau = 1.0; // period 
		omega = 2*pi/tau; // frequency 

		li = sqrt(gm*depth0)*tau;
		Xs = -13.; // generation zone position
		//Xs = -7.; // generation zone position
		x = node[noden].coordinate[1];
		//x = node[noden].coordinate[0];

		double delta = 0.4; // generation zone semi-amplitude
		double K = 2.*pi/li;
		double bs = 80./(delta*delta*li*li);
		double alpha = -1.159/3.;
                double a1=-(1.159-1.0)/3.0;
		double I1 = exp(-K*K/(4.*bs))*sqrt(pi/bs);
		double D = 2.*A0*(omega*omega - a1*gm*pow(K,4)*pow(depth0,3))/(omega*I1*K*(1. - alpha*pow(K*depth0,2)));

		//node[noden].wg = D*sin(-omega*Time)*exp(-bs*(x-Xs)*(x-Xs));
		node[noden].wg = D*sin(-omega*Time)*exp(-bs*(x-Xs)*(x-Xs));

	} else if (75 == initial_state ) { // rip channel
		int ireg=0; //0 for regular 1 for irregular
		if (ireg==0 ){
			depth0 = 0.5;
			A0 = 0.028;//0.035; // Amplitude 
			tau = 1.25; // period 
			omega = 2*pi/tau; // frequency 
	
			li = sqrt(gm*depth0)*tau;
			Xs = 3.; // generation zone position
			x = node[noden].coordinate[0] ;
			double delta = 0.3; // generation zone semi-amplitude
			double K = 2.*pi/li;
			double bs = 80./(delta*delta*li*li);
			double alpha = -1.159/3.;
                	double a1=-(1.159-1.0)/3.0;
			double I1 = exp(-K*K/(4.*bs))*sqrt(pi/bs);
			double D = 2.*A0*(omega*omega - a1*gm*pow(K,4)*pow(depth0,3))/(omega*I1*K*(1. - alpha*pow(K*depth0,2)));

			node[noden].wg = D*sin(-omega*Time)*exp(-bs*(x-Xs)*(x-Xs));
		}else{
			depth0=0.5;
			Xs=3.0;
			
			double alpha = -1.159/3.;
                	double a1=-(1.159-1.0)/3.0;
			double ll=4.0; // peak wave length
			double delta=0.4;
			double wsize=delta*ll/2.0;
			double bbs = 80./(delta*delta*ll*ll);
 			double K,omega,I1,D;
			x = node[noden].coordinate[0] ;

			if (x >= Xs-wsize && x<= Xs+wsize){
				node[noden].wg = 0.0;
				for( int i=0; i<=freq-1; i++){
					K=2.0*pi/l_wg[i];
					omega=2.0*pi/t_wg[i];
					I1=exp(-K*K/(4.*bbs))*sqrt(pi/bbs);
					D=2.0*a_wg[i]*(omega*omega-a1*gm*pow(K,4)*pow(depth0,3))/(omega*I1*K*(1.0 -alpha*pow(K*depth0,2)));

					node[noden].wg += D*cos(omega*(Time)+erand[i]);
					//printf("%d %d %le %le %d--\n",noden,i,D,erand[i],freq);
				}
				node[noden].wg *= exp(-bbs*(x-Xs)*(x-Xs));
				
			}else{
				node[noden].wg=0.0;
			}
		}
	} else if (92 == initial_state ) { // biarritz case
/*		// generation line
		double x1 = 8735.26;
		double y1 = 6833.7;
		double x2 = 3143.9655;
		double y2 = 99.1965;
		double lamda = (y2-y1)/(x2-x1);
		double gamma = -y1+lamda*x1;
		double phi = tan(lamda);

		depth0=50.0;
		Xs = lamda*node[noden].coordinate[0] - node[noden].coordinate[1] + gamma; 
			
		double alpha = -1.159/3.;
                double a1=-(1.159-1.0)/3.0;
		double ll=4.0; // peak wave length
		double delta=0.4;
		double wsize=delta*ll/2.0;
		double bbs = 80./(delta*delta*ll*ll);
 		double K,omega,I1,D;
		double distance = fabs(lamda*node[noden].coordinate[0]-node[noden].coordinate[1]+gamma)/sqrt(lamda*lamda+1);

		x = node[noden].coordinate[0] ;
		y = node[noden].coordinate[0] ;
		
		

		if (distance <= wsize){
			node[noden].wg = 0.0;
			for( int i=0; i<=freq*angle-1; i++){
				K=2.0*pi/l_wg[i];
				double kx =  cos(phi)*K;
				double ky = -sin(phi)*K;
				omega=2.0*pi/t_wg[i];
				I1=exp(-K*K/(4.*bbs))*sqrt(pi/bbs);
				D=2.0*a_wg[i]*(omega*omega-a1*gm*pow(K,4)*pow(depth0,3))/(omega*I1*K*(1.0 -alpha*pow(K*depth0,2)));

				node[noden].wg += D*cos(omega*Time-K*cos(direction(i)*x));
					//printf("%d %d %le %le %d--\n",noden,i,D,erand[i],freq);
			}
			node[noden].wg *= exp(-bbs*distance*distance);
				
		}else{
			node[noden].wg=0.0;
		}*/
	} else {
		double time_source = Time;//-dt;
	
		depth0 = 0.4572;//0.4;//0.4572;// 0.45;//0.5;//.4;
		A0 = 0.021;//0.021;//0.0195;//0.0066;//0.0195;//0.0075;//0.0913;//0.011;//0.0215;//0.01950;//0.0075;//0.0068;//0.0195;//0.035;//0.023 ; /* Amplitude */
		tau = 1.0;//1.0;//3.0;//1.0;//2.0;//2.02;//1.0;//2.0;//3.0;//2.5;//1.25;// 2.525 ;      /* period */
		omega = 2*pi/tau;    /* frequency */
	
		//ai = 0.1 ;
		li = 1.4941;//6.1784;//1.4941;//3.9077;//3.7512;//sqrt(gm*depth0)*tau;//3.6364;//4.3648;//1.4941;//3.9077;//6.1784;//1.4941;//4.7904;//sqrt(gm*depth0)*tau;
		//di = 0.5*ai*li/sqrt(20.0) ;
		//Bi = 1./( di*di ) ;
		//betai = 1 ;
	
		Xs=6.0;//-4.0 +10.;//0.0;//-14.78-0.36;//-4.;//-13.;//0.0;//40.;//15. ; // generation zone position
		x = node[noden].coordinate[0] ;
		//x = node[noden].coordinate[1] ;
		double delta = 0.8;//0.5;//0.11;//0.17;//0.5; // generation zone semi-amplitude
		double wsize= delta*li/2.0;
		if(x>=Xs-wsize && x<= Xs+wsize ) {
// For rotated domains
/*			y = node[noden].coordinate[1] ;
			double theta=pi/4.0;
			double x1=30.0;
			double y1=0.0;
			double x2=30.0;
			double y2=0.4;
			double xn1=cos(theta)*x1-sin(theta)*y1;
			double yn1=sin(theta)*x1+cos(theta)*y1;
			double xn2=cos(theta)*x2-sin(theta)*y2;
			double yn2=sin(theta)*x2+cos(theta)*y2;
	               	double l1=(yn2-yn1)/(xn2-xn1); // the coef of the line e1 that passes from xn1,yn1 xn2,yn2
			double b=-1.0;
			double c1=-l1*xn1+yn1;
        	        // coeff of the generation line 
			dis=fabs(l1*x+b*y+c1)/sqrt(pow(l1,2)+1); //distance of x,y from the generation line
		
//			node[noden].wg = -betai*li*exp(-Bi*(x - Xs)*(x - Xs))*( A0/depth0 )*sin(omega*Time);


			//Bi = 0.46;
			//node[noden].wg = 2*sqrt(gm*depth0)*Bi/sqrt(3.14)*exp(-Bi*(x - Xs)*(x - Xs))*A0*cos(omega*Time); // per sistemare la fase basta modificare l'argomento del coseno
*/		
			double K = 2.*pi/li;
			double bs = 80./(delta*delta*li*li);
			double alpha = -1.159/3.;
       		         double a1=-(1.159-1.0)/3.0;
			double I1 = exp(-K*K/(4.*bs))*sqrt(pi/bs);
			double D = 2.*A0*(omega*omega - a1*gm*pow(K,4)*pow(depth0,3))/(omega*I1*K*(1. - alpha*pow(K*depth0,2)));
	
			node[noden].wg = D*sin(-omega*time_source)*exp(-bs*(x-Xs)*(x-Xs));
			//node[noden].wg = D*sin(-omega*Time)*exp(-bs*(dis)*(dis)); //for rotated domains
		}else{
			node[noden].wg = 0.0;
		}
	}
} 

