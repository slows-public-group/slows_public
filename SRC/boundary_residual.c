/*  Copyright (C) 2021   M.Ricchiuto  <mario.ricchiuto@inria.fr>
                         M. Kazolea   <maria.kazolea@inria.fr>
                         A. Filippini <a.filippini@brgm.fr>
                         L. Arpaia    <luca.arpaia@brgm.fr>
                         N.Pattakos   <pattakosn@fastmail.com> 

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.*/

/***************************************************************************
boundary_residual.c
-------------------
  This is boundary_residual: it drives the computation of the additional
	residual coming from the boundary conditions using either a weak
 approach (nodal Steger-Waeming flux) or a strong Dirichlet imposition
-------------------
***************************************************************************/
#include <math.h>
#include "common.h"

extern const int size, face_q_pts;
extern int NBF, initial_state;

double Area_inlet_do, Area_inlet_ga;
extern double gridsize, dt, boundary_flux;
extern double temp_normal[2+1], *temp_vector, *FLUX;

extern struct node_struct *node;
extern struct boundary_struct *boundary;
extern struct boundary_nodes_struct *b_nodes;
extern struct numerical_int_struct numerical_int;

extern void (*supersonic_residual) (int);	/* Face global index */
extern void (*char_bc) (int);


void ad_flux_P(double *, double *, double);
void periodicity(int);	/* Face global index */
void first_streamline(int);
void compute_section_area_garonne(int);
void compute_section_area_dordogne(int);
void compute_section_area_gironde(int);

void boundary_conditions(void)
{
	if (initial_state == 72){ // Gironde
		Area_inlet_ga = 0.;
		Area_inlet_do = 0.;
		for (int f = 0 ; f < NBF ; f++){ // computation of the crossectional area referred to the inlet/outlet nodes
			if (b_nodes[f].type == 11 && node[b_nodes[f].node].coordinate[1] < 309000.)
				compute_section_area_gironde( f );
		}
	} else if (initial_state == 73) { // Garonne
		Area_inlet_ga = 0.;
		for (int f = 0 ; f < NBF ; f++) // computation of the crossectional area referred to the inlet/outlet nodes
			if (b_nodes[f].type == 11 && node[b_nodes[f].node].coordinate[1] < 290000.)
				compute_section_area_garonne( f );
	} else if (initial_state == 74) { // Dordogne
		Area_inlet_do = 0.;
		for (int f = 0 ; f < NBF ; f++) // computation of the crossectional area referred to the inlet/outlet nodes
			if (b_nodes[f].type == 11 && node[b_nodes[f].node].coordinate[0] > 378000.)
				compute_section_area_dordogne( f );
	} else if (initial_state == 79) { // Gironde-Garonne-Dordogne
		Area_inlet_ga = 0.;
		Area_inlet_do = 0.;
		for (int f = 0 ; f < NBF ; f++){ // computation of the crossectional area referred to the inlet/outlet nodes
			if (b_nodes[f].type == 11 && node[b_nodes[f].node].coordinate[1] < 260000.)
				compute_section_area_garonne( f );
			if (b_nodes[f].type == 11 && node[b_nodes[f].node].coordinate[0] > 420000.)
				compute_section_area_dordogne( f );
		}
	} 

	for (int f = 0; f < NBF; f++) {
		/* compute boundary flux */
		int i0 = boundary[f].node[0];
		int i1 = boundary[f].node[1];
		temp_normal[0] = -(node[i0].coordinate[1] - node[i1].coordinate[1]);
		temp_normal[1] = -(node[i1].coordinate[0] - node[i0].coordinate[0]);
		double rr = sqrt(temp_normal[0] * temp_normal[0] + temp_normal[1] * temp_normal[1]) / gridsize;

		for (int q = 0; q < face_q_pts; q++) {
			for (int i = 0; i < size; i++)
				temp_vector[i] = numerical_int.face_coordinate[q][0] * node[i0].P[0][i] + numerical_int.face_coordinate[q][1] * node[i1].P[0][i];

			ad_flux_P(temp_normal, temp_vector, rr);

			boundary_flux -= 0.5 * dt * numerical_int.face_weight[q] * FLUX[0];

			for (int i = 0; i < size; i++)
				temp_vector[i] = numerical_int.face_coordinate[q][0] * node[i0].P[2][i] + numerical_int.face_coordinate[q][1] * node[i1].P[2][i];

			ad_flux_P(temp_normal, temp_vector, rr);

			boundary_flux -= 0.5 * dt * numerical_int.face_weight[q] * FLUX[0];
		}

		/* computing boundary residual */
		//n = b_nodes[f].node;
		if (initial_state == 5 || initial_state == 9) {
			first_streamline(f);
		} else {
			if (10 == b_nodes[f].type) { // Periodic BCs
				//(1)MUST CAREFULLY CHECK "char_bc" depending on the testcase usable for sub-critical in/outlests as well
				//(2)For "periodicity" MAKE SURE that the gridfile contains the correct correspondence between periodic boundary nodes (node.mate)
				//(3)"first_streamline" is a strong wall BC a weak condition is easily obtained by minor modification of "char_bc"                              
				if (  (initial_state == 25) || (initial_state == 8) || (initial_state == 30)
				    ||(initial_state == 31)  )
					char_bc(f);
				else
					periodicity(f);
			} else if (11 == b_nodes[f].type) {
				char_bc(f);
			} else if (1 == b_nodes[f].type) { // Supersonic inlet
				if (initial_state == 13)
					first_streamline(f);
				else if ( (initial_state == 8) || (initial_state == 24) || (initial_state == 25) || (initial_state == 30)
				         || (initial_state == 31) )
					char_bc(f);
				else
					supersonic_residual(f);
			} else if (2 == b_nodes[f].type) {
				char_bc(f);
			} else if (3 == b_nodes[f].type || 20 == b_nodes[f].type) { // Strong Wall-BC
				first_streamline(f);
			}
		}
	}

}
