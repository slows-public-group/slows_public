/*  Copyright (C) 2021   M.Ricchiuto  <mario.ricchiuto@inria.fr>
                         M. Kazolea   <maria.kazolea@inria.fr>
                         A. Filippini <a.filippini@brgm.fr>
                         L. Arpaia    <luca.arpaia@brgm.fr>
                         N.Pattakos   <pattakosn@fastmail.com> 

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.*/


/***************************************************************************
memory_allocation.c
------------------
                          This is memory_allocation:
	any vector or matrix used in the code was born here
--------------------
***************************************************************************/
#include <stdio.h>
#include "common.h"
#include "my_malloc.h"

/* SIZE of the system of equations */
const int size = 3;

/* Number of VERTICES of each (spatial) sub-element */
const int vertex = 3;

/* Interpolation-Order Dependent Constants */
const int face_q_pts = 2;
const int volume_q_pts = 6;

int NE;
int NBF;
int NBF2;
int NBF3;
int NNX;
int NN;
int NNZ;
int btableimax;
int btablejmax;
int time_bc_pts;
int blending;
int initial_state;
int *is_x;
int *is_y;
int *is_e;
double *x;
double *y;
double *dx;
double *dy;
double *x_start;
double *y_start;
double *x_oldold;
double *y_oldold;
double vel[2];
double *work_vector;
double *work_vector0;
double *work_vector1;
double *work_vector2;
double *thetaM;
double *temp_vector;
double temp_normal[2+1];
double *P_bar;
double m_normal[2+1];
double *Lambda;
double *Jacobian;
double *phi_a_w0;
double *phi_a_w1;
double *phi_bed0;
double *phi_bed1;
double betaS[3];
double *h_bc;
double *time_bc;
double *phi_node;
double *phi_w;
double *FLUX;
double B[3];
double *F_loc;
double *U_C;
double *work;
double *PHI_loc;
double *phi1_w;
double *FV_ubar0;
double *FV_vbar0;
double *FV_height0;
double *FV_speed_of_sound0;
double *FV_sigma_barx;
double *FV_sigma_bary;
double *INTrec;
double **Right;
double **Left;
double **sum_K;
double **sum_K_1;
double X_temp[2];
double **dU;
double **K;
double **temp_mat1;
double **temp_mat;
double **btablex;
double **btabley;
double **btableb;
double **W;
double **PHI_d;
double **PHIN_d;
double **PHIN_scal;
double **Z;
double ***K_i;
double ***K_i_p1;
long int *ipiv;
struct element_struct *element;
struct node_struct *node;
struct boundary_struct *boundary;
struct numerical_int_struct numerical_int;
struct boundary_nodes_struct *b_nodes;

extern double **katz, **M2_total;
extern double *max_grad;

void memory_allocation(void)
{
	/* memory allocation for structure: element */
	element = MA_vector( NE + 1, sizeof(struct element_struct) );
	for (int e = 0; e < NE + 1; e++) {
		if (blending >= 2){
			element[e].FVnormal = (double**) MA_matrix(3, 2, sizeof(double) );
			element[e].cg       = MA_vector(2, sizeof(double) );
			element[e].mp	    = (double**) MA_matrix(3, 2, sizeof(double) );
			element[e].weight   = (double**) MA_matrix(3, 3, sizeof(double) );
		}
	}

    /* memory allocation for structure: node */
	node = MA_vector( NN + 1, sizeof(struct node_struct) );
	for (int i = 0; i < NN + 1; i++) {
		node[i].P              = (double**) MA_matrix(3, size, sizeof(double));
		node[i].Z              = (double**) MA_matrix(3, size, sizeof(double));
		node[i].K              = (double**) MA_matrix(3, size, sizeof(double));
		node[i].dxP            = (double**) MA_matrix(2, size, sizeof(double));
		node[i].dyP            = (double**) MA_matrix(2, size, sizeof(double));
		if (blending >= 2){
			node[i].dxP2       = (double**) MA_matrix(2, size, sizeof(double) );
			node[i].dyP2       = (double**) MA_matrix(2, size, sizeof(double) );
			node[i].dxxP2      = (double**) MA_matrix(2, size, sizeof(double) );
			node[i].dxyP2      = (double**) MA_matrix(2, size, sizeof(double) );
			node[i].dyxP2      = (double**) MA_matrix(2, size, sizeof(double) );
			node[i].dyyP2      = (double**) MA_matrix(2, size, sizeof(double) );
			node[i].DcC        = MA_vector(2, sizeof(double) );
			node[i].ss_DcC        = MA_vector(2, sizeof(double) );
			node[i].corgrad    = MA_vector(4, sizeof(double) );
			node[i].corsecgrad = MA_vector(9, sizeof(double) );
		}
		node[i].friction       = (double**) MA_matrix(3, size, sizeof(double) );
		node[i].PHI_bar        = (double**) MA_matrix(3, size, sizeof(double) );
		node[i].Res            = MA_vector(size, sizeof(double) );
		node[i].Res1           = MA_vector(size, sizeof(double) );
		node[i].Res2           = MA_vector(size, sizeof(double) );
	}

	/* memory allocation for structure:boundary */
  	boundary = MA_vector( NBF, sizeof(struct boundary_struct) );
	for (int i = 0; i < NBF; i++) {
		boundary[i].node   = MA_vector( NBF, sizeof(int) );
		boundary[i].types  = MA_vector( NBF, sizeof(int) );
	}
	b_nodes = MA_vector( NBF, sizeof(struct boundary_nodes_struct) );

	/* numerical_integration structure */
  	numerical_int.face_coordinate  = (double**) MA_matrix(face_q_pts, 2, sizeof(double) );
	numerical_int.face_weight      = MA_vector(face_q_pts, sizeof(double) );
	numerical_int.volume_coordinate= (double**) MA_matrix(volume_q_pts, 3, sizeof(double) );
	numerical_int.volume_weight    = MA_vector(volume_q_pts, sizeof(double) );

	/* vectors and matrices */
	temp_vector  = MA_vector( size, sizeof(double) );
	work_vector  = MA_vector( size, sizeof(double) );
	work_vector0 = MA_vector( size, sizeof(double) );
	work_vector1 = MA_vector( size, sizeof(double) );
	work_vector2 = MA_vector( size, sizeof(double) );
	P_bar        = MA_vector( size, sizeof(double) );
	Lambda       = MA_vector( size, sizeof(double) );
	phi_node     = MA_vector( size, sizeof(double) );
	thetaM       = MA_vector( size, sizeof(double) );
	h_bc         = MA_vector( time_bc_pts, sizeof(double) );
	time_bc      = MA_vector( time_bc_pts, sizeof(double) );
	phi_a_w0     = MA_vector( size, sizeof(double) );
	phi_a_w1     = MA_vector( size, sizeof(double) );
	phi_bed0     = MA_vector( size, sizeof(double) );
	phi_bed1     = MA_vector( size, sizeof(double) );
	FLUX         = MA_vector( size, sizeof(double) );
	PHI_loc      = MA_vector( size, sizeof(double) );
	F_loc        = MA_vector( size, sizeof(double) );
	work         = MA_vector( size, sizeof(double) );
	phi_w        = MA_vector( size, sizeof(double) );
	phi1_w       = MA_vector( size, sizeof(double) );
	U_C          = MA_vector( size, sizeof(double) );
	ipiv         = MA_vector( size, sizeof(long int) );
	PHI_d        =(double**) MA_matrix(3, size, sizeof(double) );
	PHIN_d       =(double**) MA_matrix(3, size, sizeof(double) );
	PHIN_scal    =(double**) MA_matrix(3, size, sizeof(double) );
	btablex      =(double**) MA_matrix(btableimax, btablejmax, sizeof(double) );
	btabley      =(double**) MA_matrix(btableimax, btablejmax, sizeof(double) );
	btableb      =(double**) MA_matrix(btableimax, btablejmax, sizeof(double) );
	Right        =(double**) MA_matrix(size, size, sizeof(double) );
	Left         =(double**) MA_matrix(size, size, sizeof(double) );
	sum_K        =(double**) MA_matrix(size, size, sizeof(double) );
	sum_K_1      =(double**) MA_matrix(size, size, sizeof(double) );
	Jacobian     =MA_vector(size * size, sizeof(double) );
	temp_mat     =(double**) MA_matrix(size, size, sizeof(double) );
	temp_mat1    =(double**) MA_matrix(size, size, sizeof(double) );
	W            =(double**) MA_matrix(3, size, sizeof(double) );
	dU           =(double**) MA_matrix(3, size, sizeof(double) );
	Z            =(double**) MA_matrix(3, size, sizeof(double) );
	K            =(double**) MA_matrix(3, size, sizeof(double) );
	K_i          =(double***) MA_tensor(3, size, size, sizeof(double) );
	K_i_p1       =(double***) MA_tensor(3, size, size, sizeof(double) );
	if (blending >= 2) {
		FV_ubar0           = MA_vector(3, sizeof(double) );
		FV_vbar0           = MA_vector(3, sizeof(double) );
		FV_height0         = MA_vector(3, sizeof(double) );
		FV_speed_of_sound0 = MA_vector(3, sizeof(double) );
		FV_sigma_barx      = MA_vector(3, sizeof(double) );
		FV_sigma_bary      = MA_vector(3, sizeof(double) );
		katz               = (double**) MA_matrix(12 , NN, sizeof(double) );
		M2_total           = (double**) MA_matrix(NN , 12, sizeof(double) );
                INTrec             = (double *) MA_vector( NN, sizeof(double) );
	}


	if ( ( 71 == initial_state ) || ( 85 == initial_state) ) { // Convergent channel
		NNX = (NBF - 5) / 4 - 1; //1499
		printf("You are running the tidal estuary test case:\n") ;
		printf("  Storing array for max grad. reconstruction\n") ;
		printf("                           size: %d\n", NNX) ;
		printf("Attention: valid only for constant mesh size\n") ;
		printf("\n") ;     
		max_grad = MA_vector(NNX, sizeof(double) );
	}
}
