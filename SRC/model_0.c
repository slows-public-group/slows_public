/*  Copyright (C) 2021   M.Ricchiuto  <mario.ricchiuto@inria.fr>
                         M. Kazolea   <maria.kazolea@inria.fr>
                         A. Filippini <a.filippini@brgm.fr>
                         L. Arpaia    <luca.arpaia@brgm.fr>
                         N.Pattakos   <pattakosn@fastmail.com> 

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.*/
	

/***************************************************************************
model_0.c
---------
This is model_0: it contains all the model dependent functions used for
	the solution of the 2D shallow water equations
-------------------
***************************************************************************/
#include <stdio.h>
#include <math.h>
#include "common.h"

extern const int size;
extern int NN, NE, zero_nodes;

extern double gm, height0, ubar0, vbar0, speed_of_sound0;
double h_ref, u_ref;
extern double UUinf, VVinf;
extern double cut_off_H, cut_off_U, coarse_cut_off, sigma_barx, sigma_bary, UUinf, VVinf;
extern double *FLUX, vel[2], *Lambda,  *FLUX;
extern double **PHIN_d, **Right, **Left;
extern struct node_struct *node;
extern struct element_struct *element;
extern struct boundary_nodes_struct *b_nodes;

/* node global index, local residual distributed to node, time level to update (1/2) */
void Eigenvectors(double *);
void Waves(double *);
void P_2_Z(double *, double *, double);
void Z_2_P(double *, double *, double);
void supercritical_solve(double *, double *, double *, double *, double *);
void subcritical_solve(double *, double *, double *, double *, double *);

void ad_flux_P(double *norm, double *P, double ratio)
{
	double u, v;
	if (P[0] > cut_off_U * ratio) {
		u = P[1] / P[0];
		v = P[2] / P[0];
	} else {
		u = v = 0.;
	}

	//rr = P[0]*P[0]*P[0]*P[0] ;
	//if ( rr < cut_off_U*ratio*cut_off_U*ratio )
	//	rr = cut_off_U*ratio*cut_off_U*ratio ;
	//u = sqrt( 2. ) * P[0]*P[1]/( sqrt( P[0]*P[0]*P[0]*P[0] + rr ) ) ;
	//v = sqrt( 2. ) * P[0]*P[2]/( sqrt( P[0]*P[0]*P[0]*P[0] + rr ) ) ;

	// Flux in Conservative Variables
	//FLUX[0] = P[1]*norm[0] + P[2]*norm[1];
	FLUX[0] = u * P[0] * norm[0] + v * P[0] * norm[1];
	FLUX[1] = u * FLUX[0];	// + 0.5*P[0]*P[0]*gm*norm[0] ;
	FLUX[2] = v * FLUX[0];	// + 0.5*P[0]*P[0]*gm*norm[1] ;
}

void ad_flux_Z(double *norm, double *P)
{
	// Flux in Physical Variables
	FLUX[0] = P[0] * (P[1] * norm[0] + P[2] * norm[1]);
	FLUX[1] = P[1] * FLUX[0] + 0.5 * P[0] * P[0] * gm * norm[0];
	FLUX[2] = P[2] * FLUX[0] + 0.5 * P[0] * P[0] * gm * norm[1];
}

void P_2_Z(double *P, double *Z, double ratio)
{
	double hdum = P[0];
	double udum, vdum;
	// cut off only on velocity
	if (hdum >= cut_off_U * ratio) {
//	if (hdum >= cut_off_H * ratio) {
		udum = P[1] / hdum;
		vdum = P[2] / hdum;
	} else {
		udum = 0.;
		vdum = 0.;
//		hdum=0.;
	}

	if (hdum < cut_off_H * ratio){
		hdum = 0.;
		udum = 0.;
		vdum = 0.;
	}else{
		hdum=P[0];
		udum = P[1] / hdum;
		vdum = P[2] / hdum;
	}
	//rr = P[0]*P[0]*P[0]*P[0] ;
	//if ( rr < cut_off_U*ratio*cut_off_U*ratio )
	//	rr = cut_off_U*ratio*cut_off_U*ratio ;
	//udum = sqrt( 2. ) * P[0]*P[1]/( sqrt( P[0]*P[0]*P[0]*P[0] + rr ) ) ;
	//vdum = sqrt( 2. ) * P[0]*P[2]/( sqrt( P[0]*P[0]*P[0]*P[0] + rr ) ) ;
	//
	Z[0] = hdum;
	Z[1] = udum;
	Z[2] = vdum;
}

void subcritical_solve(double *mass, double *nrg, double *hh0, double *bed0, double *depth)
{
	double c2h = *bed0 - *nrg;
	double c0h = 0.5 * ((*mass) * (*mass)) / gm;
	double h1 = *hh0;
	double Sh = h1 * h1 * h1 + c2h * h1 * h1 + c0h;
	double div = 1.;
	if (c0h > 1.)
		div = c0h;

	if (fabs(Sh) > 1.e-12) {
		int dodo = 0;
		while (dodo < 301) {
			dodo++;

			double hcr = 2. * (*nrg - *bed0) / 3.;
			h1 = (*nrg - *bed0);

			double hh;
			if (fabs(Sh) / div < 1.e-3) {
				double jac = 3. * h1 * h1 + 2. * c2h * h1;
				hh = h1 - Sh / (jac + 1.e-12);
			} else {
				Sh = h1 * h1 * h1 + c2h * h1 * h1 + c0h;
				double Scr = hcr * hcr * hcr + c2h * hcr * hcr + c0h;
				hh = hcr - Scr * (h1 - hcr) / (Sh - Scr + 1.e-12);
			}

			Sh = hh * hh * hh + c2h * hh * hh + c0h;
			double conv = fabs(Sh);
			hcr = h1;
			h1 = hh;

			if (conv < 1.e-12) //Get out of this loop
				break;
		}
		*depth = h1;
	} else {
		*depth = *hh0;
	}
}

void supercritical_solve(double *mass, double *nrg, double *uu0, double *bed0, double *speed)
{
	double c1u = 2. * gm * (*bed0 - *nrg);
	double c0u = 2. * gm * (*mass);
	double uu1 = *uu0;
	double Su = uu1 * uu1 * uu1 + c1u * uu1 + c0u;
	double div = 1.;
	if (c0u > 1.)
		div = c0u;

	if (fabs(Su) / div > 1.e-12) {
		double ucr = sqrt(2. * gm * (*nrg - *bed0) / 3.);
		uu1 = sqrt(2. * gm * (*nrg - *bed0));

		for (int dodo = 0; dodo < 301; dodo++) {
			double uu;
			if (fabs(Su) / div < 1.e-3) {
				double jac = 3. * uu1 * uu1 + c1u;
				uu = uu1 - Su / (jac + 1.e-12);
			} else {
				double Scr = ucr * ucr * ucr + c1u * ucr + c0u;
				Su = uu1 * uu1 * uu1 + c1u * uu1 + c0u;
				uu = ucr - Scr * (uu1 - ucr) / (Su - Scr + 1.e-12);
			}

			Su = uu * uu * uu + c1u * uu + c0u;
			double conv = fabs(Su) / div;
			ucr = uu1;
			uu1 = uu;

			if (conv < 1.e-12) // Get out of this loop 
				break;
		}
		*speed = uu1;
	} else {
		*speed = *uu0;
	}
}

void K_2_Z(double *K, double *Z, double h0, double b0, double ratio)
{
	double mass = sqrt(K[1] * K[1] + K[2] * K[2]);
	if (mass < 1.e-12) { // no mass
		Z[0] = h0;
		Z[1] = 0.;
		Z[2] = 0.;
	} else { // there is mass
		if (!(h0 > cut_off_U * ratio)) { // there is mass but no depth
			Z[0] = h0;
			Z[1] = 0.;
			Z[2] = 0.;
		} else { // there is mass and water
			double FRloc = (-b0 + K[0]) * (-b0 + K[0]) * (-b0 + K[0]) * 2. * gm / (mass * mass);
			if (FRloc < 27. / 4.) {
				Z[0] = h0;
				Z[1] = K[1] / h0;
				Z[2] = K[2] / h0;
			} else {	//we compute !!
				double uu, uu0, hh;
				if (fabs(FRloc - 27. / 4.) < 0.01) {
					hh = 2. * (-b0 + K[0]) / 3.;
					uu = sqrt(2. * gm * (K[0] - b0) / 3.);
				} else {	//far from no solution region !!
					// need to choose the branch so estimate the Fr number 
					uu0 = 2. * gm * (K[0] - b0 - h0);
					if (uu0 >= 0.)
						uu0 = sqrt(uu0);
					else
						uu0 = mass / h0;

					// printf("FRloc1 = %le\n",FRloc );
					// FRloc =  uu0/sqrt( gm*h0 );
					// printf("FRloc2 = %le\n",FRloc );

					if (FRloc <= 1) {
						subcritical_solve(&mass, &K[0], &h0, &b0, &hh);

						if (hh > cut_off_U * ratio) {
							uu = 2. * gm * (K[0] - b0 - hh);
							if (uu >= 0.)
								uu = sqrt(uu);
							else
								uu = mass / hh;
						} else {
							uu = 0.;
						}
					}
					if (FRloc > 1.) {
						supercritical_solve(&mass, &K[0], &uu0, &b0, &uu);
						hh = K[0] - b0 - 0.5 * uu * uu / gm;
						if (hh < 0.)
							hh = mass / uu;
					}

					if (hh > cut_off_U * ratio) {
						Z[0] = hh;
						Z[1] = uu * K[1] / mass;
						Z[2] = uu * K[2] / mass;
					} else {
						Z[0] = h0;
						Z[1] = 0.;
						Z[2] = 0.;
					}
				}
			}
		}
	}
}

//void K_2_Zlast( double *K, double *Z, double h0, double b0, double FRloc )
//{
//      int i, dodo ;
//      double uu, vv, Su, Sh, uu0, S ;
//      double c1u, c0u, c2h, c0h ;
//      double h1, uu1, hh, conv, jac, mass ;
//      
//              mass = sqrt( K[1]*K[1] + K[2]*K[2] ) ;
//      
//          if ( mass < cut_off_H )
//              {
//                      Z[0] = h0 ;
//                      Z[1] = 0. ;
//                      Z[2] = 0. ;
//              } // end if mass is small
//              else
//              {
//            if ( !( h0 > cut_off_U ) )
//                {
//                        Z[0] = h0 ;
//                        Z[1] = 0. ;
//                        Z[2] = 0. ;
//                }
//                      else
//      {
//        uu0 = FRloc*sqrt( gm*h0 ) ;
//      
//              if ( FRloc < 0 ) { printf("Problem !!!\n") ; 
//                      printf("FRloc=%le\n",FRloc);
//                      printf("K[0],h0,cut_off,b0,K[1],K[2],uu0= %le , %le , %le , %le , %le , %le , %le\n",K[0],h0,cut_off_U,b0,K[1],K[2],uu0) ;
//                      exit(-1) ;}
//              
//              c2h = b0 - K[0] ;
//              c0h = 0.5*( mass*mass )/gm ;
//              
//          c1u = c2h*2.*gm ;
//          c0u = 4.*gm*gm*c0h ;
//              
//              
//              
//              h1  = h0 ;
//              uu1 = uu0 ; 
//              
//              for ( dodo = 0 ; dodo < 3001 ; dodo ++ )
//              {
//                      Su = uu1*uu1*uu1 + c1u*uu1 + c0u ;
//                      Sh = h1*h1*h1 + c2h*h1*h1 + c0h ;
//                      
//                      if ( FRloc < 1. )
//                      {
//                              if ( fabs( Sh ) < 1.e-2 )
//                              {
//                                      jac = 3.*h1*h1 + 2.*c2h*h1 ;
//                                      hh = h1 - Sh/jac ;
//                              }
//                              else
//                              {
//                                      hh = -c0h - c2h*h1*h1 ;
//                                      if ( hh >= 0. ) hh = pow( hh, 1./3. ) ;
//                                      else { hh = -pow( fabs( hh ), 1./3. ) ; }
//                              }
//                              
//                              if ( hh > cut_off_U )
//                              {
//                                      uu = 2.*gm*( K[0] - b0 - hh ) ;
//                                      if ( uu >= 0. ) uu = sqrt( uu ) ;
//                                      else uu = mass/hh ;
//                              }
//                              else uu = uu1 ;
//                              
//                              Sh = hh*hh*hh + c2h*hh*hh + c0h ;
//                              
//                              conv = fabs( hh - h1 ) ;
//                              if ( fabs( Sh ) > conv ) conv = fabs( Sh ) ;
//                              
//                              h1    = hh ;
//                              uu1   = uu ;
//                      }
//                      else 
//                      {
//                              if ( fabs( Su ) < 1.e-2 )
//                              {
//                                      jac = 3.*uu1*uu1 + c1u ;
//                                      uu  = uu1 - Su/jac ;
//                              }
//                              else
//                              {
//                                      uu = -c0u - c1u*uu1 ;
//                                      if ( uu >= 0. ) uu = pow( uu, 1./3. ) ;
//                                      else { uu = -pow( fabs( uu ), 1./3. ) ; }
//                              }
//                              
//                              hh = K[0] - uu*uu*0.5/gm - b0 ;
//                              if ( hh < cut_off_H ) hh = 0. ;
//                              if ( hh < cut_off_U ) uu = 0. ;
//                              Su = uu*uu*uu + c1u*uu + c0u ;
//                          if ( dodo > 0 ) {
//                                      conv = fabs( uu - uu1 ) ;
//                                      if ( fabs( Su ) > conv ) conv = fabs( Su ) ;
//                          }
//                              uu1   = uu ;
//                              h1    = hh ; 
//                      } // END if Fr
//                      if ( conv < 1.e-14 ) dodo = 100000 ;
//              } // End for dodo
//              if ( ( dodo < 99999  ) && ( conv >= 1.e-2 ) ) {
//                      printf("NOT CONVERGED !!!!!! conv = %le\n",conv) ;
//              if ( conv > 1. ) printf("dodo k0, k1, k2, uu0, h0, Fr = %d %le,%le,%le,%le,%le,%le\n",dodo,K[0],K[1],K[2],uu0,h0,FRloc) ;
//                      //              exit(-1);
//              }
//              if( h1 > cut_off_U ){
//              Z[0] = h1 ;
//              Z[1] = uu1*K[1]/( mass + 1.e-12 ) ;
//              Z[2] = uu1*K[2]/( mass + 1.e-12 ) ; }
//              else {
//                      Z[0] = h0 ;
//                      Z[1] = 0. ;
//                      Z[2] = 0. ;
//              }
//      }
//}
//}

void Z_2_K(double *Z, double *K, double b0, double ratio)
{
	if (Z[0] > cut_off_U * ratio) {
		K[0] = b0 + Z[0] + 0.5 * (Z[1] * Z[1] + Z[2] * Z[2]) / gm;
		K[1] = Z[1] * Z[0];
		K[2] = Z[2] * Z[0];
	} else {
		K[0] = b0 + Z[0];
		K[1] = 0.;
		K[2] = 0.;
	}
}

void Z_2_P(double *Z, double *P, double ratio)
{
	double hdum = Z[0];
	double udum = Z[1];
	double vdum = Z[2];
/*	if (hdum < cut_off_H * ratio){
		hdum = 0.;
		udum = 0.;
		vdum = 0.;
	}
*/

	P[0] = hdum;
	P[1] = hdum * udum;
	P[2] = hdum * vdum;

/*	if ( hdum > cut_off_U*ratio ) {
		P[1] = hdum*udum ;
		P[2] = hdum*vdum ;} 
	else { P[1] = P[2] = 0. ; }

	if ( hdum > cut_off_H*ratio ) {
		P[0] = hdum;
		P[1] = hdum*udum ;
		P[2] = hdum*vdum ;} 
	else { P[0]= P[1] = P[2] = 0. ; }
	*/
}

void zero_fix(int n)
{
	//speed_max = 10. * gm;
	double f = node[n].P[2][0];

	if (f <= cut_off_H * node[n].cut_off_ratio) {
		node[n].P[2][0] = 0.;
		node[n].P[2][1] = 0.;
		node[n].P[2][2] = 0.;
	} else if (f <= cut_off_U * node[n].cut_off_ratio) {
		// else if(node[n].P[2][0] <= 10. * cut_off)
		node[n].P[2][1] = 0.;
		node[n].P[2][2] = 0.;
	}
	//else if(node[n].P[2][0] <= 100. * cut_off)
	//else //if ( f <= 100.)
	//{
	//if(fabs(node[n].P[2][1]) > speed_max * node[n].P[2][0])
	//node[n].P[2][1] = speed_max * SIGN(node[n].P[2][1]) * node[n].P[2][0];
	//if(fabs(node[n].P[2][2]) > speed_max * node[n].P[2][0])
	//node[n].P[2][2] = speed_max * SIGN(node[n].P[2][2]) * node[n].P[2][0];
	//}
}

/* Eigenvalues for 2D shallow water */
void Waves(double *n)
{
	double edge = sqrt(n[0] * n[0] + n[1] * n[1]);
	Lambda[0] = ubar0 * n[0] + vbar0 * n[1] - sigma_barx * n[0] - sigma_bary * n[1];
	Lambda[1] = Lambda[0] + speed_of_sound0 * edge;
	Lambda[2] = Lambda[0] - speed_of_sound0 * edge;
}

/* Eigenvectors for 2D shallow water */
void Eigenvectors(double *norm)
{
	const double EPS = 1e-15;
	double edge = sqrt(norm[0] * norm[0] + norm[1] * norm[1]);
	if (edge < epsilon)
		edge = 1.0;
	double nx = norm[0] / edge;
	double ny = norm[1] / edge;
	double h = height0;		//MAX(height0,0.01);
	double a = sqrt(gm * h) + EPS;
	double u = ubar0;
	double v = vbar0;

	//hu = hubar0;
	//hv = hvbar0;

	Right[0][0] = 0.0;
	Right[1][0] = -a * ny;
	Right[2][0] = a * nx;
	Right[0][1] = 1.0;
	Right[1][1] = u + a * nx;
	Right[2][1] = v + a * ny;
	Right[0][2] = 1.;
	Right[1][2] = u - a * nx;
	Right[2][2] = v - a * ny;

	//invertmat( Right, Left ) ;

	Left[0][0] = (u * ny - v * nx) / a;
	Left[0][1] = -ny / a;
	Left[0][2] = nx / a;
	Left[1][0] = (a - u * nx - v * ny) / (2. * a);
	Left[1][1] = nx / (2. * a);
	Left[1][2] = ny / (2. * a);
	Left[2][0] = (a + u * nx + v * ny) / (2. * a);
	Left[2][1] = -nx / (2. * a);
	Left[2][2] = -ny / (2. * a);
}

/* local velocity unit vector for residual scalar decomposition */
void velocity(double ratio)
{
	double u = ubar0;
	double v = vbar0;
	double norm = sqrt(u * u + v * v);
	if (height0 > cut_off_U * ratio) {
		double Fr = sqrt(height0 * gm);
		Fr = norm / Fr;

		if (Fr > 0.001) {
			vel[0] = u / norm;
			vel[1] = v / norm;
		} else {
			vel[0] = UUinf / sqrt(UUinf * UUinf + VVinf * VVinf);
			vel[1] = VVinf / sqrt(UUinf * UUinf + VVinf * VVinf);
		}
	} else {
		vel[0] = UUinf / sqrt(UUinf * UUinf + VVinf * VVinf);
		vel[1] = VVinf / sqrt(UUinf * UUinf + VVinf * VVinf);
	}
}

/* compute reference values for dissipation theta */
void compute_reference_values(void)
{
	h_ref = 0.;
	double h_min = infinity;
	double h_max = 0;
	for (int n = 0; n < NN; n++) {
		if (node[n].P[2][0] < h_min)
			h_min = node[n].P[2][0];
		else if (node[n].P[2][0] > h_max)
			h_max = node[n].P[2][0];
        
        node[n].H_norm = 0. ;
        node[n].u_norm = 0. ;
	}

	h_ref = h_max - h_min;
	u_ref = sqrt(gm * h_ref);
    
    /*START:computing reference values for nodal velocity limiting*/
    	for ( int e =0 ; e < NE ; e++){
        	double max2 =0. ;
        
        	for ( int v = 0 ; v <3 ; v++ ){
            		int nv = element[e].node[v] ;
            		if (  node[nv].Z[2][0] > max2 ) max2 = node[nv].Z[2][0] ;
        	}
        
        	for ( int v = 0 ; v <3 ; v++ ){
            		int nv = element[e].node[v] ;
            		if ( node[nv].H_norm < max2 ) node[nv].H_norm = max2 ;
        	}
    	}
    
    
    	for ( int e =0 ; e < NE ; e++){
        	double max1 =0. ;
        	double max2 = 0. ;
        
        	for ( int v = 0 ; v <3 ; v++ ){
            		int nv = element[e].node[v] ;
            		if ( node[nv].H_norm  > max2 ) max2 = node[nv].H_norm;
        	}
        
        	if ( (( node[element[e].node[0]].P[2][0] < coarse_cut_off ) &&( node[element[e].node[1]].P[2][0] < coarse_cut_off ) ) && (node[element[e].node[2]].P[2][0] < coarse_cut_off) ){
			for ( int v = 0 ; v <3 ; v++ ){
                		int nv = element[e].node[v] ;
                		if ( max2 > cut_off_U ) node[nv].u_norm = sqrt(gm*max2) ;
                		else node[nv].u_norm = 1.0 ;
            		}
        	}
        	else{
            		for ( int v = 0 ; v <3 ; v++ ){
                		int nv = element[e].node[v] ;
                		if( node[nv].P[2][0] > coarse_cut_off )
                    			if ( sqrt( node[nv].Z[2][1]*node[nv].Z[2][1] + node[nv].Z[2][2]*node[nv].Z[2][2]  ) > max1 )
                        			max1 = sqrt( node[nv].Z[2][1]*node[nv].Z[2][1] + node[nv].Z[2][2]*node[nv].Z[2][2] ) ;
            		}
            
            		double max = sqrt(gm*max2) ;
            		if ( max1 > max ) max = max1 ;
            			for ( int v = 0 ; v <3 ; v++ ){
                			int nv = element[e].node[v] ;
                			if ( node[nv].u_norm < max ) node[nv].u_norm = max ;
            			}
        	}
    	}	
    /*END:computing reference values for nodal velocity limiting*/

    
    /*START:computing reference values for nodal velocity limiting
	for ( int e =0 ; e < NE ; e++){
        	double max1 =0. ;
        	double max2 = 0. ;
        	double max = 0. ;
        	for ( int v = 0 ; v <3 ; v++ ){
            		int nv = element[e].node[v] ;
            		if( node[nv].P[2][0] > coarse_cut_off )
                		if ( sqrt( node[nv].Z[2][1]*node[nv].Z[2][1] + node[nv].Z[2][2]*node[nv].Z[2][2]  ) > max1 )
                    			max1 = sqrt( node[nv].Z[2][1]*node[nv].Z[2][1] + node[nv].Z[2][2]*node[nv].Z[2][2] ) ;
            		if ( sqrt( gm*node[nv].Z[2][0] ) > max2 ) max2 = sqrt( gm*node[nv].Z[2][0] );
        	}
        
        	max = max1 ; if ( max2 > max1 ) max = max2 ;
        
        	for ( int v = 0 ; v <3 ; v++ ){
            		int nv = element[e].node[v] ;
            		if ( node[nv].u_norm < max ) node[nv].u_norm = max ;
        	}
        
    	}
    
    	double max = 0. ;
    	for (int n = 0; n < NN; n++)
        	if ( node[n].Z[2][0] >= cut_off_U )
            		if ( node[n].u_norm > max )
                		max = node[n].u_norm ;
    
    	for (int n = 0; n < NN; n++)
        	if ( node[n].Z[2][0] <= cut_off_U )
            		node[n].u_norm =  max ;
    END:computing reference values for nodal velocity limiting*/
    
}

/* Streamline BC */
void first_streamline(int m)
{
	int n = b_nodes[m].node;
	double nx = b_nodes[m].normal[0];
	double ny = b_nodes[m].normal[1];
	double length = sqrt(nx * nx + ny * ny);
	nx /= length;
	ny /= length;
	double phin = node[n].Res[1] * nx + node[n].Res[2] * ny;
	node[n].Res[1] -= phin * nx;
	node[n].Res[2] -= phin * ny;
	double un = node[n].P[2][1] * nx + node[n].P[2][2] * ny;
	node[n].P[2][1] -= un * nx;
	node[n].P[2][2] -= un * ny;
    
}

/* Wetting Front */
void fix_wetting_front(struct element_struct *e, int v0, int v1, int v2)
{
	velocity(e->cut_off_ratio);
	double norm_vel = e->normal[v0][0] * vel[0] + e->normal[v0][1] * vel[1];
	if (1 == zero_nodes) {
		if (norm_vel <= 0.)
			for (int i = 0; i < size; i++)
				PHIN_d[v0][i] = 0.;
	} else if (2 == zero_nodes) {
		if (norm_vel >= 0.)
			for (int i = 0; i < size; i++) {
				PHIN_d[v1][i] = 0.;
				PHIN_d[v2][i] = 0.;
			}
	}
}
