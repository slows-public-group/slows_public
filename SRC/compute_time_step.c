/*  Copyright (C) 2021   M.Ricchiuto  <mario.ricchiuto@inria.fr>
                         M. Kazolea   <maria.kazolea@inria.fr>
                         A. Filippini <a.filippini@brgm.fr>
                         L. Arpaia    <luca.arpaia@brgm.fr>
                         N.Pattakos   <pattakosn@fastmail.com> 

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.*/
	

/***************************************************************************
compute_time_step.c
-------------------
    This is compute_time_step: it computes the time steps associated to
	first and second time level such that the past shield condition
	is satisfied for a linear scalar equation
-------------------
***************************************************************************/
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include "common.h"

int print_counter, time_step;
double dt, dt1, dt2, dt_old, Time, q_ratio, L_ref, gridsize;
extern int NE, NN, initial_state, numerics_friction_explicit, time_discretization;
extern double time_CFL, t_max, UUinf, VVinf, manning, cut_off_U, output_stepsize;
extern double temp_normal[2+1], time_solution,gm;
extern struct element_struct *element;
extern struct node_struct *node;

void compute_time_step(void)
{
	for (int n = 0; n < NN; n++)
		node[n].dtau = 0.;

	if (time_step == 1)
		q_ratio = 1.;
	else
		q_ratio = dt;

	double delta_t0 = infinity;
	dt_old = dt;

        if( 3 == time_discretization ) {
		dt2 = dt1;
		dt1 = dt;
	}

    /* Compute timestep */
	for (int e = 0; e < NE; e++) {
		int n1 = element[e].node[0];
		int n2 = element[e].node[1];
		int n3 = element[e].node[2];

        	double fluxlim = node[n1].u_norm ;
        	if ( node[n2].u_norm > fluxlim ) fluxlim = node[n2].u_norm ;
        	if ( node[n3].u_norm > fluxlim ) fluxlim = node[n3].u_norm ;

		double volume = element[e].new_volume/3.;
		double max = 0.;
		double norm = 0.;
		double speed = 0.;
		for (int v = 0; v < 3; v++) {
			for (int j = 0; j < 2; j++)
				temp_normal[j] = element[e].normal[v][j];

			if (sqrt(temp_normal[0] * temp_normal[0] + temp_normal[1] * temp_normal[1]) > norm)
				norm = sqrt(temp_normal[0] * temp_normal[0] + temp_normal[1] * temp_normal[1]);

			int n = element[e].node[v];

			if (fabs(node[n].Z[0][1]) + fabs(node[n].Z[0][2]) > max)
				max = fabs(node[n].Z[0][1]) + fabs(node[n].Z[0][2]);

			if (sqrt(gm * fabs(node[n].Z[0][0])) > speed)
				speed = sqrt(gm * fabs(node[n].Z[0][0]));
		}
        
		if ( max > fluxlim ) max = fluxlim ;

		// for Manning ST
		double coeff = speed * speed / gm;
		double aa = 0;
		if (pow(coeff, 3.) > cut_off_U * element[e].cut_off_ratio) {
			coeff = pow(coeff, 4. / 3.);
			aa = volume * manning * manning * gm * max / coeff;
		}//else {
		//	coeff = pow( cut_off_U*element[e].cut_off_ratio,  4./3. ) ; 
		//	aa = volume*manning*manning*gm*max/coeff ; 
		//}

		max = 0.5 * (max + speed) * norm;
		max += 0.5 * (fabs(UUinf) + fabs(VVinf)) * gridsize * element[e].length / L_ref;
		if (1 == numerics_friction_explicit)
			max += aa;

		node[element[e].node[0]].dtau += max;
		node[element[e].node[1]].dtau += max;
		node[element[e].node[2]].dtau += max;
	}

	for (int n = 0; n < NN; n++) {
		node[n].dti = node[n].mod_volume / (node[n].dtau + 1.e-20);
		if (delta_t0 > node[n].dti)
			delta_t0 = node[n].dti;

	}

	delta_t0 *= time_CFL;

	if ( (Time < print_counter*time_solution) && (Time + delta_t0 >= print_counter*time_solution ))
		delta_t0 = print_counter*time_solution - Time;
	if (Time + delta_t0 > t_max)
		delta_t0 = t_max - Time;

	/* Correct timestep for sampling */
	// Small Perturbation on lake at rest
	if (initial_state == 4) {
		if ( (Time < 0.12) && (Time + delta_t0 >= 0.12) )
			delta_t0 = 0.12 - Time;

		if ( (Time < 0.24) && (Time + delta_t0 >= 0.24) )
			delta_t0 = 0.24 - Time;

		if ( (Time < 0.36) && (Time + delta_t0 >= 0.36) )
			delta_t0 = 0.36 - Time;
	}
	// Runup: [Carriere,2003]
	if (initial_state == 23) {
		if ( (Time < 160.) && (Time + delta_t0 >= 160.) )
			delta_t0 = 160. - Time;

		if ( (Time < 175.) && (Time + delta_t0 >= 175.) )
			delta_t0 = 175. - Time;

		if ( (Time < 220.) && (Time + delta_t0 >= 220.) )
			delta_t0 = 220. - Time;
	}

	// Thacker's curved oscillations
	if (initial_state == 15) {
		if ( (Time < 2.22) && (Time + delta_t0 >= 2.22) )
			delta_t0 = 2.22 - Time;

		if ( (Time < 2.22 + 2.22 / 6.) && (Time + delta_t0 >= 2.22 + 2.22 / 6.) )
			delta_t0 = 2.22 + 2.22 / 6. - Time;

		if ( (Time < 2.22 + 2.22 / 3.) && (Time + delta_t0 >= 2.22 + 2.22 / 3.) )
				delta_t0 = 2.22 + 2.22 / 3. - Time;

		if ( (Time < 2.22 + 2.22 / 2.) && (Time + delta_t0 >= 2.22 + 2.22 / 2.) )
			delta_t0 = 2.22 + 2.22 / 2. - Time;

		if ( (Time < 2.22 + 2.22 / 2. + 2.22 / 6.) && (Time + delta_t0 >= 2.22 + 2.22 / 2. + 2.22 / 6.) )
			delta_t0 = 2.22 + 2.22 / 2. + 2.22 / 6. - Time;

		if ( (Time < 2.22 + 2.22 / 2. + 2.22 / 3.) && (Time + delta_t0 >= 2.22 + 2.22 / 2. + 2.22 / 3.) )
			delta_t0 = 2.22 + 2.22 / 2. + 2.22 / 3. - Time;

		if ( (Time < 2.22 + 2.22) && (Time + delta_t0 >= 2.22 + 2.22) )
			delta_t0 = 2.22 + 2.22 - Time;
	}

	if (initial_state == 76) {
	// Synolakis test case-nonbreaking case
/*		if ( (Time < 20.0*sqrt(1.0/gm)) && (Time + delta_t0 >= 20.0*sqrt(1.0/gm)) )
			delta_t0 = 20.0*sqrt(1.0/gm) - Time;

		if ( (Time < 26.0*sqrt(1.0/gm)) && (Time + delta_t0 >= 26.0*sqrt(1.0/gm) ))
			delta_t0 = 26.0*sqrt(1.0/gm)- Time;

		if ( (Time < 32.0*sqrt(1.0/gm)) && (Time + delta_t0 >= 32.0*sqrt(1.0/gm)) )
				delta_t0 = 32.0*sqrt(1.0/gm)- Time;

		if ( (Time < 38.0*sqrt(1.0/gm)) && (Time + delta_t0 >= 38.0*sqrt(1.0/gm) ))
			delta_t0 = 38.0*sqrt(1.0/gm)- Time;

		if ( (Time < 44.0*sqrt(1.0/gm)) && (Time + delta_t0 >= 44.0*sqrt(1.0/gm) ))
			delta_t0 = 44.0*sqrt(1.0/gm)- Time;

		if ( (Time < 50.0*sqrt(1.0/gm)) && (Time + delta_t0 >= 50.0*sqrt(1.0/gm) ))
			delta_t0 = 50.0*sqrt(1.0/gm)- Time;

		if ( (Time < 56.0*sqrt(1.0/gm)) && (Time + delta_t0 >= 56.0*sqrt(1.0/gm) ))
			delta_t0 = 56.0*sqrt(1.0/gm)- Time;

		if ( (Time < 62.0*sqrt(1.0/gm)) && (Time + delta_t0 >= 62.0*sqrt(1.0/gm) ))
			delta_t0 = 62.0*sqrt(1.0/gm)- Time;
*/
 		//Synolakis breaking case
		if ( (Time < 10.0*sqrt(1.0/gm)) && (Time + delta_t0 >= 10.0*sqrt(1.0/gm)) )
			delta_t0 = 10.0*sqrt(1.0/gm) - Time;

		if ( (Time < 15.0*sqrt(1.0/gm)) && (Time + delta_t0 >= 15.0*sqrt(1.0/gm) ))
			delta_t0 = 15.0*sqrt(1.0/gm)- Time;

		if ( (Time < 20.0*sqrt(1.0/gm)) && (Time + delta_t0 >= 20.0*sqrt(1.0/gm)) )
				delta_t0 = 20.0*sqrt(1.0/gm)- Time;

		if ( (Time < 25.0*sqrt(1.0/gm)) && (Time + delta_t0 >= 25.0*sqrt(1.0/gm) ))
			delta_t0 = 25.0*sqrt(1.0/gm)- Time;

		if ( (Time < 30.0*sqrt(1.0/gm)) && (Time + delta_t0 >= 30.0*sqrt(1.0/gm) ))
			delta_t0 = 30.0*sqrt(1.0/gm)- Time;

		if ( (Time < 45.0*sqrt(1.0/gm)) && (Time + delta_t0 >= 45.0*sqrt(1.0/gm) ))
			delta_t0 = 45.0*sqrt(1.0/gm)- Time;

		if ( (Time < 55.0*sqrt(1.0/gm)) && (Time + delta_t0 >= 55.0*sqrt(1.0/gm) ))
			delta_t0 = 55.0*sqrt(1.0/gm)- Time;

		if ( (Time < 60.0*sqrt(1.0/gm)) && (Time + delta_t0 >= 60.0*sqrt(1.0/gm) ))
			delta_t0 = 60.0*sqrt(1.0/gm)- Time;

		if ( (Time < 70.0*sqrt(1.0/gm)) && (Time + delta_t0 >= 70.0*sqrt(1.0/gm) ))
			delta_t0 = 70.0*sqrt(1.0/gm)- Time;

		if ( (Time < 80.0*sqrt(1.0/gm)) && (Time + delta_t0 >= 80.0*sqrt(1.0/gm) ))
			delta_t0 = 80.0*sqrt(1.0/gm)- Time;
	}

	if (initial_state == 77) {
	// Volker reef test case
		double times[12]={4.0436, 5.053, 6.815, 7.82, 8.94, 10.91, 13.02, 21.8082, 28.779, 31.6, 32.96, 48.96};
		for(int i=0; i<12; i++) {
			if ( (Time < times[i]) && (Time + delta_t0 >= times[i]) )
				delta_t0 = times[i] - Time;
		}
	}

	if (initial_state == 90) {
	// Solitary shelf 1d
		double times[50];
		double k=1.0;
		for (int i=0; i<50; i++){
			times[i]= k;
			k=k+1.0;
		}
		for(int i=0; i<50; i++) {
			if ( (Time < times[i]) && (Time + delta_t0 >= times[i]) )
				delta_t0 = times[i] - Time;
		}
	}

	// Briggs - Okushiri - Runup on constant slope - Shelf
	double sample_time;
	int samples_num;
	if (   (initial_state == 8)  || (initial_state == 25) 
		|| (initial_state == 34) || (initial_state == 24)) {

		if (8 == initial_state) // Conical Island [Briggs,1996] 
			samples_num = 300;
		else if (24 == initial_state) // Shelf
			samples_num = 2000;
		else if (25 == initial_state) // Okushiri
			samples_num = 250;
		else if (34 == initial_state) // Double dambreak
			samples_num = 50;
		else {
			samples_num = 0;
			printf("wrong state, try again. Thanks for playing.\n");
			exit(EXIT_FAILURE);
		}

		for (int n = 1; n <= samples_num; n++) {
			sample_time = n * output_stepsize;
			if ((Time < sample_time) && (Time + delta_t0 > sample_time))
				delta_t0 = sample_time - Time;
		}
	}


	dt = delta_t0;

        //if( 3 == time_discretization && time_step <= 2) {
        if( 3 == time_discretization && time_step <= 1) {
		dt= 0.0000025;
	}

	Time += dt;
	if (time_step == 1)
		dt_old = delta_t0;


	q_ratio /= dt;
	printf("                     Time step = %le   \n", dt);
	printf("\n");
}

void compute_time_step_fixed(void)
{
	double delta_t0 = 0;
	if (time_step == 1) {
		compute_time_step();
	} else {
		if (Time + delta_t0 > t_max) {
			delta_t0 = t_max - Time;
			q_ratio = dt / delta_t0;
		} else {
			delta_t0 = dt;
			if (q_ratio > 1.)
				q_ratio = 1. / q_ratio;
			else if (q_ratio < 1.)
				q_ratio = 1.0;
		}
		//dt = delta_t0 ;
		Time += delta_t0;
		//q_ratio /= delta_t0;
	}
	//printf("dt: %le\n", dt);
}
