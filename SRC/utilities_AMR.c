/*  Copyright (C) 2021   M.Ricchiuto  <mario.ricchiuto@inria.fr>
                         M. Kazolea   <maria.kazolea@inria.fr>
                         A. Filippini <a.filippini@brgm.fr>
                         L. Arpaia    <luca.arpaia@brgm.fr>
                         N.Pattakos   <pattakosn@fastmail.com> 

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.*/


/***************************************************************************
                               AMR_utilities.c
                           -----------------------
  This is adaptation: here are programmed all the functions that permit the
  mesh adaptation through the solution of the nonlinear system A(x)x = b.
  There are operations with sparse matrix (initialization, sparse matrix-vector
  product, diagonal elements), the assembly of moving mesh matrix, a function
  for sparse Jacobi. There are also functions that computes monitor function
  and adaptation parameters
                             -------------------
***************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "common.h"
#include "my_malloc.h"

extern struct node_struct *node;
extern struct boundary_nodes_struct *b_nodes;
extern int NN, NBF, blending, grad_reco;
extern double  L_ref, cut_off_H;
int NBNE;
extern void (*param_adaptation) (void);
extern void (*shore_rec) (void);

void grad_rec(int, int);
void lap_rec(int, int);
void grad_katz(int, int);
void bathy_rec(int, int);
void lap_bathy_rec(int, int);
void P_2_Z(double *, double *, double);
void derivative(double *, double *);

// This is a scratch routine for the user:
// he can modify the stiffness tuning locally the relaxation parameter mu
// An example for case:35 follows. The region around the corner is stiffened to avoid mesh tangling
double user_mu(double a, int i)
{
	double vect[2][4];
	vect[0][0] = 95.;
	vect[1][0] = 95.;
	vect[0][1] = 105.;
	vect[1][1] = 95.;
	vect[0][2] = 95.;
	vect[1][2] = 170.;
	vect[0][3] = 105.;
	vect[1][3] = 170.;
	double dist = 10000000.;
	double b = a;

	for (int l = 0; l < 4; l++) {
		double dist1 = sqrt( (node[i].coordinate_ite[0] - vect[0][l]) * (node[i].coordinate_ite[0] - vect[0][l])
		                    +(node[i].coordinate_ite[1] - vect[1][l]) * (node[i].coordinate_ite[1] - vect[1][l]));
		if (dist1 < dist)
			dist = dist1;
	}

	if (dist < L_ref / 40.)
		b = a * 0.01;

	// Fixed boundary nodes
	if ((i < NBF))
		b = a * 0.0;
	return b;
}



void vars_reconstruction(int size_lev, __attribute__((unused)) int time_lev, int prj_flag)
{
	grad_rec(1, 0);
	grad_rec(size_lev, 0);
	bathy_rec(0, 0);
	lap_rec(size_lev, 0);
	lap_bathy_rec(0, 0);


/*	if (blending>2){
		double *work_vector = MA_double_vector(NN);
		double *grad_vector = MA_double_vector(2*NN);
		for (int i = 0; i<3; i++){
			for (int n = 0; n<NN; n++)
				work_vector[n] = node[n].dxP[0][i];
		
			derivative(work_vector,grad_vector);
	
			for (int n = 0; n<NN; n++)
				node[n].dxyP2[0][i] = 0.5*grad_vector[n+NN];
			
			for (int n = 0; n<NN; n++)
				work_vector[n] = node[n].dyP[0][i];
		
			derivative(work_vector,grad_vector);
	
			for (int n = 0; n<NN; n++)
				node[n].dxyP2[0][i] += 0.5*grad_vector[n];
		}

		free(work_vector);
		free(grad_vector);

		grad_katz( size_lev, 0);
		for (int f = 0; f<NBF; f++){
			if (b_nodes[f].type==3){
				int n = b_nodes[f].node;
				node[n].dxP2[0][0] = node[n].dxP[0][0];
				node[n].dyP2[0][0] = node[n].dyP[0][0];
				node[n].dxP2[0][1] = node[n].dxP[0][1];
				node[n].dyP2[0][1] = node[n].dyP[0][1];
				node[n].dxP2[0][2] = node[n].dxP[0][2];
				node[n].dyP2[0][2] = node[n].dyP[0][2];
			}
		}
	}
*/
}

void node_reconstruction(void)
{
	for ( int n = 0; n < NN; n++ ){
		double dgi_x = node[n].DcC[0] - node[n].coordinate[0];
		double dgi_y = node[n].DcC[1] - node[n].coordinate[1];
		node[n].P[1][0] = node[n].P[2][0] - dgi_x*node[n].dxP2[0][0] - dgi_y*node[n].dyP2[0][0] + 0.5*dgi_x*dgi_x*node[n].dxxP2[0][0] + dgi_x*dgi_y*node[n].dxyP2[0][0] + 0.5*dgi_y*dgi_y*node[n].dyyP2[0][0];
 		node[n].P[1][1] = node[n].P[2][1] - dgi_x*node[n].dxP2[0][1] - dgi_y*node[n].dyP2[0][1] + 0.5*dgi_x*dgi_x*node[n].dxxP2[0][1] + dgi_x*dgi_y*node[n].dxyP2[0][1] + 0.5*dgi_y*dgi_y*node[n].dyyP2[0][1];
		node[n].P[1][2] = node[n].P[2][2] - dgi_x*node[n].dxP2[0][2] - dgi_y*node[n].dyP2[0][2] + 0.5*dgi_x*dgi_x*node[n].dxxP2[0][2] + dgi_x*dgi_y*node[n].dxyP2[0][2] + 0.5*dgi_y*dgi_y*node[n].dyyP2[0][2];
		P_2_Z(node[n].P[1], node[n].Z[1], node[n].cut_off_ratio);
	}
}
