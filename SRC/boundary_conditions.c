/*  Copyright (C) 2021   M.Ricchiuto  <mario.ricchiuto@inria.fr>
                         M. Kazolea   <maria.kazolea@inria.fr>
                         A. Filippini <a.filippini@brgm.fr>
                         L. Arpaia    <luca.arpaia@brgm.fr>
                         N.Pattakos   <pattakosn@fastmail.com> 

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.*/

/***************************************************************************
 model_0.c
 ---------
 This is boundary_conditions.c: It contains all the boundary conditions for
 the different test cases
 -------------------
 ***************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "common.h"

extern const int size;
extern int initial_state, iteration;
extern double Time, dt, gm, height0, ubar0, vbar0, speed_of_sound0;
double CFL, boundary_flux;
extern double UUinf, VVinf, gridsize, L_ref, cut_off_U, manning, a_RK[3][4];
extern double *temp_vector, *Lambda, *time_bc, *h_bc;
extern double *work_vector0, *work_vector1, *work_vector2, temp_normal[2+1];
extern double Area_inlet_do, Area_inlet_ga, TIDE;
extern double gaugeIN, gaugeOUT, gauge_velIN, gauge_velOUT;
extern struct node_struct *node;
extern struct boundary_nodes_struct *b_nodes;
extern struct boundary_struct *boundary;
extern double **Right, **Left;

/* node global index, local residual distributed to node, time level to update (1/2) */
void first_streamline(int);
void Eigenvectors(double *);

/* BC FOR SHALLOW WATER SYSTEM */
void char_bc_vortex(int m)
{
	int n = b_nodes[m].node;
	double x = node[n].coordinate[0];
	//y = node[n].coordinate[1];
	if ((x - 0.0) < 0.000001) {
		node[n].Res[0] = 0.0;	//( node[n].P[2][0] - 10. )*node[n].mod_volume ;
		node[n].Res[1] = 0.0;	//( node[n].P[2][1] - 6.*10.0 )*node[n].mod_volume ;
		node[n].Res[2] = 0.0;
	}
}

void char_bc_wedge(int m)
{
	int n = b_nodes[m].node;
	//x = node[n].coordinate[0];
	//y = node[n].coordinate[1];
	double nx = b_nodes[m].normal[0];
	double ny = b_nodes[m].normal[1];
	double edge = sqrt(nx * nx + ny * ny);

	temp_normal[0] = nx / edge;
	temp_normal[1] = ny / edge;

	//* Bump
	double h, u, v;
	if (iteration == 1) {
		h = node[n].P[2][0];
		u = node[n].P[2][1] / node[n].P[2][0];
		v = node[n].P[2][2] / node[n].P[2][0];
	} else {
		h = 0.5 * (node[n].P[2][0] + node[n].P[0][0]);
		u = 0.5 * (node[n].P[2][1] + node[n].P[0][1]) / h;
		v = 0.5 * (node[n].P[2][2] + node[n].P[0][2]) / h;
	}

	//if (fabs(x-4.) < 0.00001) {
	//	hex = h;
	//	uex = u;
	//	vex = v;
	//	temp_normal[0] = -1. ;
	//	temp_normal[1] = 0. ;
	//}
	double hex = 10.0;
	double uex = 1.0;
	double vex = 0.0;

	height0 = 0.5 * (hex + h);
	speed_of_sound0 = sqrt(gm * height0);
	ubar0 = 0.5 * (uex + u);
	vbar0 = 0.5 * (vex + v);
	double unex = uex * temp_normal[0] + vex * temp_normal[1];
	double un = u * temp_normal[0] + v * temp_normal[1];

	temp_vector[0] = h * un - hex * unex;
	temp_vector[0] *= dt * edge;
	temp_vector[1] = h * u * un - hex * uex * unex + 0.5 * gm * (h * h - hex * hex) * temp_normal[0];
	temp_vector[1] *= dt * edge;
	temp_vector[2] = h * v * un - hex * vex * unex + 0.5 * gm * (h * h - hex * hex) * temp_normal[1];
	temp_vector[2] *= dt * edge;

	Lambda[0] = unex;
	Lambda[1] = Lambda[0] + sqrt(gm * hex);
	Lambda[2] = Lambda[0] - sqrt(gm * hex);

	Eigenvectors(temp_normal);

	work_vector0[0] = 0.;
	work_vector0[1] = 0.;
	work_vector0[2] = 0.;

	work_vector1[0] = 0.;
	work_vector1[1] = 0.;
	work_vector1[2] = 0.;

	work_vector2[0] = 0.;
	work_vector2[1] = 0.;
	work_vector2[2] = 0.;

	double lmax = 0.;
	for (int i = 0; i < size; i++) {
		for (int j = 0; j < size; j++)
			work_vector1[i] += Left[i][j] * temp_vector[j];

		if (Lambda[i] > 0.0)
			work_vector0[i] += work_vector1[i];

		lmax = MAX(lmax, Lambda[i]);
	}

	for (int i = 0; i < size; i++)
		for (int j = 0; j < size; j++)
			node[n].Res[i] += Right[i][j] * work_vector0[j] / 2.;

	node[n].dtau = MIN(node[n].dtau, CFL / (1 + (1.5 * dt * lmax / (edge * edge))));
}

void char_bc_slope(int m)
{
	int n = b_nodes[m].node;
	double x = node[n].coordinate[0];
	double y = node[n].coordinate[1];
	double uu0 = pow(UUinf, 4. / 3.) / (25. * manning * manning);
	uu0 = pow(uu0, 3. / 10.);
	double h0 = UUinf / uu0;
	double mach = uu0 / sqrt(gm * h0);

	if (mach < 1) {
		double nx = b_nodes[m].normal[0];
		double ny = b_nodes[m].normal[1];
		double edge = sqrt(nx * nx + ny * ny);
		if (x < 1.e-8) {
			temp_normal[0] = 1.;
			temp_normal[1] = 0.;
			if (y < 1.e-8) {
				temp_normal[0] = 1. / sqrt(2.);
				temp_normal[1] = 1. / sqrt(2.);
			}
			if (1. - y < 1.e-8) {
				temp_normal[0] = 1. / sqrt(2.);
				temp_normal[1] = -1. / sqrt(2.);
			}
		}

		if (y < 1.e-8) {
			temp_normal[0] = 0.;
			temp_normal[1] = 1.;
			if (x < 1.e-8) {
				temp_normal[0] = 1. / sqrt(2.);
				temp_normal[1] = 1. / sqrt(2.);
			}
			if (1. - x < 1.e-8) {
				temp_normal[0] = -1. / sqrt(2.);
				temp_normal[1] = 1. / sqrt(2.);
			}
		}

		if (1. - x < 1.e-8) {
			temp_normal[0] = -1.;
			temp_normal[1] = 0.;
			if (y < 1.e-8) {
				temp_normal[0] = -1. / sqrt(2.);
				temp_normal[1] = 1. / sqrt(2.);
			}
			if (1. - y < 1.e-8) {
				temp_normal[0] = -1. / sqrt(2.);
				temp_normal[1] = -1. / sqrt(2.);
			}
		}

		if (1. - y < 1.e-8) {
			temp_normal[0] = 0.;
			temp_normal[1] = -1.;
			if (x < 1.e-8) {
				temp_normal[0] = 1. / sqrt(2.);
				temp_normal[1] = -1. / sqrt(2.);
			}
			if (1. - x < 1.e-8) {
				temp_normal[0] = -1. / sqrt(2.);
				temp_normal[1] = -1. / sqrt(2.);
			}
		}

		double h, u, v;
		if (iteration == 1) {
			h = node[n].Z[0][0];
			u = node[n].Z[0][1];
			v = node[n].Z[0][2];
		} else {
			h = 0.5 * (node[n].Z[2][0] + node[n].Z[0][0]);
			u = 0.5 * (node[n].Z[2][1] + node[n].Z[0][1]);
			v = 0.5 * (node[n].Z[2][2] + node[n].Z[0][2]);
		}

		double hex = h0;
		double uex = uu0 * 2. / sqrt(5.);
		double vex = uu0 / sqrt(5.);

		height0 = hex;
		speed_of_sound0 = sqrt(gm * height0);
		ubar0 = uex;
		vbar0 = vex;
		double unex = uex * temp_normal[0] + vex * temp_normal[1];
		double un = u * temp_normal[0] + v * temp_normal[1];

		temp_vector[0] = h * un - hex * unex;
		temp_vector[0] *= dt * edge;
		temp_vector[1] = h * u * un - hex * uex * unex + 0.5 * gm * (h * h - hex * hex) * temp_normal[0];
		temp_vector[1] *= dt * edge;
		temp_vector[2] = h * v * un - hex * vex * unex + 0.5 * gm * (h * h - hex * hex) * temp_normal[1];
		temp_vector[2] *= dt * edge;

		Lambda[0] = unex;
		Lambda[1] = Lambda[0] + sqrt(gm * hex);
		Lambda[2] = Lambda[0] - sqrt(gm * hex);

		Eigenvectors(temp_normal);

		work_vector0[0] = 0.;
		work_vector0[1] = 0.;
		work_vector0[2] = 0.;

		work_vector1[0] = 0.;
		work_vector1[1] = 0.;
		work_vector1[2] = 0.;

		for (int i = 0; i < size; i++) {
			for (int j = 0; j < size; j++)
				work_vector1[i] += Left[i][j] * temp_vector[j];

			if (Lambda[i] > 0.0)
				work_vector0[i] = work_vector1[i];
		}

		for (int i = 0; i < size; i++)
			for (int j = 0; j < size; j++)
				node[n].Res[i] += Right[i][j] * work_vector0[j];

		//if ( x < 1.e-8 ) {
		//              node[n].Res[1] = 0. ;
		//              node[n].Res[2] = 0. ;
		//      }
		//      
		//      if ( y < 1.e-8 ) {
		//              node[n].Res[1] = 0. ;
		//              node[n].Res[2] = 0. ;
		//      }
		//      
		//      if ( 1. - x < 1.e-8 ) { 
		//              un = ( node[n].Res[1]*2. + node[n].Res[2] )/sqrt( 5. ) ;
		//              node[n].Res[1] -= 2.*un/sqrt( 5. ) ;
		//              node[n].Res[2] -= un/sqrt( 5. ) ;
		//      }
		//      
		//      if ( 1. - y < 1.e-8 ) { 
		//              un = ( node[n].Res[1]*2. + node[n].Res[2] )/sqrt( 5. ) ;
		//              node[n].Res[1] -= 2.*un/sqrt( 5. ) ;
		//              node[n].Res[2] -= un/sqrt( 5. ) ;
		//      }
	} else {
		if (x < 1.e-8) {
			node[n].Res[0] = 0.;
			node[n].Res[1] = 0.;
			node[n].Res[2] = 0.;
		}

		if (y < 1.e-8) {
			node[n].Res[0] = 0.;
			node[n].Res[1] = 0.;
			node[n].Res[2] = 0.;
		}
	}
}


void char_bc_shelf(int m)
{
	double loc_time;
	if (iteration == 1)
		loc_time = Time - dt;
	if (iteration == 2)
		loc_time = Time - 0.5 * dt;

	int n = b_nodes[m].node;
	double x = node[n].coordinate[0];
	double y = node[n].coordinate[1];

	if (fabs(x - 45.) < 1.e-6)
		node[n].Res[1] = 0.;
	if ((fabs(y - 13.2) < 1.e-6) || (fabs(y + 13.2) < 1.e-6))
		node[n].Res[2] = 0.;

	if (x < 1.e-6) {
		double h0 = 0.78;
		double a0 = 0.39;
		double L0 = 45.0;
		double alpha = -0.390; // Nwogu optimal alpha
		double delta = a0 / h0; // nonlinear parameter
		double mu = h0 / L0; // dispersive parameter
		double C = sqrt(1.49125); // approximated celerity
		//A = (C * C - 1.0) / (delta * C);
		double B = sqrt((C * C - 1.0) / (4.0 * mu * mu * ((alpha + 1.0 / 3.0) - alpha * C * C)));
		double A1 = (C * C - 1.0) / (3.0 * delta * ((alpha + 1.0 / 3.0) - alpha * C * C));
		double A2 = -(C * C - 1.0) * (C * C - 1.0) / (2.0 * delta * C * C) * ((alpha + 1.0 / 3.0) + 2.0 * alpha * C * C) / ((alpha + 1.0 / 3.0) - alpha * C * C);
		double beta = -B * C / L0 * sqrt(gm * h0) * loc_time;
		double sech = 1. / (cosh(beta));
		double etai = a0 * A1 * sech * sech + a0 * A2 * sech * sech * sech * sech;
		double hex = etai - node[n].bed[0];
		double uex = sqrt(gm * h0) * delta * sech * sech;

		if (Time < 4.) {
			node[n].Res[0] = (node[n].P[0][0] - hex) * node[n].mod_volume;
			node[n].Res[1] = (node[n].P[0][1] - hex * uex) * node[n].mod_volume;
			node[n].Res[2] = (node[n].P[0][2]) * node[n].mod_volume;
		} else {
			//characteristic BCs with fixed height = h0
			double nx = b_nodes[m].normal[0];
			double ny = b_nodes[m].normal[1];
			double edge = sqrt(nx * nx + ny * ny);

			temp_normal[0] = 1.;
			temp_normal[1] = 0.;

			double h, u, v;
			if (iteration == 1) {
				h = node[n].Z[2][0];
				u = node[n].Z[2][1];
				v = node[n].Z[2][2];
			} else {
				h = 0.5 * (node[n].Z[2][0] + node[n].Z[0][0]);
				u = 0.5 * (node[n].Z[2][1] + node[n].Z[0][1]);
				v = 0.5 * (node[n].Z[2][2] + node[n].Z[0][2]);
			}

			// hex = h0;
			// uex = 0.;
			// vex = 0.;

			height0 = 0.5 * (hex + h);
			speed_of_sound0 = sqrt(gm * height0);
			ubar0 = 0.5 * (uex + u);
			double vex; 
			vbar0 = 0.5 * (vex + v);
			double unex = uex * temp_normal[0] + vex * temp_normal[1];
			double un = u * temp_normal[0] + v * temp_normal[1];

			temp_vector[0] = h * un - hex * unex;
			temp_vector[0] *= dt * edge;

			temp_vector[1] = h * u * un - hex * uex * unex + 0.5 * gm * (h * h - hex * hex) * temp_normal[0];
			temp_vector[1] *= dt * edge;

			temp_vector[2] = h * v * un - hex * vex * unex + 0.5 * gm * (h * h - hex * hex) * temp_normal[1];
			temp_vector[2] *= dt * edge;

			Lambda[0] = unex;
			Lambda[1] = Lambda[0] + sqrt(gm * hex);
			Lambda[2] = Lambda[0] - sqrt(gm * hex);

			Eigenvectors(temp_normal);

			work_vector0[0] = 0.;
			work_vector0[1] = 0.;
			work_vector0[2] = 0.;

			work_vector1[0] = 0.;
			work_vector1[1] = 0.;
			work_vector1[2] = 0.;

			double lmax = 0.;
			for (int i = 0; i < size; i++) {
				for (int j = 0; j < size; j++)
					work_vector1[i] += Left[i][j] * temp_vector[j];

				if (Lambda[i] > 0.0)
					work_vector0[i] += work_vector1[i];

				lmax = MAX(lmax, fabs(Lambda[i]));
			}

			for (int i = 0; i < size; i++)
				for (int j = 0; j < size; j++)
					node[n].Res[i] += Right[i][j] * work_vector0[j];
			node[n].dtau = MIN(node[n].dtau, CFL / (1 + (1.5 * dt * lmax / (edge * edge))));
		} // EOCharacteristic BCs
	}
}

void char_bc_okushiri(int m)
{
	int n = b_nodes[m].node;
	double x = node[n].coordinate[0];
	double y = node[n].coordinate[1];
	double loc_time;
	if (iteration == 1)
		loc_time = Time - dt;
	if (iteration == 2)
		loc_time = Time - 0.5 * dt;
	loc_time = Time;

	if (!(fabs(x) < 0.00001)) {
		if (x < L_ref) // HORIZONTAL WALL
			node[n].Res[2] = 0.;
		else if (y > 0.0) // VERTICAL WALL
			node[n].Res[1] = 0.;
		else {// CORNER WALL (FV HAD PROBLEMS WITH ORIGINAL BC) 
			//nx = -sqrt(2.0)/2.0 ;
			//ny = sqrt(2.0)/2.0 ;
			//phin = node[n].Res[1]*nx + node[n].Res[2]*ny ;
			//node[n].Res[1] -= phin*nx ;
			//node[n].Res[2] -= phin*ny ; 
			node[n].Res[1] = 0.;	// VERTICAL WALL
			//node[n].Res[2] = 0. ;   // HORIZONTAL WALL
		}
	} else {
		double nx = b_nodes[m].normal[0];
		double ny = b_nodes[m].normal[1];
		double edge = sqrt(nx * nx + ny * ny);

		temp_normal[0] = 1.;
		temp_normal[1] = 0.;

		double h, u, v;
		if (iteration == 1) {
			h = node[n].Z[2][0];
			u = node[n].Z[2][1];
			v = node[n].Z[2][2];
		} else {
			h = 0.5 * (node[n].Z[2][0] + node[n].Z[0][0]);
			u = 0.5 * (node[n].Z[2][1] + node[n].Z[0][1]);
			v = 0.5 * (node[n].Z[2][2] + node[n].Z[0][2]);
		}

		int jT = 1;
		int keepon = 1;
		while (keepon) {
			if (time_bc[jT] < loc_time)
				jT++;
			else
				keepon = 0;
		}

		double hex = (h_bc[jT] * (loc_time - time_bc[jT - 1]) / (time_bc[jT] - time_bc[jT - 1]) + h_bc[jT - 1] * (loc_time - time_bc[jT]) / (time_bc[jT - 1] - time_bc[jT]));
		hex += MAX(-node[n].bed[0], 0.);

		if (Time < 22.5) {
			boundary_flux += node[n].Res[0];
			node[n].Res[0] = (node[n].P[0][0] - hex) * node[n].mod_volume;
			node[n].Res[2] = node[n].P[0][2] * node[n].mod_volume;
			boundary_flux -= (node[n].P[0][0] - hex) * node[n].mod_volume;
		} else {
			double uex = 0.;
			double vex = 0.;
			height0 = 0.5 * (hex + h);
			speed_of_sound0 = sqrt(gm * height0);
			ubar0 = 0.5 * (uex + u);
			vbar0 = 0.5 * (vex + v);
			double unex = uex * temp_normal[0] + vex * temp_normal[1];
			double un = u * temp_normal[0] + v * temp_normal[1];

			temp_vector[0] = h * un - hex * unex;
			temp_vector[0] *= dt * edge;

			temp_vector[1] = h * u * un - hex * uex * unex + 0.5 * gm * (h * h - hex * hex) * temp_normal[0];
			temp_vector[1] *= dt * edge;

			temp_vector[2] = h * v * un - hex * vex * unex + 0.5 * gm * (h * h - hex * hex) * temp_normal[1];
			temp_vector[2] *= dt * edge;

			Lambda[0] = unex;
			Lambda[1] = Lambda[0] + sqrt(gm * hex);
			Lambda[2] = Lambda[0] - sqrt(gm * hex);

			Eigenvectors(temp_normal);

			work_vector0[0] = 0.;
			work_vector0[1] = 0.;
			work_vector0[2] = 0.;

			work_vector1[0] = 0.;
			work_vector1[1] = 0.;
			work_vector1[2] = 0.;

			double lmax = 0.;
			for (int i = 0; i < size; i++) {
				for (int j = 0; j < size; j++)
					work_vector1[i] += Left[i][j] * temp_vector[j];

				if (Lambda[i] > 0.0)
					work_vector0[i] += work_vector1[i];

				lmax = MAX(lmax, fabs(Lambda[i]));
			}

			for (int i = 0; i < size; i++)
				for (int j = 0; j < size; j++)
					node[n].Res[i] += Right[i][j] * work_vector0[j];
			node[n].dtau = MIN(node[n].dtau, CFL / (1 + (1.5 * dt * lmax / (edge * edge))));

			for (int j = 0; j < size; j++)
				boundary_flux -= Right[0][j] * work_vector0[j];
		}
	}
}

void char_bc_1dpaper(int m)
{
	int n = b_nodes[m].node;
	double x = node[n].coordinate[0];
	//y = node[n].coordinate[1];
	double nx = b_nodes[m].normal[0];
	double ny = b_nodes[m].normal[1];
	double edge = sqrt(nx * nx + ny * ny);

	temp_normal[0] = nx / edge;
	temp_normal[1] = ny / edge;

	double h, u;
	if (iteration == 1) {
		h = node[n].Z[2][0];
		if (h > cut_off_U * node[n].cut_off_ratio)
			u = node[n].P[0][1] / h;
		else
			u = 0.;
	} else {
		h = 0.5 * (node[n].Z[2][0] + node[n].Z[0][0]);
		if (h > cut_off_U * node[n].cut_off_ratio)
			u = 0.5 * (node[n].P[2][1] + node[n].P[0][1]) / h;
		else
			u = 0.;
	}
	double v = 0.;

	double hex, uex, vex;
	if (fabs(x) < 0.00001) {
		//first_streamline(m);
		hex = h;
		uex = 0;
		vex = 0;
		temp_normal[0] = 1.;
		temp_normal[1] = 0.;
	} else if (fabs(x - 25.) < 0.00001) {
		hex = 0.;	//cut_off_H - small;
		uex = 0.;
		vex = 0.;
		temp_normal[0] = -1.;
		temp_normal[1] = 0.;
	}

	h += 1.e-6;

	height0 = 0.5 * (hex + h);
	speed_of_sound0 = sqrt(gm * height0);
	ubar0 = 0.5 * (uex + u);
	vbar0 = 0.5 * (vex + v);
	double unex = uex * temp_normal[0] + vex * temp_normal[1];
	double un = u * temp_normal[0] + v * temp_normal[1];

	temp_vector[0] = h * un - hex * unex;
	temp_vector[0] *= dt * edge;
	temp_vector[1] = h * u * un - hex * uex * unex + 0.5 * gm * (h * h - hex * hex) * temp_normal[0];
	temp_vector[1] *= dt * edge;
	temp_vector[2] = h * v * un - hex * vex * unex + 0.5 * gm * (h * h - hex * hex) * temp_normal[1];
	temp_vector[2] *= dt * edge;

	Lambda[0] = uex;
	Lambda[1] = Lambda[0] + sqrt(gm * h);
	Lambda[2] = Lambda[0] - sqrt(gm * h);

	Eigenvectors(temp_normal);

	work_vector0[0] = 0.;
	work_vector0[1] = 0.;
	work_vector0[2] = 0.;

	work_vector1[0] = 0.;
	work_vector1[1] = 0.;
	work_vector1[2] = 0.;

	double lmax = 0.;
	for (int i = 0; i < size; i++) {
		for (int j = 0; j < size; j++)
			work_vector1[i] += Left[i][j] * temp_vector[j];

		if (Lambda[i] > 0.0)
			work_vector0[i] += work_vector1[i];

		lmax = MAX(lmax, Lambda[i]);
	}

	for (int i = 0; i < size; i++)
		for (int j = 0; j < size; j++)
			node[n].Res[i] += Right[i][j] * work_vector0[j] / 2.;
}

void char_bc_1d(int m)
{
	int n = b_nodes[m].node;
	double x = node[n].coordinate[0];
	//y = node[n].coordinate[1];
	double nx = b_nodes[m].normal[0];
	double ny = b_nodes[m].normal[1];
	double edge = sqrt(nx * nx + ny * ny);

	temp_normal[0] = nx / edge;
	temp_normal[1] = ny / edge;

	double h, u, v, huex;
	if (iteration == 1) {
		h = node[n].Z[0][0];
		v = 0.;
		huex = node[n].P[0][1];
		if (h > cut_off_U * node[n].cut_off_ratio)
			u = huex / h;
		else
			u = 0.;
	} else {
		h = 0.5 * (node[n].Z[0][0] + node[n].Z[2][0]);
		v = 0.;
		huex = 0.5 * (node[n].P[0][1] + node[n].P[2][1]);
		if (h > cut_off_U * node[n].cut_off_ratio)
			u = huex / h;
		else
			u = 0.;
	}
	h += 1.e-6;

	double al = fabs(u) + sqrt(gm * fabs(h)) + 1.e-3;

	double hex, uex;
	if (fabs(x) < 0.00001) {
		//first_streamline(m);
		hex = h;
		uex = 0;
		//vex = 0.;
		temp_normal[0] = 1.;
		temp_normal[1] = 0.;
	} else if (fabs(x - 25.) < 0.00001) {
		hex = 0.;	//cut_off_H - small;
		uex = u;
		//vex = 0.;
		temp_normal[0] = -1.;
		temp_normal[1] = 0.;
	}

	height0 = 0.5 * (hex + h);
	vbar0 = 0.;
	if (height0 < cut_off_U * node[n].cut_off_ratio)
		ubar0 = 0.;
	else
		ubar0 = 0.5 * (uex + u);

	speed_of_sound0 = sqrt(gm * height0);
	//unex = uex * temp_normal[0] + vex * temp_normal[1];
	double un = u * temp_normal[0] + v * temp_normal[1];

	temp_vector[0] = al * (h - hex) + (2. * h * un);	// hex*uex ; 
	temp_vector[0] *= dt * edge;
	temp_vector[1] = al * (h * u - hex * uex) + (0.5 * gm * (h * h + hex * hex) * temp_normal[0] + (2. * h * u * un));
	temp_vector[1] *= dt * edge;
	// temp_vector[1] = 0. ;
	temp_vector[2] = 0.;
	//al*( h*v - hex*vex ) + ( 0.5*gm*( h*h + hex*hex )*temp_normal[1] + 0.5*( h*v*un + hex*vex*unex ) ) ; 
	//temp_vector[2] *= dt * edge ;

	if (fabs(x - 25.) < 0.00001)
		for (int i = 0; i < size; i++)
			node[n].Res[i] += temp_vector[i];

	if (fabs(x) < 0.00001) {
		node[n].Res[1] = 0.;
		node[n].Res[2] = 0.;
	}
	// node[n].dtau = MIN(node[n].dtau ,CFL/(1+(1.5*dt*lmax/(edge*edge))) );
}

void char_bc_1d_dam(int m)
{
	int n = b_nodes[m].node;
	double x = node[n].coordinate[0];
	//y = node[n].coordinate[1];
	double nx = b_nodes[m].normal[0];
	double ny = b_nodes[m].normal[1];
	double edge = sqrt(nx * nx + ny * ny);

	temp_normal[0] = nx / edge;
	temp_normal[1] = ny / edge;

	double h, u, v;
	if (iteration == 1) {
		h = node[n].P[2][0];
		u = node[n].Z[2][1];
		v = node[n].Z[2][2];
	} else {
		h = (node[n].P[2][0] + node[n].P[2][0]) / 2.;
		u = (node[n].Z[0][1] + node[n].Z[2][1]) / 2.;
		v = (node[n].Z[2][2] + node[n].Z[0][2]) / 2.;
	}

	double hex, uex, vex;
	if (fabs(x) < 0.00001) {
		hex = 10.;
		uex = 0.;
		vex = 0.;
		temp_normal[0] = 1.;
		temp_normal[1] = 0.;
	} else if (fabs(x - 2000.) < 0.00001) {
		hex = 0.;
		uex = u;
		vex = v;
		temp_normal[0] = -1.;
		temp_normal[1] = 0.;
	}

	height0 = 0.5 * (hex + h);
	speed_of_sound0 = sqrt(gm * height0);
	ubar0 = 0.5 * (uex + u);
	vbar0 = 0.5 * (vex + v);
	double unex = uex * temp_normal[0] + vex * temp_normal[1];
	double un = u * temp_normal[0] + v * temp_normal[1];

	temp_vector[0] = h * un - hex * unex;
	temp_vector[0] *= dt * edge;
	temp_vector[1] = h * u * un - hex * uex * unex + 0.5 * gm * (h * h - hex * hex) * temp_normal[0];
	temp_vector[1] *= dt * edge;
	temp_vector[2] = h * v * un - hex * vex * unex + 0.5 * gm * (h * h - hex * hex) * temp_normal[1];
	temp_vector[2] *= dt * edge;

	Lambda[0] = unex;
	Lambda[1] = Lambda[0] + sqrt(gm * hex);
	Lambda[2] = Lambda[0] - sqrt(gm * hex);

	Eigenvectors(temp_normal);

	work_vector0[0] = 0.;
	work_vector0[1] = 0.;
	work_vector0[2] = 0.;

	work_vector1[0] = 0.;
	work_vector1[1] = 0.;
	work_vector1[2] = 0.;

	double lmax = 0.;
	for (int i = 0; i < size; i++) {
		for (int j = 0; j < size; j++)
			work_vector1[i] += Left[i][j] * temp_vector[j];

		if (Lambda[i] > 0.0)
			work_vector0[i] += work_vector1[i];

		lmax = MAX(lmax, Lambda[i]);
	}

	for (int i = 0; i < size; i++)
		for (int j = 0; j < size; j++)
			node[n].Res[i] += Right[i][j] * work_vector0[j] / 2.;

	node[n].dtau = MIN(node[n].dtau, CFL / (1 + (1.5 * dt * lmax / (edge * edge))));
}

void char_bc_2d_smooth(int m)
{
	int n = b_nodes[m].node;
	double x = node[n].coordinate[0];
	double y = node[n].coordinate[1];
	double nx = b_nodes[m].normal[0];
	double ny = b_nodes[m].normal[1];
	double edge = sqrt(nx * nx + ny * ny);

	temp_normal[0] = nx / edge;
	temp_normal[1] = ny / edge;

	double h, u, v;
	if (iteration == 1) {
		h = node[n].P[2][0];
		u = node[n].Z[2][1];
		v = node[n].Z[2][2];
	} else {
		h = (node[n].P[2][0] + node[n].P[2][0]) / 2.;
		u = (node[n].Z[0][1] + node[n].Z[2][1]) / 2.;
		v = (node[n].Z[2][2] + node[n].Z[0][2]) / 2.;
	}

	double hex;
	if (iteration == 1)
		hex = 1. - node[n].bed[2];
	else
		hex = 1. - 0.5 * (node[n].bed[2] + node[n].bed[0]);
	double uex = 0.;
	double vex = 0.;

	if (fabs(x) < 1.e-10) {
		temp_normal[0] = 1.;
		temp_normal[1] = 0.;
		/* if ( fabs( y ) < 1.e-10  ) {
			temp_normal[0] = 0.5*sqrt( 2.0 );
			temp_normal[1] = 0.5*sqrt( 2.0 );
		} else if ( fabs( y - 1. ) < 1.e-10  ) {
			temp_normal[0] = 0.5*sqrt( 2.0 );
			temp_normal[1] = -0.5*sqrt( 2.0 );
		} */
	} else if (fabs(x - 2.) < 1.e-10) {
		temp_normal[0] = -1.;
		temp_normal[1] = 0.;
		/*if ( fabs( y ) < 1.e-10 ) {
			temp_normal[0] = -0.5*sqrt( 2.0 );
			temp_normal[1] = 0.5*sqrt( 2.0 );
		} else if ( fabs( y -1. ) < 1.e-10 ) {
			temp_normal[0] = -0.5*sqrt( 2.0 );
			temp_normal[1] = -0.5*sqrt( 2.0 );
		} */
	}

	height0 = hex;
	speed_of_sound0 = sqrt(gm * height0);
	ubar0 = 0.5 * (uex + u);
	vbar0 = 0.5 * (vex + v);
	ubar0 += UUinf * gridsize * node[n].cut_off_ratio / L_ref;
	vbar0 += VVinf * gridsize * node[n].cut_off_ratio / L_ref;

	double unex = 0.;		//uex*temp_normal[0] + vex*temp_normal[1] ;
	double un = u * temp_normal[0] + v * temp_normal[1];

	temp_vector[0] = h * un - hex * unex;
	temp_vector[0] *= dt * edge;
	temp_vector[1] = h * u * un - hex * uex * unex + 0.5 * gm * (h * h - hex * hex) * temp_normal[0];
	temp_vector[1] *= dt * edge;
	temp_vector[2] = h * v * un - hex * vex * unex + 0.5 * gm * (h * h - hex * hex) * temp_normal[1];
	temp_vector[2] *= dt * edge;

	Lambda[0] = 0.5 * un;
	Lambda[1] = Lambda[0] + sqrt(gm * 0.5 * hex + gm * 0.5 * h);
	Lambda[2] = Lambda[0] - sqrt(gm * 0.5 * hex + gm * 0.5 * h);

	Eigenvectors(temp_normal);

	work_vector0[0] = 0.;
	work_vector0[1] = 0.;
	work_vector0[2] = 0.;

	work_vector1[0] = 0.;
	work_vector1[1] = 0.;
	work_vector1[2] = 0.;

	work_vector2[0] = 0.;
	work_vector2[1] = 0.;
	work_vector2[2] = 0.;

	double lmax = 0.; 
	for (int i = 0; i < size; i++) {
		for (int j = 0; j < size; j++)
			work_vector1[i] += Left[i][j] * temp_vector[j];

		if (Lambda[i] > 0.0)
			work_vector0[i] += work_vector1[i];

		lmax = MAX(lmax, fabs(Lambda[i]));
	}

	for (int i = 0; i < size; i++)
		for (int j = 0; j < size; j++)
			work_vector2[i] += Right[i][j] * work_vector0[j] / 2.;

	for (int i = 0; i < size; i++)
		node[n].Res[i] += work_vector2[i];

	boundary_flux -= work_vector2[0];

	if (fabs(y) < 1.e-10) {
		temp_normal[0] = 0.;
		temp_normal[1] = 1.0;

		double phin = node[n].Res[1] * temp_normal[0] + node[n].Res[2] * temp_normal[1];
		node[n].Res[1] -= phin * temp_normal[0];
		node[n].Res[2] -= phin * temp_normal[1];

		un = node[n].P[2][1] * temp_normal[0] + node[n].P[2][2] * temp_normal[1];
		node[n].P[2][1] -= un * temp_normal[0];
		node[n].P[2][2] -= un * temp_normal[1];
	}

	if (fabs(y - 1.) < 1.e-10) {
		temp_normal[0] = 0.;
		temp_normal[1] = -1.0;

		double phin = node[n].Res[1] * temp_normal[0] + node[n].Res[2] * temp_normal[1];
		node[n].Res[1] -= phin * temp_normal[0];
		node[n].Res[2] -= phin * temp_normal[1];

		un = node[n].P[2][1] * temp_normal[0] + node[n].P[2][2] * temp_normal[1];
		node[n].P[2][1] -= un * temp_normal[0];
		node[n].P[2][2] -= un * temp_normal[1];
	}
}

void char_bc_dam(int m)
{
	int n = b_nodes[m].node;
	double x = node[n].coordinate[0];
	double y = node[n].coordinate[1];
	double nx = b_nodes[m].normal[0];
	double ny = b_nodes[m].normal[1];
	double edge = sqrt(nx * nx + ny * ny);

	temp_normal[0] = nx / edge;
	temp_normal[1] = ny / edge;

	double h, u, v;
	if (iteration == 1) {
		h = node[n].Z[2][0];
		u = node[n].Z[2][1];
		v = node[n].Z[2][2];
	} else {
		h = 0.5 * (node[n].Z[2][0] + node[n].Z[0][0]);
		u = 0.5 * (node[n].Z[2][1] + node[n].Z[0][1]);
		v = 0.5 * (node[n].Z[2][2] + node[n].Z[0][2]);
	}

	double hex = h;
	double uex = u;
	double vex = v;

	if (fabs(x) < 0.00001) {
		temp_normal[0] = 1.;
		temp_normal[1] = 0.;
		/*if ( fabs( y + 1. ) < 0.00001 ) {
			temp_normal[0] = 0.5*sqrt( 2.0 );
			temp_normal[1] = 0.5*sqrt( 2.0 );
		} else if ( fabs( y - 1. ) < 0.00001 ) {
			temp_normal[0] = 0.5*sqrt( 2.0 );
			temp_normal[1] = -0.5*sqrt( 2.0 );
		}*/
	} else if (fabs(x - 100.) < 0.00001) {
		temp_normal[0] = -1.;
		temp_normal[1] = 0.;
		/*if ( fabs( y + 1. ) < 0.00001 ) {
			temp_normal[0] = -0.5*sqrt( 2.0 );
			temp_normal[1] = 0.5*sqrt( 2.0 );
		} else if ( fabs( y - 1. ) < 0.00001 ) {
			temp_normal[0] = -0.5*sqrt( 2.0 );
			temp_normal[1] = -0.5*sqrt( 2.0 );
		} */
	} else if (fabs(y) < 0.000001) {
		//vex = 0.;
		temp_normal[0] = 0.;
		temp_normal[1] = 1.;
	} else if (fabs(y - 100.) < 0.00001) {
		//vex = 0.;
		temp_normal[0] = 0.;
		temp_normal[1] = -1.;
	}

	height0 = 0.5 * (hex + h);
	speed_of_sound0 = sqrt(gm * height0);
	ubar0 = 0.5 * (uex + u);
	vbar0 = 0.5 * (vex + v);
	double unex = uex * temp_normal[0] + vex * temp_normal[1];
	double un = u * temp_normal[0] + v * temp_normal[1];

	temp_vector[0] = h * un - hex * unex;
	temp_vector[0] *= dt * edge;

	temp_vector[1] = h * u * un - hex * uex * unex + 0.5 * gm * (h * h - hex * hex) * temp_normal[0];
	temp_vector[1] *= dt * edge;

	temp_vector[2] = h * v * un - hex * vex * unex + 0.5 * gm * (h * h - hex * hex) * temp_normal[1];
	temp_vector[2] *= dt * edge;

	Lambda[0] = unex;
	Lambda[1] = Lambda[0] + sqrt(gm * hex);
	Lambda[2] = Lambda[0] - sqrt(gm * hex);

	Eigenvectors(temp_normal);

	work_vector0[0] = 0.;
	work_vector0[1] = 0.;
	work_vector0[2] = 0.;

	work_vector1[0] = 0.;
	work_vector1[1] = 0.;
	work_vector1[2] = 0.;

	double lmax = 0.;
	for (int i = 0; i < size; i++) {
		for (int j = 0; j < size; j++)
			work_vector1[i] += Left[i][j] * temp_vector[j];

		if (Lambda[i] > 0.0)
			work_vector0[i] += work_vector1[i];

		lmax = MAX(lmax, fabs(Lambda[i]));
	}

	for (int i = 0; i < size; i++)
		for (int j = 0; j < size; j++)
			node[n].Res[i] += Right[i][j] * work_vector0[j] * 0.5;
	node[n].dtau = MIN(node[n].dtau, CFL / (1 + (1.5 * dt * lmax / (edge * edge))));
}

void char_bc_lake(int m)
{
	int n = b_nodes[m].node;
	double x = node[n].coordinate[0];
	double y = node[n].coordinate[1];
	double nx = b_nodes[m].normal[0];
	double ny = b_nodes[m].normal[1];
	double edge = sqrt(nx * nx + ny * ny);

	temp_normal[0] = nx / edge;
	temp_normal[1] = ny / edge;

	double h, u, v;
	if (iteration == 1) {
		h = node[n].Z[2][0];
		u = node[n].Z[2][1];
		v = node[n].Z[2][2];
	} else {
		h = 0.5 * (node[n].Z[2][0] + node[n].Z[0][0]);
		u = 0.5 * (node[n].Z[2][1] + node[n].Z[0][1]);
		v = 0.5 * (node[n].Z[2][2] + node[n].Z[0][2]);
	}

	double hex = h;
	double uex = u;
	double vex = v;

	if (fabs(x) < 0.00001) {
		temp_normal[0] = 1.;
		temp_normal[1] = 0.;
		/*if ( fabs( y + 1. ) < 0.00001 ) {
			temp_normal[0] = 0.5*sqrt( 2.0 );
			temp_normal[1] = 0.5*sqrt( 2.0 );
		} else if ( fabs( y - 1. ) < 0.00001 ) {
			temp_normal[0] = 0.5*sqrt( 2.0 );
			temp_normal[1] = -0.5*sqrt( 2.0 );
		} */
	} else if (fabs(x - 1.) < 0.00001) {
		temp_normal[0] = -1.;
		temp_normal[1] = 0.;
		/*if ( fabs( y + 1. ) < 0.00001 ) {
			temp_normal[0] = -0.5*sqrt( 2.0 );
			temp_normal[1] = 0.5*sqrt( 2.0 );
		} else if ( fabs( y - 1. ) < 0.00001 ) {
			temp_normal[0] = -0.5*sqrt( 2.0 );
			temp_normal[1] = -0.5*sqrt( 2.0 );
		} */
	} else if (fabs(y) < 0.000001) {
		//vex = 0.;
		temp_normal[0] = 0.;
		temp_normal[1] = 1.;
	} else if (fabs(y - 1.) < 0.00001) {
		//vex = 0.;
		temp_normal[0] = 0.;
		temp_normal[1] = -1.;
	}

	height0 = 0.5 * (hex + h);
	speed_of_sound0 = sqrt(gm * height0);
	ubar0 = 0.5 * (uex + u);
	vbar0 = 0.5 * (vex + v);
	double unex = uex * temp_normal[0] + vex * temp_normal[1];
	double un = u * temp_normal[0] + v * temp_normal[1];

	temp_vector[0] = h * un - hex * unex;
	temp_vector[0] *= dt * edge;

	temp_vector[1] = h * u * un - hex * uex * unex + 0.5 * gm * (h * h - hex * hex) * temp_normal[0];
	temp_vector[1] *= dt * edge;

	temp_vector[2] = h * v * un - hex * vex * unex + 0.5 * gm * (h * h - hex * hex) * temp_normal[1];
	temp_vector[2] *= dt * edge;

	Lambda[0] = unex;
	Lambda[1] = Lambda[0] + sqrt(gm * hex);
	Lambda[2] = Lambda[0] - sqrt(gm * hex);

	Eigenvectors(temp_normal);

	work_vector0[0] = 0.;
	work_vector0[1] = 0.;
	work_vector0[2] = 0.;

	work_vector1[0] = 0.;
	work_vector1[1] = 0.;
	work_vector1[2] = 0.;

	double lmax = 0.;
	for (int i = 0; i < size; i++) {
		for (int j = 0; j < size; j++)
			work_vector1[i] += Left[i][j] * temp_vector[j];

		if (Lambda[i] > 0.0)
			work_vector0[i] += work_vector1[i];

		lmax = MAX(lmax, Lambda[i]);
	}

	for (int i = 0; i < size; i++)
		for (int j = 0; j < size; j++)
			node[n].Res[i] += Right[i][j] * work_vector0[j] * 0.5;
	node[n].dtau = MIN(node[n].dtau, CFL / (1 + (1.5 * dt * lmax / (edge * edge))));
}


void char_bc_conical_island(int m)
{
	double loc_time;
	if (iteration == 1)
		loc_time = Time - dt;
	if (iteration == 2)
		loc_time = Time - 0.5 * dt;

	int kmax = 0; //number of extra waves
	int n = b_nodes[m].node;
	double h0 = 0.32;
	double x = node[n].coordinate[0];
	//y = node[n].coordinate[1];

	if (!(fabs(x) < 1.e-6) && !(fabs(x - 25.) < 1.e-6))
		node[n].Res[2] = 0.;

	if (x < 1.e-6) {
		// loc_time = Time;
		int xs = 0.;
		h0 = 0.32;
		double alpha = 0.2;
		double c = sqrt(gm * h0 + gm * alpha * h0);
		double beta = sqrt(3. * alpha * h0 / (4. * h0 * h0 * h0)) * (x - xs + c * loc_time);
		double sech = 1. / (cosh(beta));
		double etai = alpha * h0 * sech * sech;
		double hex = h0 + etai;
		double uex = sqrt(gm / h0) * etai;

		if (kmax >= 1) {
			for (int kk = 1; kk < kmax; kk++) {
				xs = kk * 7.2;
				alpha = 0.2;
				c = sqrt(gm * h0 + gm * alpha * h0);
				beta = sqrt(3. * alpha * h0 / (4. * h0 * h0 * h0)) * (x - xs + c * loc_time);

				sech = 1. / (cosh(beta));
				etai = alpha * h0 * sech * sech;

				hex += etai;
				uex += sqrt(gm / h0) * etai;
			}
		}
		double vex = 0.;

		if (Time < 4 + 2 * kmax) {
			boundary_flux += node[n].Res[0];
			node[n].Res[0] = (node[n].P[0][0] - hex) * node[n].mod_volume;
			node[n].Res[1] = (node[n].P[0][1] - hex * uex) * node[n].mod_volume;
			node[n].Res[2] = (node[n].P[0][2]) * node[n].mod_volume;
			boundary_flux -= (node[n].P[0][0] - hex) * node[n].mod_volume;
		} else {
			//characteristic BCs with fixed height = h0
			double nx = b_nodes[m].normal[0];
			double ny = b_nodes[m].normal[1];
			double edge = sqrt(nx * nx + ny * ny);

			temp_normal[0] = 1.;
			temp_normal[1] = 0.;

			double h, u, v;
			if (iteration == 1) {
				h = node[n].Z[2][0];
				u = node[n].Z[2][1];
				v = node[n].Z[2][2];
			} else {
				h = 0.5 * (node[n].Z[2][0] + node[n].Z[0][0]);
				u = 0.5 * (node[n].Z[2][1] + node[n].Z[0][1]);
				v = 0.5 * (node[n].Z[2][2] + node[n].Z[0][2]);
			}

			//hex = h0;
			//uex = 0.;
			//vex = 0.;

			height0 = 0.5 * (hex + h);
			speed_of_sound0 = sqrt(gm * height0);
			ubar0 = 0.5 * (uex + u);
			vbar0 = 0.5 * (vex + v);
			double unex = uex * temp_normal[0] + vex * temp_normal[1];
			double un = u * temp_normal[0] + v * temp_normal[1];

			temp_vector[0] = h * un - hex * unex;
			temp_vector[0] *= dt * edge;

			temp_vector[1] = h * u * un - hex * uex * unex + 0.5 * gm * (h * h - hex * hex) * temp_normal[0];
			temp_vector[1] *= dt * edge;

			temp_vector[2] = h * v * un - hex * vex * unex + 0.5 * gm * (h * h - hex * hex) * temp_normal[1];
			temp_vector[2] *= dt * edge;

			Lambda[0] = unex;
			Lambda[1] = Lambda[0] + sqrt(gm * hex);
			Lambda[2] = Lambda[0] - sqrt(gm * hex);

			Eigenvectors(temp_normal);

			work_vector0[0] = 0.;
			work_vector0[1] = 0.;
			work_vector0[2] = 0.;

			work_vector1[0] = 0.;
			work_vector1[1] = 0.;
			work_vector1[2] = 0.;

			double lmax = 0.;
			for (int i = 0; i < size; i++) {
				for (int j = 0; j < size; j++)
					work_vector1[i] += Left[i][j] * temp_vector[j];

				if (Lambda[i] > 0.0)
					work_vector0[i] += work_vector1[i];

				lmax = MAX(lmax, fabs(Lambda[i]));
			}

			for (int i = 0; i < size; i++)
				for (int j = 0; j < size; j++)
					node[n].Res[i] += Right[i][j] * work_vector0[j];
			node[n].dtau = MIN(node[n].dtau, CFL / (1 + (1.5 * dt * lmax / (edge * edge))));

			for (int j = 0; j < size; j++)
				boundary_flux -= Right[0][j] * work_vector0[j];
		} // EOCharacteristic BCs
	}

	if (25. - x < 1.e-6) {
		double nx = b_nodes[m].normal[0];
		double ny = b_nodes[m].normal[1];
		double edge = sqrt(nx * nx + ny * ny);

		temp_normal[0] = -1.;
		temp_normal[1] = 0.;

		double h, u, v;
		if (iteration == 1) {
			h = node[n].Z[2][0];
			u = node[n].Z[2][1];
			v = node[n].Z[2][2];
		} else {
			h = 0.5 * (node[n].Z[2][0] + node[n].Z[0][0]);
			u = 0.5 * (node[n].Z[2][1] + node[n].Z[0][1]);
			v = 0.5 * (node[n].Z[2][2] + node[n].Z[0][2]);
		}

		double hex = h0;
		double uex = 0.;
		double vex = 0.;

		height0 = 0.5 * (hex + h);
		speed_of_sound0 = sqrt(gm * height0);
		ubar0 = 0.5 * (uex + u);
		vbar0 = 0.5 * (vex + v);
		double unex = uex * temp_normal[0] + vex * temp_normal[1];
		double un = u * temp_normal[0] + v * temp_normal[1];

		temp_vector[0] = h * un - hex * unex;
		temp_vector[0] *= dt * edge;

		temp_vector[1] = h * u * un - hex * uex * unex + 0.5 * gm * (h * h - hex * hex) * temp_normal[0];
		temp_vector[1] *= dt * edge;

		temp_vector[2] = h * v * un - hex * vex * unex + 0.5 * gm * (h * h - hex * hex) * temp_normal[1];
		temp_vector[2] *= dt * edge;

		Lambda[0] = unex;
		Lambda[1] = Lambda[0] + sqrt(gm * hex);
		Lambda[2] = Lambda[0] - sqrt(gm * hex);

		Eigenvectors(temp_normal);

		work_vector0[0] = 0.;
		work_vector0[1] = 0.;
		work_vector0[2] = 0.;

		work_vector1[0] = 0.;
		work_vector1[1] = 0.;
		work_vector1[2] = 0.;

		double lmax = 0.;
		for (int i = 0; i < size; i++) {
			for (int j = 0; j < size; j++)
				work_vector1[i] += Left[i][j] * temp_vector[j];

			if (Lambda[i] > 0.0)
				work_vector0[i] += work_vector1[i];

			lmax = MAX(lmax, fabs(Lambda[i]));
		}

		for (int i = 0; i < size; i++)
			for (int j = 0; j < size; j++)
				node[n].Res[i] += Right[i][j] * work_vector0[j];
		node[n].dtau = MIN(node[n].dtau, CFL / (1 + (1.5 * dt * lmax / (edge * edge))));

		for (int j = 0; j < size; j++)
			boundary_flux -= Right[0][j] * work_vector0[j];
	}
}

void char_bc_hump(int __attribute__((__unused__)) m)
{
}

void char_bc_dry(int m)
{
	int n = b_nodes[m].node;
	double x = node[n].coordinate[0];
	double y = node[n].coordinate[1];
	double nx = b_nodes[m].normal[0];
	double ny = b_nodes[m].normal[1];
	double edge = sqrt(nx * nx + ny * ny);

	temp_normal[0] = nx / edge;
	temp_normal[1] = ny / edge;

	double h, u, v;
	if (iteration == 1) {
		h = node[n].Z[2][0];
		u = node[n].Z[2][1];
		v = node[n].Z[2][2];
	} else {
		h = 0.5 * (node[n].Z[2][0] + node[n].Z[0][0]);
		u = 0.5 * (node[n].Z[2][1] + node[n].Z[0][1]);
		v = 0.5 * (node[n].Z[2][2] + node[n].Z[0][2]);
	}

	double hex = 0.5;
	double uex = 0.;
	double vex = 0.;

	if (fabs(x + 1.) < 0.00001) {
		//hex = 0.1;    
		//uex = 0.1/h;
		temp_normal[0] = 1.;
		temp_normal[1] = 0.;
		/*if ( fabs( y + 1. ) < 0.00001 ) {
			temp_normal[0] = 0.5*sqrt( 2.0 );
			temp_normal[1] = 0.5*sqrt( 2.0 );
		} else if ( fabs( y - 1. ) < 0.00001 ) {
			temp_normal[0] = 0.5*sqrt( 2.0 );
			temp_normal[1] = -0.5*sqrt( 2.0 );
		} */
	} else if (fabs(x - 1.) < 0.00001) {
		//hex = 0.1;    
		temp_normal[0] = -1.;
		temp_normal[1] = 0.;
		/*if ( fabs( y + 1. ) < 0.00001 ) {
			temp_normal[0] = -0.5*sqrt( 2.0 );
			temp_normal[1] = 0.5*sqrt( 2.0 );
		} else if ( fabs( y - 1. ) < 0.00001 ) {
			temp_normal[0] = -0.5*sqrt( 2.0 );
			temp_normal[1] = -0.5*sqrt( 2.0 );
		} */
	} else if (fabs(x) < 0.000001) {
		//uex = 0.;
		temp_normal[0] = 1.;
		temp_normal[1] = 0.;
	} else if (fabs(x - 20.) < 0.000001) {
		hex = 0.5;
		temp_normal[0] = -1.;
		temp_normal[1] = 0.;
	} else if (fabs(y + 1.) < 0.000001) {
		//vex = 0.;
		temp_normal[0] = 0.;
		temp_normal[1] = 1.;
	} else if (fabs(y - 1.) < 0.00001) {
		//vex = 0.;
		temp_normal[0] = 0.;
		temp_normal[1] = -1.;
	}

	height0 = 0.5 * (hex + h);
	speed_of_sound0 = sqrt(gm * height0);
	ubar0 = 0.5 * (uex + u);
	vbar0 = 0.5 * (vex + v);
	double unex = uex * temp_normal[0] + vex * temp_normal[1];
	double un = u * temp_normal[0] + v * temp_normal[1];

	temp_vector[0] = h * un - hex * unex;
	temp_vector[0] *= dt * edge;
	temp_vector[1] = h * u * un - hex * uex * unex + 0.5 * gm * (h * h - hex * hex) * temp_normal[0];
	temp_vector[1] *= dt * edge;
	temp_vector[2] = h * v * un - hex * vex * unex + 0.5 * gm * (h * h - hex * hex) * temp_normal[1];
	temp_vector[2] *= dt * edge;

	Lambda[0] = unex;
	Lambda[1] = Lambda[0] + sqrt(gm * hex);
	Lambda[2] = Lambda[0] - sqrt(gm * hex);

	Eigenvectors(temp_normal);

	work_vector0[0] = 0.;
	work_vector0[1] = 0.;
	work_vector0[2] = 0.;

	work_vector1[0] = 0.;
	work_vector1[1] = 0.;
	work_vector1[2] = 0.;

	double lmax = 0.;
	for (int i = 0; i < size; i++) {
		for (int j = 0; j < size; j++)
			work_vector1[i] += Left[i][j] * temp_vector[j];

		if (Lambda[i] > 0.0)
			work_vector0[i] += work_vector1[i];

		lmax = MAX(lmax, Lambda[i]);
	}

	for (int i = 0; i < size; i++)
		for (int j = 0; j < size; j++)
			node[n].Res[i] += Right[i][j] * work_vector0[j] / 2.;
	node[n].dtau = MIN(node[n].dtau, CFL / (1 + (1.5 * dt * lmax / (edge * edge))));

}

void char_bc_hump_2d(int m)
{
	int n = b_nodes[m].node;
	double x = node[n].coordinate[0];
	double y = node[n].coordinate[1];
	double nx = b_nodes[m].normal[0];
	double ny = b_nodes[m].normal[1];
	double edge = sqrt(nx * nx + ny * ny);

	temp_normal[0] = nx / edge;
	temp_normal[1] = ny / edge;

	double h, u, v;
	if (iteration == 1) {
		h = node[n].Z[2][0];
		u = node[n].Z[2][1];
		v = node[n].Z[2][2];
	} else {
		h = 0.5 * (node[n].Z[2][0] + node[n].Z[0][0]);
		u = 0.5 * (node[n].Z[2][1] + node[n].Z[0][1]);
		v = 0.5 * (node[n].Z[2][2] + node[n].Z[0][2]);
	}

	double hex = h;
	double uex = u;
	double vex = v;

	if (fabs(y - 2.) < 0.00001) {
		//hex = 0.1;    
		//uex = 0.1/h;
		temp_normal[0] = 0.;
		temp_normal[1] = -1;
		/*if ( fabs( y + 1. ) < 0.00001 ) {
			temp_normal[0] = 0.5*sqrt( 2.0 );
			temp_normal[1] = 0.5*sqrt( 2.0 );
		} else if ( fabs( y - 1. ) < 0.00001 ) {
			temp_normal[0] = 0.5*sqrt( 2.0 );
			temp_normal[1] = -0.5*sqrt( 2.0 );
		} */
	} else if (fabs(y) < 0.00001) {
		//hex = 0.1;    
		temp_normal[0] = 0.;
		temp_normal[1] = 1.;
		/*if ( fabs( y + 1. ) < 0.00001 ) {
			temp_normal[0] = -0.5*sqrt( 2.0 );
			temp_normal[1] = 0.5*sqrt( 2.0 );
		} else if ( fabs( y - 1. ) < 0.00001 ) {
			temp_normal[0] = -0.5*sqrt( 2.0 );
			temp_normal[1] = -0.5*sqrt( 2.0 );
		} */
	} else if (fabs(x) < 0.000001) {
		if (iteration == 1) {
			hex = MAX(0., 1. - node[n].bed[2]);
		} else {
			hex = 0.5 * (node[n].bed[2] + node[n].bed[0]);
			hex = MAX(0., 1. - hex);
		}
		uex = 0.;
		vex = 0.;
		temp_normal[0] = 1.;
		temp_normal[1] = 0.;
	} else if (fabs(x - 2.) < 0.000001) {
		if (iteration == 1) {
			hex = MAX(0., 1. - node[n].bed[2]);
		} else {
			hex = 0.5 * (node[n].bed[2] + node[n].bed[0]);
			hex = MAX(0., 1. - hex);
		}
		uex = 0.;
		vex = 0.;
		temp_normal[0] = -1.;
		temp_normal[1] = 0.;

	}

	height0 = 0.5 * (hex + h);
	speed_of_sound0 = sqrt(gm * height0);
	ubar0 = 0.5 * (uex + u);
	vbar0 = 0.5 * (vex + v);
	double unex = uex * temp_normal[0] + vex * temp_normal[1];
	double un = u * temp_normal[0] + v * temp_normal[1];

	temp_vector[0] = h * un - hex * unex;
	temp_vector[0] *= dt * edge;
	temp_vector[1] = h * u * un - hex * uex * unex + 0.5 * gm * (h * h - hex * hex) * temp_normal[0];
	temp_vector[1] *= dt * edge;
	temp_vector[2] = h * v * un - hex * vex * unex + 0.5 * gm * (h * h - hex * hex) * temp_normal[1];
	temp_vector[2] *= dt * edge;

	Lambda[0] = unex;
	Lambda[1] = Lambda[0] + sqrt(gm * hex);
	Lambda[2] = Lambda[0] - sqrt(gm * hex);

	Eigenvectors(temp_normal);

	work_vector0[0] = 0.;
	work_vector0[1] = 0.;
	work_vector0[2] = 0.;

	work_vector1[0] = 0.;
	work_vector1[1] = 0.;
	work_vector1[2] = 0.;

	double lmax = 0.;
	for (int i = 0; i < size; i++) {
		for (int j = 0; j < size; j++)
			work_vector1[i] += Left[i][j] * temp_vector[j];

		if (Lambda[i] > 0.0)
			work_vector0[i] += work_vector1[i];

		lmax = MAX(lmax, Lambda[i]);
	}

	for (int i = 0; i < size; i++)
		for (int j = 0; j < size; j++)
			node[n].Res[i] += Right[i][j] * work_vector0[j] / 2.;
	node[n].dtau = MIN(node[n].dtau, CFL / (1 + (1.5 * dt * lmax / (edge * edge))));
	node[n].sum_alpha += lmax;
}


void char_bc_thacker(int m)
{
	int n = b_nodes[m].node;
	for (int i = 0; i < size; i++) {
		node[n].Res[i] = 0.;
		node[n].P[2][i] = 0.;
		node[n].Z[2][i] = 0.;
	}
}

void char_bc_steady_exner(int m)
{
	int n = b_nodes[m].node;
	double x = node[n].coordinate[0];
	double y = node[n].coordinate[1];

	if (y < 1.e-6)
		node[n].Res[2] = 0.;
	if (1000. - y < 1.e-6)
		node[n].Res[2] = 0.;
	if (x < 1.e-6) {
		node[n].Res[1] = 0.;
		node[n].Res[2] = 0.;
	}
	if (1000. - x < 1.e-6) {
		node[n].Res[0] = 0.;
		node[n].Res[2] = 0.;
	}
}

void char_bc_reef(int m)
{
	int n = b_nodes[m].node;
	//x = node[n].coordinate[0];
	double y = node[n].coordinate[1];
	double nx = b_nodes[m].normal[0];
	double ny = b_nodes[m].normal[1];
	double edge = sqrt(nx * nx + ny * ny);

	temp_normal[0] = nx / edge;
	temp_normal[1] = ny / edge;

	double Z0 = 10.;
	double A0 = 2.;

	//Ttide = 12.*60.*60. + 25.*60. ;
	double Ttide = 600.;
	double htide = MAX(0, Z0 + A0 * sin(2. * pi * Time / Ttide - 0.5 * pi) - node[n].bed[2]);
	double utide = sqrt(gm / 4000) * A0 * sin(2. * pi * Time / Ttide - 0.5 * pi);

	double h, u, v;
	if (iteration == 1) {
		h = node[n].P[2][0];
		u = node[n].P[2][1] / node[n].P[2][0];
		v = node[n].P[2][2] / node[n].P[2][0];
	} else {
		h = 0.5 * (node[n].P[2][0] + node[n].P[0][0]);
		u = 0.5 * (node[n].P[2][1] + node[n].P[0][1]) / h;
		v = 0.5 * (node[n].P[2][2] + node[n].P[0][2]) / h;
	}

	double hex, uex, vex;
	if (fabs(node[n].coordinate[0] + 25000) < 1.e-3) {
		if (fabs(node[n].coordinate[1] - 25000) < 1.e-3) {
			//temp_normal[0] = 0.5*sqrt( 2. ) ;
			//temp_normal[1] = -0.5*sqrt( 2. ) ;
			temp_normal[0] = 0.;
			temp_normal[1] = -1.;

			hex = h;
			uex = u;
			vex = 0.;
		} else {
			if (fabs(node[n].coordinate[1] + 25000) < 1.e-3) {
				//temp_normal[0] = 0.5*sqrt( 2. ) ;
				//temp_normal[1] = 0.5*sqrt( 2. ) ;
				temp_normal[0] = 0.;
				temp_normal[1] = 1.;

				hex = htide;
				uex = utide;
				vex = 0.;
			} else {
				temp_normal[0] = 1.0;
				temp_normal[1] = 0.;

				hex = h;
				uex = 0.;
				vex = v;
			}
		}
	} else {
		if (fabs(node[n].coordinate[0] - 25000) < 1.e-3) {
			if (fabs(node[n].coordinate[1] - 25000) < 1.e-3) {
				//temp_normal[0] = -0.5*sqrt( 2. ) ;
				//temp_normal[1] = -0.5*sqrt( 2. ) ;
				temp_normal[0] = 0.;
				temp_normal[1] = -1.;

				hex = h;
				uex = u;
				vex = 0.;
			} else {
				if (fabs(node[n].coordinate[1] + 25000) < 1.e-3) {
					//temp_normal[0] = -0.5*sqrt( 2. ) ;
					//temp_normal[1] = 0.5*sqrt( 2. ) ;
					temp_normal[0] = 0.;
					temp_normal[1] = 1.;

					hex = htide;
					uex = utide;
					vex = 0.;
				} else {
					temp_normal[0] = -1.0;
					temp_normal[1] = 0.;

					hex = h;
					uex = 0.;
					vex = v;
				}
			}
		} else {
			temp_normal[0] = 0.;
			if (y > 0.) {
				temp_normal[1] = -1.;

				hex = h;
				uex = u;
				vex = 0.;
			} else {
				temp_normal[1] = 1.;

				hex = htide;
				uex = utide;
				vex = 0.;
			}
		}
	}

	//if ( fabs( node[n].coordinate[1] + 25000 ) < 1.e-3 ) {
	//	node[n].Res[0] =  ( node[n].P[0][0] - htide )*node[n].volume ;
	//	node[n].Res[2] =  0. ;
	//} else {
	height0 = hex;
	speed_of_sound0 = sqrt(gm * height0);
	ubar0 = uex;
	vbar0 = vex;
	double unex = uex * temp_normal[0] + vex * temp_normal[1];
	double un = u * temp_normal[0] + v * temp_normal[1];

	temp_vector[0] = h * un - hex * unex;
	temp_vector[0] *= dt * edge;
	temp_vector[1] = h * u * un - hex * uex * unex + 0.5 * gm * (h * h - hex * hex) * temp_normal[0];
	temp_vector[1] *= dt * edge;
	temp_vector[2] = h * v * un - hex * vex * unex + 0.5 * gm * (h * h - hex * hex) * temp_normal[1];
	temp_vector[2] *= dt * edge;

	Lambda[0] = unex;
	Lambda[1] = Lambda[0] + sqrt(gm * hex);
	Lambda[2] = Lambda[0] - sqrt(gm * hex);

	Eigenvectors(temp_normal);

	work_vector0[0] = 0.;
	work_vector0[1] = 0.;
	work_vector0[2] = 0.;

	work_vector1[0] = 0.;
	work_vector1[1] = 0.;
	work_vector1[2] = 0.;

	work_vector2[0] = 0.;
	work_vector2[1] = 0.;
	work_vector2[2] = 0.;

	double lmax = 0.;
	for (int i = 0; i < size; i++) {
		for (int j = 0; j < size; j++)
			work_vector1[i] += Left[i][j] * temp_vector[j];

		if (Lambda[i] > 0.0)
			work_vector0[i] += work_vector1[i];

		lmax = MAX(lmax, Lambda[i]);
	}

	for (int i = 0; i < size; i++)
		for (int j = 0; j < size; j++)
			node[n].Res[i] += Right[i][j] * work_vector0[j];

	node[n].dtau = MIN(node[n].dtau, CFL / (1 + (1.5 * dt * lmax / (edge * edge))));
}

void char_bc_run_up(int m)
{
	int n = b_nodes[m].node;
	//x = node[n].coordinate[0];
	double y = node[n].coordinate[1];
	double nx = b_nodes[m].normal[0];
	double ny = b_nodes[m].normal[1];
	double edge = sqrt(nx * nx + ny * ny);

	temp_normal[0] = nx / edge;
	temp_normal[1] = ny / edge;

	double h, u, v;
	if (iteration == 1) {
		h = node[n].P[2][0];
		u = node[n].Z[2][1];
		v = node[n].Z[2][2];
	} else {
		h = (node[n].P[2][0] + node[n].P[0][0]) / 2.;
		u = (node[n].Z[0][1] + node[n].Z[2][1]) / 2.;
		v = (node[n].Z[2][2] + node[n].Z[0][2]) / 2.;
	}

	double eta0 = 0.0;
	if (y < 0.0)
		eta0 = node[n].bed[2];

	double hex = eta0 - node[n].bed[2];
	double uex = 0.;
	double vex = 0.;

	if (fabs(y - 50000.) < 0.01) {
		temp_normal[0] = 0.;
		temp_normal[1] = -1.;

		height0 = hex;
		speed_of_sound0 = sqrt(gm * height0);
		ubar0 = 0.5 * (uex + u);
		vbar0 = 0.5 * (vex + v);
		ubar0 += UUinf * gridsize * node[n].cut_off_ratio / L_ref;
		vbar0 += VVinf * gridsize * node[n].cut_off_ratio / L_ref;

		double unex = 0.;	//uex*temp_normal[0] + vex*temp_normal[1] ;
		double un = u * temp_normal[0] + v * temp_normal[1];

		temp_vector[0] = h * un - hex * unex;
		temp_vector[0] *= dt * edge;
		temp_vector[1] = h * u * un - hex * uex * unex + 0.5 * gm * (h * h - hex * hex) * temp_normal[0];
		temp_vector[1] *= dt * edge;
		temp_vector[2] = h * v * un - hex * vex * unex + 0.5 * gm * (h * h - hex * hex) * temp_normal[1];
		temp_vector[2] *= dt * edge;

		Lambda[0] = 0.5 * un;
		Lambda[1] = Lambda[0] + sqrt(gm * 0.5 * hex + gm * 0.5 * h);
		Lambda[2] = Lambda[0] - sqrt(gm * 0.5 * hex + gm * 0.5 * h);

		Eigenvectors(temp_normal);

		work_vector0[0] = 0.;
		work_vector0[1] = 0.;
		work_vector0[2] = 0.;

		work_vector1[0] = 0.;
		work_vector1[1] = 0.;
		work_vector1[2] = 0.;

		work_vector2[0] = 0.;
		work_vector2[1] = 0.;
		work_vector2[2] = 0.;

		double lmax = 0.;
		for (int i = 0; i < size; i++) {
			for (int j = 0; j < size; j++)
				work_vector1[i] += Left[i][j] * temp_vector[j];

			if (Lambda[i] > 0.0)
				work_vector0[i] += work_vector1[i];

			lmax = MAX(lmax, fabs(Lambda[i]));
		}

		for (int i = 0; i < size; i++)
			for (int j = 0; j < size; j++)
				work_vector2[i] += Right[i][j] * work_vector0[j] / 2.;

		for (int i = 0; i < size; i++)
			node[n].Res[i] += work_vector2[i];
	}

	if (fabs(y + 200.) < 0.01)
		for (int i = 0; i < size; i++)
			node[n].Res[i] = 0.;
}

/* BC FOR PROJECTION STEP */
void char_bc_conical_island_4prj(int m)
{
	double loc_time;
	if (iteration == 1)
		loc_time = Time - dt;
	if (iteration == 2)
		loc_time = Time - 0.5 * dt;

	int kmax = 0; // number of extra waves
	int n = b_nodes[m].node;
	double h0 = 0.32;
	double x = node[n].coordinate[0];
	//y = node[n].coordinate[1];

	if (!(fabs(x) < 1.e-6) && !(fabs(x - 25.) < 1.e-6))
		node[n].Res[2] = 0.;

	if (x < 1.e-6) {
		// loc_time = Time;
		int xs = 0.;
		h0 = 0.32;
		double alpha = 0.2;
		double c = sqrt(gm * h0 + gm * alpha * h0);
		double beta = sqrt(3. * alpha * h0 / (4. * h0 * h0 * h0)) * (x - xs + c * loc_time);
		double sech = 1. / (cosh(beta));
		double etai = alpha * h0 * sech * sech;
		double hex = h0 + etai;
		double uex = sqrt(gm / h0) * etai;

		if (kmax >= 1) {
			for (int kk = 1; kk < kmax; kk++) {
				xs = kk * 7.2;
				alpha = 0.2;
				c = sqrt(gm * h0 + gm * alpha * h0);
				beta = sqrt(3. * alpha * h0 / (4. * h0 * h0 * h0)) * (x - xs + c * loc_time);

				sech = 1. / (cosh(beta));
				etai = alpha * h0 * sech * sech;

				hex += etai;
				uex += sqrt(gm / h0) * etai;
			}
		}
		//vex = 0.;
		if (Time < 4 + 2 * kmax) {
			node[n].Res[0] = (node[n].P[0][0] - hex) * node[n].mod_volume;
			node[n].Res[1] = (node[n].P[0][1] - hex * uex) * node[n].mod_volume;
			node[n].Res[2] = (node[n].P[0][2]) * node[n].mod_volume;
		}
	}
}

void char_bc_okushiri_4prj(int m)
{
	int n = b_nodes[m].node;
	double x = node[n].coordinate[0];
	double y = node[n].coordinate[1];
	double loc_time;
	if (iteration == 1)
		loc_time = Time - dt;
	if (iteration == 2)
		loc_time = Time - 0.5 * dt;

	loc_time = Time;

	if (!(fabs(x) < 0.00001)) {
		if (x < L_ref) { // HORIZONTAL WALL
			node[n].Res[2] = 0.;
		} else if (y > 0.0) { // VERTICAL WALL
			node[n].Res[1] = 0.;
		} else { // CORNER WALL (FV HAD PROBLEMS WITH ORIGINAL BC) 
			//nx = -sqrt(2.0)/2.0 ;
			//ny = sqrt(2.0)/2.0 ;
			//phin = node[n].Res[1]*nx + node[n].Res[2]*ny ;
			node[n].Res[1] = 0.0;
			//node[n].Res[2] -= phin*ny ; 
		}
	} else {
		//nx = b_nodes[m].normal[0];
		//ny = b_nodes[m].normal[1];
		//edge = sqrt(nx * nx + ny * ny);
		temp_normal[0] = 1.;
		temp_normal[1] = 0.;
		if (iteration == 1) {
			//h = node[n].Z[2][0];
			//u = node[n].Z[2][1];
			//v = node[n].Z[2][2];
		} else {
			//h = 0.5 * (node[n].Z[2][0] + node[n].Z[0][0]);
			//u = 0.5 * (node[n].Z[2][1] + node[n].Z[0][1]);
			//v = 0.5 * (node[n].Z[2][2] + node[n].Z[0][2]);
		}

		int jT = 1;
		int keepon = 1;
		while (keepon) {
			if (time_bc[jT] < loc_time)
				jT++;
			else
				keepon = 0;
		}

		double hex = (h_bc[jT] * (loc_time - time_bc[jT - 1]) / (time_bc[jT] - time_bc[jT - 1]) + h_bc[jT - 1] * (loc_time - time_bc[jT]) / (time_bc[jT - 1] - time_bc[jT]));

		hex += MAX(-node[n].bed[2], 0.);

		if (Time < 22.5) {
			node[n].Res[0] = (node[n].P[0][0] - hex) * node[n].mod_volume;
			node[n].Res[2] = node[n].P[0][2] * node[n].mod_volume;
		}
	}
}

void char_bc_convergent_channel( int m )
{
	double loc_time;
//	if ( iteration == 1 )i
//		loc_time = Time - dt;
//	else if ( iteration == 2 )
//		loc_time = Time;
//	else
//		loc_time = Time - 0.5*dt;
//      Original in Luca's code:
	if ( iteration == 1 )
		loc_time = Time - dt;
	else
		loc_time = Time - 0.5*dt;
	
	int n = b_nodes[m].node;
	double x =  node[n].coordinate[0];
	double y =  node[n].coordinate[1];
	double nx = b_nodes[m].normal[0];
	double ny = b_nodes[m].normal[1];
	double edge = sqrt( nx*nx + ny*ny );

	temp_normal[0] = nx/edge;
	temp_normal[1] = ny/edge;

	double h, u, v;
	if (( iteration == 1 ) ){//|| (iteration == 2)){
		h = node[n].Z[2][0];
		u = node[n].Z[2][1];
		v = node[n].Z[2][2]; 
	} else {
		h = 0.5*(node[n].Z[2][0]+node[n].Z[0][0]);
		u = 0.5*(node[n].Z[2][1]+node[n].Z[0][1]);
		v = 0.5*(node[n].Z[2][2] +node[n].Z[0][2]); 
	}
	
	double h_tide, u_tide, v_tide, un_tide, phin, un;
	double L_b = 1.0 , B0 = 0.1, tide_amp = 0.8 ;
 
	/* WEAK BC AT INLET: FAR FIELD TIDAL WAVE */
	double x_start = 0.;
	double x_end = 0.;
	if ( 71 == initial_state ) // Convergent channel [0, 6]*Lb
		x_end = 6.;
	else if ( 85 == initial_state ){ // Convergent channel zoom [0.9, 1.8]*Lb
		x_start = 0.9;
		x_end = 1.8;
	} else if ( 86 == initial_state ){ // Tissier ondular bore
		x_start = -150.;
		x_end = 150.;
	}
	
	if ( fabs ( x - x_start ) < 1.e-6 ) {
		if ( 71 == initial_state ) {
			h_tide = tide_amp*sin(loc_time)  - node[n].bed[2];
			u_tide = 0.0;
			v_tide = 0.0;
		} else if ( 85 == initial_state) {
			h_tide = gaugeIN;
			u_tide = gauge_velIN;
			v_tide = 0.0;
		} else if ( 86 == initial_state) {
			double a = 2.;
			double ha = 1.;
			double ua = 0.;
			double Fr = 1.4;
			double cb = Fr*sqrt(gm*ha);
			double hb1 = 0.5*( -ha + sqrt(ha*ha + 8.*ha*cb*cb/gm) );
			double hb2 = 0.5*( -ha - sqrt(ha*ha + 8.*ha*cb*cb/gm) );
			double hb = 0.;
			if ( (hb1 <= 0.) && (hb2 > 0.) )
				hb = hb2;
			else if ( (hb2 <= 0.) && (hb1 > 0.) )
				hb = hb1;
			else {
				printf("Problem in the computation of hb via the jump relations!!!");
				exit(0);
			}
			double ub = cb*(1. - ha/hb);
			h_tide = 0.5*(hb - ha)*(1. - tanh(x/a)) + ha;
			u_tide = 0.5*(ub - ua)*(1. - tanh(x/a)) + ua;
			v_tide = 0.0;
		}

		height0 = 0.5*( h_tide + h );
		speed_of_sound0 = sqrt( gm*height0 );
		ubar0 = 0.5*( u_tide + u );
		vbar0 = 0.5*( v_tide + v );

		un_tide = u_tide*temp_normal[0] + v_tide*temp_normal[1];
		un = u*temp_normal[0] + v*temp_normal[1];
	
		temp_vector[0] = h*un - h_tide*un_tide; 
		temp_vector[0] *= dt * edge;

		temp_vector[1] = h*u*un - h_tide*u_tide*un_tide + 0.5*gm*( h*h - h_tide*h_tide )*temp_normal[0]; 
		temp_vector[1] *= dt * edge;

		temp_vector[2] = h*v*un - h_tide*v_tide*un_tide + 0.5*gm*( h*h - h_tide*h_tide )*temp_normal[1]; 
		temp_vector[2] *= dt * edge;
	
		Lambda[0] = un;
		Lambda[1] = Lambda[0] + sqrt( gm*h );
		Lambda[2] = Lambda[0] - sqrt( gm*h );

		Eigenvectors( temp_normal );

		work_vector0[0] = 0.;
		work_vector0[1] = 0.;
		work_vector0[2] = 0.;

		work_vector1[0] = 0.;
		work_vector1[1] = 0.;
		work_vector1[2] = 0.;
	
		// FLUX IN CHARACTERISTIC VARIABLE
		double lmax = 0. ;
		for (int i = 0 ; i < size ; i ++ ){
			for (int j = 0 ; j < size ; j ++ )
				work_vector1[i] += Left[i][j]*temp_vector[j];

			if ( Lambda[i] > 0.0 )
				work_vector0[i] += work_vector1[i];
	
			lmax = MAX( lmax, fabs(Lambda[i]) );
		}

		// CORRECTION ON THE CHARACTERISTIC FLUX #2 IN ORDER TO OBTAIN THE INLET SIGNAL ON h 	
		work_vector0[1] = (-h_tide*node[n].volume + node[n].P[0][0]*node[n].volume - node[n].Res[0] - Right[0][0]*work_vector0[0])/Right[0][1];

		// BOUNDARY CORRECTION FLUX
		for (int i = 0 ; i < size ; i ++ )
			for (int j = 0 ; j < size ; j ++ )
				node[n].Res[i] += Right[i][j]*work_vector0[j];

		node[n].dtau = MIN(node[n].dtau ,CFL/(1+(1.5*dt*lmax/(edge*edge))));

		// WALL BC AT CORNER
		if ( y < 1.e-6 ) {
			temp_normal[0] = 0.;
			temp_normal[1] = 1.;
			phin = node[n].Res[1]*temp_normal[0] + node[n].Res[2]*temp_normal[1];

			node[n].Res[1] -= phin*temp_normal[0];
			node[n].Res[2] -= phin*temp_normal[1];
			un = node[n].P[2][1]*temp_normal[0] + node[n].P[2][2]*temp_normal[1];

			node[n].P[2][1] -= un*temp_normal[0];
			node[n].P[2][2] -= un*temp_normal[1];
		}

		if ( fabs( B0/L_b*exp(-x_start) - y ) < 1.e-6 ) {
			temp_normal[0] = - (B0/L_b) / (sqrt(1 + pow(B0/L_b,2)));
			temp_normal[1] = - 1 / (sqrt(1 + pow(B0/L_b,2)));
			phin = node[n].Res[1]*temp_normal[0] + node[n].Res[2]*temp_normal[1];
	
			node[n].Res[1] -= phin*temp_normal[0];
			node[n].Res[2] -= phin*temp_normal[1];
			un = node[n].P[2][1]*temp_normal[0] + node[n].P[2][2]*temp_normal[1];
	
			node[n].P[2][1] -= un*temp_normal[0];
			node[n].P[2][2] -= un*temp_normal[1];
		}

	} else if ( fabs( x - x_end ) < 1.e-6 ) { 
		if ( 71 == initial_state ) { // WEAK BC AT OUTLET: RIVER AT REST AT FAR FIELD
			h_tide = 1.;
			u_tide = 0.0;
			v_tide = 0.0;
		} else if ( 85 == initial_state) {
			h_tide = h;
			u_tide = u;
			v_tide = 0.0;
		} else if ( 86 == initial_state) {
			h_tide = 1.;
			u_tide = 0.0;
			v_tide = 0.0;
		}

		height0 = 0.5*( h_tide + h );
		speed_of_sound0 = sqrt( gm*height0 );
		ubar0 = 0.5*( u_tide + u );
		vbar0 = 0.5*( v_tide + v );

		un_tide = u_tide*temp_normal[0] + v_tide*temp_normal[1];
		un = u*temp_normal[0] + v*temp_normal[1];

		temp_vector[0] = h*un - h_tide*un_tide;
		temp_vector[0] *= dt * edge;

		temp_vector[1] = h*u*un - h_tide*u_tide*un_tide + 0.5*gm*( h*h - h_tide*h_tide )*temp_normal[0]; 
		temp_vector[1] *= dt * edge;

		temp_vector[2] = h*v*un - h_tide*v_tide*un_tide + 0.5*gm*( h*h - h_tide*h_tide )*temp_normal[1]; 
		temp_vector[2] *= dt * edge;
	
		Lambda[0] = un;
		Lambda[1] = Lambda[0] + sqrt( gm*h );
		Lambda[2] = Lambda[0] - sqrt( gm*h );

		Eigenvectors( temp_normal );

		work_vector0[0] = 0.;
		work_vector0[1] = 0.;
		work_vector0[2] = 0.;

		work_vector1[0] = 0.;
		work_vector1[1] = 0.;
		work_vector1[2] = 0.;

		// FLUX IN CHARACTERISTIC VARIABLE
		double lmax = 0. ;
		for ( int i = 0 ; i < size ; i ++ ) {
			for ( int j = 0 ; j < size ; j ++ )
				work_vector1[i] += Left[i][j]*temp_vector[j];
	
			if ( Lambda[i] > 0.0 ) 
				work_vector0[i] += work_vector1[i];
	
			lmax = MAX( lmax, fabs(Lambda[i]) );
		}

		if ( 85 == initial_state ){
			// CORRECTION ON THE CHARACTERISTIC FLUX #2 IN ORDER TO OBTAIN THE INLET SIGNAL ON h 	
			work_vector0[1] = (-h_tide*node[n].volume + node[n].P[0][0]*node[n].volume - node[n].Res[0] - Right[0][0]*work_vector0[0])/Right[0][1];
		}
		
		// BOUNDARY CORRECTION FLUX
		for ( int i = 0 ; i < size ; i ++ )
			for ( int j = 0 ; j < size ; j ++ )
				node[n].Res[i] += Right[i][j]*work_vector0[j];

		node[n].dtau = MIN(node[n].dtau ,CFL/(1+(1.5*dt*lmax/(edge*edge))));

		// WALL BC AT CORNER
		if ( y < 1.e-6 ) {
			temp_normal[0] = 0.;
			temp_normal[1] = 1.;
			phin = node[n].Res[1]*temp_normal[0] + node[n].Res[2]*temp_normal[1];

			node[n].Res[1] -= phin*temp_normal[0];
			node[n].Res[2] -= phin*temp_normal[1];
			un = node[n].P[2][1]*temp_normal[0] + node[n].P[2][2]*temp_normal[1];

			node[n].P[2][1] -= un*temp_normal[0];
			node[n].P[2][2] -= un*temp_normal[1];
		}
		
		if ( fabs( B0/L_b*exp(-x_end) - y ) < 1.e-6 ) {
			temp_normal[0] = - (B0/L_b)*exp(-6.) / (sqrt(1 + pow(B0/L_b*exp(-6.),2)));
			temp_normal[1] = - 1 / (sqrt(1 + pow(B0/L_b*exp(-6.),2)));
			phin = node[n].Res[1]*temp_normal[0] + node[n].Res[2]*temp_normal[1];
	
			node[n].Res[1] -= phin*temp_normal[0];
			node[n].Res[2] -= phin*temp_normal[1];
			un = node[n].P[2][1]*temp_normal[0] + node[n].P[2][2]*temp_normal[1];
	
			node[n].P[2][1] -= un*temp_normal[0];
			node[n].P[2][2] -= un*temp_normal[1];
		}
	}
}

void char_bc_gigado( int m )
{
	//double loc_time;
	//if ( iteration == 1 )
	//	loc_time = Time - dt;
	//else if ( iteration == 2 )
	//	loc_time = Time;
	//else
	//	loc_time = Time - 0.5*dt;

	int n = b_nodes[m].node ;
	double y =  node[n].coordinate[1];
	double nx = b_nodes[m].normal[0];
	double ny = b_nodes[m].normal[1];
	double edge = sqrt( nx*nx + ny*ny );

	temp_normal[0] = nx/edge ;
	temp_normal[1] = ny/edge ;

	double h, u, v;
	if (( iteration == 1 ) || (iteration ==2)){
		h = node[n].Z[2][0];
		u = node[n].Z[2][1];
		v = node[n].Z[2][2]; 
	} else {
		h = 0.5*(node[n].Z[2][0]+node[n].Z[0][0]);
		u = 0.5*(node[n].Z[2][1]+node[n].Z[0][1]);
		v = 0.5*(node[n].Z[2][2] +node[n].Z[0][2]); 
	}
	
	double un, h_tide, u_tide, v_tide, un_tide;
//	double tide_amp = 2.;
	//double tau = 12.41*3600;
	double Q_ga = 140. ;// [m^3/s] 
	double Q_do = 100. ;// [m^3/s] 

	// WEAK BC AT INLET: FAR FIELD TIDAL WAVE
	if ( y > 365000. ) { // gironde
		//h_tide = 3.29 + tide_amp*sin(2*3.14*loc_time/tau) - node[n].bed[2];
		h_tide = TIDE - node[n].bed[2];
		u_tide = 0.0;
		v_tide = 0.0;

		height0 = 0.5*( h_tide + h );
		speed_of_sound0  = sqrt( gm*height0 );
		ubar0 = 0.5*( u_tide + u );
		vbar0 = 0.5*( v_tide + v );

		un_tide = u_tide*temp_normal[0] + v_tide*temp_normal[1];
		un = u*temp_normal[0] + v*temp_normal[1];
	
		temp_vector[0] = h*un - h_tide*un_tide; 
		temp_vector[0] *= dt * edge;

		temp_vector[1] = h*u*un - h_tide*u_tide*un_tide + 0.5*gm*( h*h - h_tide*h_tide )*temp_normal[0]; 
		temp_vector[1] *= dt * edge;

		temp_vector[2] = h*v*un - h_tide*v_tide*un_tide + 0.5*gm*( h*h - h_tide*h_tide )*temp_normal[1]; 
		temp_vector[2] *= dt * edge;
	
		Lambda[0] = un;
		Lambda[1] = Lambda[0] + sqrt( gm*h );
		Lambda[2] = Lambda[0] - sqrt( gm*h );

		Eigenvectors( temp_normal );

		work_vector0[0] = 0.;
		work_vector0[1] = 0.;
		work_vector0[2] = 0.;

		work_vector1[0] = 0.;
		work_vector1[1] = 0.;
		work_vector1[2] = 0.;
	
		// FLUX IN CHARACTERISTIC VARIABLE
		double lmax = 0. ;
		for (int i = 0 ; i < size ; i ++ ) {
			for (int j = 0 ; j < size ; j ++ ) 
				work_vector1[i] += Left[i][j]*temp_vector[j];

			if ( Lambda[i] > 0.0 )
				work_vector0[i] += work_vector1[i];
	
			lmax = MAX( lmax, fabs(Lambda[i]) );
		}

		// CORRECTION ON THE CHARACTERISTIC FLUX #2 IN ORDER TO OBTAIN THE INLET SIGNAL ON h 	
		work_vector0[1] = (-h_tide*node[n].volume + node[n].P[0][0]*node[n].volume - node[n].Res[0] - Right[0][0]*work_vector0[0])/Right[0][1] ;

		// BOUNDARY CORRECTION FLUX
		for (int i = 0 ; i < size ; i ++ )
			for (int j = 0 ; j < size ; j ++ ) node[n].Res[i] += Right[i][j]*work_vector0[j];

		node[n].dtau = MIN(node[n].dtau ,CFL/(1+(1.5*dt*lmax/(edge*edge)))) ;	

		// WALL BC AT CORNERS
                if ((fabs( node[n].coordinate[0] - 3.386597e+05) < 0.000001)  && (fabs(node[n].coordinate[1] - 3.723562e+05) < 0.000001)) { // node 595
                        int n0 = boundary[m-1].node[0];
                        int n1 = boundary[m-1].node[1];
                        double nx = node[n1].coordinate[1] - node[n0].coordinate[1];
                        double ny = node[n0].coordinate[0] - node[n1].coordinate[0];
                        double length = sqrt(nx * nx + ny * ny);
                        nx /= length;
                        ny /= length;
                        double phin = node[n].Res[1] * nx + node[n].Res[2] * ny;
                        node[n].Res[1] -= phin * nx;
                        node[n].Res[2] -= phin * ny;
                        double un = node[n].P[2][1] * nx + node[n].P[2][2] * ny;
                        node[n].P[2][1] -= un * nx;
                        node[n].P[2][2] -= un * ny;
                } else if ((fabs( node[n].coordinate[0] - 3.347489e+05) < 0.000001)  && (fabs(node[n].coordinate[1] - 3.689337e+05) < 0.000001)) { // node 36
                        int n0 = boundary[m].node[0];
                        int n1 = boundary[m].node[1];
                        double nx = node[n1].coordinate[1] - node[n0].coordinate[1];
                        double ny = node[n0].coordinate[0] - node[n1].coordinate[0];
                        double length = sqrt(nx * nx + ny * ny);
                        nx /= length;
                        ny /= length;
                        double phin = node[n].Res[1] * nx + node[n].Res[2] * ny;
                        node[n].Res[1] -= phin * nx;
                        node[n].Res[2] -= phin * ny;
                        double un = node[n].P[2][1] * nx + node[n].P[2][2] * ny;
                        node[n].P[2][1] -= un * nx;
                        node[n].P[2][2] -= un * ny;
                }
		//printf("gironde %d\n",n);
	} else if ( y < 260000. ) { // garonne
		node[n].Res[1] = 0.;
		node[n].Res[2] = 0.;	
		double Qi = b_nodes[m].area/Area_inlet_ga * Q_ga; //fraction of the inlet flux to give to the node "i"
		int itr = iteration - 1;
		node[n].Res[1] = node[n].volume/a_RK[itr][1]*( -Qi/b_nodes[m].blength*temp_normal[0] + a_RK[itr][0]*node[n].P[0][1] + a_RK[itr][1]*node[n].P[2][1]);
		node[n].Res[2] = node[n].volume/a_RK[itr][1]*( -Qi/b_nodes[m].blength*temp_normal[1] + a_RK[itr][0]*node[n].P[0][2] + a_RK[itr][1]*node[n].P[2][2]);
		//printf("garonne river flow %d %d %le %le %le\n",n,iteration-1,Qi/b_nodes[m].blength*temp_normal[0],node[n].Res[1],node[n].Res[2]);
	} else { // dordogne
		node[n].Res[1] = 0.;
		node[n].Res[2] = 0.;	
		double Qi = b_nodes[m].area/Area_inlet_do * Q_do; //fraction of the inlet flux to give to the node "i"
		int itr = iteration - 1;
		node[n].Res[1] = node[n].volume/a_RK[itr][1]*( -Qi/b_nodes[m].blength*temp_normal[0] + a_RK[itr][0]*node[n].P[0][1] + a_RK[itr][1]*node[n].P[2][1]);
		node[n].Res[2] = node[n].volume/a_RK[itr][1]*( -Qi/b_nodes[m].blength*temp_normal[1] + a_RK[itr][0]*node[n].P[0][2] + a_RK[itr][1]*node[n].P[2][2]);
		//printf("dordogne river flow %d %le %le %le\n",n,Qi,node[n].Res[1],node[n].Res[2]);
	}
}

void char_bc_gironde( int m )
{
	int n = b_nodes[m].node ;
	double y =  node[n].coordinate[1];
	double nx = b_nodes[m].normal[0];
	double ny = b_nodes[m].normal[1];
	double edge = sqrt( nx*nx + ny*ny );

	temp_normal[0] = nx/edge ;
	temp_normal[1] = ny/edge ;

	double h, u, v;
	if ( iteration == 1 ) {
		h = node[n].Z[2][0];
		u = node[n].Z[2][1];
		v = node[n].Z[2][2]; 
	} else {
		h = 0.5*(node[n].Z[2][0]+node[n].Z[0][0]);
		u = 0.5*(node[n].Z[2][1]+node[n].Z[0][1]);
		v = 0.5*(node[n].Z[2][2] +node[n].Z[0][2]); 
	}
	
	double un, h_tide, u_tide, v_tide, un_tide;
	double Q_ga = 140. ;// [m^3/s] 
	double Q_do = 100. ;// [m^3/s] 

	// WEAK BC AT INLET: FAR FIELD TIDAL WAVE
	if ( y > 365000. ) { // gironde
		h_tide = 3.29 - node[n].bed[2];
		u_tide = 0.0;
		v_tide = 0.0;

		height0 = 0.5*( h_tide + h );
		speed_of_sound0  = sqrt( gm*height0 );
		ubar0 = 0.5*( u_tide + u );
		vbar0 = 0.5*( v_tide + v );

		un_tide = u_tide*temp_normal[0] + v_tide*temp_normal[1];
		un = u*temp_normal[0] + v*temp_normal[1];
	
		temp_vector[0] = h*un - h_tide*un_tide; 
		temp_vector[0] *= dt * edge;

		temp_vector[1] = h*u*un - h_tide*u_tide*un_tide + 0.5*gm*( h*h - h_tide*h_tide )*temp_normal[0]; 
		temp_vector[1] *= dt * edge;

		temp_vector[2] = h*v*un - h_tide*v_tide*un_tide + 0.5*gm*( h*h - h_tide*h_tide )*temp_normal[1]; 
		temp_vector[2] *= dt * edge;
	
		Lambda[0] = un;
		Lambda[1] = Lambda[0] + sqrt( gm*h );
		Lambda[2] = Lambda[0] - sqrt( gm*h );

		Eigenvectors( temp_normal );

		work_vector0[0] = 0.;
		work_vector0[1] = 0.;
		work_vector0[2] = 0.;

		work_vector1[0] = 0.;
		work_vector1[1] = 0.;
		work_vector1[2] = 0.;
	
		// FLUX IN CHARACTERISTIC VARIABLE
		double lmax = 0. ;
		for (int i = 0 ; i < size ; i ++ ) {
			for (int j = 0 ; j < size ; j ++ ) 
				work_vector1[i] += Left[i][j]*temp_vector[j];

			if ( Lambda[i] > 0.0 )
				work_vector0[i] += work_vector1[i];
	
			lmax = MAX( lmax, fabs(Lambda[i]) );
		}

		// CORRECTION ON THE CHARACTERISTIC FLUX #2 IN ORDER TO OBTAIN THE INLET SIGNAL ON h 	
		work_vector0[1] = (-h_tide*node[n].volume + node[n].P[0][0]*node[n].volume - node[n].Res[0] - Right[0][0]*work_vector0[0])/Right[0][1] ;

		// BOUNDARY CORRECTION FLUX
		for (int i = 0 ; i < size ; i ++ )
			for (int j = 0 ; j < size ; j ++ ) node[n].Res[i] += Right[i][j]*work_vector0[j];

		node[n].dtau = MIN(node[n].dtau ,CFL/(1+(1.5*dt*lmax/(edge*edge)))) ;	

		// WALL BC AT CORNERS
		if ((fabs( node[n].coordinate[0] - 3.386597e+05) < 0.000001)  && (fabs(node[n].coordinate[1] - 3.723562e+05) < 0.000001)) { // node 595
			int n0 = boundary[m-1].node[0];
			int n1 = boundary[m-1].node[1];
			double nx = node[n1].coordinate[1] - node[n0].coordinate[1];
			double ny = node[n0].coordinate[0] - node[n1].coordinate[0];
			double length = sqrt(nx * nx + ny * ny);
			nx /= length;
			ny /= length;
			double phin = node[n].Res[1] * nx + node[n].Res[2] * ny;
			node[n].Res[1] -= phin * nx;
			node[n].Res[2] -= phin * ny;
			double un = node[n].P[2][1] * nx + node[n].P[2][2] * ny;
			node[n].P[2][1] -= un * nx;
			node[n].P[2][2] -= un * ny;
		} else if ((fabs( node[n].coordinate[0] - 3.347489e+05) < 0.000001)  && (fabs(node[n].coordinate[1] - 3.689337e+05) < 0.000001)) { // node 36
			int n0 = boundary[m].node[0];
			int n1 = boundary[m].node[1];
			double nx = node[n1].coordinate[1] - node[n0].coordinate[1];
			double ny = node[n0].coordinate[0] - node[n1].coordinate[0];
			double length = sqrt(nx * nx + ny * ny);
			nx /= length;
			ny /= length;
			double phin = node[n].Res[1] * nx + node[n].Res[2] * ny;
			node[n].Res[1] -= phin * nx;
			node[n].Res[2] -= phin * ny;
			double un = node[n].P[2][1] * nx + node[n].P[2][2] * ny;
			node[n].P[2][1] -= un * nx;
			node[n].P[2][2] -= un * ny;
		}
		//printf("gironde %d\n",n);
	} else if ( y < 304500. ) { // garonne
		node[n].Res[1] = 0.;
		node[n].Res[2] = 0.;	
		double Qi = b_nodes[m].area/Area_inlet_ga * Q_ga; //fraction of the inlet flux to give to the node "i"
		int itr = iteration - 1;
		node[n].Res[1] = node[n].volume/a_RK[itr][1]*( -Qi/b_nodes[m].blength*temp_normal[0] + a_RK[itr][0]*node[n].P[0][1] + a_RK[itr][1]*node[n].P[2][1]);
		node[n].Res[2] = node[n].volume/a_RK[itr][1]*( -Qi/b_nodes[m].blength*temp_normal[1] + a_RK[itr][0]*node[n].P[0][2] + a_RK[itr][1]*node[n].P[2][2]);
		//printf("garonne river flow %d %d %le %le %le\n",n,iteration-1,Qi/b_nodes[m].blength*temp_normal[0],node[n].Res[1],node[n].Res[2]);
	} else { // dordogne
		node[n].Res[1] = 0.;
		node[n].Res[2] = 0.;	
		double Qi = b_nodes[m].area/Area_inlet_do * Q_do; //fraction of the inlet flux to give to the node "i"
		int itr = iteration - 1;
		node[n].Res[1] = node[n].volume/a_RK[itr][1]*( -Qi/b_nodes[m].blength*temp_normal[0] + a_RK[itr][0]*node[n].P[0][1] + a_RK[itr][1]*node[n].P[2][1]);
		node[n].Res[2] = node[n].volume/a_RK[itr][1]*( -Qi/b_nodes[m].blength*temp_normal[1] + a_RK[itr][0]*node[n].P[0][2] + a_RK[itr][1]*node[n].P[2][2]);
		//printf("dordogne river flow %d %le %le %le\n",n,Qi,node[n].Res[1],node[n].Res[2]);
	}
}

void char_bc_garonne( int m )
{
	int n = b_nodes[m].node ;
	//double x =  node[n].coordinate[0];
	double y =  node[n].coordinate[1];
	double nx = b_nodes[m].normal[0];
	double ny = b_nodes[m].normal[1];
	double edge = sqrt( nx*nx + ny*ny );

	temp_normal[0] = nx/edge ;
	temp_normal[1] = ny/edge ;

	double h, u, v;
	if ( iteration == 1 ) {
		h = node[n].Z[2][0];
		u = node[n].Z[2][1];
		v = node[n].Z[2][2]; 
	} else {
		h = 0.5*(node[n].Z[2][0]+node[n].Z[0][0]);
		u = 0.5*(node[n].Z[2][1]+node[n].Z[0][1]);
		v = 0.5*(node[n].Z[2][2] +node[n].Z[0][2]); 
	}
	
	double un, h_tide, u_tide, v_tide, un_tide;
	double Q_ga = 140. ;// [m^3/s] 

	// WEAK BC AT INLET: FAR FIELD TIDAL WAVE
	if ( y > 290000. ) { // gironde
		h_tide = 3.29150 - node[n].bed[2];
		u_tide = 0.0;
		v_tide = 0.0;

		height0 = 0.5*( h_tide + h );
		speed_of_sound0  = sqrt( gm*height0 );
		ubar0 = 0.5*( u_tide + u );
		vbar0 = 0.5*( v_tide + v );

		un_tide = u_tide*temp_normal[0] + v_tide*temp_normal[1];
		un = u*temp_normal[0] + v*temp_normal[1];
	
		temp_vector[0] = h*un - h_tide*un_tide; 
		temp_vector[0] *= dt * edge;

		temp_vector[1] = h*u*un - h_tide*u_tide*un_tide + 0.5*gm*( h*h - h_tide*h_tide )*temp_normal[0]; 
		temp_vector[1] *= dt * edge;

		temp_vector[2] = h*v*un - h_tide*v_tide*un_tide + 0.5*gm*( h*h - h_tide*h_tide )*temp_normal[1]; 
		temp_vector[2] *= dt * edge;
	
		Lambda[0] = un;
		Lambda[1] = Lambda[0] + sqrt( gm*h );
		Lambda[2] = Lambda[0] - sqrt( gm*h );

		Eigenvectors( temp_normal );

		work_vector0[0] = 0.;
		work_vector0[1] = 0.;
		work_vector0[2] = 0.;

		work_vector1[0] = 0.;
		work_vector1[1] = 0.;
		work_vector1[2] = 0.;
	
		// FLUX IN CHARACTERISTIC VARIABLE
		double lmax = 0. ;
		for (int i = 0 ; i < size ; i ++ ) {
			for (int j = 0 ; j < size ; j ++ ) 
				work_vector1[i] += Left[i][j]*temp_vector[j];

			if ( Lambda[i] > 0.0 )
				work_vector0[i] += work_vector1[i];
	
			lmax = MAX( lmax, fabs(Lambda[i]) );
		}

		// CORRECTION ON THE CHARACTERISTIC FLUX #2 IN ORDER TO OBTAIN THE INLET SIGNAL ON h 	
		work_vector0[1] = (-h_tide*node[n].volume + node[n].P[0][0]*node[n].volume - node[n].Res[0] - Right[0][0]*work_vector0[0])/Right[0][1] ;

		// BOUNDARY CORRECTION FLUX
		for (int i = 0 ; i < size ; i ++ )
			for (int j = 0 ; j < size ; j ++ ) node[n].Res[i] += Right[i][j]*work_vector0[j];

		node[n].dtau = MIN(node[n].dtau ,CFL/(1+(1.5*dt*lmax/(edge*edge)))) ;	

		// WALL BC AT CORNERS
                if ((fabs( node[n].coordinate[0] - 3.734110e+05) < 0.000001)  && (fabs(node[n].coordinate[1] - 3.017930e+05) < 0.000001)) { // node 222
			int n0 = boundary[m-1].node[0];
			int n1 = boundary[m-1].node[1];
			double nx = node[n1].coordinate[1] - node[n0].coordinate[1];
			double ny = node[n0].coordinate[0] - node[n1].coordinate[0];
			double length = sqrt(nx * nx + ny * ny);
			nx /= length;
			ny /= length;
			double phin = node[n].Res[1] * nx + node[n].Res[2] * ny;
			node[n].Res[1] -= phin * nx;
			node[n].Res[2] -= phin * ny;
			double un = node[n].P[2][1] * nx + node[n].P[2][2] * ny;
			node[n].P[2][1] -= un * nx;
			node[n].P[2][2] -= un * ny;
                } else if ((fabs( node[n].coordinate[0] - 3.725900e+05) < 0.000001)  && (fabs(node[n].coordinate[1] - 3.017240e+05) < 0.000001)) { // node 0
			int n0 = boundary[m].node[0];
			int n1 = boundary[m].node[1];
			double nx = node[n1].coordinate[1] - node[n0].coordinate[1];
			double ny = node[n0].coordinate[0] - node[n1].coordinate[0];
			double length = sqrt(nx * nx + ny * ny);
			nx /= length;
			ny /= length;
			double phin = node[n].Res[1] * nx + node[n].Res[2] * ny;
			node[n].Res[1] -= phin * nx;
			node[n].Res[2] -= phin * ny;
			double un = node[n].P[2][1] * nx + node[n].P[2][2] * ny;
			node[n].P[2][1] -= un * nx;
			node[n].P[2][2] -= un * ny;
		}
		//printf("gironde %d\n",n);
	} else if ( y < 290000. ) { // La Reole
		node[n].Res[1] = 0.;
		node[n].Res[2] = 0.;	
		double Qi = b_nodes[m].area/Area_inlet_ga * Q_ga; //fraction of the inlet flux to give to the node "i"
		int itr = iteration - 1;
		node[n].Res[1] = node[n].volume/a_RK[itr][1]*( -Qi/b_nodes[m].blength*temp_normal[0] + a_RK[itr][0]*node[n].P[0][1] + a_RK[itr][1]*node[n].P[2][1]);
		node[n].Res[2] = node[n].volume/a_RK[itr][1]*( -Qi/b_nodes[m].blength*temp_normal[1] + a_RK[itr][0]*node[n].P[0][2] + a_RK[itr][1]*node[n].P[2][2]);
		//printf("garonne river flow %d %d %le %le %le\n",n,iteration-1,Qi/b_nodes[m].blength*temp_normal[0],node[n].Res[1],node[n].Res[2]);
	} 
}

void char_bc_dordogne( int m )
{
	int n = b_nodes[m].node ;
	double x =  node[n].coordinate[0];
	//double y =  node[n].coordinate[1];
	double nx = b_nodes[m].normal[0];
	double ny = b_nodes[m].normal[1];
	double edge = sqrt( nx*nx + ny*ny );

	temp_normal[0] = nx/edge ;
	temp_normal[1] = ny/edge ;

	double h, u, v;
	if ( iteration == 1 ) {
		h = node[n].Z[2][0];
		u = node[n].Z[2][1];
		v = node[n].Z[2][2]; 
	} else {
		h = 0.5*(node[n].Z[2][0]+node[n].Z[0][0]);
		u = 0.5*(node[n].Z[2][1]+node[n].Z[0][1]);
		v = 0.5*(node[n].Z[2][2] +node[n].Z[0][2]); 
	}
	
	double un, h_tide, u_tide, v_tide, un_tide;
	double Q_do = 100. ;// [m^3/s] 

	// WEAK BC AT INLET: FAR FIELD TIDAL WAVE
	if ( x < 378000. ) { // gironde
		h_tide = 3.292 - node[n].bed[2];
		u_tide = 0.0;
		v_tide = 0.0;

		height0 = 0.5*( h_tide + h );
		speed_of_sound0  = sqrt( gm*height0 );
		ubar0 = 0.5*( u_tide + u );
		vbar0 = 0.5*( v_tide + v );

		un_tide = u_tide*temp_normal[0] + v_tide*temp_normal[1];
		un = u*temp_normal[0] + v*temp_normal[1];
	
		temp_vector[0] = h*un - h_tide*un_tide; 
		temp_vector[0] *= dt * edge;

		temp_vector[1] = h*u*un - h_tide*u_tide*un_tide + 0.5*gm*( h*h - h_tide*h_tide )*temp_normal[0]; 
		temp_vector[1] *= dt * edge;

		temp_vector[2] = h*v*un - h_tide*v_tide*un_tide + 0.5*gm*( h*h - h_tide*h_tide )*temp_normal[1]; 
		temp_vector[2] *= dt * edge;
	
		Lambda[0] = un;
		Lambda[1] = Lambda[0] + sqrt( gm*h );
		Lambda[2] = Lambda[0] - sqrt( gm*h );

		Eigenvectors( temp_normal );

		work_vector0[0] = 0.;
		work_vector0[1] = 0.;
		work_vector0[2] = 0.;

		work_vector1[0] = 0.;
		work_vector1[1] = 0.;
		work_vector1[2] = 0.;
	
		// FLUX IN CHARACTERISTIC VARIABLE
		double lmax = 0. ;
		for (int i = 0 ; i < size ; i ++ ) {
			for (int j = 0 ; j < size ; j ++ ) 
				work_vector1[i] += Left[i][j]*temp_vector[j];

			if ( Lambda[i] > 0.0 )
				work_vector0[i] += work_vector1[i];
	
			lmax = MAX( lmax, fabs(Lambda[i]) );
		}

		// CORRECTION ON THE CHARACTERISTIC FLUX #2 IN ORDER TO OBTAIN THE INLET SIGNAL ON h 	
		work_vector0[1] = (-h_tide*node[n].volume + node[n].P[0][0]*node[n].volume - node[n].Res[0] - Right[0][0]*work_vector0[0])/Right[0][1] ;

		// BOUNDARY CORRECTION FLUX
		for (int i = 0 ; i < size ; i ++ )
			for (int j = 0 ; j < size ; j ++ ) node[n].Res[i] += Right[i][j]*work_vector0[j];

		node[n].dtau = MIN(node[n].dtau ,CFL/(1+(1.5*dt*lmax/(edge*edge)))) ;	

		// WALL BC AT CORNERS
                if ((fabs( node[n].coordinate[0] - 3.744460e+05) < 0.000001)  && (fabs(node[n].coordinate[1] - 3.066120e+05) < 0.000001)) { // node 8
			int n0 = boundary[m-1].node[0];
			int n1 = boundary[m-1].node[1];
			double nx = node[n1].coordinate[1] - node[n0].coordinate[1];
			double ny = node[n0].coordinate[0] - node[n1].coordinate[0];
			double length = sqrt(nx * nx + ny * ny);
			nx /= length;
			ny /= length;
			double phin = node[n].Res[1] * nx + node[n].Res[2] * ny;
			node[n].Res[1] -= phin * nx;
			node[n].Res[2] -= phin * ny;
			double un = node[n].P[2][1] * nx + node[n].P[2][2] * ny;
			node[n].P[2][1] -= un * nx;
			node[n].P[2][2] -= un * ny;
                } else if ((fabs( node[n].coordinate[0] - 3.736990e+05) < 0.000001)  && (fabs(node[n].coordinate[1] - 3.059840e+05) < 0.000001)) { // node 0
			int n0 = boundary[m].node[0];
			int n1 = boundary[m].node[1];
			double nx = node[n1].coordinate[1] - node[n0].coordinate[1];
			double ny = node[n0].coordinate[0] - node[n1].coordinate[0];
			double length = sqrt(nx * nx + ny * ny);
			nx /= length;
			ny /= length;
			double phin = node[n].Res[1] * nx + node[n].Res[2] * ny;
			node[n].Res[1] -= phin * nx;
			node[n].Res[2] -= phin * ny;
			double un = node[n].P[2][1] * nx + node[n].P[2][2] * ny;
			node[n].P[2][1] -= un * nx;
			node[n].P[2][2] -= un * ny;
		}
		//printf("gironde %d\n",n);
	} else if ( x > 378000. ) { // Pessac
		node[n].Res[1] = 0.;
		node[n].Res[2] = 0.;	
		double Qi = b_nodes[m].area/Area_inlet_do * Q_do; //fraction of the inlet flux to give to the node "i"
		int itr = iteration - 1;
		node[n].Res[1] = node[n].volume/a_RK[itr][1]*( -Qi/b_nodes[m].blength*temp_normal[0] + a_RK[itr][0]*node[n].P[0][1] + a_RK[itr][1]*node[n].P[2][1]);
		node[n].Res[2] = node[n].volume/a_RK[itr][1]*( -Qi/b_nodes[m].blength*temp_normal[1] + a_RK[itr][0]*node[n].P[0][2] + a_RK[itr][1]*node[n].P[2][2]);
	} 
}

void char_bc_oregon( int m )
{
	int n = b_nodes[m].node ;
	double x =  node[n].coordinate[0];
	//double y =  node[n].coordinate[1];
	double nx = b_nodes[m].normal[0];
	double ny = b_nodes[m].normal[1];
	double edge = sqrt( nx*nx + ny*ny );
        double loc_time=Time;//+14.9;//51.425;// 16.3348; //15.2645;
       


	int jT = 1;
	int keepon = 1;
	while (keepon) {
		if (time_bc[jT] < loc_time)
			jT++;
		else
			keepon = 0;
	}


	double hex = (h_bc[jT] * (loc_time - time_bc[jT - 1]) / (time_bc[jT] - time_bc[jT - 1]) + h_bc[jT - 1] * (loc_time - time_bc[jT]) / (time_bc[jT - 1] - time_bc[jT]));
	hex += MAX(-node[n].bed[0], 0.);

	if(loc_time>= 104.7) hex =0.0;

	temp_normal[0] = nx/edge ;
	temp_normal[1] = ny/edge ;

	double h, u, v;
	if ( iteration == 1 ) {
		h = node[n].Z[2][0];
		u = node[n].Z[2][1];
		v = node[n].Z[2][2]; 
	} else {
		h = 0.5*(node[n].Z[2][0]+node[n].Z[0][0]);
		u = 0.5*(node[n].Z[2][1]+node[n].Z[0][1]);
		v = 0.5*(node[n].Z[2][2] +node[n].Z[0][2]); 
	}
	

	// WEAK BC AT INLET:
	if ( x <= 2.07 ) { // gironde
		double uex=0.0;
		double vex=0.0;

		height0 = 0.5*( hex + h );
		speed_of_sound0  = sqrt( gm*height0 );
		ubar0 = 0.5*( uex + u );
		vbar0 = 0.5*( vex + v );

		double unex = uex*temp_normal[0] + vex*temp_normal[1];
		double un = u*temp_normal[0] + v*temp_normal[1];
	
		temp_vector[0] = h*un - hex*unex; 
		temp_vector[0] *= dt * edge;

		temp_vector[1] = h*u*un - hex*uex*unex + 0.5*gm*( h*h - hex*hex )*temp_normal[0]; 
		temp_vector[1] *= dt * edge;

		temp_vector[2] = h*v*un - hex*vex*unex + 0.5*gm*( h*h - hex*hex )*temp_normal[1]; 
		temp_vector[2] *= dt * edge;
	
		Lambda[0] = un; //!!!!!!!!!!!!! un or unex
		Lambda[1] = Lambda[0] + sqrt( gm*h );
		Lambda[2] = Lambda[0] - sqrt( gm*h );

		Eigenvectors( temp_normal );

		work_vector0[0] = 0.;
		work_vector0[1] = 0.;
		work_vector0[2] = 0.;

		work_vector1[0] = 0.;
		work_vector1[1] = 0.;
		work_vector1[2] = 0.;
	
		// FLUX IN CHARACTERISTIC VARIABLE
		double lmax = 0. ;
		for (int i = 0 ; i < size ; i ++ ) {
			for (int j = 0 ; j < size ; j ++ ) 
				work_vector1[i] += Left[i][j]*temp_vector[j];

			if ( Lambda[i] > 0.0 )
				work_vector0[i] += work_vector1[i];
	
			lmax = MAX( lmax, fabs(Lambda[i]) );
		}

		// CORRECTION ON THE CHARACTERISTIC FLUX #2 IN ORDER TO OBTAIN THE INLET SIGNAL ON h 	
		work_vector0[1] = (-hex*node[n].volume + node[n].P[0][0]*node[n].volume - node[n].Res[0] - Right[0][0]*work_vector0[0])/Right[0][1] ;

		// BOUNDARY CORRECTION FLUX
		for (int i = 0 ; i < size ; i ++ )
			for (int j = 0 ; j < size ; j ++ ) node[n].Res[i] += Right[i][j]*work_vector0[j];

		node[n].dtau = MIN(node[n].dtau ,CFL/(1+(1.5*dt*lmax/(edge*edge)))) ;	

		// WALL BC AT CORNERS
                if ((fabs( node[n].coordinate[0] - 2.07) < 0.000001)  && (fabs(node[n].coordinate[1] - 8.49) < 0.000001)) { 
			int n0 = boundary[m-1].node[0];
			int n1 = boundary[m-1].node[1];
			double nx = node[n1].coordinate[1] - node[n0].coordinate[1];
			double ny = node[n0].coordinate[0] - node[n1].coordinate[0];
			double length = sqrt(nx * nx + ny * ny);
			nx /= length;
			ny /= length;
			double phin = node[n].Res[1] * nx + node[n].Res[2] * ny;
			node[n].Res[1] -= phin * nx;
			node[n].Res[2] -= phin * ny;
			double un = node[n].P[2][1] * nx + node[n].P[2][2] * ny;
			node[n].P[2][1] -= un * nx;
			node[n].P[2][2] -= un * ny;
                } else if ((fabs( node[n].coordinate[0] - 2.07) < 0.000001)  && (fabs(node[n].coordinate[1] + 6.01) < 0.000001)) { // node 0
			int n0 = boundary[m].node[0];
			int n1 = boundary[m].node[1];
			double nx = node[n1].coordinate[1] - node[n0].coordinate[1];
			double ny = node[n0].coordinate[0] - node[n1].coordinate[0];
			double length = sqrt(nx * nx + ny * ny);
			nx /= length;
			ny /= length;
			double phin = node[n].Res[1] * nx + node[n].Res[2] * ny;
			node[n].Res[1] -= phin * nx;
			node[n].Res[2] -= phin * ny;
			double un = node[n].P[2][1] * nx + node[n].P[2][2] * ny;
			node[n].P[2][1] -= un * nx;
			node[n].P[2][2] -= un * ny;
		}
	} 
}

void char_bc_solitary( int m )
{
	int n = b_nodes[m].node;
	double x =  node[n].coordinate[0];
	//double y =  node[n].coordinate[1];

	//double loc_time;
	//if ( iteration == 1 )
	//	loc_time = Time - dt;
	//if ( iteration == 2 )
	//	loc_time = Time - 0.5*dt;
	//loc_time = Time;

	// si osserva un problema sui corners che non permette la convergenza all'ordine 3.
	if( !(fabs(x) < 0.00001) ) {
		if ( x < L_ref ) // HORIZONTAL WALL
			node[n].Res[2] = 0.;
		else // VERTICAL WALL
			node[n].Res[1] = 0.;
	} else {
		node[n].Res[1] = 0.; // VERTICAL WALL
	}
}

void char_bc_rip_channel( int m )
{
	int n = b_nodes[m].node;
	double x =  node[n].coordinate[0];
	//if( (fabs(x - 0.) < 0.0000001 ) || (fabs(x - 30.) < 0.0000001))
	if( (fabs(x - 0.) < 0.0000001 ) || (fabs(x - 700.) < 0.0000001))
		node[n].Res[1] = 0.;
	else
		node[n].Res[2] = 0.;
}

void char_bc_synolakis( int m )
{
}

void char_bc_volker_reef( int m )
{
}

void char_bc_bar( int m )
{
}

void char_bc_shoaling( int m )
{
}

void char_bc_elliptic_shoal(int m)
{
}

void char_bc_circ_shoal(int m)
{
}
