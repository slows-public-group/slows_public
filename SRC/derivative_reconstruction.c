/*  Copyright (C) 2021   M.Ricchiuto  <mario.ricchiuto@inria.fr>
                         M. Kazolea   <maria.kazolea@inria.fr>
                         A. Filippini <a.filippini@brgm.fr>
                         L. Arpaia    <luca.arpaia@brgm.fr>
                         N.Pattakos   <pattakosn@fastmail.com> 

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.*/



/***************************************************************************
                               reconstruction.c
                           -----------------------
  This is reconstruction: here are implemented two reconstructions:

  - gradient operator -  weight area average // Green Gauss formula

	grad f_i = \frac{ \sum{K\in D_i} grad f_K |K|}{\sum{K\in D_i}|K| }
	
    f_i will be the vector of conservative vars (h hu hv), 
    the water height (h+b), the bathy (b), the step function,
    the ramp function and finally the hyperbolic tangent.
	
  - hessian operator  - symmetric L2 projection

	hess f_i  = \frac{ \sum{K\in D_i} grad grad f_i |K|/3 }{\sum{K\in D_i}|K|/3}

    f_i in this case will be the water height (h+b)	   
                             -------------------
***************************************************************************/
#include <math.h>
#include <stdio.h>
#include "common.h"
#include "my_malloc.h"
extern const int vertex;
extern int NN, NE, NBF, NBF2, zero_nodes;
extern int corner1,corner2,corner3,corner4;
extern double KK, cut_off_H, cut_off_U;
extern struct node_struct *node;
extern struct element_struct *element;
extern struct boundary_struct *boundary;
extern struct boundary_nodes_struct *b_nodes;
extern struct boundary_nodes2_struct *b_nodes2;
extern struct boundary_nodes3_struct *b_nodes3;
extern struct numerical_int_struct numerical_int;
extern double **katz, **M2_total;

/* This function reconstructs the gradient of cons. variables */
void grad_rec(int size_lev, int time_lev)
{
	// size_lev = 1    => water height rec. for adaptation                                
	// size_lev = size => cons.vars rec. for FV-MUSCL scheme
	// time_lev = 0 => RK1
	// time_lev = 1 => RK2
	for (int n = 0; n < NN; n++) {
		node[n].flag = 0;
		for (int l = 0; l < size_lev; l++) {
			if(size_lev ==1){
				node[n].dxeta = 0.0;
				node[n].dyeta= 0.0;
			}else{
				node[n].dxP[time_lev][l] = 0.0;
				node[n].dyP[time_lev][l] = 0.0;
			}
		}
	}

	for (int e = 0; e < NE; e++) {
		// Partially-dry and dry cell detection 
		double max_Htot0 = -10.;
		zero_nodes = 0;

		for (int i = 0; i < vertex; i++) {
			int m = element[e].node[i];
			if (!node[m].dry_flag2) {
				if (node[m].P[2][0] + node[m].bed[2] > max_Htot0)
					max_Htot0 = node[m].P[2][0] + node[m].bed[2];
			} else {
				zero_nodes++;
			}
		}

		if(3 != zero_nodes) {
			for (int l = 0; l < size_lev; l++) {
				double GradK_x = 0.0;
				double GradK_y = 0.0;

				for (int i = 0; i < vertex; i++) {
					int m = element[e].node[i];
					GradK_x += element[e].normal[i][0] * node[m].P[2][l];
					GradK_y += element[e].normal[i][1] * node[m].P[2][l];

					// water height gradient: \nabla (h+b)
					if (size_lev == 1) {
						double bathy;
						// Correction on shoreline: WB rec.    
						if ((node[m].dry_flag2) && ((node[m].bed[2] + cut_off_H * node[m].cut_off_ratio) > max_Htot0))
							bathy = max_Htot0;
						else
							bathy = node[m].bed[2];

						GradK_x += element[e].normal[i][0] * bathy;
						GradK_y += element[e].normal[i][1] * bathy;
					}
				}

				GradK_x *= 0.5;
				GradK_y *= 0.5;

				for (int i = 0; i < vertex; i++) {
					int m = element[e].node[i];
					if(size_lev==1){
						node[m].dxeta += GradK_x;
						node[m].dyeta += GradK_y;
					}else{
						node[m].dxP[time_lev][l] += GradK_x;
						node[m].dyP[time_lev][l] += GradK_y;
					}
				}
			}
		}
	}

	// Correction for periodic boundaries
     	for ( int f = 0 ; f < NBF ; f ++ ) {
        	if(b_nodes[f].type == 10) {
            	int n1 = b_nodes[f].node ;
			if(node[n1].flag == 0) {		
				int mate = node[n1].mate ;
				for (int l = 0 ; l < size_lev ; l++ ) {
					node[n1].dxP[time_lev][l] += node[mate].dxP[time_lev][l] ;
					node[mate].dxP[time_lev][l] = node[n1].dxP[time_lev][l] ;

					node[n1].dyP[time_lev][l] += node[mate].dyP[time_lev][l] ;
					node[mate].dyP[time_lev][l] = node[n1].dyP[time_lev][l] ;
		
					node[mate].flag = 1;
					node[n1].flag = 1;
				}
			}
		 }
    	}

	// Green-Gauss formula       
	for (int n = 0; n < NN; n++) {
		for (int l = 0; l < size_lev; l++) {
					if(size_lev==1){
						node[n].dxeta /= (node[n].volume *3.0);
						node[n].dyeta /= (node[n].volume *3.0);
					}else{
						node[n].dxP[time_lev][l] /= (node[n].volume * 3.0);
						node[n].dyP[time_lev][l] /= (node[n].volume * 3.0);
					}
		}

		// L2 norm of water height gradient
		if (size_lev == 1)
			node[n].Grad = sqrt(node[n].dxP[time_lev][0] * node[n].dxP[time_lev][0] + node[n].dyP[time_lev][0] * node[n].dyP[time_lev][0]);
	}
	    // Correction for wall boundaries
        for (int f = 0; f < NBF; f++) {
                if(b_nodes[f].type==3){
                        int n0 = b_nodes[f].node;
                        double length = sqrt(pow(b_nodes[f].normal[0], 2) + pow(b_nodes[f].normal[1], 2));
                        for (int l = 0; l < size_lev; l++) {
                                double dnP = node[n0].dxP[time_lev][l] * b_nodes[f].normal[0] + node[n0].dyP[time_lev][l] * b_nodes[f].normal[1];
				if(size_lev==1){
                                	node[n0].dxeta -= dnP*b_nodes[f].normal[0]/pow(length, 2);
                                	node[n0].dyeta -= dnP*b_nodes[f].normal[1]/pow(length, 2);
				}else{
                                	node[n0].dxP[time_lev][l] -= dnP*b_nodes[f].normal[0]/pow(length, 2);
                                	node[n0].dyP[time_lev][l] -= dnP*b_nodes[f].normal[1]/pow(length, 2);
				}
                        }
                }
        }


}

void lap_rec(int size_lev, int time_lev)
{
	// size_lev = 1    => water height rec. for adaptation                                
	// size_lev = size => cons.vars rec. for FV-MUSCL scheme
	// time_lev = 0 => RK1
	// time_lev = 1 => RK2
	for (int n = 0; n < NN; n++) {
		node[n].flag = 0;
		for (int l = 0; l < size_lev; l++) {
			node[n].dxxP2[time_lev][l] = 0.0;
			node[n].dxyP2[time_lev][l] = 0.0;
			node[n].dyyP2[time_lev][l] = 0.0;
		}
	}

	for (int e = 0; e < NE; e++) {
		// Partially-dry and dry cell detection 
		double max_Htot0 = -10.;
		zero_nodes = 0;

		for (int i = 0; i < vertex; i++) {
			int m = element[e].node[i];
			if (!node[m].dry_flag2) {
				if (node[m].P[2][0] + node[m].bed[2] > max_Htot0)
					max_Htot0 = node[m].P[2][0] + node[m].bed[2];
			} else {
				zero_nodes++;
			}
		}

		if(3 != zero_nodes) {
			for (int l = 0; l < size_lev; l++) {
				double GradK_x = 0.0;
				double GradK_y = 0.0;
				double GradK_xy = 0.0;

				for (int i = 0; i < vertex; i++) {
					int m = element[e].node[i];
					GradK_x += element[e].normal[i][0] * node[m].dxP[time_lev][l];
					GradK_y += element[e].normal[i][1] * node[m].dyP[time_lev][l];
					GradK_xy += element[e].normal[i][1] * node[m].dxP[time_lev][l];

					// water height gradient: \nabla (h_x+b_x) etc..
					if (size_lev == 1) {
						double bathyx,bathyy;
						// Correction on shoreline: WB rec.    
						if ((node[m].dry_flag2) && ((node[m].bed[2] + cut_off_H * node[m].cut_off_ratio) > max_Htot0)){
							bathyx = max_Htot0;
							bathyy = max_Htot0;
						}else{
							bathyx = node[m].dxbed[time_lev];
							bathyy = node[m].dybed[time_lev];
						}

						GradK_x += element[e].normal[i][0] * bathyx;
						GradK_y += element[e].normal[i][1] * bathyy;
						GradK_xy += element[e].normal[i][1] * bathyx;
					}
				}

				GradK_x *= 0.5;
				GradK_y *= 0.5;
				GradK_xy *= 0.5;

				for (int i = 0; i < vertex; i++) {
					int m = element[e].node[i];
					node[m].dxxP2[time_lev][l] += GradK_x;
					node[m].dyyP2[time_lev][l] += GradK_y;
					node[m].dxyP2[time_lev][l] += GradK_xy;
				}
			}
		}
	}

	// Correction for periodic boundaries
     	for ( int f = 0 ; f < NBF ; f ++ ) {
        	if(b_nodes[f].type == 10) {
            	int n1 = b_nodes[f].node ;
			if(node[n1].flag == 0) {		
				int mate = node[n1].mate ;
				for (int l = 0 ; l < size_lev ; l++ ) {
					node[n1].dxxP2[time_lev][l] += node[mate].dxxP2[time_lev][l] ;
					node[mate].dxxP2[time_lev][l] = node[n1].dxxP2[time_lev][l] ;

					node[n1].dyyP2[time_lev][l] += node[mate].dyyP2[time_lev][l] ;
					node[mate].dyyP2[time_lev][l] = node[n1].dyyP2[time_lev][l] ;

					node[n1].dxyP2[time_lev][l] += node[mate].dxyP2[time_lev][l] ;
					node[mate].dxyP2[time_lev][l] = node[n1].dxyP2[time_lev][l] ;
		
					node[mate].flag = 1;
					node[n1].flag = 1;
				}
			}
		 }
    	}

	// Green-Gauss formula       
	for (int n = 0; n < NN; n++) {
		for (int l = 0; l < size_lev; l++) {
			node[n].dxxP2[time_lev][l] /= (node[n].volume * 3.0);
			node[n].dyyP2[time_lev][l] /= (node[n].volume * 3.0);
			node[n].dxyP2[time_lev][l] /= (node[n].volume * 3.0);
		}

		// L2 norm of water height gradient
//		if (size_lev == 1)
//			node[n].Grad = sqrt(node[n].dxP[time_lev][0] * node[n].dxP[time_lev][0] + node[n].dyP[time_lev][0] * node[n].dyP[time_lev][0]);
	}

	    // Correction for wall boundaries
        for (int f = 0; f < NBF; f++) {
		if(b_nodes[f].type==3){
			//int n0 = b_nodes[f].node;
			//double length = sqrt(pow(b_nodes[f].normal[0], 2) + pow(b_nodes[f].normal[1], 2));
			for (int l = 0; l < size_lev; l++) {
				//double dnP = node[n0].dxxP2[time_lev][l] * b_nodes[f].normal[0] + node[n0].dyyP2[time_lev][l] * b_nodes[f].normal[1];
				//double dnP2 = node[n0].dxxP2[time_lev][l] * b_nodes[f].normal[0] + node[n0].dxyP2[time_lev][l] * b_nodes[f].normal[1];
				//node[n0].dxxP2[time_lev][l] -= dnP*b_nodes[f].normal[0]/pow(length, 2);
				//node[n0].dyyP2[time_lev][l] -= dnP*b_nodes[f].normal[1]/pow(length, 2);

				//node[n0].dxyP2[time_lev][l] -= dnP2*b_nodes[f].normal[1]/pow(length, 2);
			}
		}
	}

}

/* This function reconstructs the gradient of cons. variables */
void grad_katz( int size_lev, int time_lev )
{
	static double **ff = 0;
	if ( 0 == ff)
		ff = (double**)MA_matrix(5,NN, sizeof(double) );

  	// Gradient reconstruction - quadratic least square method -
	//int time_level_tmp;
	//int flag=0;
	int zero_nodes;
	int max_Htot0;
  	if (time_lev !=0) {  
		//time_level_tmp = time_lev;
		time_lev=0;
		//flag=1;
  	}

  	for (int n = 0 ; n < NN ; n++ ) { 
       		for ( int l = 0 ; l < size_lev ; l++ )	 	
         	{
	  		node[n].dxP2[time_lev][l] = 0.0 ; 
          		node[n].dyP2[time_lev][l] = 0.0 ; 
	  		node[n].dxxP2[time_lev][l] = 0.0 ; 
          		node[n].dxyP2[time_lev][l] = 0.0 ; 
	  		node[n].dyyP2[time_lev][l] = 0.0 ; 
	 	}
      	} 

	for (int l = 0 ; l < size_lev ; l++ ) // for on the 3 equations
	{
  		for (int i=0; i<NN; i++){
       			node[i].flag = 0 ;
			ff[0][i] = 0.; ff[1][i] = 0.; ff[2][i] = 0.; ff[3][i] = 0.; ff[4][i] = 0.;
		}
	
		// Computation of the summation over the neighbouring nodes :
		for (int e = 0 ; e < NE ; e++ ) {
			// Partially-dry and dry cell detection	
			max_Htot0 = -10. ; zero_nodes = 0 ;	
			for (int i = 0 ; i < vertex ; i++ ) {
	  			int m = element[e].node[i] ;
		  		if ( !node[m].dry_flag2 ){ 
	     				if (  node[m].P[2][0] + node[m].bed[2] >  max_Htot0 ) 
						max_Htot0 = node[m].P[2][0] + node[m].bed[2] ; 
		   		} 
				else zero_nodes ++ ;
			}

			if (zero_nodes!=3){
				/******************************************************************/
		  		/***   Computation of the quadratic least square coefficients	***/
				/******************************************************************/
// The summation on the elements counts two times the "ij" contribution if the edge connecting the two nodes is an 
// internal one, and one time if the edge is on the boundary. For this reason we do another summation on the boundary edges and
// dividing everything by 2 at the end.

				for ( int i = 0 ; i < vertex-1 ; i++ ){ // for on the vertices to compute the contributions 0-1 0-2 and 1-2 on the triangle
					for (int  j = i+1 ; j < vertex ; j++ ){
										
	      					int ni = element[e].node[i] ;	
						int nj = element[e].node[j] ;
						double rij_x = node[nj].coordinate[0] - node[ni].coordinate[0]; // The length of the egde ij
						double rij_y = node[nj].coordinate[1] - node[ni].coordinate[1];
						/***********************************************/
						// For the conservation of the mean:
						/***********************************************/
						rij_x = node[nj].DcC[0] - node[ni].DcC[0];
						rij_y = node[nj].DcC[1] - node[ni].DcC[1];
						/***********************************************/
						double wij = 1./sqrt(pow(rij_x,2) + pow(rij_y,2));
					
						ff[0][ni] += wij * (node[ni].P[2][l] - node[nj].P[2][l]) * rij_x ;
						ff[1][ni] += wij * (node[ni].P[2][l] - node[nj].P[2][l]) * rij_y ;
						ff[2][ni] += wij * (node[ni].P[2][l] - node[nj].P[2][l]) * rij_x * rij_x ;
						ff[3][ni] += wij * (node[ni].P[2][l] - node[nj].P[2][l]) * rij_x * rij_y ;
						ff[4][ni] += wij * (node[ni].P[2][l] - node[nj].P[2][l]) * rij_y * rij_y ;

						ff[0][nj] += wij * (node[ni].P[2][l] - node[nj].P[2][l]) * rij_x ;
						ff[1][nj] += wij * (node[ni].P[2][l] - node[nj].P[2][l]) * rij_y ;
						ff[2][nj] -= wij * (node[ni].P[2][l] - node[nj].P[2][l]) * rij_x * rij_x ;
						ff[3][nj] -= wij * (node[ni].P[2][l] - node[nj].P[2][l]) * rij_x * rij_y ;
						ff[4][nj] -= wij * (node[ni].P[2][l] - node[nj].P[2][l]) * rij_y * rij_y ;
					}
				}
	 		}
		}

//		TODO put an IF the nodes are dry!!!!!!!!
		for (int f = 0; f < NBF; f++) {
			if (b_nodes[f].type!=10){
				int ni = boundary[f].node[0] ;	
				int nj = boundary[f].node[1] ;
				double rij_x = node[nj].coordinate[0] - node[ni].coordinate[0]; // The length of the egde ij
				double rij_y = node[nj].coordinate[1] - node[ni].coordinate[1];
				/***********************************************/
				// For the conservation of the mean:
				/***********************************************/
				rij_x = node[nj].DcC[0] - node[ni].DcC[0];
				rij_y = node[nj].DcC[1] - node[ni].DcC[1];
				/***********************************************/
				double wij = 1./sqrt(pow(rij_x,2) + pow(rij_y,2));

				ff[0][ni] += wij * (node[ni].P[2][l] - node[nj].P[2][l]) * rij_x ;
				ff[1][ni] += wij * (node[ni].P[2][l] - node[nj].P[2][l]) * rij_y ;
				ff[2][ni] += wij * (node[ni].P[2][l] - node[nj].P[2][l]) * rij_x * rij_x ;
				ff[3][ni] += wij * (node[ni].P[2][l] - node[nj].P[2][l]) * rij_x * rij_y ;
				ff[4][ni] += wij * (node[ni].P[2][l] - node[nj].P[2][l]) * rij_y * rij_y ;
	
				ff[0][nj] += wij * (node[ni].P[2][l] - node[nj].P[2][l]) * rij_x ;
				ff[1][nj] += wij * (node[ni].P[2][l] - node[nj].P[2][l]) * rij_y ;
				ff[2][nj] -= wij * (node[ni].P[2][l] - node[nj].P[2][l]) * rij_x * rij_x ;
				ff[3][nj] -= wij * (node[ni].P[2][l] - node[nj].P[2][l]) * rij_x * rij_y ;
				ff[4][nj] -= wij * (node[ni].P[2][l] - node[nj].P[2][l]) * rij_y * rij_y ;
			}
		}
	
		// Correction for periodic boundaries
	     	for ( int f = 0 ; f < NBF ; f ++ ) {
        		if(b_nodes[f].type == 10) {
		            	int n1 = b_nodes[f].node ;
				if(node[n1].flag == 0) {		
					int mate = node[n1].mate ;
					ff[0][n1] += ff[0][mate];
					ff[1][n1] += ff[1][mate];
					ff[2][n1] += ff[2][mate];
					ff[3][n1] += ff[3][mate];
					ff[4][n1] += ff[4][mate];

					ff[0][mate] = ff[0][n1];
					ff[1][mate] = ff[1][n1];
					ff[2][mate] = ff[2][n1];
					ff[3][mate] = ff[3][n1];
					ff[4][mate] = ff[4][n1];

					node[mate].flag = 1;
					node[n1].flag = 1;
				}
			 }
	    	}

		for (int n = 0; n < NN; n++){
			ff[0][n] *= 0.5; ff[1][n] *= 0.5; ff[2][n] *= 0.5; ff[3][n] *= 0.5; ff[4][n] *= 0.5;
		}

		/***********************************************************************/
		/***  Solution of the  quadratic least square minimization problem   ***/
		/***********************************************************************/
		
		// Solution of the system solving for the gradient and Hessian:		
		for (int i = 0 ; i < NN ; i++ )	{
			double a1, a2, a3, a4, a5;
			double b1, b2, b3, b4, b5;
			double c1, c2, c3, c4, c5;
			double d1, d2, d3, d4, d5;
			double e1, e2, e3, e4, e5;
			double f1, f2, f3, f4, f5;
			a1 = katz[0][i]; b1 = katz[1][i]; c1 = katz[3][i]*0.5; d1 = katz[4][i]; e1 = katz[5][i]*0.5;
			a2 = katz[1][i]; b2 = katz[2][i]; c2 = katz[4][i]*0.5; d2 = katz[5][i]; e2 = katz[6][i]*0.5;
			a3 = katz[3][i]; b3 = katz[4][i]; c3 = katz[7][i]*0.5; d3 = katz[8][i]; e3 = katz[9][i]*0.5;
			a4 = katz[4][i]; b4 = katz[5][i]; c4 = katz[8][i]*0.5; d4 = katz[9][i]; e4 = katz[10][i]*0.5;
			a5 = katz[5][i]; b5 = katz[6][i]; c5 = katz[9][i]*0.5; d5 = katz[10][i];e5 = katz[11][i]*0.5;
			f1 = -ff[0][i];   f2 = -ff[1][i]; f3 = -ff[2][i];   f4 = -ff[3][i];      f5 = -ff[4][i];

			double dxP, dyP, dxxP, dxyP, dyyP; 

//			if ((a4*a4 >= 1e-12 || b4*b4 >= 1e-12 || c4*c4 >= 1e-12 || d4*d4 >= 1e-12 || e4*e4 >= 1e-12)){//&& (node[i].num_neigh>=5)){
			if (node[i].num_neigh>=5){
				double A = f2 - a2/a1*f1 ;
				double B = a2/a1*c1 - c2 ;
				double C = a2/a1*d1 - d2 ;
				double D = a2/a1*e1 - e2 ;
				double E = b2 - a2/a1*b1 ;
				double F = (f3 - a3/a1*f1) + (a3/a1*b1 - b3)*A/E ;
				double G = (a3/a1*d1 - d3) + (a3/a1*b1 - b3)*C/E ;
				double H = (a3/a1*e1 - e3) + (a3/a1*b1 - b3)*D/E ;
				double I = (c3 - a3/a1*c1) + (b3 - a3/a1*b1)*B/E ;
				double L = (f4 - a4/a1*f1) + (a4/a1*b1 - b4)*A/E + ((a4/a1*c1 - c4) + (a4/a1*b1 - b4)*B/E)*F/I ; 
				double M = (a4/a1*e1 - e4) + (a4/a1*b1 - b4)*D/E + ((a4/a1*c1 - c4) + (a4/a1*b1 - b4)*B/E)*H/I ;
				double N = (d4 - a4/a1*d1) + (b4 - a4/a1*b1)*C/E + ((c4 - a4/a1*c1) + (b4 - a4/a1*b1)*B/E)*G/I ;
				double P = ((f5 - a5/a1*f1) + (a5/a1*b1 - b5)*A/E + ((a5/a1*c1 - c5) + (a5/a1*b1 - b5)*B/E)*F/I) +
				    ((a5/a1*d1 - d5) + (a5/a1*b1 - b5)*C/E + ((a5/a1*c1 - c5) + (a5/a1*b1 - b5)*B/E)*G/I)*L/N ; 
				double Q = ((e5 - a5/a1*e1) + (b5 - a5/a1*b1)*D/E + ((c5 - a5/a1*c1) + (b5 - a5/a1*b1)*B/E)*H/I) +
				    ((d5 - a5/a1*d1) + (b5 - a5/a1*b1)*C/E + ((c5 - a5/a1*c1) + (b5 - a5/a1*b1)*B/E)*G/I)*M/N ; 
	
				dyyP = P/Q;	
				dxyP = (L + M*dyyP)/N ;
				dxxP = (F + G*dxyP + H*dyyP)/I ;
				dyP = (A + B*dxxP + C*dxyP + D*dyyP)/E ;
				dxP = (f1 - b1*dyP - c1*dxxP - d1*dxyP - e1*dyyP)/a1 ; 		
//				if (i==1878)
//					printf("sopra %le %le %le %le %le\n",dxP, dyP, dxxP, dxyP, dyyP);
			} else {
			//if (i==1878){
//			if (node[i].num_neigh==4){
				//	printf("sotto %le \n",node[i].dxyP2[time_lev][l]);
				double A = f2 - a2/a1*f1 ;
				double B = a2/a1*c1 - c2 ;
				double C = a2/a1*d1 - d2 ;
				double D = a2/a1*e1 - e2 ;
				double E = b2 - a2/a1*b1 ;
				double F = (f3 - a3/a1*f1) + (a3/a1*b1 - b3)*A/E ;
				double G = (a3/a1*d1 - d3) + (a3/a1*b1 - b3)*C/E ;
				double H = (a3/a1*e1 - e3) + (a3/a1*b1 - b3)*D/E ;
				double I = (c3 - a3/a1*c1) + (b3 - a3/a1*b1)*B/E ;
				double L = (f5 - a5/a1*f1) + (a5/a1*b1 - b5)*A/E + ((a5/a1*c1 - c5) + (a5/a1*b1 - b5)*B/E)*F/I ; 
				double M = (a5/a1*d1 - d5) + (a5/a1*b1 - b5)*C/E + ((a5/a1*c1 - c5) + (a5/a1*b1 - b5)*B/E)*G/I ;
				double N = (e5 - a5/a1*e1) + (b5 - a5/a1*b1)*D/E + ((c5 - a5/a1*c1) + (b5 - a5/a1*b1)*B/E)*H/I ;
				
				dxyP = node[i].dxyP2[0][l];
				dyyP = (L + M*dxyP)/N ;
				dxxP = (F + G*dxyP + H*dyyP)/I ;
				dyP = (A + B*dxxP + C*dxyP + D*dyyP)/E ;
				dxP = (f1 - b1*dyP - c1*dxxP - d1*dxyP - e1*dyyP)/a1 ; 		
//				if (i==1878)
//					printf("sotto %le %le %le %le %le\n",dxP, dyP, dxxP, dxyP, dyyP);
			}

			node[i].dxP2[time_lev][l] = dxP ;
			node[i].dyP2[time_lev][l] = dyP ;
			node[i].dxxP2[time_lev][l] = dxxP ;
			node[i].dxyP2[time_lev][l] = dxyP ;
			node[i].dyyP2[time_lev][l] = dyyP ;
		}

		// Solution of the system solving for gradient only:		
/*		for (int i = 0 ; i < NN ; i++ ){
//		for (int f = 0 ; f < NBF ; f++ ) {
//			if (b_nodes[f].type==3) {
//				int i = b_nodes[i].node;
				double a1 = katz[0][i]; 
				double b1 = katz[1][i];
				double c1 = katz[2][i];
				double d1 = ff[0][i]; 
				double e1 = ff[1][i];

				double dxP = -(c1*d1 - b1*e1)/(a1*c1 - b1*b1);
				double dyP = -(e1*a1 - b1*d1)/(a1*c1 - b1*b1);
			
				node[i].dxP2[time_lev][l] = dxP ;
				node[i].dyP2[time_lev][l] = dyP ;
//			}
		}
*/   	}


//Boundary correction for wall boundary conditions!!!!!!!!!!!!
        for (int f=0; f<NBF; f++){
	        int n0=b_nodes[f].node;
        	if(b_nodes[f].type == 3){
	                double length=sqrt(pow(b_nodes[f].normal[0],2) + pow(b_nodes[f].normal[1],2));
                	for (int l = 0 ; l < size_lev ; l++ ){
	                   double dnP2 = node[n0].dxP2[time_lev][l]*b_nodes[f].normal[0] +node[n0].dyP2[time_lev][l]*b_nodes[f].normal[1];
	                   node[n0].dxP2[time_lev][l] -= dnP2*b_nodes[f].normal[0]/pow(length,2);
	                   node[n0].dyP2[time_lev][l] -= dnP2*b_nodes[f].normal[1]/pow(length,2);
			}
		}
                for (int l = 0 ; l < size_lev ; l++ ){
	        	node[n0].dxxP2[time_lev][l] = 0.;
		        node[n0].dxyP2[time_lev][l] = 0.;
		        node[n0].dyyP2[time_lev][l] = 0.;
		}
	}
}

/* This function reconstructs the gradient of the bathymetry */
void bathy_rec(int time_lev, int prj_flag)
{
	// proj_flag = 0 => bathy rec. for FV-MUSCL scheme
	// time_lev = 0 => RK1: grad. rec. for bed[0]                           
	// time_lev = 1 => RK2: grad. rec. for bed[2]
	// proj_flag = 1 => bathy rec. for FV-MUSCL projection
	// time_lev = 0 => RK1: grad. rec. for bed[1]

	int *dry_flag = MA_vector( NN, sizeof(int) );
	for (int n = 0; n < NN; n++) {
		node[n].flag = 0;
		node[n].dxbed[time_lev] = 0.0;
		node[n].dybed[time_lev] = 0.0;
		if (time_lev == 0)
			dry_flag[n] = node[n].dry_flag0;
		else
			dry_flag[n] = node[n].dry_flag2;
	}

	for (int e = 0; e < NE; e++) {
		double GradK_x = 0.0;
		double GradK_y = 0.0;
		double max_Htot = -0.0;

		for (int i = 0; i < vertex; i++) {
			int m = element[e].node[i];
			if (!dry_flag[m])
				//if ((node[m].P[2 * time_lev + prj_flag][0] + node[m].bed[2 * time_lev + prj_flag] + cut_off_H * node[m].cut_off_ratio) > max_Htot)
				//	max_Htot = node[m].P[2 * time_lev + prj_flag][0] + node[m].bed[2 * time_lev + prj_flag];
				if ((node[m].P[2][0] + node[m].bed[2] + cut_off_H * node[m].cut_off_ratio) > max_Htot)
					max_Htot = node[m].P[2][0] + node[m].bed[2];
		}

		for (int i = 0; i < vertex; i++) {
			int m = element[e].node[i];

			double bathy;
			if(prj_flag==0){
			// Correction on shoreline: WB rec.      
			//if ((dry_flag[m]) && ((node[m].bed[2 * time_lev + prj_flag] + cut_off_H * node[m].cut_off_ratio) > max_Htot))
			if ((dry_flag[m]) && ((node[m].bed[2] + cut_off_H * node[m].cut_off_ratio) > max_Htot))
				bathy = max_Htot;
			else
				//bathy = node[m].bed[2 * time_lev + prj_flag];
				bathy = node[m].bed[2];
			}else
				bathy = node[m].bed[2];

			GradK_x += element[e].normal[i][0] * bathy;
			GradK_y += element[e].normal[i][1] * bathy;
		}

		GradK_x *= 0.5;
		GradK_y *= 0.5;

		for (int i = 0; i < vertex; i++) {
			int m = element[e].node[i];
			node[m].dxbed[time_lev] += GradK_x;
			node[m].dybed[time_lev] += GradK_y;
		}
	}
	MA_free( dry_flag );

	// Correction for periodic boundaries
	for (int f = 0 ; f < NBF ; f ++ ) {
         	if(b_nodes[f].type == 10) {
            		int n1 = b_nodes[f].node ;
	    		if(node[n1].flag == 0) {		
				int mate = node[n1].mate ;
 	            		node[n1].dxbed[time_lev] += node[mate].dxbed[time_lev] ;
	            		node[mate].dxbed[time_lev] = node[n1].dxbed[time_lev] ;
	            		node[n1].dybed[time_lev] += node[mate].dybed[time_lev] ;
	            		node[mate].dybed[time_lev] = node[n1].dybed[time_lev] ;
		    		node[mate].flag = 1;
		    		node[n1].flag = 1;
	    		}
	 	}
    	}

	// Green-Gauss formula
	for (int n = 0; n < NN; n++) {
		node[n].dxbed[time_lev] /= (node[n].volume * 3.0);
		node[n].dybed[time_lev] /= (node[n].volume * 3.0);
	}
	// Correction for wall boundaries
	for (int f = 0; f < NBF; f++) {
		if(b_nodes[f].type==3){
			int n0 = b_nodes[f].node;
			double length = sqrt(pow(b_nodes[f].normal[0], 2) + pow(b_nodes[f].normal[1], 2));
			double dnbed = node[n0].dxbed[time_lev] * b_nodes[f].normal[0] + node[n0].dybed[time_lev] * b_nodes[f].normal[1];
			node[n0].dxbed[time_lev] -= dnbed*b_nodes[f].normal[0]/pow(length, 2);
			node[n0].dybed[time_lev] -= dnbed*b_nodes[f].normal[1]/pow(length, 2);
		}
	}
}

void lap_bathy_rec(int time_lev, int prj_flag)
{
	// proj_flag = 0 => bathy rec. for FV-MUSCL scheme
	// time_lev = 0 => RK1: grad. rec. for bed[0]                           
	// time_lev = 1 => RK2: grad. rec. for bed[2]
	// proj_flag = 1 => bathy rec. for FV-MUSCL projection
	// time_lev = 0 => RK1: grad. rec. for bed[1]

	int *dry_flag = MA_vector( NN, sizeof(int) );
	for (int n = 0; n < NN; n++) {
		node[n].flag = 0;
		node[n].dxxbed[time_lev] = 0.0;
		node[n].dyybed[time_lev] = 0.0;
		node[n].dxybed[time_lev] = 0.0;
		if (time_lev == 0)
			dry_flag[n] = node[n].dry_flag0;
		else
			dry_flag[n] = node[n].dry_flag2;
	}

	for (int e = 0; e < NE; e++) {
		double GradK_x = 0.0;
		double GradK_y = 0.0;
		double GradK_xy = 0.0;
		double max_Htot = -0.0;

		for (int i = 0; i < vertex; i++) {
			int m = element[e].node[i];
			if (!dry_flag[m])
				//if ((node[m].P[2 * time_lev + prj_flag][0] + node[m].bed[2 * time_lev + prj_flag] + cut_off_H * node[m].cut_off_ratio) > max_Htot)
				//	max_Htot = node[m].P[2 * time_lev + prj_flag][0] + node[m].bed[2 * time_lev + prj_flag];
				if ((node[m].P[2][0] + node[m].bed[2] + cut_off_H * node[m].cut_off_ratio) > max_Htot){
					max_Htot = node[m].P[2][0] + node[m].bed[2];
				}
		}

		for (int i = 0; i < vertex; i++) {
			int m = element[e].node[i];

			double bathyx,bathyy;
			// Correction on shoreline: WB rec.      
			//if ((dry_flag[m]) && ((node[m].bed[2 * time_lev + prj_flag] + cut_off_H * node[m].cut_off_ratio) > max_Htot))
			if ((dry_flag[m]) && ((node[m].bed[2] + cut_off_H * node[m].cut_off_ratio) > max_Htot)){
				bathyx = max_Htot;  //-------------ATTENTION IT HAS TO CHANGE
				bathyy = max_Htot;
			}else{
				//bathy = node[m].bed[2 * time_lev + prj_flag];
				bathyx = node[m].dxbed[time_lev];
				bathyy = node[m].dybed[time_lev];
			}

			GradK_x  += element[e].normal[i][0] * bathyx;
			GradK_xy += element[e].normal[i][1] * bathyx;
			GradK_y  += element[e].normal[i][1] * bathyy;

		}

		GradK_x *= 0.5;
		GradK_y *= 0.5;
		GradK_xy *= 0.5;

		for (int i = 0; i < vertex; i++) {
			int m = element[e].node[i];
			node[m].dxxbed[time_lev] += GradK_x;
			node[m].dyybed[time_lev] += GradK_y;
			node[m].dxybed[time_lev] += GradK_xy;
		}
	}
	MA_free( dry_flag );

	// Correction for periodic boundaries
	for (int f = 0 ; f < NBF ; f ++ ) {
         	if(b_nodes[f].type == 10) {
            		int n1 = b_nodes[f].node ;
	    		if(node[n1].flag == 0) {		
				int mate = node[n1].mate ;
 	            		node[n1].dxxbed[time_lev] += node[mate].dxxbed[time_lev] ;
	            		node[mate].dxxbed[time_lev] = node[n1].dxxbed[time_lev] ;
	            		node[n1].dyybed[time_lev] += node[mate].dyybed[time_lev] ;
	            		node[mate].dyybed[time_lev] = node[n1].dyybed[time_lev] ;
	            		node[n1].dxybed[time_lev] += node[mate].dxybed[time_lev] ;
	            		node[mate].dxybed[time_lev] = node[n1].dxybed[time_lev] ;
		    		node[mate].flag = 1;
		    		node[n1].flag = 1;
	    		}
	 	}
    	}

	// Green-Gauss formula
	for (int n = 0; n < NN; n++) {
		node[n].dxxbed[time_lev] /= (node[n].volume * 3.0);
		node[n].dyybed[time_lev] /= (node[n].volume * 3.0);
		node[n].dxybed[time_lev] /= (node[n].volume * 3.0);
	}
	// Correction for wall boundaries
	for (int f = 0; f < NBF; f++) {
		if(b_nodes[f].type==3){
			//int n0 = b_nodes[f].node;
			//double length = sqrt(pow(b_nodes[f].normal[0], 2) + pow(b_nodes[f].normal[1], 2));
			//double dnbed = node[n0].dxxbed[time_lev] * b_nodes[f].normal[0] + node[n0].dyybed[time_lev] * b_nodes[f].normal[1];
//			node[n0].dxxbed[time_lev] -= dnbed*b_nodes[f].normal[0]/pow(length, 2);
//			node[n0].dyybed[time_lev] -= dnbed*b_nodes[f].normal[1]/pow(length, 2);
//
		//	dnbed = node[n0].dxxbed[time_lev] * b_nodes[f].normal[0] + node[n0].dxybed[time_lev] * b_nodes[f].normal[1];
		//	node[n0].dxybed[time_lev] -= dnbed*b_nodes[f].normal[1]/pow(length, 2);

	        	/*node[n0].dxxbed[time_lev] = 0.;
		        node[n0].dxybed[time_lev] = 0.;
		        node[n0].dyybed[time_lev] = 0.;*/
		}
	}
}

