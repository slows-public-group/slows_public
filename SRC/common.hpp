/*  Copyright (C) 2021   M.Ricchiuto  <mario.ricchiuto@inria.fr>
                         M. Kazolea   <maria.kazolea@inria.fr>
                         A. Filippini <a.filippini@brgm.fr>
                         L. Arpaia    <luca.arpaia@brgm.fr>
                         N.Pattakos   <pattakosn@fastmail.com> 

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.*/

#ifndef COMMON_HPP
#define COMMON_HPP
struct element_struct {
	int node[3];
	double normal[3][2];
	double normal_0[3][2];
	double **FVnormal;
	double volume;
	double new_volume;
	double volume_0;
	double beds[2];
	double nu;
	double length;
	double cut_off_ratio;
	double phi[3];
	double *cg;
	double **mp;
	double **weight;
};

struct node_struct {
	int mate;
	int flag;
	int flag_boundary[3];
	double **P;
	double P1[3];
	double P2[3];
	double **Z;
	double **K;
	double **friction;
	double bed[3];
	double rhs[2];
	double rhs1[2];
	double rhs2[2];
	double *Res;
	double *Res1;
	double *Res2;
	double coordinate[3];
	double coordinate_ite[2];
	double coordinate_new[2];
	double coordinate_old[2];
	double coordinate_0[2];
	double *DcC;
	double *ss_DcC;
	double vel[2];
	double dtau;
	double dti;
	double tt;
	double h1;
	double h2;
    	double u_norm ; // added to limit the velocity
    	double H_norm ; // added to limit the velocity
	int negative;
	int dry_flag0;
	int dry_flag1;
	int dry_flag2;
	int dry_flag3;
	int dry_flag4;
	int fully_dry;
	double volume;
	double old_volume;
	double mod_volume;
	double ss_volume;
	double max_Htot0;
	double deltaB;
	double sum_alpha;
	double length;
	double cut_off_ratio;
	double **dxP;
	double **dyP;
	double **dxP2;
	double **dyP2;
	double **dxxP2;
	double **dxyP2;
	double **dyxP2;
	double **dyyP2;
	double dxbed[2];
	double dybed[2];
	double dxxbed[2];
	double dxybed[2];
	double dyxbed[2];
	double dyybed[2];
	double dxeta;
	double dyeta;
	double dxxeta;
	double dyyeta;
	double dxyeta;
	double dyxeta;
	double Grad;
	double Hess;
	double *corgrad;    //Correction for the first gradient (M1^-1)
	double *corsecgrad; //Correction for the second gradient (M2^-1)
	double shore;
	double pp;
	double omega;
	double diss;	
	double **PHI_bar;
	double wg;
	double tur[2]; //term added for the turbulence eddy vicosity (breaking)
	double u_ave;
	double v_ave;
	double cell_ave[3]; // averaged value of h,hu,hv in the FV cell with "center" the node
	int neigh[15]; // neighbouring nodes: DEFAULT maximum number of neighbours = 15
	int num_neigh; // number of neighbours
	double neigh_vec[15][2]; //normal vectors
	double moments[3];
	double moments2[3];
	double secop[6]; //the matrix for the construction of the second order first derivative
	double der_moments[6]; // the first derivatives of the momennts
	double intbase[3];
};

struct boundary_struct {
	int *node;
	int *types;
	int type;
	double normal[2];
};

struct numerical_int_struct {
	double **face_coordinate;
	double *face_weight;
	double **volume_coordinate;
	double *volume_weight;

};

struct boundary_nodes_struct {
	int node;
	int type;
	int mate;
	int flag;
	int flag_mm;		// flag for moving mesh bc
	double normal[2];
	double area ; // for inlet/outlet nodes is the cross-sectional area associated to the node (to impose the volume flux)
	double blength ; // for inlet/outlet nodes is the boundary length associated to the node (to impose the volume flux)
	double dxP[3];
	double dyP[3];
	double dxxP[3];
	double dxyP[3];
	double dyyP[3];
};
struct boundary_nodes2_struct {
	int node2;
	double dxP[3];
	double dyP[3];
	double dxxP[3];
	double dxyP[3];
	double dyyP[3];
	double dxbed;
	double dybed;
	double corgrad[4];
	double moments2[3];
	int boundary; // indicates the noe of the four boundaries that is closer to the node
	double dist_bound[2]; // distance of xg,yg to the above boundary
};
struct boundary_nodes3_struct {
	int node3;
//	double dxP[3];
//	double dyP[3];
//	double dxbed;
//	double dybed;
//	double corgrad[4];
	double moments2[3];
	int boundary; // indicates the noe of the four boundaries that is closer to the node
	double dist_bound[2]; // distance of xg,yg to the above boundary
};

struct probe_struct {
        int node;
        double *coordinate;
        double dist;
};
#endif
