/*  Copyright (C) 2021   M.Ricchiuto  <mario.ricchiuto@inria.fr>
                         M. Kazolea   <maria.kazolea@inria.fr>
                         A. Filippini <a.filippini@brgm.fr>
                         L. Arpaia    <luca.arpaia@brgm.fr>
                         N.Pattakos   <pattakosn@fastmail.com> 

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.*/


#include <math.h>
#include "common.h"
extern int initial_state;
extern struct node_struct *node;

/** apply sponge boundary conditions to the domain (only when GN is used) */
void sponge_bc(int n)
{
	if ( 70 == initial_state) {
		double x1 = 10.0;//-2.;//10; //give the coordinate where the left layer starts
		double x2 = 60.0;//26.;//60.; //85.; // give the coordinate where the right layer starts
		double lengthx = 10.0;//5.0;//10.; //15.;
               
		if (node[n].coordinate[0] <= x1){   // left sponge layer
			node[n].P[2][0] = (node[n].P[2][0]+node[n].bed[2]) * sqrt(fabs(1.0-pow((x1-node[n].coordinate[0])/lengthx,2))) - node[n].bed[2];
			node[n].P[2][1]= node[n].P[2][1]* sqrt(fabs(1.0-pow((x1-node[n].coordinate[0])/lengthx,2)));
			node[n].P[2][2]= node[n].P[2][2]* sqrt(fabs(1.0-pow((x1-node[n].coordinate[0])/lengthx,2)));
		}
		if(node[n].coordinate[0]>= x2){   // right sponge layer
			node[n].P[2][0] = (node[n].P[2][0] + node[n].bed[2]) * sqrt(fabs(1.0-pow((node[n].coordinate[0]-x2)/lengthx,2))) - node[n].bed[2];
			node[n].P[2][1]=node[n].P[2][1]* sqrt(fabs(1.0-pow((node[n].coordinate[0]-x2)/lengthx,2)));
			node[n].P[2][2]=node[n].P[2][2]* sqrt(fabs(1.0-pow((node[n].coordinate[0]-x2)/lengthx,2)));
		}
	/*	double y1 = 10;//8.; //give the coordinate where the bottom layer starts (inside the domain)
		double y2 = 60;//62.; // give the coordinate where the top layer starts
		double length_top = 10;//8.;
		double length_bottom = 10;//8.;
               
		if(node[n].coordinate[1]<= y1){   // bottom sponge layer
			node[n].P[2][0]=(node[n].P[2][0]+node[n].bed[2])* sqrt(fabs(1.0-pow((y1-node[n].coordinate[1])/length_bottom,2))) -node[n].bed[2];
			node[n].P[2][1]= node[n].P[2][1]* sqrt(fabs(1.0-pow((y1-node[n].coordinate[1])/length_bottom,2)));
			node[n].P[2][2]= node[n].P[2][2]* sqrt(fabs(1.0-pow((y1-node[n].coordinate[1])/length_bottom,2)));
		}
		if(node[n].coordinate[1]>= y2){   // top sponge layer
			node[n].P[2][0]=(node[n].P[2][0]+node[n].bed[2])* sqrt(fabs(1.0-pow((node[n].coordinate[1]-y2)/length_top,2))) -node[n].bed[2];
			node[n].P[2][1]=node[n].P[2][1]* sqrt(fabs(1.0-pow((node[n].coordinate[1]-y2)/length_top,2)));
			node[n].P[2][2]=node[n].P[2][2]* sqrt(fabs(1.0-pow((node[n].coordinate[1]-y2)/length_top,2)));
		}*/
	}


	if ( 75 == initial_state) { // rip_current
		double x1 = 2.9; //give the coordinate where the left layer starts
		double lengthx = 2.9;
               
		if(node[n].coordinate[0]<= x1) {  // left sponge layer
			node[n].P[2][0]=(node[n].P[2][0]+node[n].bed[2])* sqrt(fabs(1.0-pow((x1-node[n].coordinate[0])/lengthx,2))) -node[n].bed[2];

			node[n].P[2][1]= node[n].P[2][1]* sqrt(fabs(1.0-pow((x1-node[n].coordinate[0])/lengthx,2)));
			node[n].P[2][2]= node[n].P[2][2]* sqrt(fabs(1.0-pow((x1-node[n].coordinate[0])/lengthx,2)));
		}
	}
       if ( 76 == initial_state || 83 == initial_state) { // synolakis
		double x1 = -20.;
                double x2 = 50.;  // give the coordinate where the right layer starts
                double lengthx = 10.;

		if(node[n].coordinate[0]<= x1){   // left sponge layer
			node[n].P[2][0]=(node[n].P[2][0]+node[n].bed[2])* sqrt(fabs(1.0-pow((x1-node[n].coordinate[0])/lengthx,2))) -node[n].bed[2];
			node[n].P[2][1]= node[n].P[2][1]* sqrt(fabs(1.0-pow((x1-node[n].coordinate[0])/lengthx,2)));
			node[n].P[2][2]= node[n].P[2][2]* sqrt(fabs(1.0-pow((x1-node[n].coordinate[0])/lengthx,2)));
		}
		if(node[n].coordinate[0]>= x2){   // right sponge layer
			node[n].P[2][0]=(node[n].P[2][0]+node[n].bed[2])* sqrt(fabs(1.0-pow((node[n].coordinate[0]-x2)/lengthx,2))) -node[n].bed[2];
			node[n].P[2][1]=node[n].P[2][1]* sqrt(fabs(1.0-pow((node[n].coordinate[0]-x2)/lengthx,2)));
			node[n].P[2][2]=node[n].P[2][2]* sqrt(fabs(1.0-pow((node[n].coordinate[0]-x2)/lengthx,2)));
		}

        }
	if ( 78 == initial_state) { // wave over a bar
		double x1 = -5.;//10.; //give the coordinate where the left layer starts (inside the domain)
		double x2 = 26.;//24.; //60; // give the coordinate where the right layer starts
		double lengthx = 5.;//10.0;

//		double y1 = -2.; 
//		double y2 = 26; 
//		double length_top = 5.0;
//		double length_bottom = 5.0;

		if(node[n].coordinate[0]<= x1){   // left sponge layer
			node[n].P[2][0]=(node[n].P[2][0]+node[n].bed[2])* sqrt(fabs(1.0-pow((x1-node[n].coordinate[0])/lengthx,2))) -node[n].bed[2];
			node[n].P[2][1]= node[n].P[2][1]* sqrt(fabs(1.0-pow((x1-node[n].coordinate[0])/lengthx,2)));
			node[n].P[2][2]= node[n].P[2][2]* sqrt(fabs(1.0-pow((x1-node[n].coordinate[0])/lengthx,2)));
		}
		if(node[n].coordinate[0]>= x2){   // right sponge layer
			node[n].P[2][0]=(node[n].P[2][0]+node[n].bed[2])* sqrt(fabs(1.0-pow((node[n].coordinate[0]-x2)/lengthx,2))) -node[n].bed[2];
			node[n].P[2][1]=node[n].P[2][1]* sqrt(fabs(1.0-pow((node[n].coordinate[0]-x2)/lengthx,2)));
			node[n].P[2][2]=node[n].P[2][2]* sqrt(fabs(1.0-pow((node[n].coordinate[0]-x2)/lengthx,2)));
		}

/*		if(node[n].coordinate[1]<= y1){   // bottom sponge layer
			node[n].P[2][0]=(node[n].P[2][0]+node[n].bed[2])* sqrt(fabs(1.0-pow((y1-node[n].coordinate[1])/length_bottom,2))) -node[n].bed[2];
			node[n].P[2][1]= node[n].P[2][1]* sqrt(fabs(1.0-pow((y1-node[n].coordinate[1])/length_bottom,2)));
			node[n].P[2][2]= node[n].P[2][2]* sqrt(fabs(1.0-pow((y1-node[n].coordinate[1])/length_bottom,2)));
		}
		if(node[n].coordinate[1]>= y2){   // top sponge layer
			node[n].P[2][0]=(node[n].P[2][0]+node[n].bed[2])* sqrt(fabs(1.0-pow((node[n].coordinate[1]-y2)/length_top,2))) -node[n].bed[2];
			node[n].P[2][1]=node[n].P[2][1]* sqrt(fabs(1.0-pow((node[n].coordinate[1]-y2)/length_top,2)));
			node[n].P[2][2]=node[n].P[2][2]* sqrt(fabs(1.0-pow((node[n].coordinate[1]-y2)/length_top,2)));
		}*/
		
/*
		double x1 = 0.0;//-10;//-7.; //left down corner of the (unroated) domain 
		double y1 = 0.0; 

		double x2 = 70;//29;//31; // right down corner of the domain
		double y2 = 0; // 

		double x3 = 70;//29;//31.; //right up corner of the domain
		double y3 = 0.8;//0.43;

		double x4 = 0.0;//-10;//-7; // left up corner of the domain
		double y4 = 0.8;//0.43; 

		double length = 10.0;
		double theta=0.0;//pi/2.0; //rotation angle

		double x=node[n].coordinate[0];
		double y=node[n].coordinate[1];
	
		double xn1=cos(theta)*x1-sin(theta)*y1;
		double yn1=sin(theta)*x1+cos(theta)*y1;
		double xn2=cos(theta)*x2-sin(theta)*y2;
		double yn2=sin(theta)*x2+cos(theta)*y2;
		double xn3=cos(theta)*x3-sin(theta)*y3;
		double yn3=sin(theta)*x3+cos(theta)*y3;
		double xn4=cos(theta)*x4-sin(theta)*y4;
		double yn4=sin(theta)*x4+cos(theta)*y4;
		 
                double a1=(yn3-yn2)/(xn3-xn2); // the coef of the line e1 that passes from xn2,yn2 xn3,yn3
		double b=-1.0;
		double c1=-a1*xn2+yn2;

                double a2=(yn4-yn1)/(xn4-xn1); // the coef of the line e2 that passes from xn1,yn1 xn4,yn4
		double c2=-a2*xn1+yn1;

		double d1 =fabs(a1*x+b*y+c1)/sqrt(pow(a1,2)+1); //distance from a point to the firt line
		double d2 =fabs(a2*x+b*y+c2)/sqrt(pow(a2,2)+1); //distance from a point to the firt line

			printf("%le %le %le %le %le\n",x,a1,a2,c1,c2);
		if(d1<= length){   // if the point is close to the e1 line
			node[n].P[2][0]=(node[n].P[2][0]+node[n].bed[2])* sqrt(fabs(1.0-pow(d1/length,2))) -node[n].bed[2];
			node[n].P[2][1]= node[n].P[2][1]* sqrt(fabs(1.0-pow(d1/length,2)));
			node[n].P[2][2]= node[n].P[2][2]* sqrt(fabs(1.0-pow(d1/length,2)));
		}
		if(d2<= length){   // if the point is close to the e1 line
			node[n].P[2][0]=(node[n].P[2][0]+node[n].bed[2])* sqrt(fabs(1.0-pow(d2/length,2))) -node[n].bed[2];
			node[n].P[2][1]= node[n].P[2][1]* sqrt(fabs(1.0-pow(d2/length,2)));
			node[n].P[2][2]= node[n].P[2][2]* sqrt(fabs(1.0-pow(d2/length,2)));
		}
*/
	}
	if (81 == initial_state) { //elliptic shoal
		double y1 = -13;//-7.;//-13.; //give the coordinate where the bottom layer starts (inside the domain)
		double y2 = 10.; // give the coordinate where the top layer starts
		double length_bottom =4;//3.; //4.;
		double length_top = 5.;
               
		if(node[n].coordinate[1]<= y1){   // bottom sponge layer
			node[n].P[2][0]=(node[n].P[2][0]+node[n].bed[2])* sqrt(fabs(1.0-pow((y1-node[n].coordinate[1])/length_bottom,2))) -node[n].bed[2];
			node[n].P[2][1]= node[n].P[2][1]* sqrt(fabs(1.0-pow((y1-node[n].coordinate[1])/length_bottom,2)));
			node[n].P[2][2]= node[n].P[2][2]* sqrt(fabs(1.0-pow((y1-node[n].coordinate[1])/length_bottom,2)));
		}
		if(node[n].coordinate[1]>= y2){   // top sponge layer
			node[n].P[2][0]=(node[n].P[2][0]+node[n].bed[2])* sqrt(fabs(1.0-pow((node[n].coordinate[1]-y2)/length_top,2))) -node[n].bed[2];
			node[n].P[2][1]=node[n].P[2][1]* sqrt(fabs(1.0-pow((node[n].coordinate[1]-y2)/length_top,2)));
			node[n].P[2][2]=node[n].P[2][2]* sqrt(fabs(1.0-pow((node[n].coordinate[1]-y2)/length_top,2)));
		}
	 
	/*	double x1 = -13.; //give the coordinate where the bottom layer starts (inside the domain)
		double x2 = 10.; // give the coordinate where the top layer starts
		double length_top = 5.;
		double length_bottom = 4.;
               
		if(node[n].coordinate[0]<= x1){   // bottom sponge layer
			node[n].P[2][0]=(node[n].P[2][0]+node[n].bed[2])* sqrt(fabs(1.0-pow((x1-node[n].coordinate[0])/length_bottom,2))) -node[n].bed[2];
			node[n].P[2][1]= node[n].P[2][1]* sqrt(fabs(1.0-pow((x1-node[n].coordinate[0])/length_bottom,2)));
			node[n].P[2][2]= node[n].P[2][2]* sqrt(fabs(1.0-pow((x1-node[n].coordinate[0])/length_bottom,2)));
		}
		if(node[n].coordinate[0]>= x2){   // top sponge layer
			node[n].P[2][0]=(node[n].P[2][0]+node[n].bed[2])* sqrt(fabs(1.0-pow((node[n].coordinate[0]-x2)/length_top,2))) -node[n].bed[2];
			node[n].P[2][1]=node[n].P[2][1]* sqrt(fabs(1.0-pow((node[n].coordinate[0]-x2)/length_top,2)));
			node[n].P[2][2]=node[n].P[2][2]* sqrt(fabs(1.0-pow((node[n].coordinate[0]-x2)/length_top,2)));
		}*/
	}
	if (82 == initial_state) { //circ shoal
		double x1 = 6; //-4.; //give the coordinate where the left layer starts (inside the domain)
		double x2 = 40;// 30.; // give the coordinate where the right layer starts
		double lengthx = 6.;

		if(node[n].coordinate[0]<= x1){   // left sponge layer
			node[n].P[2][0]=(node[n].P[2][0]+node[n].bed[2])* sqrt(fabs(1.0-pow((x1-node[n].coordinate[0])/lengthx,2))) -node[n].bed[2];
			node[n].P[2][1]= node[n].P[2][1]* sqrt(fabs(1.0-pow((x1-node[n].coordinate[0])/lengthx,2)));
			node[n].P[2][2]= node[n].P[2][2]* sqrt(fabs(1.0-pow((x1-node[n].coordinate[0])/lengthx,2)));
		}
		if(node[n].coordinate[0]>= x2){   // right sponge layer
			node[n].P[2][0]=(node[n].P[2][0]+node[n].bed[2])* sqrt(fabs(1.0-pow((node[n].coordinate[0]-x2)/lengthx,2))) -node[n].bed[2];
			node[n].P[2][1]=node[n].P[2][1]* sqrt(fabs(1.0-pow((node[n].coordinate[0]-x2)/lengthx,2)));
			node[n].P[2][2]=node[n].P[2][2]* sqrt(fabs(1.0-pow((node[n].coordinate[0]-x2)/lengthx,2)));
		}
		double li = 1.4941;//6.1784;//1.4941;;
		double Xs=6.0; //generation zone position
		double x = node[n].coordinate[0] ;
		double delta = 0.8;//0.5;// generation zone semi-amplitude
		double wsize= delta*li/2.0;
		double lengthw=wsize;

		if(x>= Xs-wsize  && x<=Xs) 
			node[n].P[2][2]=node[n].P[2][2]* sqrt(fabs(1.0-pow((node[n].coordinate[0]-(Xs-wsize))/lengthw,2)));

		if(x>=Xs  && x<= Xs+wsize ) 
			node[n].P[2][2]= node[n].P[2][2]* sqrt(fabs(1.0-pow(((Xs+wsize)-node[n].coordinate[0])/lengthw,2)));
			
	}
	if (86 == initial_state) { // Tissier ondular bore
		double x2 = 140.; // give the coordinate where the right layer starts
		double lengthx = 10.;

		if(node[n].coordinate[0]>= x2){   // right sponge layer
			node[n].P[2][0]=(node[n].P[2][0]+node[n].bed[2])* sqrt(fabs(1.0-pow((node[n].coordinate[0]-x2)/lengthx,2))) -node[n].bed[2];
			node[n].P[2][1]=node[n].P[2][1]* sqrt(fabs(1.0-pow((node[n].coordinate[0]-x2)/lengthx,2)));
			node[n].P[2][2]=node[n].P[2][2]* sqrt(fabs(1.0-pow((node[n].coordinate[0]-x2)/lengthx,2)));
		}
	}
	if (90 == initial_state) { // Solitary shelf 1d
		double x1 = 10.; //give the coordinate where the left layer starts (inside the domain)
		double x2 = 270.; // give the coordinate where the right layer starts
		double lengthx = 10.;

		if(node[n].coordinate[0]<= x1){   // left sponge layer
			node[n].P[2][0]=(node[n].P[2][0]+node[n].bed[2])* sqrt(fabs(1.0-pow((x1-node[n].coordinate[0])/lengthx,2))) -node[n].bed[2];
			node[n].P[2][1]= node[n].P[2][1]* sqrt(fabs(1.0-pow((x1-node[n].coordinate[0])/lengthx,2)));
			node[n].P[2][2]= node[n].P[2][2]* sqrt(fabs(1.0-pow((x1-node[n].coordinate[0])/lengthx,2)));
		}
		if(node[n].coordinate[0]>= x2){   // right sponge layer
			node[n].P[2][0]=(node[n].P[2][0]+node[n].bed[2])* sqrt(fabs(1.0-pow((node[n].coordinate[0]-x2)/lengthx,2))) -node[n].bed[2];
			node[n].P[2][1]=node[n].P[2][1]* sqrt(fabs(1.0-pow((node[n].coordinate[0]-x2)/lengthx,2)));
			node[n].P[2][2]=node[n].P[2][2]* sqrt(fabs(1.0-pow((node[n].coordinate[0]-x2)/lengthx,2)));
		}
	}
	if (88 == initial_state) { // Hansen and Svendsen
		double x1 = -16; //give the coordinate where the left layer starts (inside the domain)
		double x2 = 16.; // give the coordinate where the right layer starts
		double lengthx = 10.;

		if(node[n].coordinate[0]<= x1){   // left sponge layer
			node[n].P[2][0]=(node[n].P[2][0]+node[n].bed[2])* sqrt(fabs(1.0-pow((x1-node[n].coordinate[0])/lengthx,2))) -node[n].bed[2];
			node[n].P[2][1]= node[n].P[2][1]* sqrt(fabs(1.0-pow((x1-node[n].coordinate[0])/lengthx,2)));
			node[n].P[2][2]= node[n].P[2][2]* sqrt(fabs(1.0-pow((x1-node[n].coordinate[0])/lengthx,2)));
		}
		if(node[n].coordinate[0]>= x2){   // right sponge layer
			node[n].P[2][0]=(node[n].P[2][0]+node[n].bed[2])* sqrt(fabs(1.0-pow((node[n].coordinate[0]-x2)/lengthx,2))) -node[n].bed[2];
			node[n].P[2][1]=node[n].P[2][1]* sqrt(fabs(1.0-pow((node[n].coordinate[0]-x2)/lengthx,2)));
			node[n].P[2][2]=node[n].P[2][2]* sqrt(fabs(1.0-pow((node[n].coordinate[0]-x2)/lengthx,2)));
		}
	}
	if (93 == initial_state) { // solitary interaction with a vertical cylinder
		double x1 = 2; //give the coordinate where the left layer starts (inside the domain)
		double x2 = 12.; // give the coordinate where the right layer starts
		double lengthx = 2.;

		if(node[n].coordinate[0]<= x1){   // left sponge layer
			node[n].P[2][0]=(node[n].P[2][0]+node[n].bed[2])* sqrt(fabs(1.0-pow((x1-node[n].coordinate[0])/lengthx,2))) -node[n].bed[2];
			node[n].P[2][1]= node[n].P[2][1]* sqrt(fabs(1.0-pow((x1-node[n].coordinate[0])/lengthx,2)));
			node[n].P[2][2]= node[n].P[2][2]* sqrt(fabs(1.0-pow((x1-node[n].coordinate[0])/lengthx,2)));
		}
		if(node[n].coordinate[0]>= x2){   // right sponge layer
			node[n].P[2][0]=(node[n].P[2][0]+node[n].bed[2])* sqrt(fabs(1.0-pow((node[n].coordinate[0]-x2)/lengthx,2))) -node[n].bed[2];
			node[n].P[2][1]=node[n].P[2][1]* sqrt(fabs(1.0-pow((node[n].coordinate[0]-x2)/lengthx,2)));
			node[n].P[2][2]=node[n].P[2][2]* sqrt(fabs(1.0-pow((node[n].coordinate[0]-x2)/lengthx,2)));
		}
	}
}
