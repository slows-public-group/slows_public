/*  Copyright (C) 2021   M.Ricchiuto  <mario.ricchiuto@inria.fr>
                         M. Kazolea   <maria.kazolea@inria.fr>
                         A. Filippini <a.filippini@brgm.fr>
                         L. Arpaia    <luca.arpaia@brgm.fr>
                         N.Pattakos   <pattakosn@fastmail.com> 

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.*/


/***************************************************************************
                              upwind_matrices.c
                              -----------------
 This is upwind_matrices: the computation of the upwind parameters K_PLUS 
 fully using the eigenspectrum of the system and the compputation of the 
 flux Jacobians projected on the normals is performed here
                             -------------------

***************************************************************************/
#include <stdio.h>
#include <math.h>
#include "common.h"

extern const int size;
extern int Mscaling, numerics_friction_explicit;
extern double sigma_barx, sigma_bary, cut_off_U, UUinf, VVinf, manning, dt, ubar0, vbar0, height0, gm, alpha_LF, L_ref;
extern double speed_of_sound0, gridsize, CFL;
extern double **Right, **Left, ***K_i, ***K_i_p1;
extern double *Lambda, *FV_ubar0, *FV_vbar0, *FV_height0, *FV_speed_of_sound0, *FV_sigma_barx, *FV_sigma_bary, temp_normal[2+1];

extern struct node_struct *node;
extern struct element_struct *element;

void Eigenvectors(double *);	/* normals, variables, RIGHT EIGENVECTORS */
void Waves(double *);	/* normals, variables, WAVES */

/* UPWIND MATRIX FOR SHALLOW WATER SYSTEM */
/* RD */
void compute_RD_upwind_Keys(int e)
{
	double hh = 1.e-3 * speed_of_sound0*gridsize;
	double speed = 0.0;
	double norm = 0.;
	double max = 0.;
	double volume = element[e].volume;
	alpha_LF = 0.0;
    
    double fluxlim = 0.;
    for (int v = 0; v < 3; v++)
    {
        int nn = element[e].node[v];
        if ( node[nn].u_norm > fluxlim ) fluxlim = node[nn].u_norm ;
    }
    
	for (int v = 0; v < 3; v++) {
		for (int j = 0; j < 2; j++)
			temp_normal[j] = element[e].normal[v][j];

		if (sqrt(temp_normal[0] * temp_normal[0] + temp_normal[1] * temp_normal[1]) > norm)
			norm = sqrt(temp_normal[0] * temp_normal[0] + temp_normal[1] * temp_normal[1]);

		int nn = element[e].node[v];
        
        double uux = node[nn].Z[2][1] ;
        double vvy = node[nn].Z[2][2] ;
        double velnorm = sqrt( uux*uux + vvy*vvy );
        double velratio = velnorm/fluxlim ;
        if ( velratio < 1. ) velratio = 1.0 ;
        uux /= velratio ;
        vvy /= velratio ;

		if (fabs( uux - node[nn].vel[0]) + fabs(vvy - node[nn].vel[1]) > max)
			max = fabs(uux - node[nn].vel[0]) + fabs(vvy - node[nn].vel[1]);

		if (sqrt(gm * fabs(node[nn].Z[2][0])) > speed)
			speed = sqrt(gm * fabs(node[nn].Z[2][0]));

		double nx = temp_normal[0] * 0.5;
		double ny = temp_normal[1] * 0.5;
		double sigman = nx * sigma_barx + ny * sigma_bary;

		/* Assembling K_i : Advective part */
		K_i[v][0][0] = 0.;
		K_i[v][0][1] = nx;
		K_i[v][0][2] = ny;

		K_i[v][1][0] = (speed_of_sound0 * speed_of_sound0 * nx - ubar0 * (ubar0 * nx + vbar0 * ny));
		K_i[v][1][1] = (ubar0 * nx + vbar0 * ny + ubar0 * nx);
		K_i[v][1][2] = ubar0 * ny;

		K_i[v][2][0] = (speed_of_sound0 * speed_of_sound0 * ny - vbar0 * (ubar0 * nx + vbar0 * ny));
		K_i[v][2][1] = vbar0 * nx;
		K_i[v][2][2] = (ubar0 * nx + vbar0 * ny + vbar0 * ny);

		// MSCALING
		if (Mscaling) {
			Eigenvectors(temp_normal);
			Waves(temp_normal);

			for (int i = 0; i < size; i++) {
				/* Assembling K_i : ALE part */
				K_i[v][i][i] -= sigman;

				for (int j = 0; j < size; j++) {
					K_i_p1[v][i][j] = 0.;

					/* Building K1 plus */
					for (int k = 0; k < size; k++) {
						double abss = fabs(Lambda[k]);

						if (abss < 2. * hh)
							abss = hh + 0.25 * (abss * abss) / hh;

						K_i_p1[v][i][j] += 0.5 * Right[i][k] * abss * Left[k][j];
					}
				}
			}
		}
	}

	double coeff = speed * speed / gm;
	double aa = 0;
	if (1 == numerics_friction_explicit) {
		if (coeff > cut_off_U * element[e].cut_off_ratio) {
			coeff = pow(coeff, 4. / 3.);
			aa = volume * manning * manning * gm * max / coeff;
		}
	}

	max = 0.5 * (max + speed) * norm;
	max += 0.5 * (fabs(UUinf) + fabs(VVinf)) * gridsize * element[e].cut_off_ratio / L_ref;
	max += aa;
	alpha_LF = max;

	element[e].nu = 1.5 * dt * alpha_LF / (element[e].volume);

	for (int v = 0; v < 3; v++) {
		int n = element[e].node[v];
		node[n].sum_alpha += alpha_LF;
		node[n].dtau = MIN(node[n].dtau, CFL / (1. + element[e].nu));
	}
}

/* FV */
void compute_FV_upwind_Keys(int e)
{
	for (int v = 0; v < 3; v++) {
		double hh = 1.e-3 * FV_speed_of_sound0[v];

		for (int j = 0; j < 2; j++)
			temp_normal[j] = element[e].FVnormal[v][j];

		//sigman = 0.0;
		height0 = FV_height0[v];
		ubar0 = FV_ubar0[v];
		vbar0 = FV_vbar0[v];
		speed_of_sound0 = FV_speed_of_sound0[v];
		sigma_barx = FV_sigma_barx[v];
		sigma_bary = FV_sigma_bary[v];

		Eigenvectors(temp_normal);
		Waves(temp_normal);

		/* Building K1 plus */
		for (int i = 0; i < size; i++) {
			for (int j = 0; j < size; j++) {
				K_i_p1[v][i][j] = 0.;
				for (int k = 0; k < size; k++) {
					double abss = fabs(Lambda[k]);

					if (abss < 2. * hh)
						abss = hh + 0.25 * (abss * abss) / hh;

					K_i_p1[v][i][j] += 0.5 * Right[i][k] * abss * Left[k][j];
				}
			}
		}
	}
}

