/*  Copyright (C) 2021   M.Ricchiuto  <mario.ricchiuto@inria.fr>
                         M. Kazolea   <maria.kazolea@inria.fr>
                         A. Filippini <a.filippini@brgm.fr>
                         L. Arpaia    <luca.arpaia@brgm.fr>
                         N.Pattakos   <pattakosn@fastmail.com> 

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.*/


/** flag the breaking nodes according to the physical creteria based on surface gradient,
 * the time derivatve of the free surface elevation and the Froud number */
#include <stdio.h>
#include <math.h>
#include <assert.h>
#include <stdlib.h>
#include <time.h>
#include<vector>
#include "common.h"
#include "nht.h"
#include "my_malloc.h"
#include "derivative_reconstruction.h"
#include "surface_velocity.h"
#include "breaking_hybrid.h"

using std::vector;

static double maxValue( double *array, int size )
{
	/* enforce the contract */
	assert( array && size );

	double localMax = array[0];
	for ( int i = 1; i < size; ++i )
		if ( array[i] > localMax )
			localMax = array[i];
	return localMax;
}

// Finds the minumum Value of an Array of doubles. If flag is one, zero values are excluded from computation.
// If flag is 2, values less or equal than 0 are excluded from computation.
static double minValue( double *array, int size, int flag )
{
	/* enforce the contract */
	assert( array && size );

	double localMin = array[0];
	if ( flag == 1 ) {
		for ( int i = 1; i < size; ++i )
			if ( array[i] != 0  && array[i] < localMin )
				localMin = array[i];
	} else if( flag == 2 ) {
		for ( int i = 1; i < size; ++i )
			if ( array[i] > 0  && array[i] < localMin )
				localMin = array[i];
	}

	return localMin;
}

void breaking_hybrid( double GRAVITY, double mu, size_t NN, node_s *node, double gb, double phi )
{
	vector< int > bb_tmp( NN, 0 );
	for ( size_t n = 0; n < NN; n++ )
		node->bb[n] = 0;

	int iflagc = 0;
	int iflage = 0;
	int iflag_brk = 0;
	int iflagfr = 0;


	for ( size_t n = 0; n < NN; n++ ) {
		if ( node->SWL[n] != 0. && node->h[n] > 0. ) {
		//if ( node->h[n] > 0. ) {
			double uin = gb * sqrt( GRAVITY * node->h[n] ); //maybe node[n].SWL ?
			double h_t =  fabs(node->gradhu[n] + node->gradhv[n + NN]);
			//double h_t = sqrt(pow(node->gradhu[n],2) + pow(node->gradhv[n + NN],2));
			//double gradh = sqrt( pow( node[n].dxeta + node[n].dxbed, 2 ) + pow( node[n].dyeta + node[n].dybed, 2 ) );
			double gradh = sqrt( pow( node->gradeta[n], 2 ) + pow( node->gradeta[n + NN], 2) );

			if ( fabs(h_t) > uin / mu || gradh >= phi / mu ) {
				node->bb[n] = 1;
				iflag_brk = 1;
				if( fabs(h_t) > uin / mu){
					iflagc = 1;
				} else {
					iflage = 1;
				}
			}
		}
	}

	for ( size_t n = 0; n < NN; n++ )
		bb_tmp[n] = node->bb[n];
	for ( size_t n = 0; n < NN; n++ ) {
		if ( bb_tmp[n] == 1 ) {
			for ( int j = 1; j < node->neighbouring_nodes[n]; j++ )
				node->bb[node->neighbours[n][j]] = 1;
		}
	}

	if ( iflagc == 1 || iflage == 1 ) {
		vector< vector < int > > igroup;
		for ( size_t n = 0; n < NN; n++ ) {
			bb_tmp[n] = node->bb[n];
		}
		// Wave tracking
		for ( size_t n = 0; n < NN; n++ ) {
			if ( bb_tmp[n] == 1 ) {
				vector< int > grp_to_add;
				grp_to_add.push_back( n );
				bb_tmp[ n ] = 0;
				size_t icount = 0;
				do {
					size_t nn = grp_to_add[ icount ];
					for ( size_t j = 1; j < (size_t)node->neighbouring_nodes[ n ]; j++ ) {
						if ( bb_tmp[ node->neighbours[ nn ][ j ] ] == 1 ) {
							grp_to_add.push_back( node->neighbours[ nn ][ j ] );
							bb_tmp[node->neighbours[ nn ][ j ] ] = 0;
						}
					}
					icount = icount + 1;
				} while( icount < grp_to_add.size() );
				igroup.push_back( grp_to_add );
			}
		}


		// Extent the region according to the wave height
		vector< double > tmp_brkh( NN, 0 );
		vector< double > tmp_brkh2( NN, 0 );
		for ( size_t k = 0; k < igroup.size(); k++ ) {
			double dminx = 100000.0;
			double dmaxx =-100000.0;
			double dminy = 100000.0;
			double dmaxy =-100000.0;
			for ( size_t icount = 0; icount < igroup[k].size(); ++icount ) {
				size_t nn = igroup[k][icount];
				if ( node->bb[nn] == 1 ) {
					dminx = fmin( dminx, node->coordinate[nn][0] );
					dmaxx = fmax( dmaxx, node->coordinate[nn][0] );
					dminy = fmin( dminy, node->coordinate[nn][1] );
					dmaxy = fmax( dmaxy, node->coordinate[nn][1] );
				}
				tmp_brkh[nn] = node->eta[nn];
				tmp_brkh2[nn] = node->h[nn];
			}
			double brkh = maxValue( &tmp_brkh[ 0 ], NN ) - minValue( &tmp_brkh[ 0 ], NN, 1 );
			double brkswi = dmaxx - dminx;
			double blr = 2.5 * 2.9 * mu * brkh;
			double bnsw = 0.0;
			if( blr > brkswi ) {
				bnsw = 0.5 * ( blr - brkswi );
				dmaxx = dmaxx + bnsw;
				dminx = dminx - bnsw;

				// Find the Froude number
				double Fr = sqrt( (pow( ((2.0 * maxValue( &tmp_brkh2[ 0 ], NN ) / minValue( &tmp_brkh2[ 0 ], NN, 2 )) + 1.0), 2 ) - 1.0) / 8.0 );
				for ( size_t i = 0; i < NN; i++ ) {
					if (   node->coordinate[i][0] >= dminx && node->coordinate[i][0] <= dmaxx
					    && node->coordinate[i][1] >= dminy && node->coordinate[i][1] <= dmaxy ) {
						node->bb[i] = 1;
						if ( Fr <= 1.3 ) {
							node->bb[i] = 0;
							iflagfr=1;
						}
					}
				}
			}
		}
		if ( iflag_brk == 1 )
			printf(" BREAKING IS ON: itrack %zu, iflagc %d, iflage %d, iflagfr %d \n", igroup.size(), iflagc, iflage, iflagfr);
	}
}

void kinen( size_t NN, size_t NE, size_t NBF, double *cut_off, element_s *element, node_s *node, boundary_nodes_s *b_nodes, edge_s *edge, non_hydro_s *data, double timep, double sigma, double lt, double cd)
{
	double dt = data->time - timep;
	static double *vt = NULL;
	if ( !vt )
		vt = (double*)MA_vector( NN, sizeof(double) );
	static double *RHS = NULL;
	if ( !RHS )
		RHS = (double*)MA_vector( NN, sizeof(double) );

	//computation of the surface velocity
	surface_vel( NN, element, node, b_nodes, edge );

	for ( size_t n = 0; n < NN; n++ )
		vt[n] = sqrt( node->k[n] ) * lt;

	static double *gradk = NULL;
	if ( !gradk )
		gradk = (double*)MA_vector( 2 * NN, sizeof(double) ); /* The first NN elements are the dx of gradk and the last NN are the dy elements of gradk */
	grad_reconstruction( NN, NE, NBF, cut_off, element, node, b_nodes, node->k, gradk, 0 );

	for ( size_t n = 0; n < NN; n++ ) {
		double Q1 = 0.;
		double Q2 = 0.;
		double Q3 = 0.;
		double Q4 = 0.;
		double Q5 = 0.;
		double Q6 = 0.;

		for ( int i = 1; i < node->neighbouring_nodes[n]; i++ ) {
			int n0 = n;
			int edge_local = node->adj_edges[n][i];
			int e1 = edge->element[edge_local][0];
			int e2 = edge->element[edge_local][1];

			for ( int j = 1; j <= 2; j++ ) {
				int e;
				if ( j == 1 )
					e = e1;	//This if works only for internal elements
				else
					e = e2;

				if ( e != -1 ) {
					int n0flag;//, n1flag, n2flag;
					double normal_n0[2];
					if ( n0 == element->node[e][0] || node->mate[n0] == element->node[e][0] ) {
						normal_n0[0] = element->normal[e][0][0];
						normal_n0[1] = element->normal[e][0][1];
						n0flag = 0;
						//n1flag = 1;
						//n2flag = 2;
					} else if ( n0 == element->node[e][1] || node->mate[n0] == element->node[e][1] ) {
						normal_n0[0] = element->normal[e][1][0];
						normal_n0[1] = element->normal[e][1][1];
						n0flag = 1;
						//n1flag = 2;
						//n2flag = 0;
					} else {
						normal_n0[0] = element->normal[e][2][0];
						normal_n0[1] = element->normal[e][2][1];
						n0flag = 2;
						//n1flag = 1;
						//n2flag = 0;
					}

					double tmp_sum1 = node->us[element->node[e][0]][0] * node->k[element->node[e][0]]
					                 +node->us[element->node[e][1]][0] * node->k[element->node[e][1]]
					                 +node->us[element->node[e][2]][0] * node->k[element->node[e][2]];
					double tmp_sum2 = node->us[element->node[e][0]][1] * node->k[element->node[e][0]]
					                 +node->us[element->node[e][1]][1] * node->k[element->node[e][1]]
					                 +node->us[element->node[e][2]][1] * node->k[element->node[e][2]];
					Q1 += 1./6.*(tmp_sum1*normal_n0[0] + tmp_sum2*normal_n0[1]);

					double tmp_sum3 = element->normal[e][0][0] * node->us[element->node[e][0]][0]
					                 +element->normal[e][1][0] * node->us[element->node[e][1]][0]
					                 +element->normal[e][2][0] * node->us[element->node[e][2]][0];
					double tmp_sum4 = element->normal[e][0][1] * node->us[element->node[e][0]][1]
					                 +element->normal[e][1][1] * node->us[element->node[e][1]][1]
					                 +element->normal[e][2][1] * node->us[element->node[e][2]][1];

					// decomment if you want to use mass lumping on this term
					double tmp_sum5 = element->volume[e]/3.*node->k[element->node[e][n0flag]];
					Q2 += 1./(4.*element->volume[e])*(tmp_sum3 + tmp_sum4)*tmp_sum5;

					double tmp_sum6 = element->normal[e][0][0] * node->k[element->node[e][0]]
					                 +element->normal[e][1][0] * node->k[element->node[e][1]]
					                 +element->normal[e][2][0] * node->k[element->node[e][2]];
					double tmp_sum7 = element->normal[e][0][1] * node->k[element->node[e][0]]
					                 +element->normal[e][1][1] * node->k[element->node[e][1]]
					                 +element->normal[e][2][1] * node->k[element->node[e][2]];
					double tmp_sum8 = vt[element->node[e][0]] + vt[element->node[e][1]] + vt[element->node[e][2]];
					Q3 -= sigma/(12.*element->volume[e])*(tmp_sum6*normal_n0[0] + tmp_sum7*normal_n0[1])*tmp_sum8;

					double tmp_sum9 = element->normal[e][0][0] * vt[element->node[e][0]]
					                 +element->normal[e][1][0] * vt[element->node[e][1]]
					                 +element->normal[e][2][0] * vt[element->node[e][2]];
					double tmp_sum10 = element->normal[e][0][1] * vt[element->node[e][0]]
					                  +element->normal[e][1][1] * vt[element->node[e][1]]
					                  +element->normal[e][2][1] * vt[element->node[e][2]];
					Q4 += 1./(12.*element->volume[e])*(tmp_sum6*tmp_sum9 + tmp_sum7*tmp_sum10);

					//decomment if you want to use mass lumping
					Q5 += node->bb[element->node[e][n0flag]] * pow(lt,2)/sqrt(cd)* element->volume[e]/3.0 * pow(pow(node->uz[element->node[e][n0flag]][0],2) + pow(node->uz[element->node[e][n0flag]][1],2) ,3./2.);
					//decomment if you want to use mass lumping
					Q6 -= cd/lt*element->volume[e]/3.0 *pow(node->k[element->node[e][n0flag]],3./2.);
				}
			}

			/* BOUNDARY TERMS */
			if ( edge->bflag[edge_local] == 3 ) {
				Q1 -= 0.5 * node->k[n0] * (node->us[n0][0] * b_nodes->normal[node->bindex[n0]][0] + node->us[n0][1] * b_nodes->normal[node->bindex[n0]][1]);
				Q3 += 0.5 * sigma * vt[n0] * (gradk[n0] * b_nodes->normal[node->bindex[n0]][0] + gradk[n0 + NN] * b_nodes->normal[node->bindex[n0]][1]);
			}
		}

		RHS[n] = Q1 + Q2 + Q3 + Q4 + Q5 + Q6;
	}
	// 1] using mass lumping :

	double maxb = 0.;
	//check if breaking is alive
	for ( size_t n = 0; n < NN; n++ ) {
		if ( node->bb[n] != 0 )
			maxb = 1.0;
	}
	double max = 0.;
	for ( size_t n = 0; n < NN; n++ ) {
		node->k[n] +=  dt / node->volume[n] * RHS[n];
		//if ( node[n].k <= 0.0 || node[n].coordinate[0] <= 20.02 )
		//if ( node[n].k <= 0.0 || node[n].bb == 0 )
		//if ( node[n].k <= 0.0 || node[n].SWL == 0 )
		if( node->k[n] <= 0.0 )
			node->k[n] = 0.0;
		if( node->k[n] > max )
			max = node->k[n];
		if( maxb == 0. )
			node->k[n] = 0.;
		//if ( node[n].k > 0 && node[n].SWL == 0 )
		//	maxb = 1.
	}
	printf( "---------maximum value of k %le\n", max );

	for ( size_t n = 0; n < NN; n++ )
		vt[n] = sqrt( node->k[n] ) * lt * (node->gradhu[n] + node->gradhv[n + NN]);

	grad_reconstruction( NN, NE, NBF, cut_off, element, node, b_nodes, vt, node->brk, 0 );
}
