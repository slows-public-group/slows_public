/*  Copyright (C) 2021   M.Ricchiuto  <mario.ricchiuto@inria.fr>
                         M. Kazolea   <maria.kazolea@inria.fr>
                         A. Filippini <a.filippini@brgm.fr>
                         L. Arpaia    <luca.arpaia@brgm.fr>
                         N.Pattakos   <pattakosn@fastmail.com> 

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.*/
	

/***************************************************************************
                          breaking_hybrid
                               --------
This file flags the breaking nodes according to the physical creteria based on
surface gradient, the time derivatve of the free surface elevation and the Froud
number
 ***************************************************************************/
#include <math.h>
#include <assert.h>
#include <stdlib.h>
#include <time.h>
#include "common.h"
#include "surface_velocity.h"

 /* Nodal bathymetry value averaged among the neighbours */
void average( double **w, int avgTimes, int offset, int NN, node_s *node )
{
	double work_vect[NN];
	double count[NN];
	// arithmetic average....maybe we should account for node volume

	for ( int k = 1; k < avgTimes; k++ ) {
		for ( int n = 0; n < NN; n++ ) {
			work_vect[n] = 0.;
			count[n] = 0.;
		}

		for ( int n = 0; n < NN; n++ ) {
			for ( int j = 0; j < node->neighbouring_nodes[n]; j++ ) {
				if ( node->bflag[node->neighbours[n][j]] == 10 && node->bflag[n] == 10 ) {
					if ( node->neighbours[n][j] == n ) {
						work_vect[n] += w[node->neighbours[n][j]][offset];
						count[n] += 1;
					} else {
						work_vect[n] += 0.5 * w[node->neighbours[n][j]][offset];
						count[n] += 0.5;
					}
				} else {
					work_vect[n] += w[node->neighbours[n][j]][offset];
					count[n] += 1.;
				}
			}
		}

		for ( int n = 0; n < NN; n++ )
			w[n][offset] = work_vect[n]/count[n];
	}
}

void surface_vel(int NN, element_s *element, node_s *node, boundary_nodes_s *b_nodes, edge_s *edge)
{
	//!!!!NIKOS only the first NN elements of gradu and gradhu are used in here...
	//!!!!NIKOS only the last NN elements of gradv and gradhv are used in here...

	for (int n = 0; n < NN; n++) {
		double Q11[2] = {0}; // for the surface velocity us
		double Q21[2] = {0};
		double Q31[2] = {0};
		double Q41[2] = {0};

		double Q12[2] = {0}; // for the gradient of uz atributed in the surface
		double Q22[2] = {0};
		double Q32[2] = {0};

		for (int i = 1; i < node->neighbouring_nodes[n]; i++) {
			int n0 = n;
			int edge_local = node->adj_edges[n][i];
			int e1 = edge->element[edge_local][0];
			int e2 = edge->element[edge_local][1];

			for (int j = 1; j <= 2; j++) {
				int e;
				if (j == 1)
					e = e1;	//This if works only for internal elements
				else
					e = e2;

				if (e != -1) {
					int n0flag;//, n1flag, n2flag;
					double normal_n0[2];
					if (n0 == element->node[e][0] || node->mate[n0] == element->node[e][0]) {
						normal_n0[0] = element->normal[e][0][0];
						normal_n0[1] = element->normal[e][0][1];
						n0flag = 0;
						//n1flag = 1;
						//n2flag = 2;
					} else if (n0 == element->node[e][1] || node->mate[n0] == element->node[e][1]) {
						normal_n0[0] = element->normal[e][1][0];
						normal_n0[1] = element->normal[e][1][1];
						n0flag = 1;
						//n1flag = 2;
						//n2flag = 0;
					} else {
						normal_n0[0] = element->normal[e][2][0];
						normal_n0[1] = element->normal[e][2][1];
						n0flag = 2;
						//n1flag = 1;
						//n2flag = 0;
					}

					double tmp_sum1 = element->normal[e][0][0] * node->u[element->node[e][0]]
						+element->normal[e][1][0] * node->u[element->node[e][1]]
						+element->normal[e][2][0] * node->u[element->node[e][2]];
					double tmp_sum2 = element->normal[e][0][1] * node->v[element->node[e][0]]
						+element->normal[e][1][1] * node->v[element->node[e][1]]
						+element->normal[e][2][1] * node->v[element->node[e][2]];
					double tmp_sum3 = -1./6.*pow(node->SWL[element->node[e][0]],2) + 1./3.*pow(node->eta[element->node[e][0]],2) +1./6 *node->SWL[element->node[e][0]] * node->eta[element->node[e][0]]
						-1./6.*pow(node->SWL[element->node[e][1]],2) + 1./3.*pow(node->eta[element->node[e][1]],2) +1./6 *node->SWL[element->node[e][1]] * node->eta[element->node[e][1]]
						-1./6.*pow(node->SWL[element->node[e][2]],2) + 1./3.*pow(node->eta[element->node[e][2]],2) +1./6 *node->SWL[element->node[e][2]]*node->eta[element->node[e][2]];

					double tmp_coeff = 1./(12.*element->volume[e])*(tmp_sum1+tmp_sum2);

					Q11[0] +=tmp_coeff*tmp_sum3*normal_n0[0];
					Q11[1] +=tmp_coeff*tmp_sum3*normal_n0[1];


					double tmp_sum4 = element->normal[e][0][0] * node->SWL[element->node[e][0]]
					                 +element->normal[e][1][0] * node->SWL[element->node[e][1]]
					                 +element->normal[e][2][0] * node->SWL[element->node[e][2]];
					double tmp_sum5 = element->normal[e][0][1] * node->SWL[element->node[e][0]]
					                 +element->normal[e][1][1] * node->SWL[element->node[e][1]]
					                 +element->normal[e][2][1] * node->SWL[element->node[e][2]];
					double tmp_sum6 = element->normal[e][0][0] * node->eta[element->node[e][0]]
					                 +element->normal[e][1][0] * node->eta[element->node[e][1]]
					                 +element->normal[e][2][0] * node->eta[element->node[e][2]];
					double tmp_sum7 = element->normal[e][0][1] * node->eta[element->node[e][0]]
					                 +element->normal[e][1][1] * node->eta[element->node[e][1]]
					                 +element->normal[e][2][1] * node->eta[element->node[e][2]];
					double tmp_sum8 = element->normal[e][0][0] * node->eta[element->node[e][0]] * node->SWL[element->node[e][0]]
					                 +element->normal[e][1][0] * node->eta[element->node[e][1]] * node->SWL[element->node[e][1]]
					                 +element->normal[e][2][0] * node->eta[element->node[e][2]] * node->SWL[element->node[e][2]];
					double tmp_sum9 = element->normal[e][0][1] * node->eta[element->node[e][0]] * node->SWL[element->node[e][0]]
					                 +element->normal[e][1][1] * node->eta[element->node[e][1]] * node->SWL[element->node[e][1]]
					                 +element->normal[e][2][1] * node->eta[element->node[e][2]] * node->SWL[element->node[e][2]];


					//Q21[0] += tmp_coeff*(-1./3. *(0.5*node[element[e].node[n0flag]].SWL + 0.25*node[element[e].node[n1flag]].SWL + 0.25*node[element[e].node[n2flag]].SWL)*tmp_sum4
					//		   + 2./3. *(0.5*node[element[e].node[n0flag]].eta + 0.25*node[element[e].node[n1flag]].eta + 0.25*node[element[e].node[n2flag]].eta)*tmp_sum6
					//		   + 1./6. *tmp_sum8);
					//Q21[1] += tmp_coeff*(-1./3. *(0.5*node[element[e].node[n0flag]].SWL + 0.25*node[element[e].node[n1flag]].SWL + 0.25*node[element[e].node[n2flag]].SWL)*tmp_sum5
					//		   + 2./3. *(0.5*node[element[e].node[n0flag]].eta + 0.25*node[element[e].node[n1flag]].eta + 0.25*node[element[e].node[n2flag]].eta)*tmp_sum7
					//		   + 1./6. *tmp_sum9);
					//decomment if you want to do mass lumping
					Q21[0] += tmp_coeff*(-1./3. *node->SWL[element->node[e][n0flag]] *tmp_sum4 + 2./3.*node->eta[element->node[e][n0flag]] * tmp_sum6 + 1./6.*tmp_sum8);
					Q21[1] += tmp_coeff*(-1./3. *node->SWL[element->node[e][n0flag]] *tmp_sum5 + 2./3.*node->eta[element->node[e][n0flag]] * tmp_sum7 + 1./6.*tmp_sum9);

					double tmp_sum10 = element->normal[e][0][0] * node->hu[element->node[e][0]]
					                  +element->normal[e][1][0] * node->hu[element->node[e][1]]
					                  +element->normal[e][2][0] * node->hu[element->node[e][2]];
					double tmp_sum11 = element->normal[e][0][1] * node->hv[element->node[e][0]]
					                  +element->normal[e][1][1] * node->hv[element->node[e][1]]
					                  +element->normal[e][2][1] * node->hv[element->node[e][2]];

					double tmp_sum12 = node->h[element->node[e][0]] + node->h[element->node[e][1]] + node->h[element->node[e][2]];

					tmp_coeff = 1./(24.*element->volume[e])*(tmp_sum10+tmp_sum11)*tmp_sum12;

					Q31[0] +=tmp_coeff*normal_n0[0];
					Q31[1] +=tmp_coeff*normal_n0[1];

					tmp_coeff = 1./(24.*element->volume[e])*(tmp_sum10+tmp_sum11);

					Q41[0] +=tmp_coeff*(tmp_sum4+tmp_sum6);
					Q41[1] +=tmp_coeff*(tmp_sum5+tmp_sum7);

					// Terms for the gradient of uz atributed in the surface
					tmp_sum3 =  node->eta[element->node[e][0]] + node->eta[element->node[e][1]] + node->eta[element->node[e][2]];
					tmp_coeff = 1./(12.*element->volume[e])*(tmp_sum1+tmp_sum2)*tmp_sum3;

					Q12[0] +=tmp_coeff*normal_n0[0];
					Q12[1] +=tmp_coeff*normal_n0[1];

					Q22[0] += 1./(12.*element->volume[e])*(tmp_sum1+tmp_sum2)*tmp_sum6;
					Q22[1] += 1./(12.*element->volume[e])*(tmp_sum1+tmp_sum2)*tmp_sum7;

					tmp_coeff = 1./(4.*element->volume[e])*(tmp_sum10+tmp_sum11);

					Q32[0] +=tmp_coeff*normal_n0[0];
					Q32[1] +=tmp_coeff*normal_n0[1];
				}
			}
			/****************************
			 **** 	BOUNDARY TERMS  ****
			 ****************************/
			if (edge->bflag[edge_local] == 3) {
				double tmp_coeff = 0.5*(-1./6.*pow(node->SWL[n0],2) + 1./3. * pow(node->eta[n0],2) + 1./6. * node->eta[n0] * node->SWL[n0]) * (node->gradu[n0] + node->gradv[n0 + NN]);

				Q21[0] -= tmp_coeff*b_nodes->normal[node->bindex[n0]][0];
				Q21[1] -= tmp_coeff*b_nodes->normal[node->bindex[n0]][1];

				tmp_coeff = 0.25*node->h[n0] * (node->gradhu[n0] + node->gradhv[n0 + NN]);

				Q41[0] -= tmp_coeff*b_nodes->normal[node->bindex[n0]][0];
				Q41[1] -= tmp_coeff*b_nodes->normal[node->bindex[n0]][1];

				// Terms for the gradient of uz atributed in the surface

				tmp_coeff = 0.5*node->eta[n0] * (node->gradu[n0] + node->gradv[n0 + NN]);

				Q22[0] -= tmp_coeff*b_nodes->normal[node->bindex[n0]][0];
				Q22[1] -= tmp_coeff*b_nodes->normal[node->bindex[n0]][1];

				tmp_coeff = 0.5 * ( node->gradhu[n0] + node->gradhv[n0 + NN]);

				Q32[0] -= tmp_coeff*b_nodes->normal[node->bindex[n0]][0];
				Q32[1] -= tmp_coeff*b_nodes->normal[node->bindex[n0]][1];
			}
		}
		//decomment if you want to do mass lamping
		node->us[n][0] = 1./node->volume[n] *(Q11[0] + Q21[0] + Q31[0] + Q41[0]) +node->u[n];
		node->us[n][1] = 1./node->volume[n] *(Q11[1] + Q21[1] + Q31[1] + Q41[1]) +node->v[n];

		node->uz[n][0] = 1./node->volume[n] *(Q12[0] + Q22[0] + Q32[0] );
		node->uz[n][1] = 1./node->volume[n] *(Q12[1] + Q22[1] + Q32[1] );
	}

	// apply the average
	average( node->us, 0, 4, NN, node );
	average( node->us, 1, 4, NN, node );
	average( node->uz, 0, 4, NN, node );
	average( node->uz, 1, 4, NN, node );
}
