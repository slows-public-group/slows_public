/*  Copyright (C) 2021   M.Ricchiuto  <mario.ricchiuto@inria.fr>
                         M. Kazolea   <maria.kazolea@inria.fr>
                         A. Filippini <a.filippini@brgm.fr>
                         L. Arpaia    <luca.arpaia@brgm.fr>
                         N.Pattakos   <pattakosn@fastmail.com> 

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.*/

    
#ifndef RHS_DISPERSIVE_TERMS_H
#define RHS_DISPERSIVE_TERMS_H
#include "common.h"
#include <stdlib.h>
void get_W( double GRAVITY, size_t NN, size_t NE, size_t NBF, size_t NNZ, node_s *node, element_s *element, boundary_nodes_s *b_nodes, model_t model_Tmat, mass_t SPDForm, double alpha);
void R_assembly(double GRAVITY, size_t NN, size_t NE, size_t NBF, model_t model_Tmat, node_s *node, element_s *element, boundary_nodes_s *b_nodes);
void RNCF_assembly( size_t NN, size_t NE, size_t NBF, node_s *node, element_s *element, boundary_nodes_s *b_nodes);
void get_f( size_t NN, size_t NBF, node_s *node, boundary_nodes_s *b_nodes );
void solve( double mu, size_t NN, size_t NBF,size_t NN_sparse, double alpha, node_s *node, model_t model_Tmat, mass_t choice,  boundary_nodes_s *b_nodes);
#endif
