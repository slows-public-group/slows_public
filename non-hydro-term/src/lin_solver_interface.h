/*  Copyright (C) 2021   M.Ricchiuto  <mario.ricchiuto@inria.fr>
                         M. Kazolea   <maria.kazolea@inria.fr>
                         A. Filippini <a.filippini@brgm.fr>
                         L. Arpaia    <luca.arpaia@brgm.fr>
                         N.Pattakos   <pattakosn@fastmail.com> 

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.*/


#pragma once
class lin_solver_interface
{
    public:
        virtual ~lin_solver_interface() {}
	// Pass a sparse matrix in COO format, initialize sparse solver and factorize
	virtual void init_factorize(int N, int nnz, int* ia, int* ja, double* A) = 0;
	// Solve with b as a RHS of the sparse system initialized and factorized with init_factorize
	virtual void solve(double *b, int = 0. ) = 0;
};
