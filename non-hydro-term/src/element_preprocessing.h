/*  Copyright (C) 2021   M.Ricchiuto  <mario.ricchiuto@inria.fr>
                         M. Kazolea   <maria.kazolea@inria.fr>
                         A. Filippini <a.filippini@brgm.fr>
                         L. Arpaia    <luca.arpaia@brgm.fr>
                         N.Pattakos   <pattakosn@fastmail.com> 

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.*/


#ifndef ELEMENT_PREPROCESSING_H
#define ELEMENT_PREPROCESSING_H
#ifdef __cplusplus
extern "C" {
#endif
void preprocessing( size_t NN, size_t NE, size_t NBF, size_t *NN_sparse, size_t *NP,
                    element_s *element,
                    edge_s *edge,
                    node_s *node,
                    boundary_s *boundary,
                    boundary_nodes_s *b_nodes );
#ifdef __cplusplus
}
#endif
#endif
