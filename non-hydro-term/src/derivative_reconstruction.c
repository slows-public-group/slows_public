/*  Copyright (C) 2021   M.Ricchiuto  <mario.ricchiuto@inria.fr>
                         M. Kazolea   <maria.kazolea@inria.fr>
                         A. Filippini <a.filippini@brgm.fr>
                         L. Arpaia    <luca.arpaia@brgm.fr>
                         N.Pattakos   <pattakosn@fastmail.com> 

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.*/


/***************************************************************************
                          derivative_recunstruction.h
                               --------
This file performs the reconstrunction of the auxiliar variables needed to discretize the
non-hydrostatic terms by means of P1 finite elements.
 ***************************************************************************/
#include <math.h>
#include "common.h"
#include <assert.h>
#include "derivative_reconstruction.h"

void grad_reconstruction( size_t NN, size_t NE, size_t NBF, double *cut_off,
                                 element_s *element,
                                 node_s *node,
                                 boundary_nodes_s *b_nodes,
				 double *vec, double *gradvec,
				 int Hindex) // Hindex = 1 if a correction to the gradient reconstruction is needed in dry fronts; Hindex = 0 elsewhere;
{
	double cut_off_H = cut_off[0];
	assert ( (fabs(Hindex) < 2) && "Hindex = 1 for gradH reconstruction, otherwise Hindex = 0" );

	for ( size_t n = 0; n < NN; n++ ) {
		node->flag[n] = 0;
		gradvec[n] = 0.;
		gradvec[n + NN] = 0.;
	}

	for ( size_t e = 0; e < NE; e++ ) {
		double max_Htot = -10.;
		int zero_nodes = 0;

		for ( int i = 0; i < VERTEX; i++ ) {
			int m = element->node[e][i];
			if ( node->h[m] > cut_off_H ) {
				if ( node->h[m] + node->bed[m] > max_Htot )
					max_Htot = node->h[m] + node->bed[m];
			} else {
				zero_nodes++;
			}
		}

		if ( 3 != zero_nodes ) {
			// we reconstruct the gradient of the quantity w on the element
			double dxw1 = 0., dyw2 = 0.;
			for ( int v = 0; v < VERTEX; v++ ) {
				double nx = element->normal[e][v][0];
				double ny = element->normal[e][v][1];
				int n = element->node[e][v];

				// Bathymetry correction for Well-Balancedness
				if (Hindex == 1){// Hindex = 1 if a correction to the gradient reconstruction is needed in dry fronts; Hindex = 0 elsewhere;
					double bathy;
					if ((node->h[n] < cut_off_H) && (node->bed[n] + cut_off_H >  max_Htot))
						bathy = max_Htot;
					else
						bathy = node->bed[n];
					dxw1 += (node->h[n] + bathy) * nx * 0.5 / element->volume[e];
					dyw2 += (node->h[n] + bathy) * ny * 0.5 / element->volume[e];
				} else {
					dxw1 += vec[n] * nx * 0.5 / element->volume[e];
					dyw2 += vec[n] * ny * 0.5 / element->volume[e];
				}
			}

			for ( int v = 0; v < VERTEX; v++ ) {
				int n = element->node[e][v];
				double kdt = 1. / node->volume[n];
				gradvec[n] += kdt * element->volume[e] / 3. * dxw1;
				gradvec[n + NN] += kdt * element->volume[e] / 3. * dyw2;
			}
		}
	}

	for ( size_t f = 0; f < NBF; f++ ) {
		int n0 = b_nodes->node[f];
                if ( b_nodes->type[f] == 3 ) {//Correction for wall b.c.
                        double length = sqrt( pow( b_nodes->normal[f][0], 2) + pow( b_nodes->normal[f][1], 2 ) );

                        {
                        double t1 = (gradvec[n0] * b_nodes->normal[f][0] + gradvec[n0 + NN] * b_nodes->normal[f][1]) * b_nodes->normal[f][0] / pow(length, 2);
//#pragma omp atomic 
                        gradvec[n0] -= t1;
                        }

                        {
                        double t1 = (gradvec[n0] * b_nodes->normal[f][0] + gradvec[n0 + NN] * b_nodes->normal[f][1]) * b_nodes->normal[f][1] / pow(length, 2);
//#pragma omp atomic
                        gradvec[n0 + NN] -= t1;
                        }

                } else if ( b_nodes->type[f] == 10 ) {//Correction for periodic b.c.
			if ( node->flag[n0] == 0 ) {
				int mate = node->mate[n0];
				gradvec[n0] += gradvec[mate];
				gradvec[mate] = gradvec[n0];
				gradvec[n0 + NN] += gradvec[mate + NN];
				gradvec[mate + NN] = gradvec[n0 + NN];
				node->flag[mate] = 1;
				node->flag[n0] = 1;
			}
		}
	}
}

void bed_reconstruction( size_t NN, size_t NE, size_t NBF, element_s *element, node_s *node,boundary_nodes_s *b_nodes)
{
	for ( size_t n = 0; n < NN; n++ ) {
		node->flag[n] = 0;
		node->dxbed[n] = 0.;
		node->dybed[n] = 0.;
	}

	// we reconstruct the gradient of the quantity bed
	for ( size_t e = 0; e < NE; e++ ) {
		double dxw1 = 0., dyw2 = 0.;
		for ( int v = 0; v < VERTEX; v++ ) {
			double nx = element->normal[e][v][0];
			double ny = element->normal[e][v][1];
			int n = element->node[e][v];
			dxw1 += node->bed[n] * nx * 0.5 / element->volume[e];
			dyw2 += node->bed[n] * ny * 0.5 / element->volume[e];
		}

		for ( int v = 0; v < VERTEX; v++ ) {
			int n = element->node[e][v];
			double kdt = 1. / node->volume[n];
			node->dxbed[n] += kdt * element->volume[e] / 3. * dxw1;
			node->dybed[n] += kdt * element->volume[e] / 3. * dyw2;
		}
	}

	for ( size_t f = 0; f < NBF; f++ ) {
		int n0 = b_nodes->node[f];
               if ( b_nodes->type[f] == 3 ) {//Correction for wall b.c.
                        double length = sqrt( pow( b_nodes->normal[f][0], 2 ) + pow( b_nodes->normal[f][1], 2 ) );
                        node->dxbed[n0] -= ( node->dxbed[n0] * b_nodes->normal[f][0] + node->dybed[n0] * b_nodes->normal[f][1] ) * b_nodes->normal[f][0] / pow( length, 2 );
                        node->dybed[n0] -= ( node->dxbed[n0] * b_nodes->normal[f][0] + node->dybed[n0] * b_nodes->normal[f][1]) * b_nodes->normal[f][1] / pow( length, 2 );
                }else if ( b_nodes->type[f] == 10 ) {//Correction for periodic b.c.
			if ( node->flag[n0] == 0 ) {
				int mate = node->mate[n0];
				node->dxbed[n0] += node->dxbed[mate];
				node->dxbed[mate] = node->dxbed[n0];
				node->dybed[n0] += node->dybed[mate];
				node->dybed[mate] = node->dybed[n0];
				node->flag[mate] = 1;
				node->flag[n0] = 1;
			}
		}
	}
}
