/*  Copyright (C) 2021   M.Ricchiuto  <mario.ricchiuto@inria.fr>
                         M. Kazolea   <maria.kazolea@inria.fr>
                         A. Filippini <a.filippini@brgm.fr>
                         L. Arpaia    <luca.arpaia@brgm.fr>
                         N.Pattakos   <pattakosn@fastmail.com> 

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.*/


/***************************************************************************
                                  rhs_dispersive_terms.c
                                   -----
    This file contains the subroutines that compute the right hand side dispersive terms of the elliptic part
 This is the subroutine which solves the linear system (I+\alpha*T)\phi = W-R
 ***************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "common.h"
#include "my_malloc.h"
#include "rhs_dispersive_terms.h"
#include "time.h"
#include "lin_solver_interface.h"
#if defined(WITH_SPARSE_MKL) || defined(WITH_IMKL)
#include "mkl.h"
#endif

//clock_t start,end;
//double cpu_time_used;

extern double *GMM, *HGMM, *T, ***INTM, **INTM3, **INTM4;
extern int *IT, *JT;
extern lin_solver_interface* T_mkl;
extern lin_solver_interface* Ttemp_mkl;   //id_solve
extern lin_solver_interface* Ttemp_f_mkl; //id_f
extern lin_solver_interface* gmm_matrix;  //id_w

extern int *IGMM, *JGMM;

static double *tmpin = NULL;
static double *Ttemp = NULL;
static double *tmp = NULL;
static double **grad_eta = NULL;
static double **grad_dxeta = NULL;
static double **grad_dyeta = NULL;
static double **grad_h = NULL;
static double **grad_h2mSWL2 = NULL;
static double **grad_u = NULL;
static double **grad_v = NULL;
static double **grad_b = NULL;
static double **grad_dxb = NULL;
static double **grad_dyb = NULL;
static double **grad_fx = NULL;
static double **grad_fy = NULL;
static double ***grad_phi = NULL;

#ifdef WITH_MUMPS
// subroutine that computes the dot product of a matrix and a vector in sparse form COO
static void matrixdotvec_coo(int n, int nnz, double *A, int *A_i, int *A_j, double *B, double *C)
{
#pragma omp parallel for
	for (int i = 0; i < n; ++i)
		C[i] = 0.0;
#pragma omp parallel for
	for (int i = 0; i < nnz; ++i)
		C[A_i[i] - 1] += A[i] * B[A_j[i] - 1];
}
#endif

void get_W( double GRAVITY, size_t NN, size_t NE, size_t NBF, size_t NNZ, node_s *node, element_s *element, boundary_nodes_s *b_nodes, model_t model_Tmat, mass_t SPDForm, double alpha)
{
	if ( !tmpin )
		tmpin = (double*)MA_vector( 2 * NN, sizeof(double) );

#pragma omp parallel for
	for ( size_t n = 0; n < 2 * NN; n++ ) {
		tmpin[n] = 0.;
		node->W[n] = 0.;
	}

	// We construct the g*h*grad(eta):
	// 1] using mass lumping :
//	for ( size_t n = 0; n < NN; n++ ) {
//		tmpin[n] = GRAVITY * node->gradeta[n];
//		tmpin[n+NN] = GRAVITY * node->gradeta[n + NN];
//	}
//	if ( model_Tmat != MODEL_NCF ) {
//		for ( size_t n = 0; n < NN; n++ ) {
//			tmpin[n] *= node->h[n];
//			tmpin[n+NN] *= node->h[n];
//		}
//	}

	// 2] use the mass matrix inversion :
#pragma omp parallel for
	for ( size_t n = 0; n < NN; n++ ) {
		// multiply by node[n].volume because we want to use the integral value of the gradient while node[n].dxeta is already divided by node[n].volume
//		tmpin[n] = GRAVITY * node->gradeta[n] * node->volume[n];
//		tmpin[n + NN] = GRAVITY * node->gradeta[n + NN] * node->volume[n];
//When you solve for psi:
		tmpin[n] = -(node->h[n]*GRAVITY * node->gradeta[n])/alpha;
		tmpin[n + NN] = -(node->h[n]*GRAVITY * node->gradeta[n + NN])/alpha;
	}
	if ( model_Tmat != MODEL_NCF ) {
	//if ( model_Tmat == MODEL_FULL && SPDForm == MASS_GALERKIN ) {
#pragma omp parallel for
		for ( size_t n = 0; n < NN; n++ ) {
			tmpin[n] *= node->h[n];
			tmpin[n + NN] *= node->h[n];
		}
	}
//
	// Boundary correction only valid for square domains!!!!!
#pragma omp parallel for
	for (size_t f = 0; f < NBF; f++)
		if (3 == b_nodes->type[f]) {
			size_t n = b_nodes->node[f];
			double length = sqrt(b_nodes->normal[f][0]*b_nodes->normal[f][0] + b_nodes->normal[f][1]*b_nodes->normal[f][1]);
			auto tmp_result = fabs( b_nodes->normal[f][1]/length ) ;
#pragma omp atomic
			tmpin[n] *= tmp_result;
			auto tmp_result2 = fabs( b_nodes->normal[f][0]/length );
#pragma omp atomic
			tmpin[n+NN] *= tmp_result2;
		}

//when you solve for phi
//	gmm_matrix->solve(tmpin, 2*NN);

	// We multiply by the matrix Tdc :
//#ifdef WITH_MUMPS
//	matrixdotvec_coo( 2 * NN, NNZ, T, IT, JT, tmpin, node->W );
//#endif
//#if defined(WITH_SPARSE_MKL) || defined(WITH_IMKL)
//	const char transa = 'N';
//	//void mkl_dcsrgemv( const char *transa, const MKL_INT *m , const double *a , const MKL_INT *ia , const MKL_INT *ja , const double *x , double *y );
//	//mkl_dcsrgemv( &transa, 2*NN, T, IT, JT, tmpin, node->W );
//	const MKL_INT n = 2*NN;
//	const MKL_INT nnz = NNZ;
//	mkl_dcoogemv( &transa, &n, T, IT, JT, &nnz, tmpin, node->W );
//#endif


//when you solve for psi

	    for ( size_t e = 0; e < NE; e++) {
                for (int i = 0; i < VERTEX; i++) {
                        int ni = element->node[e][i];
                        double INT = 0.;
                        double sumx = 0., sumy=0; ;
                        for (int j = 0; j < VERTEX; j++) {
                                int nj = element->node[e][j];
                                for (int k = 0; k < VERTEX; k++) {
                                        int nk = element->node[e][k];

                                        INT = INTM3[e][9*i+3*j+k];//element->volume[e]*int_F<quadrature_order::FOURTH>(i, j, k);
                                        sumx -= GRAVITY*INT*node->h[nj]*node->gradeta[nk]/alpha;
                                        sumy -= GRAVITY*INT*node->h[nj]*node->gradeta[nk+NN]/alpha;
                                }

                        }

                        double total_sum = sumx;
#pragma omp atomic update
                        node->W[ni] += total_sum;

                        total_sum = sumy ;
#pragma omp atomic update
                        node->W[ni+NN] += total_sum;

                }
	    }
}

// subroutine that computes the auxiliar variable f needed to compute R
void get_f( size_t NN, size_t NBF, node_s *node, boundary_nodes_s *b_nodes)
{
	// construction of the right hand side of the linear system
	// We construct the h*grad(\eta) as to multiply by the matrix Tdc
#pragma omp parallel for
	for ( size_t n = 0; n < NN; n++) {
		node->f[n] = node->h[n] * node->gradeta[n] * node->volume[n];
		node->f[n + NN] = node->h[n] * node->gradeta[n + NN] * node->volume[n];
		if ( node->bflag[n] == 3 ) {
			node->f[n] = 0.;
			node->f[n+NN] = 0.;
		}
	}
//	for (size_t f = 0; f < NBF; f++){
//		if (3 == b_nodes->type[f]) {
//			size_t n = b_nodes->node[f];
//			double length = sqrt(b_nodes->normal[f][0]*b_nodes->normal[f][0] + b_nodes->normal[f][1]*b_nodes->normal[f][1]);
//			double tmpf1 =node->f[n];
//			double tmpf2 =node->f[n+NN];
//			node->f[n] = (tmpf1*b_nodes->normal[f][0] + tmpf2*b_nodes->normal[f][1])/length  ;
//			node->f[n+NN] = (-tmpf1*b_nodes->normal[f][1] + tmpf2*b_nodes->normal[f][0])/length  ;
//		}
//	}
	Ttemp_f_mkl->solve(node->f, 2*NN);
}


/* This function assembles the term R of the elliptic equation (I + alpha T)* PHI = RHS*/
void R_assembly(double GRAVITY, size_t NN, size_t NE, size_t NBF, model_t model_Tmat, node_s *node, element_s *element, boundary_nodes_s *b_nodes)
{
	if ( !tmp )
		tmp = (double*)MA_vector( NE, sizeof(double) );
	if ( !grad_u )
		grad_u = (double**)MA_matrix(NE,2, sizeof(double) );
	if ( !grad_v )
		grad_v = (double**)MA_matrix(NE,2, sizeof(double) );
	if ( !grad_b )
		grad_b = (double**)MA_matrix(NE,2, sizeof(double) );
	if ( !grad_dxb )
		grad_dxb = (double**)MA_matrix(NE,2, sizeof(double) );
	if ( !grad_dyb )
		grad_dyb = (double**)MA_matrix(NE,2, sizeof(double) );
	if ( !grad_phi )
		grad_phi = (double***)MA_tensor(NE,3,2, sizeof(double) );

#pragma omp parallel for
	for ( size_t n = 0; n < NN; n++) {
		node->R[n] = 0.;
		node->R[n+NN] = 0.;
	}


#pragma omp parallel for
	for ( size_t e = 0; e < NE; e++) {
		tmp[e] = 0.;
		grad_u[e][0] = 0.;
		grad_u[e][1] = 0.;
		grad_v[e][0] = 0.;
		grad_v[e][1] = 0.;
		grad_b[e][0] = 0.;
		grad_b[e][1] = 0.;
		grad_dxb[e][0] = 0.;
		grad_dxb[e][1] = 0.;
		grad_dyb[e][0] = 0.;
		grad_dyb[e][1] = 0.;
		for (int i = 0; i < VERTEX; i++) {
			grad_phi[e][i][0] = 0.;
			grad_phi[e][i][1] = 0.;
		}
	}
#pragma omp parallel for
	for ( size_t e = 0; e < NE; e++) {
		double max_Htot = 0.0;
		for (int i = 0; i < VERTEX; i++) {
			int n = element->node[e][i];
			if (node->h[n]>1.e-4)
				if ((node->h[n] + node->bed[n] + 1.e-4 ) > max_Htot)
					max_Htot = node->h[n]+ node->bed[n];
		}
	
		for (int i = 0; i < VERTEX; i++) {
			int n = element->node[e][i];

			double bathy;
			// Correction on shoreline: WB rec.      
			if ((node->h[n]<1.e-4) && ((node->bed[n] + 1.e-4 ) > max_Htot))
				bathy = max_Htot;
			else
				bathy = node->bed[n];

			//grad_b[e][0] += 1./(2*element->volume[e])*bathy*element->normal[e][i][0];
			//grad_b[e][1] += 1./(2*element->volume[e])*bathy*element->normal[e][i][1];

			grad_u[e][0] += 1./(2*element->volume[e])*node->u[n]*element->normal[e][i][0];
			grad_u[e][1] += 1./(2*element->volume[e])*node->u[n]*element->normal[e][i][1];
			grad_v[e][0] += 1./(2*element->volume[e])*node->v[n]*element->normal[e][i][0];
			grad_v[e][1] += 1./(2*element->volume[e])*node->v[n]*element->normal[e][i][1];
			//grad_b[e][0] += 1./(2*element->volume[e])*node->bed[n]*element->normal[e][i][0];
			//grad_b[e][1] += 1./(2*element->volume[e])*node->bed[n]*element->normal[e][i][1];
			grad_b[e][0] += node->dxbed[n]/3.0;
			grad_b[e][1] += node->dybed[n]/3.0;
			grad_dxb[e][0] += 1./(2*element->volume[e])*node->dxbed[n]*element->normal[e][i][0];
			grad_dxb[e][1] += 1./(2*element->volume[e])*node->dxbed[n]*element->normal[e][i][1];
			grad_dyb[e][0] += 1./(2*element->volume[e])*node->dybed[n]*element->normal[e][i][0];
			grad_dyb[e][1] += 1./(2*element->volume[e])*node->dybed[n]*element->normal[e][i][1];
			grad_phi[e][i][0] = 1./(2*element->volume[e])*element->normal[e][i][0];
			grad_phi[e][i][1] = 1./(2*element->volume[e])*element->normal[e][i][1];
		}
		tmp[e] = -grad_u[e][0]*grad_v[e][1] + grad_u[e][1]*grad_v[e][0];
		tmp[e] += pow(grad_u[e][0] + grad_v[e][1],2);
	}

#pragma omp parallel for
	for ( size_t e = 0; e < NE; e++) {
		for (int i = 0; i < VERTEX; i++) {
			int ni = element->node[e][i];
			double coeff_x = -2./3.*grad_phi[e][i][0]*tmp[e];
			double coeff_y = -2./3.*grad_phi[e][i][1]*tmp[e];
			double coeff2_x = grad_b[e][0]*tmp[e];
			double coeff2_y = grad_b[e][1]*tmp[e];
			double coeff3a_x = -1./2.*grad_phi[e][i][0]*grad_dxb[e][0];
			double coeff3a_y = -1./2.*grad_phi[e][i][1]*grad_dxb[e][0];
			double coeff3b_x = -1./2.*grad_phi[e][i][0]*grad_dyb[e][1];
			double coeff3b_y = -1./2.*grad_phi[e][i][1]*grad_dyb[e][1];
			double coeff3c_x = -grad_phi[e][i][0]*grad_dxb[e][1];
			double coeff3c_y = -grad_phi[e][i][1]*grad_dxb[e][1];
			double coeff4a_x = grad_b[e][0]*grad_dxb[e][0];
			double coeff4a_y = grad_b[e][1]*grad_dxb[e][0];
			double coeff4b_x = grad_b[e][0]*grad_dyb[e][1];
			double coeff4b_y = grad_b[e][1]*grad_dyb[e][1];
			double coeff4c_x = 2.*grad_b[e][0]*grad_dxb[e][1];
			double coeff4c_y = 2.*grad_b[e][1]*grad_dxb[e][1];

			double INT = 0.;
			double sum = 0., sum2 = 0., sum3a = 0., sum3b = 0., sum3c = 0.;
			double sum4a = 0., sum4b = 0., sum4c = 0.;
			for (int j = 0; j < VERTEX; j++) {
				int nj = element->node[e][j];
				for (int k = 0; k < VERTEX; k++) {
					int nk = element->node[e][k];
					for (int l = 0; l < VERTEX; l++) {
						int nl = element->node[e][l];
						for (int m = 0; m < VERTEX; m++) {
							int nm = element->node[e][m];
							INT = INTM4[e][27*j+9*k +3*l +m];//element->volume[e]*int_F<quadrature_order::FOURTH>(j, k, l, m );
							sum3a += INT*node->h[nj]*node->h[nk]*node->u[nl]*node->u[nm];
							sum3b += INT*node->h[nj]*node->h[nk]*node->v[nl]*node->v[nm];
							sum3c += INT*node->h[nj]*node->h[nk]*node->u[nl]*node->v[nm];
						}
						INT = INTM4[e][27*i+9*j +3*k +l];//element->volume[e]*int_F<quadrature_order::FOURTH>(i, j, k, l );
						sum4a += INT*node->h[nj]*node->u[nk]*node->u[nl];
						sum4b += INT*node->h[nj]*node->v[nk]*node->v[nl];
						sum4c += INT*node->h[nj]*node->u[nk]*node->v[nl];

						INT =INTM3[e][9*j+3*k+l];//element->volume[e]*int_F<quadrature_order::FOURTH>(j, k, l );
						sum += INT*node->h[nj]*node->h[nk]*node->h[nl];
					}
					INT = INTM3[e][9*i+3*j+k];//element->volume[e]*int_F<quadrature_order::FOURTH>(i, j, k);
					sum2 += INT*node->h[nj]*node->h[nk];
				}

			}

			double total_sum = coeff_x*sum + coeff2_x*sum2 + coeff3a_x*sum3a + coeff3b_x*sum3b + coeff3c_x*sum3c + coeff4a_x*sum4a + coeff4b_x*sum4b + coeff4c_x*sum4c;
#pragma omp atomic update
			node->R[ni] += total_sum;

			/*node->R[ni] += coeff_x*sum;
			node->R[ni] += coeff2_x*sum2;
			node->R[ni] += coeff3a_x*sum3a + coeff3b_x*sum3b + coeff3c_x*sum3c;
			node->R[ni] += coeff4a_x*sum4a + coeff4b_x*sum4b + coeff4c_x*sum4c;*/


			total_sum = coeff_y*sum + coeff2_y*sum2 + coeff3a_y*sum3a + coeff3b_y*sum3b + coeff3c_y*sum3c + coeff4a_y*sum4a + coeff4b_y*sum4b + coeff4c_y*sum4c;
#pragma omp atomic update
			node->R[ni+NN] += total_sum;

			/*node->R[ni+NN] += coeff_y*sum;
			node->R[ni+NN] += coeff2_y*sum2;
			node->R[ni+NN] += coeff3a_y*sum3a + coeff3b_y*sum3b + coeff3c_y*sum3c;
			node->R[ni+NN] += coeff4a_y*sum4a + coeff4b_y*sum4b + coeff4c_y*sum4c; */
		}
	}

	if (model_Tmat == MODEL_DIAG){
		if ( !grad_h )
			grad_h = (double**)MA_matrix(NE,2, sizeof(double) );
		if ( !grad_eta )
			grad_eta = (double**)MA_matrix(NE,2, sizeof(double) );
		if ( !grad_dxeta )
			grad_dxeta = (double**)MA_matrix(NE,2, sizeof(double) );
		if ( !grad_dyeta )
			grad_dyeta = (double**)MA_matrix(NE,2, sizeof(double) );
		if ( !grad_fx )
			grad_fx = (double**)MA_matrix(NE,2, sizeof(double) );
		if ( !grad_fy )
			grad_fy = (double**)MA_matrix(NE,2, sizeof(double) );
		if ( !grad_h2mSWL2 )
			grad_h2mSWL2 = (double**)MA_matrix(NE,2, sizeof(double) );
		for ( size_t e = 0; e < NE; e++) {
			grad_h[e][0] = 0.;
			grad_h[e][1] = 0.;
			grad_eta[e][0] = 0.;
			grad_eta[e][1] = 0.;
			grad_dxeta[e][0] = 0.;
			grad_dxeta[e][1] = 0.;
			grad_dyeta[e][0] = 0.;
			grad_dyeta[e][1] = 0.;
			grad_fx[e][0] = 0.;
			grad_fx[e][1] = 0.;
			grad_fy[e][0] = 0.;
			grad_fy[e][1] = 0.;
			grad_h2mSWL2[e][0] = 0.;
			grad_h2mSWL2[e][1] = 0.;
		}
#pragma omp parallel for
		for ( size_t e = 0; e < NE; e++) {
			for (int i = 0; i < VERTEX; i++) {
				int n = element->node[e][i];
				grad_h[e][0] += 1./(2*element->volume[e])*node->h[n]*element->normal[e][i][0];
				grad_h[e][1] += 1./(2*element->volume[e])*node->h[n]*element->normal[e][i][1];
				grad_eta[e][0] += 1./(2*element->volume[e])*node->eta[n]*element->normal[e][i][0];
				grad_eta[e][1] += 1./(2*element->volume[e])*node->eta[n]*element->normal[e][i][1];
				grad_dxeta[e][0] += 1./(2*element->volume[e])*node->gradeta[n]*element->normal[e][i][0];
				grad_dxeta[e][1] += 1./(2*element->volume[e])*node->gradeta[n]*element->normal[e][i][1];
				grad_dyeta[e][0] += 1./(2*element->volume[e])*node->gradeta[n+NN]*element->normal[e][i][0];
				grad_dyeta[e][1] += 1./(2*element->volume[e])*node->gradeta[n+NN]*element->normal[e][i][1];
				grad_fx[e][0] += 1./(2*element->volume[e])*node->f[n]*element->normal[e][i][0];
				grad_fx[e][1] += 1./(2*element->volume[e])*node->f[n]*element->normal[e][i][1];
				grad_fy[e][0] += 1./(2*element->volume[e])*node->f[n+NN]*element->normal[e][i][0];
				grad_fy[e][1] += 1./(2*element->volume[e])*node->f[n+NN]*element->normal[e][i][1];
				grad_h2mSWL2[e][0] += 1./(2*element->volume[e])*( pow(node->h[n],2) - pow(node->SWL[n],2) )*element->normal[e][i][0];
				grad_h2mSWL2[e][1] += 1./(2*element->volume[e])*( pow(node->h[n],2) - pow(node->SWL[n],2) )*element->normal[e][i][1];
			}
			tmp[e] = grad_b[e][0]*grad_eta[e][0] + grad_b[e][1]*grad_eta[e][1];
		}

#pragma omp parallel for
		for ( size_t e = 0; e < NE; e++) {
			for (int i = 0; i < VERTEX; i++) {
				int ni = element->node[e][i];
				double coeff5_x = GRAVITY*(-grad_h[e][1]*grad_dxeta[e][1] + grad_h[e][0]*grad_dyeta[e][1]);
				double coeff5_y = GRAVITY*(grad_h[e][1]*grad_dxeta[e][0] - grad_h[e][0]*grad_dyeta[e][0]);
				double coeff6_x = GRAVITY/2.*grad_phi[e][i][0]*tmp[e];
				double coeff6_y = GRAVITY/2.*grad_phi[e][i][1]*tmp[e];
				double coeff7_x = GRAVITY/2.*(grad_dxeta[e][0] + grad_dyeta[e][1])*grad_b[e][0];
				double coeff7_y = GRAVITY/2.*(grad_dxeta[e][0] + grad_dyeta[e][1])*grad_b[e][1];
				double coeff8_x = -GRAVITY*grad_b[e][0]*tmp[e];
				double coeff8_y = -GRAVITY*grad_b[e][1]*tmp[e];
				double coeff9_x = -GRAVITY/3.*(grad_phi[e][i][0]*grad_fx[e][0] + grad_phi[e][i][1]*grad_fx[e][1]);
				double coeff9_y = -GRAVITY/3.*(grad_phi[e][i][0]*grad_fy[e][0] + grad_phi[e][i][1]*grad_fy[e][1]);
				double coeff10 = GRAVITY*element->volume[e]/18.*(grad_phi[e][i][0]*grad_h2mSWL2[e][0] + grad_phi[e][i][1]*grad_h2mSWL2[e][1]);

				double INT = 0.;
				double sum57 = 0., sum6 = 0., sum8 = 0., sum9 = 0.;
				double sum10_x = 0., sum10_y = 0.;
				for (int j = 0; j < VERTEX; j++) {
					int nj = element->node[e][j];
					for (int k = 0; k < VERTEX; k++) {
						int nk = element->node[e][k];
						INT = INTM3[e][9*i+3*j+k];//element->volume[e]*int_F<quadrature_order::FOURTH>(i, j, k );
						sum57 += INT*node->h[nj]*node->h[nk];

						INT = INTM[e][j][k];//element->volume[e]*int_F<quadrature_order::FOURTH>(j, k );
						sum6 += INT*node->h[nj]*node->h[nk];
						sum9 += INT*(node->h[nj]*node->h[nk] - node->SWL[nj]*node->SWL[nk]);
					}
					INT = INTM[e][i][j];//element->volume[e]*int_F<quadrature_order::FOURTH>(i, j );
					sum8 += INT*node->h[nj];

					sum10_x += node->f[nj];
					sum10_y += node->f[nj+NN];
				}

				auto tmp_result = (coeff5_x + coeff7_x)*sum57 + coeff6_x*sum6 + coeff8_x*sum8 + coeff9_x*sum9 + coeff10*sum10_x;
#pragma omp atomic
				node->R[ni] += tmp_result;

				//double total_sum = (coeff5_x + coeff7_x)*sum57 +coeff6_x*sum6 + coeff8_x*sum8 + coeff9_x*sum9 +coeff10*sum10_x;
				//node->R[ni] += total_sum;

				auto tmp_result2 = (coeff5_y + coeff7_y)*sum57 + coeff6_y*sum6 + coeff8_y*sum8 + coeff9_y*sum9 + coeff10*sum10_y;
#pragma omp atomic
				node->R[ni+NN] += tmp_result2;

				//total_sum = (coeff5_y + coeff7_y)*sum57 + coeff6_y*sum6 +coeff8_y*sum8 + coeff9_y*sum9 + coeff10*sum10_y;
				//node->R[ni+NN] += total_sum;
			}
		}
	}

	// Periodic boundary conditions:
#pragma omp parallel for
	for( size_t n = 0 ; n < NN ; n++ ){
		node->flag[n] = 0 ;
	}

	for ( size_t f = 0; f < NBF; f++)
		if (b_nodes->type[f]==10){
			int n = b_nodes->node[f];
			if ( node->flag[n] == 0 ) {
				int m = node->mate[n];
				node->R[n] += node->R[m];
				node->R[n+NN] += node->R[m+NN];
				node->R[m] = node->R[n];
				node->R[m+NN] = node->R[n+NN];
				node->flag[m] = 1;
				node->flag[n] = 1;
			}
		}

}

void RNCF_assembly( size_t NN, size_t NE, size_t NBF, node_s *node, element_s *element, boundary_nodes_s *b_nodes)
{
	if ( !tmp )
		tmp = (double*)MA_vector(NE, sizeof(double) );
	if ( !grad_h )
		grad_h = (double**)MA_matrix(NE,2, sizeof(double) );
	if ( !grad_u )
		grad_u = (double**)MA_matrix(NE,2, sizeof(double) );
	if ( !grad_v )
		grad_v = (double**)MA_matrix(NE,2, sizeof(double) );
	if ( !grad_b )
		grad_b = (double**)MA_matrix(NE,2, sizeof(double) );
	if ( !grad_dxb )
		grad_dxb = (double**)MA_matrix(NE,2, sizeof(double) );
	if ( !grad_dyb )
		grad_dyb = (double**)MA_matrix(NE,2, sizeof(double) );
	if ( !grad_phi )
		grad_phi = (double***)MA_tensor(NE,3,2, sizeof(double) );

	for ( size_t n = 0; n < NN; n++) {
		node->R[n] = 0.;
		node->R[n+NN] = 0.;
	}

	for ( size_t e = 0; e < NE; e++) {
		tmp[e] = 0.;
		grad_h[e][0] = 0.;
		grad_h[e][1] = 0.;
		grad_u[e][0] = 0.;
		grad_u[e][1] = 0.;
		grad_v[e][0] = 0.;
		grad_v[e][1] = 0.;
		grad_b[e][0] = 0.;
		grad_b[e][1] = 0.;
		grad_dxb[e][0] = 0.;
		grad_dxb[e][1] = 0.;
		grad_dyb[e][0] = 0.;
		grad_dyb[e][1] = 0.;
		for (int i = 0; i < VERTEX; i++) {
			grad_phi[e][i][0] = 0.;
			grad_phi[e][i][1] = 0.;
		}
	}

	for ( size_t e = 0; e < NE; e++) {
		for (int i = 0; i < VERTEX; i++) {
			int n = element->node[e][i];
			grad_h[e][0] += 1./(2*element->volume[e])*node->h[n]*element->normal[e][i][0];
			grad_h[e][1] += 1./(2*element->volume[e])*node->h[n]*element->normal[e][i][1];
			grad_u[e][0] += 1./(2*element->volume[e])*node->u[n]*element->normal[e][i][0];
			grad_u[e][1] += 1./(2*element->volume[e])*node->u[n]*element->normal[e][i][1];
			grad_v[e][0] += 1./(2*element->volume[e])*node->v[n]*element->normal[e][i][0];
			grad_v[e][1] += 1./(2*element->volume[e])*node->v[n]*element->normal[e][i][1];
			grad_b[e][0] += 1./(2*element->volume[e])*node->bed[n]*element->normal[e][i][0];
			grad_b[e][1] += 1./(2*element->volume[e])*node->bed[n]*element->normal[e][i][1];
			grad_dxb[e][0] += 1./(2*element->volume[e])*node->dxbed[n]*element->normal[e][i][0];
			grad_dxb[e][1] += 1./(2*element->volume[e])*node->dxbed[n]*element->normal[e][i][1];
			grad_dyb[e][0] += 1./(2*element->volume[e])*node->dybed[n]*element->normal[e][i][0];
			grad_dyb[e][1] += 1./(2*element->volume[e])*node->dybed[n]*element->normal[e][i][1];
			grad_phi[e][i][0] = 1./(2*element->volume[e])*element->normal[e][i][0];
			grad_phi[e][i][1] = 1./(2*element->volume[e])*element->normal[e][i][1];
		}
		tmp[e] = -grad_u[e][0]*grad_v[e][1] + grad_u[e][1]*grad_v[e][0];
		tmp[e] += pow(grad_u[e][0] + grad_v[e][1],2);
	}

	for ( size_t e = 0; e < NE; e++) {
		for (int i = 0; i < VERTEX; i++) {
			int ni = element->node[e][i];
			double coeff_x = -2./3.*grad_phi[e][i][0]*tmp[e];
			double coeff_y = -2./3.*grad_phi[e][i][1]*tmp[e];
			double coeff23_x = (2./3.*grad_h[e][0] + grad_b[e][0])*tmp[e];
			double coeff23_y = (2./3.*grad_h[e][0] + grad_b[e][1])*tmp[e];
			double coeff4a_x = -1./2.*grad_phi[e][i][0]*grad_dxb[e][0];
			double coeff4a_y = -1./2.*grad_phi[e][i][1]*grad_dxb[e][0];
			double coeff4b_x = -1./2.*grad_phi[e][i][0]*grad_dyb[e][1];
			double coeff4b_y = -1./2.*grad_phi[e][i][1]*grad_dyb[e][1];
			double coeff4c_x = -grad_phi[e][i][0]*grad_dxb[e][1];
			double coeff4c_y = -grad_phi[e][i][1]*grad_dxb[e][1];
			double coeff56a_x = (1./2.*grad_h[e][0] + grad_b[e][0])*grad_dxb[e][0];
			double coeff56a_y = (1./2.*grad_h[e][1] + grad_b[e][1])*grad_dxb[e][0];
			double coeff56b_x = (1./2.*grad_h[e][0] + grad_b[e][0])*grad_dyb[e][1];
			double coeff56b_y = (1./2.*grad_h[e][1] + grad_b[e][1])*grad_dyb[e][1];
			double coeff56c_x = (grad_h[e][0] + 2.*grad_b[e][0])*grad_dxb[e][1];
			double coeff56c_y = (grad_h[e][1] + 2.*grad_b[e][1])*grad_dxb[e][1];

			double INT = 0.;
			double sum = 0., sum23 = 0., sum4a = 0., sum4b = 0., sum4c = 0.;
			double sum56a = 0., sum56b = 0., sum56c = 0.;
			for (int j = 0; j < VERTEX; j++) {
				int nj = element->node[e][j];
				for (int k = 0; k < VERTEX; k++) {
					int nk = element->node[e][k];
					for (int l = 0; l < VERTEX; l++) {
						int nl = element->node[e][l];
						INT = INTM3[e][9*j+3*k+l];//element->volume[e]*int_F<quadrature_order::FOURTH>(j, k, l );
						sum4a += INT*node->h[nj]*node->u[nk]*node->u[nl];
						sum4b += INT*node->h[nj]*node->v[nk]*node->v[nl];
						sum4c += INT*node->h[nj]*node->u[nk]*node->v[nl];
					}
					INT = INTM[e][j][k];//element->volume[e]*int_F<quadrature_order::FOURTH>(j, k );
					sum += INT*node->h[nj]*node->h[nk];

					INT = INTM3[e][9*i+3*j+k];//element->volume[e]*int_F<quadrature_order::FOURTH>(i, j, k );
					sum56a += INT*node->u[nj]*node->u[nk];
					sum56b += INT*node->v[nj]*node->v[nk];
					sum56c += INT*node->u[nj]*node->v[nk];
				}
				INT = INTM[e][i][j];//element->volume[e]*int_F<quadrature_order::FOURTH>(i, j );
				sum23 += INT*node->h[nj];

			}
			node->R[ni] += coeff_x*sum;
			node->R[ni] += coeff23_x*sum23;
			node->R[ni] += coeff4a_x*sum4a + coeff4b_x*sum4b + coeff4c_x*sum4c;
			node->R[ni] += coeff56a_x*sum56a + coeff56b_x*sum56b + coeff56c_x*sum56c;

			node->R[ni+NN] += coeff_y*sum;
			node->R[ni+NN] += coeff23_y*sum23;
			node->R[ni+NN] += coeff4a_y*sum4a + coeff4b_y*sum4b + coeff4c_y*sum4c;
			node->R[ni+NN] += coeff56a_y*sum56a + coeff56b_y*sum56b + coeff56c_y*sum56c;
		}
	}

	// Periodic boundary conditions:
	for( size_t n = 0 ; n < NN ; n++ )
		node->flag[n] = 0 ;

	for ( size_t f = 0; f < NBF; f++)
		if (b_nodes->type[f]==10){
			int n = b_nodes->node[f];
			if ( node->flag[n] == 0 ) {
				int m = node->mate[n];
				node->R[n] += node->R[m];
				node->R[n+NN] += node->R[m+NN];
				node->R[m] = node->R[n];
				node->R[m+NN] = node->R[n+NN];
				node->flag[m] = 1;
				node->flag[n] = 1;
			}
		}

}

/** This function solves the linear system (I+\alpha*T)\phi = W-R */
void solve(double mu, size_t NN, size_t NBF, size_t NNZ, double alpha, node_s *node, model_t model_Tmat, mass_t choice,  boundary_nodes_s *b_nodes)
{
//	start=clock();
#pragma omp parallel for
	for ( size_t n = 0; n < NN; n++ ) {
		node->PHI[n] = node->W[n] - node->R[n]; //+ node[n].brkx;
		node->PHI[n + NN] = node->W[n + NN] - node->R[n + NN]; //+ node[n].brky;
		if ( node->bflag[n] == 3 ) {
			node->PHI[n] = 0.;
			node->PHI[n+NN] = 0.;
		}
		
	//	if(node->h[n]<1.e-3){
	//		node->PHI[n] = 0.;
	//		node->PHI[n+NN] = 0.;
	//	}
	}
//	if ( model_Tmat == MODEL_DIAG ) {
//		for (size_t f = 0; f < NBF; f++){
//			if (3 == b_nodes->type[f]) {
//				size_t n = b_nodes->node[f];
//				double length = sqrt(b_nodes->normal[f][0]*b_nodes->normal[f][0] + b_nodes->normal[f][1]*b_nodes->normal[f][1]);
//				double tmpp1=node->PHI[n];
//				double tmpp2=node->PHI[n+NN];
//				node->PHI[n] =  (tmpp1*b_nodes->normal[f][0] + tmpp2*b_nodes->normal[f][1])/length ;
//				node->PHI[n+NN] =  (-tmpp1*b_nodes->normal[f][1] + tmpp2*b_nodes->normal[f][0])/length ;
//			}
//		}
//	}

	if ( model_Tmat != MODEL_DIAG ) {
		if ( !Ttemp )
			Ttemp = (double*)MA_vector( 4 * NNZ, sizeof(double) );
		double gal;
#pragma omp parallel for
		for ( size_t i = 0; i < NNZ; i++ ) {
			if ( choice == MASS_H_GALERKIN )
				gal = HGMM[i];
			else
				gal = GMM[i];

			int itmp = IT[i] - 1;
			if (static_cast<size_t>( IT[i] ) - 1 >= NN)
					itmp = IT[i] - NN - 1;

			if ( IT[i] == JT[i] ) { // on the block diagonal
				// 1] Mass lumping:
//				Ttemp[i] = node->volume[itmp]/(mu*mu) + alpha*T[i];
//				Ttemp[i+3*NNZ] = node->volume[itmp]/(mu*mu) + alpha*T[i+3*NNZ];

				// 2] No-Mass lumping:
				Ttemp[i] = gal/(mu*mu) + alpha*T[i];
				Ttemp[i+3*NNZ] = gal/(mu*mu) + alpha*T[i+3*NNZ];

				if ( node->bflag[itmp] == 3 ){ // Wall boundary conditions
					Ttemp[i] = T[i];
					Ttemp[i+3*NNZ] = T[i+3*NNZ];
				}
			} else { // not on the block diagonal
				// 1] Mass lumping:
//				Ttemp[i] = alpha*T[i];
//				Ttemp[i+3*NNZ] = alpha*T[i+3*NNZ];

				// 2] No-Mass lumping:
				Ttemp[i] = gal/(mu*mu) + alpha * T[i];
				Ttemp[i+3*NNZ] = gal/(mu*mu) + alpha * T[i+3*NNZ];

				if ( node->bflag[itmp] == 3 ){ // Wall boundary conditions
					Ttemp[i] = T[i];
					Ttemp[i+3*NNZ] = T[i+3*NNZ];
				}
			}
			Ttemp[i+NNZ] = alpha*T[i+NNZ];
			Ttemp[i+2*NNZ] = alpha*T[i+2*NNZ];

		}
/*		FILE *out = fopen("../output/Ttemp2.dat", "w");
		for (int i = 0; i < 4*NNZ; i++ ){
			fprintf(out,"%le %d %d\n",Ttemp[i],IT[i],JT[i]);
		}
		fclose(out);
*/

		Ttemp_mkl->init_factorize(2*NN, 4*NNZ, IT, JT, Ttemp);
//		end=clock();
//		cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;
//		printf("time needed to construct the total matrix %le\n",cpu_time_used);

	}
//	start=clock();
	Ttemp_mkl->solve(node->PHI, 2*NN);
//	end=clock();
//	cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;
//	printf("time for solve is %le\n",cpu_time_used);

	if ( choice == MASS_H_GALERKIN || model_Tmat == MODEL_NCF){
#pragma omp parallel for
		for ( size_t n = 0; n < NN; n++ ) {
			node->PHI[n] *= node->h[n];
			node->PHI[n + NN] *= node->h[n];
		}
	}
}
