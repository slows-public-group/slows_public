/*  Copyright (C) 2021   M.Ricchiuto  <mario.ricchiuto@inria.fr>
                         M. Kazolea   <maria.kazolea@inria.fr>
                         A. Filippini <a.filippini@brgm.fr>
                         L. Arpaia    <luca.arpaia@brgm.fr>
                         N.Pattakos   <pattakosn@fastmail.com> 

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.*/


/* the geometry of each element is computed and processed here to have all
 the necessary informations for the computation */
#include <stdio.h>
#include <math.h>
#include "common.h"
#include "element_preprocessing.h"

/* Function that computes the value of the edge given the two nodes */
static int find_edge(edge_s *edge, int n1, int n2, int NEDGE)
{
	for (int i = 0; i < NEDGE; i++)
		if ((n1 == edge->node[i][0] && n2 == edge->node[i][1]) || (n1 == edge->node[i][1] && n2 == edge->node[i][0]))
			return i;
	return -1;
}

static void find_elem(int edge, int NE, element_s *element, int vect[2])
{
	// initialize to -1 so as to distinguish the boundary edges from the ones containing element 0
	vect[0] = vect[1] = -1;
	int j = 0;
	for (int e = 0; e < NE; e++) {
		if (edge == element->edge[e][0] || edge == element->edge[e][1] || edge == element->edge[e][2]) {
			vect[j] = e;
			j = j + 1;
		}
	}
}

/*  Preprocessing for linear P1 interpolation */
// This subroutine computes the normal vectors, the volume of the triangles, the neighbors of the nodes, the number of non zero elements for the sparse matrix!!!
/* Compute edge structures */
void preprocessing( size_t NN, size_t NE, size_t NBF, size_t *NN_sparse, size_t *NP,
                       element_s *element,
		       edge_s *edge,
		       node_s *node,
		       boundary_s *boundary,
		       boundary_nodes_s *b_nodes)
{
	for ( size_t n = 0; n < NN; n++ ) {
		node->neighbours[n][0] = n;
		node->neighbouring_nodes[n] += 1;
	} /* neighbour 0 is the node itself, the others are those connected by an edge */

	/* Computation of the nodal normals */
	for ( size_t e = 0; e < NE; e++ ) {
		//we take normals pointing outside of the elements
		int n0 = element->node[e][0];
		int n1 = element->node[e][1];
		int n2 = element->node[e][2];
		double x0 = node->coordinate[n0][0];
		double y0 = node->coordinate[n0][1];
		double x1 = node->coordinate[n1][0];
		double y1 = node->coordinate[n1][1];
		double x2 = node->coordinate[n2][0];
		double y2 = node->coordinate[n2][1];
		element->normal[e][0][0] = (y1 - y2);
		element->normal[e][0][1] = (x2 - x1);

		element->normal[e][1][0] = (y2 - y0);
		element->normal[e][1][1] = (x0 - x2);

		element->normal[e][2][0] = (y0 - y1);
		element->normal[e][2][1] = (x1 - x0);

		/* Compute the volume of the element */
		element->volume[e] = 0.5 * fabs( element->normal[e][2][0] * element->normal[e][1][1] - element->normal[e][1][0] * element->normal[e][2][1] );

		for ( int v = 0; v < 3; v++ )
			node->volume[element->node[e][v]] += element->volume[e] / 3.;
	}


	for ( size_t e = 0; e < NE; e++ ) {
		element->flag[e][0] = 0;
		element->flag[e][1] = 0;
		element->flag[e][2] = 0;
	}
	/* flags used for edges/node neighbours recovery */

	printf("******************************************\n");
	printf("******************************************\n");
	printf("***  Looking for node neighbours ....  ***\n");
	printf("***  .... might take some time  ....   ***\n");
	printf("******************************************\n");
	printf("******************************************\n");

	for ( size_t e = 0; e < NE; e++ ) { /* main element loop start */
		/* reconstructing edges, viz node neighbours */
		int n1 = element->node[e][0];
		int n2 = element->node[e][1];                              //!!!!!NIKOS TODO: REMOVE CODE DUPLICATION
		int n3 = element->node[e][2];
		int neigh_counter1 = node->neighbouring_nodes[n1];
		int newmate = 1;
		for ( int j = 0; j < neigh_counter1; j++ )
			if ( node->neighbours[n1][j] == n2 )
				newmate = 0;
		if ( newmate ) {
			node->neighbours[n1][neigh_counter1] = n2;
			node->neighbouring_nodes[n1] += 1;
		}
		neigh_counter1 = node->neighbouring_nodes[n1];
		newmate = 1;
		for ( int j = 0; j < neigh_counter1; j++ )
			if ( node->neighbours[n1][j] == n3 )
				newmate = 0;
		if ( newmate ) {
			node->neighbours[n1][neigh_counter1] = n3;
			node->neighbouring_nodes[n1] += 1;
		}

		n1 = element->node[e][2];
		n2 = element->node[e][0];
		n3 = element->node[e][1];
		neigh_counter1 = node->neighbouring_nodes[n1];
		newmate = 1;
		for ( int j = 0; j < neigh_counter1; j++ )
			if ( node->neighbours[n1][j] == n2 )
				newmate = 0;
		if ( newmate ) {
			node->neighbours[n1][neigh_counter1] = n2;
			node->neighbouring_nodes[n1] += 1;
		}
		neigh_counter1 = node->neighbouring_nodes[n1];
		newmate = 1;
		for ( int j = 0; j < neigh_counter1; j++ )
			if ( node->neighbours[n1][j] == n3 )
				newmate = 0;
		if ( newmate ) {
			node->neighbours[n1][neigh_counter1] = n3;
			node->neighbouring_nodes[n1] += 1;
		}

		n1 = element->node[e][1];
		n2 = element->node[e][2];
		n3 = element->node[e][0];
		neigh_counter1 = node->neighbouring_nodes[n1];
		newmate = 1;
		for (int j = 0; j < neigh_counter1; j++)
			if (node->neighbours[n1][j] == n2)
				newmate = 0;
		if (newmate) {
			node->neighbours[n1][neigh_counter1] = n2;
			node->neighbouring_nodes[n1] += 1;
		}
		neigh_counter1 = node->neighbouring_nodes[n1];
		newmate = 1;
		for (int j = 0; j < neigh_counter1; j++)
			if (node->neighbours[n1][j] == n3)
				newmate = 0;
		if (newmate) {
			node->neighbours[n1][neigh_counter1] = n3;
			node->neighbouring_nodes[n1] += 1;
		}
	}



	printf("******************************************\n");
	printf("******************************************\n");
	printf("***  Neighbours structure built  ....  ***\n");
	printf("******************************************\n");
	printf("******************************************\n");

	/* Compute boundary nodes */
	for ( size_t f = 0; f < NBF; f++ ) {
		int n = boundary->node[f][0];
		node->bflag[n] = boundary->types[f][0];
	}


	/* Boundary Normals */
	size_t m = 0;
	for ( size_t f = 0; f < NBF; f++ ) {
		int n1 = boundary->node[f][0];
		int n2 = boundary->node[f][1];
		double x1 = node->coordinate[n1][0];
		double y1 = node->coordinate[n1][1];
		double x2 = node->coordinate[n2][0];
		double y2 = node->coordinate[n2][1];
		double nx1 = y2 - y1;
		double ny1 = x1 - x2;

		int flag = 0;
		for ( size_t g = 0; g < NBF; g++ ) {
			if ( boundary->node[g][1] == n1 ) {
				b_nodes->node[m] = n1;
				node->bindex[n1] = m;
				int m1 = boundary->node[g][0];
				int m2 = boundary->node[g][1];
				x1 = node->coordinate[m1][0];
				y1 = node->coordinate[m1][1];
				x2 = node->coordinate[m2][0];
				y2 = node->coordinate[m2][1];
				double nx2 = y2 - y1;
				double ny2 = x1 - x2;

				b_nodes->normal[m][0] = 0.5 * (nx1 + nx2);
				b_nodes->normal[m][1] = 0.5 * (ny1 + ny2);
/*				if (n1==0 || n1==2){
					b_nodes[m].normal[0] = nx1;
					b_nodes[m].normal[1] = ny1;
					printf("n %d nx %le ny %le\n",n1,nx1,ny1);
				} else if (n1==1 || n1==3){
					b_nodes[m].normal[0] = nx2;
					b_nodes[m].normal[1] = ny2;
					printf("n %d nx %le ny %le\n",n1,nx2,ny2);
				}
*/				/*******************************************
				**** ATTENTION: Default choice for types ***
				*******************************************/
				b_nodes->type[m] = boundary->types[f][0];

				flag += 1;
			}
		}

		if ( flag == 0 )
			printf( "error in boundary preprocessing, boundary not close in node %d\n", n1 );

		m += 1;
	}
	if ( m != NBF )
		printf("ERROR in the Boundary preprocessing !!!!!!!!");

	int ned = 0;
	for ( size_t n = 0; n < NN; n++ ) {
		for ( int i = 1; i < node->neighbouring_nodes[n]; i++ ) {
			if ( n < (size_t)node->neighbours[n][i] ) {
				int m = node->neighbours[n][i];
				edge->node[ned][0] = n;
				edge->node[ned][1] = m;
				ned = ned + 1;

				// Apply a convention on the normal of the edges:
				edge->normal[ned][0] = -(node->coordinate[m][1] - node->coordinate[n][1]);
				edge->normal[ned][1] = (node->coordinate[m][0] - node->coordinate[n][0]);
			}
		}
	}
	int NEDGE = ned;

	for ( size_t e = 0; e < NE; e++ ) {
		int n0 = element->node[e][0];
		int n1 = element->node[e][1];
		int n2 = element->node[e][2];

		// edge "i" is the opposite edge to node "i"
		element->edge[e][2] = find_edge(edge, n0, n1, NEDGE);
		element->edge[e][1] = find_edge(edge, n2, n0, NEDGE);
		element->edge[e][0] = find_edge(edge, n1, n2, NEDGE);
	}

	// Construct the connection between every node and the opposite normals connecting its neighbours
	for ( int i = 0; i < NEDGE; i++ ) {
		// initialization with -1 in order to distinguish the boundary edges from the ones containing element 0
		//edge->element[i][0] = -1;
		//edge->element[i][1] = -1;
		int temp[2];
		find_elem(i, NE, element, temp);
		edge->element[i][0] = temp[0];
		edge->element[i][1] = temp[1];
	}

	//Construct the adjacent edges of a node
	for ( size_t n = 0; n < NN; n++ ) {
		// we set adj_edge[0]=-1 in order to distinguish from the edge of index 0
		node->adj_edges[n][0] = -1;
		for ( int i = 1; i < node->neighbouring_nodes[n]; i++ ) {
			int n0 = n;
			int n1 = node->neighbours[n][i];
			node->adj_edges[n][i] = find_edge(edge, n0, n1, NEDGE);
		}
	}

	for ( size_t f = 0; f < NBF; f++ ) {
		int n0 = boundary->node[f][0];
		int n1 = boundary->node[f][1];
		int edge_local = find_edge( edge, n0, n1, NEDGE );
		edge->bflag[edge_local] = boundary->type[f];
	}


	/* Correction for Periodic b.c. */
	/* Volume correction for Periodic Bound. Cond. */
	for( size_t n = 0 ; n < NN ; n++ ){
		node->flag[n] = 0 ;
		node->bindex_per[n] = -1 ;
	}

	int count = 0;
	for ( size_t f = 0 ; f < NBF ; f++ ) {
		int n1 = b_nodes->node[f];
		if ( b_nodes->type[f] == 10 ) {
			node->bindex_per[n1] = count;
			count += 1;
			if ( node->flag[n1] == 0 ) {
				int mate = node->mate[n1];
				node->volume[n1] += node->volume[mate];
				node->volume[mate] = node->volume[n1];
				node->flag[mate] = 1;
				node->flag[n1] = 1;
			}
		}
	}
	*NP = count;

	for ( size_t f = 0; f < NBF; f++ ) {
		int n = b_nodes->node[f];
		node->flag[n]=0;
	}
	for ( size_t f = 0; f < NBF; f++ ) {
		int n = b_nodes->node[f];
		if ( node->bflag[n] == 10 ) {
			int mate=node->mate[n];
			if ( node->flag[n] == 0 ) {
				int tmp_nb = node->neighbouring_nodes[mate];
				for ( int i = 1; i < node->neighbouring_nodes[n]; i++ ) {
					int k = node->neighbouring_nodes[mate];
					node->neighbours[mate][k] = node->neighbours[n][i];
					node->neighbouring_nodes[mate] += 1;
					node->adj_edges[mate][k] = node->adj_edges[n][i];
				}
				for ( int i = 1; i < tmp_nb; i++ ) {
					int k=node->neighbouring_nodes[n];
					node->neighbours[n][k] = node->neighbours[mate][i];
					node->neighbouring_nodes[n] += 1;
					node->adj_edges[n][k] = node->adj_edges[mate][i];
				}
				node->flag[n] = 1;
				node->flag[mate] = 1;
			}
		}
	}

	// Number of elements of the sparse matrix we will have(just the number of one block):
	// So the Galerkin matrix will have 2*NN_sparse elements, while the T full matrix will have 4*NN_sparse elements and the T const diag matrix 2*NNsparse elements.
	*NN_sparse = 0;
	for ( size_t e = 0; e < NN; e++ )
		*NN_sparse += node->neighbouring_nodes[e];
}
