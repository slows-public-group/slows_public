/*  Copyright (C) 2021   M.Ricchiuto  <mario.ricchiuto@inria.fr>
                         M. Kazolea   <maria.kazolea@inria.fr>
                         A. Filippini <a.filippini@brgm.fr>
                         L. Arpaia    <luca.arpaia@brgm.fr>
                         N.Pattakos   <pattakosn@fastmail.com> 

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.*/
	


#ifndef COMMON_H
#define COMMON_H
static const int VERTEX = 3;

typedef enum { MASS_NONE, MASS_GALERKIN, MASS_H_GALERKIN } mass_t;
typedef enum { MODEL_FULL, MODEL_DIAG, MODEL_NCF } model_t;

/** ADD A DESCRIPTION OF THE PURPOSE OF THE STRUCT HERE
 */
typedef struct
{
	double *h;
	double *u;
	double *v;
	double *dxh;
	double *dxhu;
	double *dxhv;
	double *dxeta;
	double *dyh;
	double *dyhu;
	double *dyhv;
	double *dyeta;
	double *eta;
	double *phi;
	double time;
	int *bb; // Accounts for the breaking nodes. 1 if the node is breaking 0 if it is not
	double *k; //kinetic energy for the breaking model
	double *e; //energy for the GN equations
} non_hydro_s;

typedef struct
{
	int **node;
	double *volume;
	double ***normal;
	int **edge;
	char **flag; /* edge flag used in edge/node neighbours recovery */
} element_s;

typedef struct
{
	int **element;
	char *bflag;
	int **node;
	double **normal;
} edge_s;

typedef struct
{
	double *h;
	double *u;
	double *v;
	double *eta;
	double *hu;
	double *hv;
	double *SWL;
	double *bed;
	double *W;
	double *R;
	double *PHI; /** The lower NN + 1 elements are the x-component of PHI and the upper NN + 1 the y- components, ie the x,y components of PHI are: (PHI[n], PHI[n+NN]) */
	double *dxbed;
	double *dybed;
	double *gradh; /** The lower NN + 1 elements are the x-component of grad eta and the upper NN + 1 the y- components, ie the x,y components of gradeta are: (gradh[n], gradh[n+NN]) */
	double *gradeta; /** The lower NN + 1 elements are the x-component of grad eta and the upper NN + 1 the y- components, ie the x,y components of gradeta are: (gradeta[n], gradeta[n+NN]) */
	double *gradu; /** The lower NN + 1 elements are the x-component of gradu and the upper NN + 1 the y- components, ie the x,y components of gradu are: (gradu[n], gradu[n+NN]) */
	double *gradv; /** The lower NN + 1 elements are the x-component of gradv and the upper NN + 1 the y- components, ie the x,y components of gradv are: (gradv[n], gradv[n+NN]) */
	double *gradhu; /** The lower NN + 1 elements are the x-component of gradhu and the upper NN + 1 the y- components, ie the x,y components of gradhu are: (gradhu[n], gradhu[n+NN]) */
	double *gradhv; /** The lower NN + 1 elements are the x-component of gradhv and the upper NN + 1 the y- components, ie the x,y components of gradhv are: (gradhv[n], gradhv[n+NN]) */
	double *f;
	double **coordinate;
	double *volume;
	int *bb; // Accounts for the breaking nodes. 1 if the node is breaking 0 if it is not
	int **adj_edges; // Allocate memory to store the neighboring edges opposite to the node. Admitting a maximum of 20 neigbouring nodes !!!
	int *neighbouring_nodes; /* total number of neigbouring nodes */
	int **neighbours; // integers of neighbouring nodes needed for Jacobian entries.  integers for Jacobian entries used for nearest and non nearest neighbours. Admitting a maximum of 20 neigbouring nodes !!!
	int *bflag;
	int *bindex; // It gives the ordering of the boundary node struct
	int *bindex_per; // It gives the ordering of the nodes with periodic boundary conditions. Default value is : bindex_per = -1
	int *mate;
	char *flag;
	double *k; //kinetic energy for each node, used in wave breaking treatment
	double *brk; /** Diffusive term added for wave breaking. The lower NN + 1 elements are the x-component of breaking and the upper NN + 1 the y- components, ie the x,y components of brk are: (brk[n],brk[n+NN]) */
	double **us; //surface velocity
	double **uz; //derivative of the u(z) respective toz evaluated in the free surface
	double *e; //energy for the GN equations
} node_s;

// Faces that belong to the boundary
typedef struct
{
	int **node;
	int **types;
	int *type;
	double **normal;
} boundary_s;

// Nodes that belong to the boundary
typedef struct
{
	int *node;
	int *type;
	int *mate;
	char *flag;
	double **normal;
} boundary_nodes_s;
#endif
