/*  Copyright (C) 2021   M.Ricchiuto  <mario.ricchiuto@inria.fr>
                         M. Kazolea   <maria.kazolea@inria.fr>
                         A. Filippini <a.filippini@brgm.fr>
                         L. Arpaia    <luca.arpaia@brgm.fr>
                         N.Pattakos   <pattakosn@fastmail.com> 

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.*/


#pragma once
#ifdef WITH_SPARSE_MKL
#include <vector>
#include "mkl.h"
#include "lin_solver_interface.h"

class lin_solver_mkl_direct : public lin_solver_interface {
private:
	// Internal solver memory pointer pt:
	//   32-bit: int pt[64];
	//   64-bit: long int pt[64]
	// or void *pt[64] should be OK on both architectures
	void *pt[64] = {0};
	MKL_INT mtype = 11; // Real and nonsymmetric matrix
	MKL_INT iparm[64] = {0};
	const MKL_INT maxfct = 1;
	const MKL_INT mnum = 1;
	const MKL_INT msglvl = 0;
	const MKL_INT nrhs = 1;

	sparse_matrix_t a_csr_h = 0;
	double* a_csr3;
	std::vector<MKL_INT> a_csr3_ia;
	MKL_INT *a_csr3_ja = NULL;
	MKL_INT rows = 0;
	std::vector<double> x;

	void release_a_csr();
	void pardiso_init();
public:
	lin_solver_mkl_direct();
	lin_solver_mkl_direct(int rowsIN, int nnz, int* ia, int* ja, double* a);

	~lin_solver_mkl_direct();

	void init_factorize(int rowsIN, int nnz, int* ia, int* ja, double* a) override;
	void solve(double *b, int N) override;

	void release_pt(); // Release internal memory
};
#endif
