/*  Copyright (C) 2021   M.Ricchiuto  <mario.ricchiuto@inria.fr>
                         M. Kazolea   <maria.kazolea@inria.fr>
                         A. Filippini <a.filippini@brgm.fr>
                         L. Arpaia    <luca.arpaia@brgm.fr>
                         N.Pattakos   <pattakosn@fastmail.com> 

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.*/
	


/** This is the main function for the non hydrostatic term reconstruction:
  * it starts the computation and drives it to the end.
  */
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <assert.h>
#include "my_malloc.h"
#include "common.h"
#include "nht.h"
#include "rhs_dispersive_terms.h"
#include "derivative_reconstruction.h"
#include "element_preprocessing.h"
#include "breaking_hybrid.h"
#include "sparse.h"
#include "time.h"
#include "lin_solver_interface.h"
#include "lin_solver_mumps.h"
#include "lin_solver_mkl_direct.h"

#define CHECK_FSCANF(NUM,COMMAND,FNAME) do { \
	if ( NUM != (COMMAND) ) { \
		fprintf( stderr, "NHT: Error reading from file %s", FNAME ); \
		return EXIT_FAILURE; \
	} } while(0)

static double GRAVITY;
static double mu;
static double cut_off[2];
static size_t NN, NE, NBF;
static double alpha;
static model_t model_Tmat;
static int model_brk;
static mass_t SPDForm;
static double param_brk[5];
static edge_s edge;
static node_s node;
static element_s element;
static boundary_nodes_s b_nodes;
static double timep = 0.;
static size_t NN_sparse = 0;
static size_t NP = 0; // number of nodes with periodic boundary conditions
static non_hydro_s data;
lin_solver_interface* T_mkl;
lin_solver_interface* Ttemp_mkl;
lin_solver_interface* Ttemp_f_mkl;
lin_solver_interface* gmm_matrix;

static int go( void )
{
#pragma omp parallel for
	for ( size_t n = 0; n < NN; n++ ) {
		node.hu[n] = data.h[n] * data.u[n];
		node.hv[n] = data.h[n] * data.v[n];
		node.dxbed[n] = node.gradeta[n] - node.gradh[n];
		node.dybed[n] = node.gradeta[n+NN] - node.gradh[n+NN];
	}
/*	grad_reconstruction( NN, NE, NBF, cut_off, &element, &node, &b_nodes, node.eta, node.gradeta, 1 );
	grad_reconstruction( NN, NE, NBF, cut_off, &element, &node, &b_nodes, node.h, node.gradh, 0 );
	grad_reconstruction( NN, NE, NBF, cut_off, &element, &node, &b_nodes, node.u, node.gradu, 0 );
	grad_reconstruction( NN, NE, NBF, cut_off, &element, &node, &b_nodes, node.v, node.gradv, 0 );
	grad_reconstruction( NN, NE, NBF, cut_off, &element, &node, &b_nodes, node.hu, node.gradhu, 0 );
	grad_reconstruction( NN, NE, NBF, cut_off, &element, &node, &b_nodes, node.hv, node.gradhv, 0 );
*/

	void (*block_func) (int, int, int, size_t, size_t, size_t, double **, double **, double ***,
			node_s *, element_s *,
			boundary_nodes_s *);

	if ( model_Tmat == MODEL_FULL ) {
		if ( SPDForm == MASS_GALERKIN ) {
			block_func = Tblock_assembly;
		} else if ( SPDForm == MASS_H_GALERKIN ) {
			block_func = TSPDblock_assembly;
			HGMM_assembly(NN, NE, NN_sparse, NP, &node, &element, &b_nodes);
		} else {
			printf( "NHT: ERROR: Somehow wrong matrix was chosen while solving\n" );
			return EXIT_FAILURE;
		}
		T_assembly(NN, NE, NN_sparse, NP, &node, &element, &b_nodes, block_func);
		get_W( GRAVITY, NN, NE, NBF, 4 * NN_sparse, &node, &element, &b_nodes, model_Tmat, SPDForm, alpha);
		R_assembly( GRAVITY, NN, NE, NBF, model_Tmat, &node, &element, &b_nodes);
	} else if ( model_Tmat == MODEL_DIAG ) {
		get_f( NN, NBF, &node, &b_nodes );
		get_W( GRAVITY, NN, NE, NBF, 4 * NN_sparse, &node, &element, &b_nodes, model_Tmat, SPDForm, alpha);
		R_assembly( GRAVITY, NN, NE, NBF, model_Tmat, &node, &element, &b_nodes);
	} else if ( model_Tmat == MODEL_NCF ) {
		block_func = TNCFblock_assembly;
		T_assembly(NN, NE, NN_sparse, NP, &node, &element, &b_nodes, block_func);
		get_W( GRAVITY, NN, NE, NBF, 4 * NN_sparse, &node, &element, &b_nodes, model_Tmat, SPDForm, alpha);
		RNCF_assembly( NN, NE, NBF, &node, &element, &b_nodes);
	} else {
		printf( "NHT: ERROR: Somehow wrong model was chosen while solving\n" );
		return EXIT_FAILURE;
	}


	// Check for wave breaking
	if ( model_brk == 2 ) {
		if ( data.time != timep )
			breaking_hybrid( GRAVITY, mu, NN, &node, param_brk[0], param_brk[1] );
		kinen( NN, NE, NBF, cut_off, &element, &node, &b_nodes, &edge, &data, timep, param_brk[2], param_brk[3], param_brk[4] );
	}
	//solve( mu, NN, NN_sparse, alpha, &node, model_Tmat, MASS_GALERKIN );
	solve( mu, NN, NBF, NN_sparse, alpha, &node, model_Tmat, SPDForm, &b_nodes);

// When you solve for psi
	for ( size_t n = 0; n < NN; n++ ){
            node.PHI[n] = node.PHI[n] + (GRAVITY*node.h[n]*node.gradeta[n])/alpha;
            node.PHI[n+NN] = node.PHI[n+NN] + (GRAVITY*node.h[n]*node.gradeta[n+NN])/alpha;
	}

	if ( (model_brk == 1) && ( data.time != timep ) ) {
		breaking_hybrid( GRAVITY, mu, NN, &node, param_brk[0], param_brk[1] );
	}

	// ENERGY
	// computation of the energy of the GN equations using the formula by the book (p.173)
	// The water waves problem, mathematical Analysis and Asymptotics, David Lannes, 2013
#pragma omp parallel for
	for ( size_t n = 0; n < NN; n++ ) {
		node.e[n]  = 0.5 * node.h[n] * (pow( node.u[n], 2 ) + pow( node.v[n], 2 ));
		node.e[n] += 0.5 * GRAVITY * pow( node.eta[n], 2 );
		node.e[n] += 0.5 * node.h[n] * ( 1./3. * pow( fabs( node.h[n] * (node.gradu[n] + node.gradv[n]) - 3. / 2. * (node.dxbed[n] * node.u[n] + node.dybed[n] * node.v[n]) ), 2 ) + 1. / 4. * pow( fabs( node.dxbed[n] * node.u[n] + node.dybed[n] * node.v[n] ), 2 ));
		node.e[n]=0.0;

	}

//#pragma omp parallel for
//	for ( size_t n = 0; n < NN; n++ ){
//                 //printf("---%d %d\n",n,node.bb[n]);
//		if (node.h[n]<cut_off[0]){
//			node.PHI[n] = 0.;
//			node.PHI[n+NN] = 0.;
//		}
//         }
	timep = data.time;

	return EXIT_SUCCESS;
}

extern "C"
int nht_phi_bb_e( double *h, double *u, double *v, double *eta, double *dxh, double *dxhu, double *dxhv, double *dxeta, double *dyh, double *dyhu, double *dyhv, double *dyeta, size_t _NN, double time, double *phi, int *bb, double *e )
{
	if ( NN != _NN ) {
		printf( "NHT: ERROR: data size set at init(%zd) does not match input data size(%zd)\n", NN, _NN );
		return EXIT_FAILURE;
	}
	data.h = node.h =  (double *) h;
	data.u = node.u =  (double *) u;
	data.v = node.v =  (double *)  v;
	data.eta = node.eta = (double *) eta;
	data.phi = node.PHI = phi;
	data.bb = node.bb = bb;
	data.e = node.e = e;
	data.time = time;

#pragma omp parallel for
	for(size_t n = 0; n < NN; n++){
		node.gradh[ n ] = dxh[ n ];
		node.gradh[ n + NN ] = dyh[ n ];
		node.gradeta[ n ] = dxeta[ n ];
		node.gradeta[ n + NN ] = dyeta[ n ];
		node.gradhu[ n ] = dxhu[ n ];
		node.gradhu[ n + NN ] = dyhu[ n ];
		node.gradhv[ n ] = dxhv[ n ];
		node.gradhv[ n + NN ] = dyhv[ n ];

		node.gradu[ n ] = (node.gradhu[ n ] - node.gradh[ n ]*node.u[ n ] )/node.h[ n ];  //Put a check for wet dry fronts
		node.gradu[ n + NN ] = (node.gradhu[ n + NN ] - node.gradh[ n + NN ]*node.u[ n ] )/node.h[ n  ];  //Put a check for wet dry fronts
		node.gradv[ n ] = (node.gradhv[ n ] - node.gradh[ n ]*node.v[ n ] )/node.h[ n ];  //Put a check for wet dry fronts
		node.gradv[ n + NN ] = (node.gradhv[ n + NN ] - node.gradh[ n + NN ]*node.v[ n  ] )/node.h[ n  ];  //Put a check for wet dry fronts
	}

	return go( );
}

extern "C"
int nht_phi_bb_k_e( double *h, double *u, double *v, double *eta, double *dxh, double *dxhu, double *dxhv, double *dxeta, double *dyh, double *dyhu, double *dyhv, double *dyeta, size_t _NN, double time, double *phi, int *bb,double *e, double *k )
{
	data.k = node.k = k;

	assert( (0 == 1) && "If next line is OK, delete this assert");
	return nht_phi_bb_e( h, u, v, eta, dxh, dxhu, dxhv, dxeta, dyh, dyhu, dyhv, dyeta, _NN, time, phi, bb, e );
}

extern "C"
int nht_new ( double alphaIn, double gravityIn, double muIn, double href, double cut_offIn[2],
              int model_TmatIn, int model_brkIn, double param_brkIn[ 5 ], int massIn, double *bedIn, double *dxbedIn, double *dybedIn, size_t _NN,
	      int ** el_node, double** no_coord, int* no_mate, int** bnode, int** btypes, int* btype, double** bnormal )
{
	NN = _NN;

	alpha = alphaIn;

	GRAVITY = gravityIn;

	mu = muIn;

	cut_off[0] = cut_offIn[0];
	cut_off[1] = cut_offIn[1];

	model_brk = model_brkIn;

 	if ( 0 == model_TmatIn )
		model_Tmat = MODEL_FULL;
 	else if ( 1 == model_TmatIn )
		model_Tmat = MODEL_DIAG;
 	else if ( 2 == model_TmatIn )
		model_Tmat = MODEL_NCF;
	else {
		printf( "NHT: ERROR: You chose a wrong model\n" );
		return EXIT_FAILURE;
	}
#ifdef WITH_SPARSE_MKL
	T_mkl = new lin_solver_mkl_direct();
	Ttemp_mkl = new lin_solver_mkl_direct();
	gmm_matrix = new lin_solver_mkl_direct();
	if( model_Tmat == MODEL_DIAG )
		Ttemp_f_mkl = new lin_solver_mkl_direct();
#endif
#ifdef WITH_MUMPS
	T_mkl = new lin_solver_mumps();
	Ttemp_mkl = new lin_solver_mumps();
	gmm_matrix = new lin_solver_mumps();
	if( model_Tmat == MODEL_DIAG )
		Ttemp_f_mkl = new lin_solver_mumps();
#endif
#if !defined(WITH_SPARSE_MKL) && !defined(WITH_MUMPS)
	printf("NHT library can not run without a sparse matrix linear solver. Please recompile using one.\n");
	return EXIT_FAILURE;
#endif


	if ( 0 == massIn )
		 SPDForm = MASS_NONE;
	else if ( 1 == massIn )
		 SPDForm = MASS_GALERKIN;
	else if ( 2 == massIn )
		 SPDForm = MASS_H_GALERKIN;
	else {
		printf( "NHT: ERROR: You chose a wrong matrix type\n" );
		return EXIT_FAILURE;
	}

	if ( (model_TmatIn != MODEL_FULL) && (massIn != MASS_NONE) ) {
		printf( "NHT: ERROR: Matrix formats are available only for T-matrix = FULL. Choose SPD Format = NULL instead. \n" );
		return EXIT_FAILURE;
	}

	for ( int i = 0 ; i < 5 ; i++ )
		param_brk[i] = param_brkIn[i];

	assert ( ( NN != 0 ) && ( NE != 0 ) && ( NBF != 0 ) && "Cowardly refusing to allocate zero sized arrays" );

	// data->h   = MA_vector( NN, sizeof(double) );
	// data->u   = MA_vector( NN, sizeof(double) );
	// data->v   = MA_vector( NN, sizeof(double) );
	// data->eta = MA_vector( NN, sizeof(double) );
	// data->phi = MA_vector( 2 * NN, sizeof(double) );
	// data->k   = MA_vector( NN, sizeof(double) );
	// data->e   = MA_vector( NN, sizeof(double) );
	// data->bb  = MA_vector( NN, sizeof(char) );
	data.time = 0.;

	// guard depends on whether this is an actual library call or a call from nht_new_from_file
	if ( element.node != el_node ) {
		element.node = (int**) MA_matrix( NE + 1, 3, sizeof( int ) );
		for ( size_t e = 0; e < NE + 1; ++e ) {
			element.node[ e ][ 0 ] = el_node[ e] [ 0 ];
			element.node[ e ][ 1 ] = el_node[ e] [ 1 ];
			element.node[ e ][ 2 ] = el_node[ e] [ 2 ];
		}
	}
	element.edge = (int**) MA_matrix( NE + 1, 3, sizeof( int ) );
	element.volume = (double*) MA_vector( NE + 1, sizeof( double ) );
	element.normal = (double***) MA_tensor( NE + 1, 3, 2, sizeof( double ) );
	element.flag = (char**) MA_matrix( NE + 1, 3, sizeof( char ) );

	// Overestimation of the dimension
	edge.normal = (double**) MA_matrix( 3 * NE + 1, 2, sizeof( double ) );
	edge.element = (int**) MA_matrix( 3 * NE + 1, 2, sizeof( int ) );
	edge.node = (int**) MA_matrix( 3 * NE + 1, 2, sizeof( int ) );
	edge.bflag = (char*) MA_vector( 3 * NE + 1, sizeof( char ) );

	node.h = 0; // data->h;
	node.u = 0; // data->u;
	node.v = 0; // data->v;
	node.eta = 0; //data->eta;
	node.hu = (double*) MA_vector( NN, sizeof(double) );
	node.hv = (double*) MA_vector( NN, sizeof(double) );
	node.SWL = (double*) MA_vector( NN, sizeof(double) );
	node.bed = (double*) MA_vector( NN, sizeof(double) );
	node.W = (double*) MA_vector( 2 * NN, sizeof(double) );
	node.R = (double*) MA_vector( 2 * NN, sizeof(double) );
	node.PHI = 0; //data->phi;
	node.dxbed = (double*) MA_vector( NN, sizeof(double) );
	node.dybed = (double*) MA_vector( NN, sizeof(double) );
	node.gradh = (double*) MA_vector( 2 * NN, sizeof(double) );
	node.gradeta = (double*) MA_vector( 2 * NN, sizeof(double) );
	node.gradu = (double*) MA_vector( 2 * NN, sizeof(double) );
	node.gradv = (double*) MA_vector( 2 * NN, sizeof(double) );
	node.gradhu = (double*) MA_vector( 2 * NN, sizeof(double) );
	node.gradhv = (double*) MA_vector( 2 * NN, sizeof(double) );
	node.f = (double*) MA_vector( 2 * NN, sizeof(double) );
	// guard depends on whether this is an actual library call or a call from nht_new_from_file
	if ( node.coordinate != no_coord ) {
		node.coordinate = (double**) MA_matrix( NN, 2, sizeof( double ) );
		for ( size_t n = 0; n < NN; n++ ) {
			node.coordinate[ n ][ 0 ] = no_coord[ n ] [ 0 ];
			node.coordinate[ n ][ 1 ] = no_coord[ n ] [ 1 ];
		}
	}
	node.volume = (double*) MA_vector( NN, sizeof(double) );
	node.bb = 0; // data->bb;
	node.adj_edges = (int**) MA_matrix( NN, 20, sizeof(int) );
	node.neighbouring_nodes = (int*) MA_vector( NN, sizeof(int) );
	node.neighbours = (int**) MA_matrix( NN, 20, sizeof(int) );
	node.bflag = (int*) MA_vector( NN, sizeof(int) );
	node.bindex = (int*) MA_vector( NN, sizeof(int) );
	node.bindex_per = (int*) MA_vector( NN, sizeof(int) );
	// guard depends on whether this is an actual library call or a call from nht_new_from_file
	if ( node.mate != no_mate ) {
		node.mate = (int*) MA_vector( NN, sizeof(int) );
		for ( size_t n = 0; n < NN; n++ )
			node.mate[ n ] = no_mate[ n ];
	}
	node.flag = (char*) MA_vector( NN, sizeof(char) );
	node.k = 0; // data->k;
	node.brk = (double*) MA_vector( 2 * NN, sizeof(double) );
	node.us = (double**) MA_matrix( NN, 2, sizeof(double) );
	node.uz = (double**) MA_matrix( NN, 2, sizeof(double) );
	node.e = 0; // data->e;

	b_nodes.node = (int*) MA_vector( NBF, sizeof(int) );
	b_nodes.type = (int*) MA_vector( NBF, sizeof(int) );
	b_nodes.mate = (int*) MA_vector( NBF, sizeof(int) );
	b_nodes.flag = (char*) MA_vector( NBF, sizeof(char) );
	b_nodes.normal = (double**) MA_matrix( NBF, 2, sizeof(double) );

	/* geometry pre-processing */
	printf( "          *************************************\n" );
	printf( "          **  Processing element geometry... **\n" );
	boundary_s boundary;
	boundary.node = (int**)bnode;
	boundary.types = (int**)btypes;
	boundary.type = (int*)btype;
	boundary.normal = (double**)bnormal;
	preprocessing( NN, NE, NBF, &NN_sparse, &NP, &element, &edge, &node, &boundary, &b_nodes );

	for ( size_t  n = 0; n < NN; n++ ) {
		node.bed[n] = bedIn[n];
		node.dxbed[n] = dxbedIn[n];
		node.dybed[n] = dybedIn[n];
		node.SWL[n] = href - node.bed[n];
		if (node.SWL[n] < 0.)
			node.SWL[n] = href;
	}


	sparse_initialize( NE, NN_sparse, NP, &node, &element );

	GMM_assembly( NN, NE, NN_sparse, NP, &node, &element, &b_nodes );

	// construction of node.dxbed and node.dybed
	//bed_reconstruction( NN, NE, NBF, &element, &node, &b_nodes ); //commented when it comes from outside
	//for(int n=0; n<NN; n++){
	//	printf("%d %le %le\n",n,dxbedIn[n],node.dxbed[n]);
	//}
	//exit(0);
	// const diag
	if ( model_Tmat == 1 )
		Tdiag_assembly( mu, NN, NE, NN_sparse, NP, alpha, &node, &element, &b_nodes );


	return EXIT_SUCCESS;
}

//void non_hydr_init(char *gridname, double alphaIn, double gravityIn, double muIn, double href, double *cut_offIn,
//                   double *bedIn, non_hydro_s *data, int model_TmatIn, int model_brkIn, double *param_brkIn, int massIn )
extern "C"
int nht_new_from_file( char *gridname, double alphaIn, double gravityIn, double muIn, double href, double cut_offIn[2],
                       int model_TmatIn, int model_brkIn, double param_brkIn[ 5 ], int massIn, double *bedIn, double *dxbedIn, double *dybedIn, size_t NNin )
{
	/* Read the header of the external .grd FRID_FILE to determine the number of nodes NN, elements NE and boundary faces NBF */
	FILE *grid = fopen( gridname, "r" );
	if ( NULL == grid ) {
		printf( "NHT: ERROR: External spatial grid file was not found\n" );
		return EXIT_FAILURE;
	}
	int dummy;
	CHECK_FSCANF ( 4, fscanf(grid, "%d %zu %zu %zu \n", &dummy, &NE, &NN, &NBF), gridname );
	if ( NN != NNin ) {
		printf( "NHT: ERROR: given size does not match size read from file: input: %zd - read %zd\n", NNin, NN );
		return EXIT_FAILURE;
	}

	/* Process geometry */
	/* Read the external GRID_FILE (.grd) */
	printf("          *************************************\n");
	printf("          **     Reading the gridfile.....   **\n");
	printf("          *************************************\n");
	printf("\n");
	/*  The grid-file format to be used should be the .grd (modified .dpl).
	    It is structured as follows:
	    - header      : dimension NE NN NF NBF
	    - blank line
	    - e:=1 -> NE  : e_node_1 ... e_node_vertex
	                    e_neigh_1 ... e_neigh_vertex
	                    e_neigh_face_1 ... e_neigh_face_vertex
	    - blank line
	    - n:=1 -> NN  : n_x_1 ... n_x_dimension n_mate
	    - blank line
	    - f:=1 -> NBF : f_n1... f_ndimension
	                    type_n1... type_ndimension
	                    f_type
	*/
	CHECK_FSCANF ( 0, fscanf( grid, "\n" ), gridname );

	element.node = (int**) MA_matrix( NE + 1, 3, sizeof( int ) );
	for ( size_t e = 0; e < NE; e++ )
		CHECK_FSCANF( 3, fscanf( grid, "%d %d %d\n", &element.node[e][0], &element.node[e][1], &element.node[e][2]), gridname );
	CHECK_FSCANF ( 0, fscanf( grid, "\n" ), gridname );

	node.coordinate = (double**) MA_matrix( NN, 2, sizeof(double) );
	node.mate = (int*) MA_vector( NN, sizeof(int) );
	for ( size_t n = 0; n < NN; n++ ) {
		CHECK_FSCANF ( 3, fscanf( grid, "%le %le %d\n", &node.coordinate[n][0], &node.coordinate[n][1], &node.mate[n]), gridname );
		CHECK_FSCANF ( 0, fscanf( grid, "\n" ), gridname );

		/* Rotation of the coordinates */
		//double x = node.coordinate[n][0];
		//double y = node.coordinate[n][1];
		//double theta = acos(-1)/2.;
		//node.coordinate[n][0] = -y;//cos(theta)*x - sin(theta)*y;
		//node.coordinate[n][1] = x;//sin(theta)*x + cos(theta)*y;
		/* Scaling of the coordiates */
		//node.coordinate[n][0] *= 1. / 50000.;
		//node.coordinate[n][1] *= 500. / 50000.*exp(-node.coordinate[n][0]);
	}

	/* allocate boundary structure */
	boundary_s boundary;
	boundary.node = (int**) MA_matrix( NBF, 2, sizeof(int) );
	boundary.types = (int**) MA_matrix( NBF, 2, sizeof(int) );
	boundary.type = (int*) MA_vector( NBF, sizeof(int) );
	boundary.normal = (double**) MA_matrix( NBF, 2, sizeof(double) );

	for ( size_t f = 0; f < NBF; f++ ){
		CHECK_FSCANF ( 5,
			       fscanf( grid, "%d %d %d %d %d\n",
				       &boundary.node[f][0], &boundary.node[f][1],
				       &boundary.types[f][0], &boundary.types[f][1],
				       &boundary.type[f] ),
			       gridname);
		if(boundary.type[f]==20) boundary.type[f]=3;
		if(boundary.types[f][0]==20) boundary.types[f][0]=3;
		if(boundary.types[f][1]==20) boundary.types[f][1]=3;
	}
	CHECK_FSCANF ( 0, fscanf( grid, "\n" ), gridname );
	fclose( grid );

	int ret = nht_new ( alphaIn,
                            gravityIn,
                            muIn,
                            href,
                            cut_offIn,
                            model_TmatIn,
                            model_brkIn,
                            param_brkIn,
                            massIn,
                            bedIn,
			    dxbedIn,
                            dybedIn,
                            NN,
                            element.node,
                            node.coordinate,
                            node.mate,
                            boundary.node,
                            boundary.types,
                            boundary.type,
                            boundary.normal );
	MA_free( boundary.node );
	MA_free( boundary.types );
	MA_free( boundary.type );
	MA_free( boundary.normal );

	return ret;
}

//NIKOS TODO: this list is not complete, there are other allocations in some other files as well which are now leaked
extern "C"
void nht_delete( void )
{
	MA_free( element.node );
	MA_free( element.edge );
	MA_free( element.volume );
	MA_free( element.normal );
	MA_free( element.flag );

	MA_free( edge.normal );
	MA_free( edge.element );
	MA_free( edge.node );
	MA_free( edge.bflag );

	MA_free( node.hu );
	MA_free( node.hv );
	MA_free( node.SWL );
	MA_free( node.bed );
	MA_free( node.W );
	MA_free( node.R );
	MA_free( node.dxbed );
	MA_free( node.dybed );
	MA_free( node.gradh );
	MA_free( node.gradeta );
	MA_free( node.gradu );
	MA_free( node.gradv );
	MA_free( node.gradhu );
	MA_free( node.gradhv );
	MA_free( node.f );
	MA_free( node.coordinate );
	MA_free( node.volume );
	MA_free( node.adj_edges );
	MA_free( node.neighbouring_nodes );
	MA_free( node.neighbours );
	MA_free( node.bflag );
	MA_free( node.bindex );
	MA_free( node.bindex_per );
	MA_free( node.mate );
	MA_free( node.flag );
	MA_free( node.brk );
	MA_free( node.us );
	MA_free( node.uz );

	MA_free( b_nodes.node );
	MA_free( b_nodes.type );
	MA_free( b_nodes.mate );
	MA_free( b_nodes.flag );
	MA_free( b_nodes.normal );
}
